<?php

$vendorDir = dirname(__DIR__);

return array (
  'miloschuman/yii2-highcharts-widget' => 
  array (
    'name' => 'miloschuman/yii2-highcharts-widget',
    'version' => '4.0.4.1',
    'alias' => 
    array (
      '@miloschuman/highcharts' => $vendorDir . '/miloschuman/yii2-highcharts-widget/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'philippfrenzel/yii2fullcalendar' => 
  array (
    'name' => 'philippfrenzel/yii2fullcalendar',
    'version' => '2.2.7.0',
    'alias' => 
    array (
      '@yii2fullcalendar' => $vendorDir . '/philippfrenzel/yii2fullcalendar',
    ),
  ),
);
