<?php
use kartik\grid\GridView;
use kartik\mpdf\Pdf;
use \kartik\datecontrol\Module;

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'fr-FR',
    'modules' => [
    'auth' => [
    	'class' => 'app\modules\auth\Module',
    	'supportEmail' => 'support@mydomain.com', // Email for notifications
    	'superAdmins' => ['admin'], // SuperAdmin users
       ],
       'datecontrol' =>  [
	       'class' => '\kartik\datecontrol\Module',
	       'displaySettings' => [
		       Module::FORMAT_DATE => 'dd-MM-yyyy',
		       Module::FORMAT_TIME => 'HH:mm:ss a',
		       Module::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a',
	       ],
	       
	       // format settings for saving each date attribute (PHP format example)
	       'saveSettings' => [
		       Module::FORMAT_DATE => 'php:U', // saves as unix timestamp
		       Module::FORMAT_TIME => 'php:H:i:s',
		       Module::FORMAT_DATETIME => 'php:Y-m-d H:i:s',
	       ],
	  ],
     'gridview' =>  [
       'class' => '\kartik\grid\Module',
       ]
     ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'barth',
        ],
        'pdf' => [
        	'class' => Pdf::classname(),
        	'format' => Pdf::FORMAT_LETTER,
        	'orientation' => Pdf::ORIENT_LANDSCAPE,
        	'destination' => Pdf::DEST_BROWSER,
        	// refer settings section for all configuration options
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
	        'class' => '\yii\rbac\DbManager',
        	'defaultRoles' => ['guest'],
	        'ruleTable' => 'auth_rule', // Optional
	        'itemTable' => 'auth_item',  // Optional
	        'itemChildTable' => 'auth_item_child',  // Optional
	        'assignmentTable' => 'auth_assignment',  // Optional
        ],
        'user' => [
        	//'class' => 'app\modules\auth\components\User',
        	'identityClass' => 'app\modules\auth\models\User', // or replace to your custom identityClass
        	'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
