<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Recents;

/**
 * ScrRecents represents the model behind the search form about `app\models\Recents`.
 */
class ScrRecents extends Recents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'famille', 'memberid', 'MANM_HH1_HH_MALAD_71_E4_71', 'MANM_HH1_HH_MALAD_71_E5_71', 'MANM_HH1_HH_MALAD_71_E6_71', 'MANM_HH1_HH_MALAD_71_E7_71', 'MANM_HH1_HH_MALAD_71_E7B_71'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Recents::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id' => $this->Id,
            'famille' => $this->famille,
            'memberid' => $this->memberid,
            'MANM_HH1_HH_MALAD_71_E4_71' => $this->MANM_HH1_HH_MALAD_71_E4_71,
            'MANM_HH1_HH_MALAD_71_E5_71' => $this->MANM_HH1_HH_MALAD_71_E5_71,
            'MANM_HH1_HH_MALAD_71_E6_71' => $this->MANM_HH1_HH_MALAD_71_E6_71,
            'MANM_HH1_HH_MALAD_71_E7_71' => $this->MANM_HH1_HH_MALAD_71_E7_71,
            'MANM_HH1_HH_MALAD_71_E7B_71' => $this->MANM_HH1_HH_MALAD_71_E7B_71,
        ]);

        return $dataProvider;
    }
}
