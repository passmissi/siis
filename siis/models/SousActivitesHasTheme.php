<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sous_activites_has_theme".
 *
 * @property integer $id_sous_activite
 * @property integer $id_type_theme
 * @property string $id_plan
 */
class SousActivitesHasTheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sous_activites_has_theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sous_activite', 'id_type_theme'], 'required'],
            [['id_sous_activite', 'id_type_theme','id_plan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sous_activite' => Yii::t('app', 'Id Sous Activite'),
            'id_type_theme' => Yii::t('app', 'Id Type Theme'),
            'id_plan' => Yii::t('app', 'Id Plan'),
        ];
    }
}
