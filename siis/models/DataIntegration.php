<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_integration".
 *
 * @property string $id
 * @property string $user_id
 * @property string $plan
 * @property string $activite
 * @property string $details_integration
 * @property integer $type_activite
 * @property integer $type_sous_activites
 * @property string $source
 * @property string $date_creation
 * @property string $case_id_famille
 * @property string $case_id_membre
 * @property integer $type_theme
 * @property string $theme
 * @property string $vaccin
 * @property double $poids
 * @property double $taille
 * @property integer $rapportpt
 * @property double $pb
 * @property integer $oedeme
 * @property string $en
 * @property string $type_reference
 * @property string $secteur
 * @property string $institution
 * @property string $type_evenement
 * @property string $date_evenement
 * @property string $type_distribution
 * @property double $quantite
 * @property string $form_id
 * @property string $form_name
 * @property double $location_longitude
 * @property double $location_latitude
 * @property double $location_precision
 * @property double $location_altitude
 * @property string $commentaire
 * @property integer $isValid
 */
class DataIntegration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_integration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'details_integration', 'type_activite', 'type_sous_activites', 'source', 'date_creation', 'case_id_famille', 'case_id_membre', 'form_id', 'form_name'], 'required'],
            [['plan', 'activite', 'details_integration', 'type_activite', 'type_sous_activites', 'type_theme', 'rapportpt', 'oedeme', 'isValid'], 'integer'],
            [['date_creation', 'date_evenement'], 'safe'],
            [['poids', 'taille', 'pb', 'quantite', 'location_longitude', 'location_latitude', 'location_precision', 'location_altitude'], 'number'],
            [['user_id', 'theme'], 'string', 'max' => 75],
            [['source', 'vaccin', 'en', 'type_reference', 'secteur', 'type_evenement', 'type_distribution'], 'string', 'max' => 50],
            [['case_id_famille', 'case_id_membre', 'institution', 'form_id', 'form_name'], 'string', 'max' => 100],
            [['commentaire'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'plan' => Yii::t('app', 'Plan'),
            'activite' => Yii::t('app', 'Activite'),
            'details_integration' => Yii::t('app', 'Details Integration'),
            'type_activite' => Yii::t('app', 'Type Activite'),
            'type_sous_activites' => Yii::t('app', 'Type Sous Activites'),
            'source' => Yii::t('app', 'Source'),
            'date_creation' => Yii::t('app', 'Date Creation'),
            'case_id_famille' => Yii::t('app', 'Case Id Famille'),
            'case_id_membre' => Yii::t('app', 'Case Id Membre'),
            'type_theme' => Yii::t('app', 'Type Theme'),
            'theme' => Yii::t('app', 'Theme'),
            'vaccin' => Yii::t('app', 'Vaccin'),
            'poids' => Yii::t('app', 'Poids'),
            'taille' => Yii::t('app', 'Taille'),
            'rapportpt' => Yii::t('app', 'Rapportpt'),
            'pb' => Yii::t('app', 'Pb'),
            'oedeme' => Yii::t('app', 'Oedeme'),
            'en' => Yii::t('app', 'En'),
            'type_reference' => Yii::t('app', 'Type Reference'),
            'secteur' => Yii::t('app', 'Secteur'),
            'institution' => Yii::t('app', 'Institution'),
            'type_evenement' => Yii::t('app', 'Type Evenement'),
            'date_evenement' => Yii::t('app', 'Date Evenement'),
            'type_distribution' => Yii::t('app', 'Type Distribution'),
            'quantite' => Yii::t('app', 'Quantite'),
            'form_id' => Yii::t('app', 'Form ID'),
            'form_name' => Yii::t('app', 'Form Name'),
            'location_longitude' => Yii::t('app', 'Location Longitude'),
            'location_latitude' => Yii::t('app', 'Location Latitude'),
            'location_precision' => Yii::t('app', 'Location Precision'),
            'location_altitude' => Yii::t('app', 'Location Altitude'),
            'commentaire' => Yii::t('app', 'Commentaire'),
            'isValid' => Yii::t('app', 'Is Valid'),
        ];
    }
}
