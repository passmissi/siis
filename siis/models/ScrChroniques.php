<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Chroniques;

/**
 * ScrChroniques represents the model behind the search form about `app\models\Chroniques`.
 */
class ScrChroniques extends Chroniques
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'famille', 'memberid', 'souffrir_maladie_chronique', 'avoir_traitement', 'duree_traitement', 'raison_non_traitement', 'avoir_autres_maladies', 'MANM_HH1_HH_MALAD_81_E2_2_81', 'MANM_HH1_HH_MALAD_81_E2_3_81', 'MANM_HH1_HH_MALAD_81_E2_4_81', 'MANM_HH1_HH_MALAD_81_E3_1_81', 'MANM_HH1_HH_MALAD_81_E3_2_81', 'MANM_HH1_HH_MALAD_81_E3_3_81'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Chroniques::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id' => $this->Id,
            'famille' => $this->famille,
            'memberid' => $this->memberid,
            'souffrir_maladie_chronique' => $this->souffrir_maladie_chronique,
            'avoir_traitement' => $this->avoir_traitement,
            'duree_traitement' => $this->duree_traitement,
            'raison_non_traitement' => $this->raison_non_traitement,
            'avoir_autres_maladies' => $this->avoir_autres_maladies,
            'MANM_HH1_HH_MALAD_81_E2_2_81' => $this->MANM_HH1_HH_MALAD_81_E2_2_81,
            'MANM_HH1_HH_MALAD_81_E2_3_81' => $this->MANM_HH1_HH_MALAD_81_E2_3_81,
            'MANM_HH1_HH_MALAD_81_E2_4_81' => $this->MANM_HH1_HH_MALAD_81_E2_4_81,
            'MANM_HH1_HH_MALAD_81_E3_1_81' => $this->MANM_HH1_HH_MALAD_81_E3_1_81,
            'MANM_HH1_HH_MALAD_81_E3_2_81' => $this->MANM_HH1_HH_MALAD_81_E3_2_81,
            'MANM_HH1_HH_MALAD_81_E3_3_81' => $this->MANM_HH1_HH_MALAD_81_E3_3_81,
        ]);

        return $dataProvider;
    }
}
