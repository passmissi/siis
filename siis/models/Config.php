<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property integer $operateur
 * @property string $cle
 * @property integer $valeur
 * @property string $description
 * @property integer $isValid
 *
 * @property Operateurs $operateur0
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['operateur', 'cle', 'valeur', 'isValid'], 'required'],
            [['operateur', 'valeur', 'isValid'], 'integer'],
            [['cle'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'operateur' => Yii::t('app', 'Operateur'),
            'cle' => Yii::t('app', 'Cle'),
            'valeur' => Yii::t('app', 'Valeur'),
            'description' => Yii::t('app', 'Description'),
            'isValid' => Yii::t('app', 'Is Valid'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperateur0()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'operateur']);
    }
    
    public function getCommunes()
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$query = (new \yii\db\Query())->select(['valeur'])->from('config')->where(['id' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1]);
		$command = $query->createCommand();
		$data = $command->queryAll();
		$titles = '';
		foreach($data as $row) {
		    $titles .= $row['valeur'] . ', ';
		}
		return rtrim($titles, ', ');
    }
}
