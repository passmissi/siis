<?php

namespace app\models;

use Yii;
use yii\base\Model;

class MonitoringRegional extends Model
{
	public $mois;
	public $annee;
	public $ts;
	public $statszone;
	public $indicateurs;
	public $dataexporting;
	public $map;
	public $type;

	public $communes;
	public $indicateurs2;
	
	
	public function rules()
	{
		return [
			['statszone', 'string', 'max' => 150],
			['mois', 'string', 'max' => 45],
			['type', 'string', 'max' => 45],
			['communes', 'string', 'max' => 45],
			['indicateurs2', 'string', 'max' => 45],
			['annee', 'integer'],
			['ts', 'integer'],
			['indicateurs', 'string', 'max' => 128],
			['dataexporting', 'string', 'max' => 128],
			['map', 'string', 'max' => 128],
		];
	}

	public function attributeLabels()
	{
		return [
			'statszone' => 'Statistics Zone',
			'mois' => 'Mois',
			'annee' => 'Annee',
			'ts' => 'TS',
			'indicateurs'  => 'Indicateurs',
			'dataexporting' => 'Data Exporting',
			'map'    => 'map',
			'type'    => 'Type',
			'communes' => 'Communes',
			'indicateurs2' => 'Indicateurs',
		];
	}
}