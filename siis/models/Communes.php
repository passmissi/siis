<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "communes".
 *
 * @property string $id_communal
 * @property string $n_communal
 */
class Communes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'communes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_communal', 'n_communal'], 'required'],
            [['id_communal'], 'integer'],
            [['n_communal'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_communal' => Yii::t('app', 'Id Communal'),
            'n_communal' => Yii::t('app', 'N Communal'),
        ];
    }
}
