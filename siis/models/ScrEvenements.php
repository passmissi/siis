<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Evenements;

/**
 * ScrEvenements represents the model behind the search form about `app\models\Evenements`.
 */
class ScrEvenements extends Evenements
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'famille', 'id_membre', 'plan', 'mplan', 'evenements', 'createdBy'], 'integer'],
            [['date_evenements', 'commentaires', 'createdOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Evenements::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'famille' => $this->famille,
            'id_membre' => $this->id_membre,
            'plan' => $this->plan,
            'mplan' => $this->mplan,
            'evenements' => $this->evenements,
            'date_evenements' => $this->date_evenements,
            'createdBy' => $this->createdBy,
            'createdOn' => $this->createdOn,
        ]);

        $query->andFilterWhere(['like', 'commentaires', $this->commentaires]);

        return $dataProvider;
    }
}
