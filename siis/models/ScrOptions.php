<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Options;

/**
 * ScrOptions represents the model behind the search form about `app\models\Options`.
 */
class ScrOptions extends Options
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['dimension', 'question', 'colonne', 'table_source', 'options'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Options::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'dimension', $this->dimension])
            ->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'colonne', $this->colonne])
            ->andFilterWhere(['like', 'table_source', $this->table_source])
            ->andFilterWhere(['like', 'options', $this->options]);

        return $dataProvider;
    }
}
