<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_integration_evenement".
 *
 * @property string $id
 * @property string $details_integration
 * @property string $source
 * @property string $date_creation
 * @property string $user_id
 * @property integer $code_famille
 * @property string $membre
 * @property integer $type_evenement
 * @property string $nom_evenement
 * @property string $date_evenement
 * @property string $form_id
 * @property string $form_name
 * @property double $location_longitude
 * @property double $location_latitude
 * @property double $location_precision
 * @property double $location_altitude
 * @property string $commentaire
 * @property integer $isValid
 */
class DataIntegrationEvenement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_integration_evenement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['details_integration', 'source', 'date_creation', 'user_id', 'membre', 'type_evenement', 'nom_evenement', 'date_evenement'], 'required'],
            [['details_integration', 'code_famille', 'type_evenement', 'isValid'], 'integer'],
            [['date_creation', 'date_evenement'], 'safe'],
            [['location_longitude', 'location_latitude', 'location_precision', 'location_altitude'], 'number'],
            [['source'], 'string', 'max' => 50],
            [['user_id'], 'string', 'max' => 75],
            [['membre', 'nom_evenement', 'form_id', 'form_name'], 'string', 'max' => 100],
            [['commentaire'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'details_integration' => Yii::t('app', 'Details Integration'),
            'source' => Yii::t('app', 'Source'),
            'date_creation' => Yii::t('app', 'Date Creation'),
            'user_id' => Yii::t('app', 'User ID'),
            'code_famille' => Yii::t('app', 'Code Famille'),
            'membre' => Yii::t('app', 'Membre'),
            'type_evenement' => Yii::t('app', 'Type Evenement'),
            'nom_evenement' => Yii::t('app', 'Nom Evenement'),
            'date_evenement' => Yii::t('app', 'Date Evenement'),
            'form_id' => Yii::t('app', 'Form ID'),
            'form_name' => Yii::t('app', 'Form Name'),
            'location_longitude' => Yii::t('app', 'Location Longitude'),
            'location_latitude' => Yii::t('app', 'Location Latitude'),
            'location_precision' => Yii::t('app', 'Location Precision'),
            'location_altitude' => Yii::t('app', 'Location Altitude'),
            'commentaire' => Yii::t('app', 'Commentaire'),
            'isValid' => Yii::t('app', 'Is Valid'),
        ];
    }
}
