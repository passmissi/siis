<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Mort;

/**
 * ScrMort represents the model behind the search form about `app\models\Mort`.
 */
class ScrMort extends Mort
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Id', 'famille', 'MANM_HH1_ID_MANM_91_B2_91', 'MANM_HH1_ID_MANM_91_B3_91', 'MANM_HH1_ID_MANM_91_B7_91'], 'integer'],
            [['MANM_HH1_ID_MANM_91_B1_1_91', 'MANM_HH1_ID_MANM_91_B1_2_91'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Mort::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'Id' => $this->Id,
            'famille' => $this->famille,
            'MANM_HH1_ID_MANM_91_B2_91' => $this->MANM_HH1_ID_MANM_91_B2_91,
            'MANM_HH1_ID_MANM_91_B3_91' => $this->MANM_HH1_ID_MANM_91_B3_91,
            'MANM_HH1_ID_MANM_91_B7_91' => $this->MANM_HH1_ID_MANM_91_B7_91,
        ]);

        $query->andFilterWhere(['like', 'MANM_HH1_ID_MANM_91_B1_1_91', $this->MANM_HH1_ID_MANM_91_B1_1_91])
            ->andFilterWhere(['like', 'MANM_HH1_ID_MANM_91_B1_2_91', $this->MANM_HH1_ID_MANM_91_B1_2_91]);

        return $dataProvider;
    }
}
