<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monitoringplan".
 *
 * @property string $id
 * @property integer $Plan
 * @property integer $isExecuted
 * @property integer $isExecutedBy
 * @property string $isExecutedOn
 * @property integer $isActif
 * @property integer $isClosed
 * @property integer $mode
 * @property integer $isReg
 * @property integer $ts_nreg
 * @property integer $akf_nreg
 * @property string $mois_nreg
 * @property integer $annee_nreg
 *
 * @property Monitoringactivites[] $monitoringactivites
 * @property Mode $mode0
 * @property Operateurs $isExecutedBy0
 * @property Plans $plan
 */
class Monitoringplan extends \yii\db\ActiveRecord
{
	public $akf;
	public $listact = array();
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monitoringplan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Plan', 'isExecuted', 'isExecutedBy', 'isActif', 'isClosed', 'mode', 'isReg', 'akf_nreg', 'annee_nreg'], 'integer'],
            [['isExecuted', 'isExecutedBy', 'mode', 'akf', 'listact', 'Plan', 'akf_nreg', 'mois_nreg','annee_nreg'], 'required'],
            [['isExecutedOn'], 'safe'],
            [['mois_nreg'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'Plan' => Yii::t('app', 'Plan'),
            'isExecuted' => Yii::t('app', 'Is Executed'),
            'isExecutedBy' => Yii::t('app', 'Is Executed By'),
            'isExecutedOn' => Yii::t('app', 'Is Executed On'),
            'isActif' => Yii::t('app', 'Is Actif'),
            'listact' => Yii::t('app', 'Liste des Activites'),
            'isClosed' => Yii::t('app', 'Is Closed'),
            'mode' => Yii::t('app', 'Mode'),
            'isReg' => Yii::t('app', 'Is Reg'),
            'ts_nreg' => Yii::t('app', 'TS'),
            'akf_nreg' => Yii::t('app', 'AKF'),
            'mois_nreg' => Yii::t('app', 'Mois'),
            'annee_nreg' => Yii::t('app', 'Annee'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMonitoringactivites()
    {
        return $this->hasMany(Monitoringactivites::className(), ['mplan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMode0()
    {
        return $this->hasOne(Mode::className(), ['id' => 'mode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsExecutedBy0()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'isExecutedBy']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlan()
    {
        return $this->hasOne(Plans::className(), ['id' => 'Plan']);
    }
}
