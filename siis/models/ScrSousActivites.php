<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SousActivites;

/**
 * ScrSousActivites represents the model behind the search form about `app\models\SousActivites`.
 */
class ScrSousActivites extends SousActivites
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_activite', 'id_type_sous_activite', 'id_membre', 'id_famille', 'id_plan'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SousActivites::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_activite' => $this->id_activite,
            'id_type_sous_activite' => $this->id_type_sous_activite,
            'id_membre' => $this->id_membre,
            'id_famille' => $this->id_famille,
            'id_plan' => $this->id_plan,
        ]);

        return $dataProvider;
    }
    
    public function search17($params, $id)
    {
    	$query = SousActivites::find(['id_activite' => $id]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	return $dataProvider;
    }
}
