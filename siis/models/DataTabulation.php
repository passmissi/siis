<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_tabulation".
 *
 * @property integer $id
 * @property string $form_id
 * @property string $form_name
 * @property string $path
 * @property string $date_creation
 * @property string $source
 */
class DataTabulation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_tabulation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['form_id', 'form_name', 'path', 'date_creation', 'source'], 'required'],
            [['date_creation'], 'safe'],
            [['form_id'], 'string', 'max' => 75],
            [['form_name', 'source'], 'string', 'max' => 50],
            [['path'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'form_id' => Yii::t('app', 'Form ID'),
            'form_name' => Yii::t('app', 'Form Name'),
            'path' => Yii::t('app', 'Path'),
            'date_creation' => Yii::t('app', 'Date Creation'),
            'source' => Yii::t('app', 'Source'),
        ];
    }
}
