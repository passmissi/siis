<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ObjectifsVie;

/**
 * ScrObjectifsVie represents the model behind the search form about `app\models\ObjectifsVie`.
 */
class ScrObjectifsVie extends ObjectifsVie
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'famille', 'objectif', 'createdBy'], 'integer'],
            [['niveau', 'date_objectif', 'commentaire', 'createdOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ObjectifsVie::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'famille' => $this->famille,
            'objectif' => $this->objectif,
            'date_objectif' => $this->date_objectif,
            'createdBy' => $this->createdBy,
            'createdOn' => $this->createdOn,
        ]);

        $query->andFilterWhere(['like', 'niveau', $this->niveau])
            ->andFilterWhere(['like', 'commentaire', $this->commentaire]);

        return $dataProvider;
    }
}
