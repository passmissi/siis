<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RefEvenements;

/**
 * ScrRefEvenements represents the model behind the search form about `app\models\RefEvenements`.
 */
class ScrRefEvenements extends RefEvenements
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'createdBy'], 'integer'],
            [['libelle', 'description', 'createdOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RefEvenements::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'createdBy' => $this->createdBy,
            'createdOn' => $this->createdOn,
        ]);

        $query->andFilterWhere(['like', 'libelle', $this->libelle])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
