<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property integer $id
 * @property string $mois
 * @property integer $annee
 * @property integer $commune
 * @property integer $ts
 * @property integer $akf
 * @property integer $is_sync
 * @property integer $isMonitoring
 * @property integer $isPlanned
 * @property string $isPlannedOn
 * @property integer $isPlannedBy
 * @property integer $isExecuted
 * @property string $isExecutedOn
 * @property integer $isExecutedBy
 * @property integer $isDeleted
 * @property integer $isClosed
 * @property integer $mode
 *
 * @property Activites[] $activites
 * @property Mode $mode0
 * @property Operateurs $ts0
 * @property Operateurs $akf0
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['annee', 'commune', 'ts', 'akf', 'is_sync', 'isMonitoring', 'isPlanned', 'isPlannedBy', 'isExecuted', 'isExecutedBy', 'isDeleted', 'isClosed', 'mode','dimagi'], 'integer'],
            [['isPlannedOn', 'isExecutedOn'], 'safe'],
        	[['annee'], 'required', 'on' => 'plants'],
            [['mois','akf','annee'],'required'],
            [['mois'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mois' => Yii::t('app', 'Mois'),
            'annee' => Yii::t('app', 'Annee'),
            'commune' => Yii::t('app', 'Commune'),
            'ts' => Yii::t('app', 'Ts'),
            'akf' => Yii::t('app', 'Akf'),
            'is_sync' => Yii::t('app', 'Is Sync'),
            'isMonitoring' => Yii::t('app', 'Is Monitoring'),
            'isPlanned' => Yii::t('app', 'Is Planned'),
            'isPlannedOn' => Yii::t('app', 'Is Planned On'),
            'isPlannedBy' => Yii::t('app', 'Is Planned By'),
            'isExecuted' => Yii::t('app', 'Is Executed'),
            'isExecutedOn' => Yii::t('app', 'Is Executed On'),
            'isExecutedBy' => Yii::t('app', 'Is Executed By'),
            'isDeleted' => Yii::t('app', 'Is Deleted'),
            'isClosed' => Yii::t('app', 'Is Closed'),
            'mode' => Yii::t('app', 'Mode'),
        	'dimagi' => Yii::t('app', 'Dimagi'),
            'nomplan' => Yii::t('app', 'Plan'),
        ];
    }

    public function getQuerysplit($data, $param)
    {
    	$titles = '';
    	foreach($data as $row) {
    		$titles .= $row[$param] . ', ';
    	}
    	$datax = ''.rtrim($titles, ', ').'';
    	$datay = '('.preg_replace("/'/", '', $datax).')';
    	
    	return $datay;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivites()
    {
        return $this->hasMany(Activites::className(), ['id_plan' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMode0()
    {
        return $this->hasOne(Mode::className(), ['id' => 'mode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTs0()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'ts']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkf0()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'akf']);
    }
    
    public function getNomplan()
    {
    	return $this->mois.' '.$this->annee;
    }
    
    public function getDataHeaderXMLDimagi($name_form, $timestart, $timeend, $username, $instanceid){
	    	$data = '<?xml version="1.0" ?>
	                 	<data uiversion="1" name="'.$name_form.'"
	                 		  xmlns:jrm="http://dev.commcarehq.org/jr/xforms"
	                 		  xmlns="http://siis.org/externalcaseupdate" >
	    
		    				<meta xmlns:cc="http://commcarehq.org/xforms">
								<timeStart>'.$timestart.'</timeStart>
								<timeEnd>'.$timeend.'</timeEnd>
								<username>'.$username.'</username>
								<instanceID>'.$instanceid.'</instanceID>
							</meta>
	    	';
	    	return $data;
    }
    
    public function getDataHeaderXMLDimagi22($name_form){
    	$data = '<?xml version="1.0" ?>
	                 <data uiversion="1" name="'.$name_form.'" xmlns:jrm="http://dev.commcarehq.org/jr/xforms"
	                 	xmlns="http://siis.org/externalcaseupdate" >';
    	return $data;
    }
    
    public function getDataXMLDimagi($caseid, $datemodified,$casename, $ownerid, $casetype, $type, $place,
    		$notes, $subtype,$date,$famille, $idfamille, $akfid, $akfname){
	    	$data = '<case case_id="'.$caseid.'"  
								  date_modified="'.$datemodified.'" 
								  xmlns="http://commcarehq.org/case/transaction/v2" > 
								  
									<create>
										<case_name>'.$casename.'</case_name> 
										<owner_id>'.$ownerid.'</owner_id>  
										<case_type>'.$casetype.'</case_type> 
									</create>
								
									<update>
										<activite_type>'.$type.'</activite_type> 
										<activite_place>'.$place.'</activite_place>
										<activite_notes>'.$notes.'</activite_notes> 
										<activite_subtype>'.$subtype.'</activite_subtype>
										<activite_date>'.$date.'</activite_date> 
										<activite_source>siis</activite_source> 
										<nom_de_famille>'.$famille.'</nom_de_famille> 
										<id_de_famille>'.$idfamille.'</id_de_famille> 
										<akf_id>'.$akfid.'</akf_id>
										<akf_name>'.$akfname.'</akf_name> 
									</update>
					 </case>
	    	';
	    	
	    	return $data;
    }
    
    
    public function getDataXMLDimagi22($caseid, $datemodified,$external_id, $id_de_famille){
    	$data = '<case case_id="'.$caseid.'" date_modified="'.$datemodified.'"
					  xmlns="http://commcarehq.org/case/transaction/v2">
								  		
					  <update>
						   <external_id>'.$external_id.'</external_id>
						   <id_de_famille>'.$id_de_famille.'</id_de_famille>
					  </update>
				</case>';
    
    	return $data;
    }
    
    public function getDataXMLDimagi23($caseid, $datemodified,$client_sub_id, $id_de_famille){
    	$data = '<case case_id="'.$caseid.'" date_modified="'.$datemodified.'"
					  xmlns="http://commcarehq.org/case/transaction/v2">
    
					  <update>
						   <client_sub_id>'.$client_sub_id.'</client_sub_id>
						   <id_de_client>'.$id_de_famille.'.'.$client_sub_id.'</id_de_client>
						   <id_de_famille>'.$id_de_famille.'</id_de_famille>
					  </update>
				</case>';
    
    	return $data;
    }
    
    public function getDataEndXMLDimagi(){
    	$data = '</data>';
    	return $data;
    }
    
    public function getMonthNumber($monthStr) {
    	$m = trim($monthStr);
    	switch ($m) {
    		case "Janvier":
    			$m = "01";
    			break;
    		case "Fevrier":
    			$m = "02";
    			break;
    		case "Mars":
    			$m = "03";
    			break;
    		case "Avril":
    			$m = "04";
    			break;
    		case "Mai":
    			$m = "05";
    			break;
    		case "Juin":
    			$m = "06";
    			break;
    		case "Juillet":
    			$m = "07";
    			break;
    		case "Aout":
    			$m = "08";
    			break;
    		case "Septembre":
    			$m = "09";
    			break;
    		case "Octobre":
    			$m = "10";
    			break;
    		case "Novembre":
    			$m = "11";
    			break;
    		case "Decembre":
    			$m = "12";
    			break;
    		default:
    			break;
    	}
    	return $m;
    }
    
    public function getNumberMonth($monthid) {
    	$m = trim($monthid);
    	switch ($m) {
    		case "01":
    			$m = "Janvier";
    			break;
    		case "02":
    			$m = "Fevrier";
    			break;
    		case "03":
    			$m = "Mars";
    			break;
    		case "04":
    			$m = "Avril";
    			break;
    		case "05":
    			$m = "Mai";
    			break;
    		case "06":
    			$m = "Juin";
    			break;
    		case "07":
    			$m = "Juillet";
    			break;
    		case "08":
    			$m = "Aout";
    			break;
    		case "09":
    			$m = "Septembre";
    			break;
    		case "10":
    			$m = "Octobre";
    			break;
    		case "11":
    			$m = "Novembre";
    			break;
    		case "12":
    			$m = "Decembre";
    			break;
    		default:
    			break;
    	}
    	return $m;
    }
    
    public static function v4()
    {
    	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
    			// 32 bits for "time_low"
    			mt_rand(0, 0xffff), mt_rand(0, 0xffff),
    			// 16 bits for "time_mid"
    			mt_rand(0, 0xffff),
    			// 16 bits for "time_hi_and_version",
    			// four most significant bits holds version number 4
    			mt_rand(0, 0x0fff) | 0x4000,
    			// 16 bits, 8 bits for "clk_seq_hi_res",
    			// 8 bits for "clk_seq_low",
    			// two most significant bits holds zero and one for variant DCE1.1
    			mt_rand(0, 0x3fff) | 0x8000,
    			// 48 bits for "node"
    			mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    	);
    }
}
