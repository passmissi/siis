<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "family_akf".
 *
 * @property string $famille
 * @property integer $operateur_akf
 * @property integer $operateur_ts
 * @property integer $commune
 * @property string $localite
 * @property string $createdOn
 *
 * @property Famille $famille0
 * @property Operateurs $operateurAkf
 */
class FamilyAkf extends \yii\db\ActiveRecord
{
	public $nombre;
	public $quantite;
	public $assignes;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'family_akf';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'operateur_akf','localite','quantite'], 'required'],
            [['famille', 'operateur_akf', 'operateur_ts', 'commune'], 'integer'],
            [['createdOn'], 'safe'],
            [['localite'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'famille' => Yii::t('app', 'Famille'),
            'operateur_akf' => Yii::t('app', 'Operateur Akf'),
            'operateur_ts' => Yii::t('app', 'Operateur Ts'),
            'commune' => Yii::t('app', 'Commune'),
            'localite' => Yii::t('app', 'Localite'),
            'createdOn' => Yii::t('app', 'Created On'),
            'quantite' => Yii::t('app', 'Quantite non assignee'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamille0()
    {
        return $this->hasOne(Famille::className(), ['id' => 'famille']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperateurAkf()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'operateur_akf']);
    }
}
