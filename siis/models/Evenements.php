<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evenements".
 *
 * @property string $id
 * @property string $famille
 * @property integer $id_membre
 * @property integer $plan
 * @property string $mplan
 * @property integer $evenements
 * @property string $date_evenements
 * @property string $commentaires
 * @property integer $createdBy
 * @property string $createdOn
 *
 * @property Famille $famille0
 * @property RefEvenements $evenements0
 */
class Evenements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'evenements';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'plan', 'mplan', 'evenements', 'date_evenements'], 'required'],
            [['famille', 'id_membre', 'plan', 'mplan', 'evenements', 'createdBy'], 'integer'],
            [['date_evenements', 'createdOn'], 'safe'],
            [['commentaires'], 'string', 'max' => 500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'famille' => Yii::t('app', 'Famille'),
            'id_membre' => Yii::t('app', 'Id Membre'),
            'plan' => Yii::t('app', 'Plan'),
            'mplan' => Yii::t('app', 'Mplan'),
            'evenements' => Yii::t('app', 'Evenements'),
            'date_evenements' => Yii::t('app', 'Date Evenements'),
            'commentaires' => Yii::t('app', 'Commentaires'),
            'createdBy' => Yii::t('app', 'Created By'),
            'createdOn' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamille0()
    {
        return $this->hasOne(Famille::className(), ['id' => 'famille']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvenements0()
    {
        return $this->hasOne(RefEvenements::className(), ['id' => 'evenements']);
    }
}
