<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property integer $id
 * @property string $dimension
 * @property string $question
 * @property string $colonne
 * @property string $table_source
 * @property string $options
 */
class Options extends \yii\db\ActiveRecord
{
	public $commune;
	public $section_communale;
	public $localite;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['dimension', 'question', 'commune'], 'required'],
            [['dimension', 'colonne', 'table_source'], 'string', 'max' => 50],
            [['options','section_communale','localite'], 'string', 'max' => 400]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'dimension' => Yii::t('app', 'Dimension'),
            'question' => Yii::t('app', 'Dimension/Question'),
            'colonne' => Yii::t('app', 'Colonne'),
            'table_source' => Yii::t('app', 'Table Source'),
            'options' => Yii::t('app', 'Options'),
        	'commune' => Yii::t('app', 'Commune'),
        	'section_communale' => Yii::t('app', 'Section Communale'),
        	'localite' => Yii::t('app', 'Localite'),
        ];
    }
    
    public function getData($obj, $commune, $section_communale, $localite)
    {
    	if($obj->table_source == 'famille'){
    		if($commune == "Saut d'eau"){
    			$commune = "Saut d\'eau";
    		}
    		
    		$sql="select ".$obj->colonne." as colonne, count(*) as total
    		  	  from ".$obj->table_source."
    		      where commune = '".$commune."' ";
    		
    		if(!empty($section_communale) && !empty($localite)){
    			$sql .= "and section_communale = '".$section_communale."'
    					 and localite = '".$localite."'";
    		}elseif(empty($section_communale) && !empty($localite)){
    			$sql .= "and localite = '".$localite."'";
    		}elseif(!empty($section_communale) && empty($localite)){
    			$sql .= "and section_communale = '".$section_communale."'";
    		}
    		
    		if($obj->colonne == 'BIENS_HH_F13'){
    			$sql .= "and BIENS_HH_F12 = 1";
    		}elseif($obj->colonne == 'BIENS_HH_F14'){
    			$sql .= "and BIENS_HH_F13 = 1";
    		}elseif($obj->colonne == 'SEVIS_KONPOTMAN_HH_I2'){
    			$sql .= "and SEVIS_KONPOTMAN_HH_I1 = 1";
    		}
    		
    		$sql .= " group by ".$obj->colonne."";
    	}else{
    		
    	}
    	
    	/* $sql="select ".$obj->colonne." as colonne, count(*) as total 
    		  from ".$obj->table_source."
    		  where commune = '".$commune."' 
    		  group by $obj->colonne"; */
    	
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total[$i][0] = (int) $tot_p[$i]["total"];
    		$total[$i][1] = $tot_p[$i]["colonne"];
    	}
    			 
    	return $total;
    }
}
