<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_sous_activites".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $description
 */
class TypeSousActivites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_sous_activites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle'], 'required'],
            [['libelle', 'description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'libelle' => Yii::t('app', 'Libelle'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
