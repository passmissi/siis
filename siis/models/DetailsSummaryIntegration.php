<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "details_summary_integration".
 *
 * @property string $id
 * @property string $id_integration
 * @property string $type
 * @property string $id_form
 * @property string $source
 * @property string $date_creation
 *
 * @property DataSummaryIntegration $idIntegration
 */
class DetailsSummaryIntegration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'details_summary_integration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_integration', 'type', 'id_form', 'source'], 'required'],
            [['id_integration'], 'integer'],
            [['date_creation'], 'safe'],
            [['type', 'source'], 'string', 'max' => 50],
            [['id_form'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_integration' => Yii::t('app', 'Id Integration'),
            'type' => Yii::t('app', 'Type'),
            'id_form' => Yii::t('app', 'Id Form'),
            'source' => Yii::t('app', 'Source'),
            'date_creation' => Yii::t('app', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdIntegration()
    {
        return $this->hasOne(DataSummaryIntegration::className(), ['id' => 'id_integration']);
    }
}
