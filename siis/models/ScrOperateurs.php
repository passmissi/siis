<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Operateurs;

/**
 * ScrOperateurs represents the model behind the search form about `app\models\Operateurs`.
 */
class ScrOperateurs extends Operateurs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sexe', 'akf_lie', 'commune_lie'], 'integer'],
            [['nom', 'prenom', 'email', 'fonction', 'role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Operateurs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sexe' => $this->sexe,
            'akf_lie' => $this->akf_lie,
            'commune_lie' => $this->commune_lie,
        ]);

        $query->andFilterWhere(['like', 'nom', $this->nom])
            ->andFilterWhere(['like', 'prenom', $this->prenom])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'fonction', $this->fonction])
            ->andFilterWhere(['like', 'role', $this->role]);

        return $dataProvider;
    }
    
    public function search13($params, $id)
    {
    	$query = Operateurs::find()->where(['akf_lie' => $id]);
    	 
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'sexe' => $this->sexe,
    			'akf_lie' => $this->akf_lie,
    			'commune_lie' => $this->commune_lie,
    			]);
    
    	$query->andFilterWhere(['like', 'nom', $this->nom])
    	->andFilterWhere(['like', 'prenom', $this->prenom])
    	->andFilterWhere(['like', 'email', $this->email])
    	->andFilterWhere(['like', 'fonction', $this->fonction])
    	->andFilterWhere(['like', 'role', $this->role]);
    
    	return $dataProvider;
    }
    
}
