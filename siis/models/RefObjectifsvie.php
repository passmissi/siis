<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_objectifsvie".
 *
 * @property integer $id
 * @property string $libelle
 * @property string $description
 * @property string $createdBy
 * @property string $createdOn
 *
 * @property ObjectifsVie[] $objectifsVies
 */
class RefObjectifsvie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_objectifsvie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle'], 'required'],
            [['createdBy'], 'integer'],
            [['createdOn'], 'safe'],
            [['libelle'], 'string', 'max' => 100],
            [['description'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'libelle' => Yii::t('app', 'Libelle'),
            'description' => Yii::t('app', 'Description'),
            'createdBy' => Yii::t('app', 'Created By'),
            'createdOn' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectifsVies()
    {
        return $this->hasMany(ObjectifsVie::className(), ['objectif' => 'id']);
    }
}
