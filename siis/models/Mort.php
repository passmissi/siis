<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mort".
 *
 * @property string $Id
 * @property string $famille
 * @property string $MANM_HH1_ID_MANM_91_B1_1_91
 * @property string $MANM_HH1_ID_MANM_91_B1_2_91
 * @property integer $MANM_HH1_ID_MANM_91_B2_91
 * @property integer $MANM_HH1_ID_MANM_91_B3_91
 * @property integer $MANM_HH1_ID_MANM_91_B7_91
 */
class Mort extends \yii\db\ActiveRecord
{
	public $id_membre;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mort';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille'], 'required'],
            [['famille', 'MANM_HH1_ID_MANM_91_B2_91', 'MANM_HH1_ID_MANM_91_B3_91', 'MANM_HH1_ID_MANM_91_B7_91'], 'integer'],
            [['MANM_HH1_ID_MANM_91_B1_1_91', 'MANM_HH1_ID_MANM_91_B1_2_91'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => Yii::t('app', 'ID'),
            'famille' => Yii::t('app', 'Famille'),
            'MANM_HH1_ID_MANM_91_B1_1_91' => Yii::t('app', 'Prenom'),
            'MANM_HH1_ID_MANM_91_B1_2_91' => Yii::t('app', 'Nom'),
            'MANM_HH1_ID_MANM_91_B2_91' => Yii::t('app', 'Sexe'),
            'MANM_HH1_ID_MANM_91_B3_91' => Yii::t('app', 'Age'),
            'MANM_HH1_ID_MANM_91_B7_91' => Yii::t('app', 'Relation Chef Famille'),
        ];
    }
}
