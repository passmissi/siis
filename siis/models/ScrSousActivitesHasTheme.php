<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SousActivitesHasTheme;

/**
 * ScrSousActivitesHasTheme represents the model behind the search form about `app\models\SousActivitesHasTheme`.
 */
class ScrSousActivitesHasTheme extends SousActivitesHasTheme
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sous_activite', 'id_type_theme'], 'integer'],
            [['id_plan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SousActivitesHasTheme::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_sous_activite' => $this->id_sous_activite,
            'id_type_theme' => $this->id_type_theme,
        ]);

        $query->andFilterWhere(['like', 'id_plan', $this->id_plan]);

        return $dataProvider;
    }
}
