<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monitoringsousactivites".
 *
 * @property string $id
 * @property string $mplan
 * @property string $mactivites
 * @property integer $plan
 * @property integer $activite
 * @property integer $sousactivites
 * @property integer $mode
 * @property integer $type_sous_activites
 * @property integer $isExecuted
 * @property integer $isExecutedBy
 * @property string $isExecutedOn
 * @property integer $isActif
 * @property integer $isClosed
 * @property integer $typetheme
 * @property string $id_famille
 * @property string $id_membre
 * @property string $date_creation
 * @property string $vaccin
 * @property double $poids
 * @property double $taille
 * @property integer $rapportpt
 * @property double $pb
 * @property integer $oedeme
 * @property string $en
 * @property string $type_reference
 * @property string $commentaire
 * @property double $quantite
 * @property string $secteur
 * @property string $institution
 * @property integer $evenement_type
 * @property string $date_evenement
 * @property string $create_date
 * @property string $evenement
 * @property integer $isReg
 * @property integer $qteBen_reg
 * @property integer $type_activites_reg
 * @property integer $akf_reg
 */
class Monitoringsousactivites extends \yii\db\ActiveRecord
{
	public $type;
	public $mois;
	public $annee;
	public $choix;
	public $activites;
	public $akf;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monitoringsousactivites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	//[['mplan', 'mode','date_creation', 'id_famille', 'id_membre', 'qteBen_reg', 'type_activites_reg'], 'required'],
            [['mplan', 'mode', 'type_sous_activites','date_creation', 'id_famille', 'id_membre', 'qteBen_reg', 'type_activites_reg'], 'required'],
        	//[['mplan', 'mode','date_creation', 'id_famille', 'id_membre'], 'required', 'on' => 'msousact'],
        	[['mplan', 'mactivites','annee', 'plan', 'activite', 'sousactivites', 'mode', 'type_sous_activites', 'isExecuted', 'isExecutedBy', 'isActif', 'isClosed', 'typetheme', 'id_famille', 'id_membre', 'rapportpt', 'oedeme', 'evenement_type', 'isReg', 'qteBen_reg', 'type_activites_reg', 'akf_reg'], 'integer'],
            [['isExecutedOn', 'date_creation', 'date_evenement', 'create_date'], 'safe'],
            [['poids', 'taille', 'pb', 'quantite','annee'], 'number'],
        	[['qteBen_reg'],'number','min'=>0,'max'=>200],
            [['vaccin', 'en', 'type_reference', 'secteur','mois'], 'string', 'max' => 50],
            [['commentaire', 'institution', 'evenement'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mplan' => Yii::t('app', 'Mplan'),
            'mactivites' => Yii::t('app', 'Mactivites'),
            'plan' => Yii::t('app', 'Plan'),
            'activite' => Yii::t('app', 'Activite'),
            'sousactivites' => Yii::t('app', 'Sousactivites'),
            'mode' => Yii::t('app', 'Mode'),
            'type_sous_activites' => Yii::t('app', 'Type Sous Activites'),
            'isExecuted' => Yii::t('app', 'Is Executed'),
            'isExecutedBy' => Yii::t('app', 'Is Executed By'),
            'isExecutedOn' => Yii::t('app', 'Is Executed On'),
            'isActif' => Yii::t('app', 'Is Actif'),
            'isClosed' => Yii::t('app', 'Is Closed'),
            'typetheme' => Yii::t('app', 'Typetheme'),
            'id_famille' => Yii::t('app', 'Id Famille'),
            'id_membre' => Yii::t('app', 'Id Membre'),
            'date_creation' => Yii::t('app', 'Date'),
            'vaccin' => Yii::t('app', 'Vaccin'),
            'poids' => Yii::t('app', 'Poids'),
            'taille' => Yii::t('app', 'Taille'),
            'rapportpt' => Yii::t('app', 'Rapportpt'),
            'pb' => Yii::t('app', 'Pb'),
            'oedeme' => Yii::t('app', 'Oedeme'),
            'en' => Yii::t('app', 'En'),
            'type_reference' => Yii::t('app', 'Type Reference'),
            'commentaire' => Yii::t('app', 'Commentaire'),
            'quantite' => Yii::t('app', 'Quantite'),
            'secteur' => Yii::t('app', 'Secteur'),
            'institution' => Yii::t('app', 'Institution'),
            'evenement_type' => Yii::t('app', 'Evenement Type'),
            'date_evenement' => Yii::t('app', 'Date Evenement'),
            'create_date' => Yii::t('app', 'Create Date'),
            'evenement' => Yii::t('app', 'Evenement'),
            'isReg' => Yii::t('app', 'Is Reg'),
            'qteBen_reg' => Yii::t('app', 'Ben.'),
            'type_activites_reg' => Yii::t('app', 'Activites'),
        	'mois' => Yii::t('app', 'Mois'),
        	'annee' => Yii::t('app', 'Annee'),
        ];
    }
    
    public function getDataPerType($mois, $annee, $id, $type)
    {
    	$sql = "select count(*) as total
				from activites a JOIN plans p on (a.`id_plan` = p.id)
				where p.`mode` = 1 and p.mois = '".$mois."' and p.annee = '".$annee."' and p.ts = '".$id."' and a.id_type_activite = '".$type."'
				group by a.`id_type_activite`";
    
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    	    $total[$i][0] = (int) $tot_p[$i]["total"];
    		//$total[$i][1] = $tot_p[$i]["type"];
    	}
    
    	return $total;
    }
    
    public function getDataSousActivitesPerType($mois, $annee, $id, $type)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$plan = Plans::find()->where(['mode' => 1, 'isDeleted' => 0, 'isClosed' => 0, 
    			'isMonitoring' => 0, 'isPlanned' => 1, 'mois' => $mois, 'annee' => $annee, 'ts' => $id])->all();
    	
    	$total = array();
    	$listact = array();
    	$i = 0;
    	
    	foreach ($plan as $c){
    		$listact[$i] = $c->id;
    		$i += 1;
    		//print_r($c->id);
    	}
    	print_r($listact[1]);
    	
    	$monitoring = Monitoringsousactivites::find()->where(['in', 'plan', $listact])
    		->andWhere(['mode' => 1, 'isExecuted' => 1, 'isActif' => 1, 
    				'isClosed' => 1, 'type_sous_activites' => $type])->count();
    	
    	$total[0] = $monitoring;
    	
    	return $total;
    }
}
