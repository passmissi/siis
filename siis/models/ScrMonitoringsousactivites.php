<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monitoringsousactivites;

/**
 * ScrMonitoringsousactivites represents the model behind the search form about `app\models\Monitoringsousactivites`.
 */
class ScrMonitoringsousactivites extends Monitoringsousactivites
{
	/**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mplan', 'mactivites','annee','plan', 'activite', 'sousactivites', 'mode', 'type_sous_activites', 'isExecuted', 'isExecutedBy', 'isActif', 'isClosed', 'typetheme', 'id_famille', 'id_membre', 'rapportpt', 'oedeme', 'evenement_type'], 'integer'],
            [['isExecutedOn', 'date_creation', 'vaccin', 'en', 'type_reference', 'commentaire', 'secteur', 'institution', 'date_evenement', 'create_date', 'evenement'], 'safe'],
            [['poids', 'taille', 'pb', 'quantite'], 'number'],
        	[['mois'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monitoringsousactivites::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mplan' => $this->mplan,
            'mactivites' => $this->mactivites,
            'plan' => $this->plan,
            'activite' => $this->activite,
            'sousactivites' => $this->sousactivites,
            'mode' => $this->mode,
            'type_sous_activites' => $this->type_sous_activites,
            'isExecuted' => $this->isExecuted,
            'isExecutedBy' => $this->isExecutedBy,
            'isExecutedOn' => $this->isExecutedOn,
            'isActif' => $this->isActif,
            'isClosed' => $this->isClosed,
            'typetheme' => $this->typetheme,
            'id_famille' => $this->id_famille,
            'id_membre' => $this->id_membre,
            'date_creation' => $this->date_creation,
            'poids' => $this->poids,
            'taille' => $this->taille,
            'rapportpt' => $this->rapportpt,
            'pb' => $this->pb,
            'oedeme' => $this->oedeme,
            'quantite' => $this->quantite,
            'evenement_type' => $this->evenement_type,
            'date_evenement' => $this->date_evenement,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'vaccin', $this->vaccin])
            ->andFilterWhere(['like', 'en', $this->en])
            ->andFilterWhere(['like', 'type_reference', $this->type_reference])
            ->andFilterWhere(['like', 'commentaire', $this->commentaire])
            ->andFilterWhere(['like', 'secteur', $this->secteur])
            ->andFilterWhere(['like', 'institution', $this->institution])
            ->andFilterWhere(['like', 'evenement', $this->evenement]);

        return $dataProvider;
    }
    
    public function search13($params, $id1, $id)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$query = Monitoringsousactivites::find()->where(['mplan' => $id, 'isExecutedBy' => $operateur->id, 'mode' => 1, 'isReg' => 0, 'akf_reg' => $id1]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'mplan' => $this->mplan,
    			'mactivites' => $this->mactivites,
    			'plan' => $this->plan,
    			'activite' => $this->activite,
    			'sousactivites' => $this->sousactivites,
    			'mode' => $this->mode,
    			'type_sous_activites' => $this->type_sous_activites,
    			'isExecuted' => $this->isExecuted,
    			'isExecutedBy' => $this->isExecutedBy,
    			'isExecutedOn' => $this->isExecutedOn,
    			'isActif' => $this->isActif,
    			'isClosed' => $this->isClosed,
    			'typetheme' => $this->typetheme,
    			'id_famille' => $this->id_famille,
    			'id_membre' => $this->id_membre,
    			'date_creation' => $this->date_creation,
    			'poids' => $this->poids,
    			'taille' => $this->taille,
    			'rapportpt' => $this->rapportpt,
    			'pb' => $this->pb,
    			'oedeme' => $this->oedeme,
    			'quantite' => $this->quantite,
    			'evenement_type' => $this->evenement_type,
    			'date_evenement' => $this->date_evenement,
    			'create_date' => $this->create_date,
    			]);
    
    	$query->andFilterWhere(['like', 'vaccin', $this->vaccin])
    	->andFilterWhere(['like', 'en', $this->en])
    	->andFilterWhere(['like', 'type_reference', $this->type_reference])
    	->andFilterWhere(['like', 'commentaire', $this->commentaire])
    	->andFilterWhere(['like', 'secteur', $this->secteur])
    	->andFilterWhere(['like', 'institution', $this->institution])
    	->andFilterWhere(['like', 'evenement', $this->evenement]);
    
    	return $dataProvider;
    }
    
    public function search21($params, $id, $mois, $annee)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    
    	$query = Plans::find()->where(['ts' => $id, 'mode' => 1, 'commune' => $operateur->commune_lie, 'mois' => $mois, 'annee' => $annee]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere(['like', 'mois', $this->mois]);
    	$query->andFilterWhere(['like', 'annee', $this->annee]);
    
    	return $dataProvider;
    }
    
   /*  public function search23($params, $datay)
    {
    	
    } */
    
    public function search22($params, $datay, $mois,$annee)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$annee1 = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	
    	if($mois != '' && $annee != ''){
    		$plans_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    		
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    		
    		if($datayz != "()"){
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz.'');
    		}
    		else{
    			/* $plans_data1 = Plans::find()->where(['mode' => 1])
    				->andWhere('commune IN '.$datay.'')->asArray()->all();
    			
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz1.''); */
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan < 0 ');
    		}
    	}
    	elseif ($mois != '' && $annee == ''){
    		$plans_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee1, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    		
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    		
    		if($datayz != "()"){
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz.'');
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    				->andWhere('commune IN '.$datay.'')->asArray()->all();
    			
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    			
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz1.'');
    		}
    	}
    	elseif ($mois == '' && $annee != ''){
    		$plans_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    		
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    		
    		if($datayz != "()"){
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz.'');
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    				->andWhere('commune IN '.$datay.'')->asArray()->all();
    			
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    			
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz1.'');
    		}
    	}
    	elseif ($mois == '' && $annee == ''){
    		$plans_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee1, 'mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    		
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    		
    		if($datayz != "()"){
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz.'');
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    				->andWhere('commune IN '.$datay.'')->asArray()->all();
    			
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    			
    			$query = Monitoringsousactivites::find()->where(['mode' => 1])->andWhere('plan IN '.$datayz1.'');
    		}
    	}
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
            'type_sous_activites' => $this->type_sous_activites,
            'typetheme' => $this->typetheme,
        ]);
    
    	return $dataProvider;
    }
    
    public function search23($datay, $mois,$annee,$type,$type1)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$annee1 = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	 
    	if($mois != '' && $annee != ''){
    		$plans_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    	
    		if($datayz != "()"){
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    	
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    		}
    	}
    	elseif ($mois != '' && $annee == ''){
    		$plans_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee1, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    	
    		if($datayz != "()"){
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    	
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    		}
    	}
    	elseif ($mois == '' && $annee != ''){
    		$plans_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    	
    		if($datayz != "()"){
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    	
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz1.'')->all();
    			}
    		}
    	}
    	elseif ($mois == '' && $annee == ''){
    		$plans_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee1, 'mode' => 1])
    		->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    		$titles = '';
    		foreach($plans_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$datax = ''.rtrim($titles, ', ').'';
    		$datayz = '('.preg_replace("/'/", '', $datax).')';
    	
    		if($datayz != "()"){
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    		}
    		else{
    			$plans_data1 = Plans::find()->where(['mode' => 1])
    			->andWhere('commune IN '.$datay.'')->asArray()->all();
    	
    			$titles1 = '';
    			foreach($plans_data1 as $row) {
    				$titles1 .= $row['id'] . ', ';
    			}
    			$datax1 = ''.rtrim($titles1, ', ').'';
    			$datayz1 = '('.preg_replace("/'/", '', $datax1).')';
    	
    			if($type != 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type != 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'type_sous_activites' => $type])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 != 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1, 'typetheme' => $type1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    			elseif ($type == 0 && $type1 == 0){
    				$query = Monitoringsousactivites::find()->where(['mode' => 1])
    				->andWhere('plan IN '.$datayz.'')->all();
    			}
    		}
    	}
    	
		return $query;
    }
	
    public function search55($params, $datax)
    {
    	if($datax != "()"){
   			$query = Monitoringsousactivites::find()->where('plan IN '.$datax.'');
    	}else{
    		$query = Monitoringsousactivites::find()->where(['plan'=>-1]);
    	}
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
            'id' => $this->id,
            'mplan' => $this->mplan,
            'mactivites' => $this->mactivites,
            'plan' => $this->plan,
            'activite' => $this->activite,
            'sousactivites' => $this->sousactivites,
            'mode' => $this->mode,
            'type_sous_activites' => $this->type_sous_activites,
            'isExecuted' => $this->isExecuted,
            'isExecutedBy' => $this->isExecutedBy,
            'isExecutedOn' => $this->isExecutedOn,
            'isActif' => $this->isActif,
            'isClosed' => $this->isClosed,
            'typetheme' => $this->typetheme,
            'id_famille' => $this->id_famille,
            'id_membre' => $this->id_membre,
            'date_creation' => $this->date_creation,
            'poids' => $this->poids,
            'taille' => $this->taille,
            'rapportpt' => $this->rapportpt,
            'pb' => $this->pb,
            'oedeme' => $this->oedeme,
            'quantite' => $this->quantite,
            'evenement_type' => $this->evenement_type,
            'date_evenement' => $this->date_evenement,
            'create_date' => $this->create_date,
        ]);

        $query->andFilterWhere(['like', 'vaccin', $this->vaccin])
            ->andFilterWhere(['like', 'en', $this->en])
            ->andFilterWhere(['like', 'type_reference', $this->type_reference])
            ->andFilterWhere(['like', 'commentaire', $this->commentaire])
            ->andFilterWhere(['like', 'secteur', $this->secteur])
            ->andFilterWhere(['like', 'institution', $this->institution])
            ->andFilterWhere(['like', 'evenement', $this->evenement]);

        return $dataProvider;
    }
}
