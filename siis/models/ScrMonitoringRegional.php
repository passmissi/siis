<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MonitoringRegional;
use yii\bootstrap\Alert;

class ScrMonitoringRegional extends MonitoringRegional
{
	public $mois;
	public $annee;
	public $indicateurs2;
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['indicateurs2', 'annee'], 'integer'],
			[['mois'], 'safe'],
		];
	}
	
	public function statsindicateurs2()
	{
		$query = Monitoringsousactivites::find()->where([
			'type_sous_activites' => 6, 'vaccin'=>12
		]);
			
		$dataProvider = new ActiveDataProvider([
				'query' => $query,
		]);
			
		return $dataProvider;
	}
	
	public function statsindicateurs($mois, $annee, $communes, $indicateurs2)
	{
		/* $result = array();
		
		$result[0] = 10;
		$result[1] = 10;
		$result[2] = 10;
		$result[3] = 10;
		$result[4] = 10;
		$result[5] = 10;
		$result[6] = 10;
		$result[7] = 10;
		$result[8] = 10;
		$result[9] = 10;
		return $result; */
		
		$plan = new Plans();
		$data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'commune' => $communes])->asArray()->all();
		$datax = $plan->getQuerysplit($data, 'id');
		$commune = Communes::findOne($communes);
		
		if($indicateurs2 == 1){
			$query = Monitoringsousactivites::find()->where([
				'type_sous_activites' => 6, 'vaccin'=>12
			])->andWhere('plan IN'.$datax.'');
			
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);
		}else{
			$query = Monitoringsousactivites::find();
				
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);
		}
		
		return $dataProvider;
	}
	
	public function search12($params,$mois,$annee)
	{
		$query = DataIntegration::find()->where('MONTH(date_creation) = '.$mois.'')
					->andWhere('YEAR(date_creation) = '.$annee.'');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);
		
		$this->load($params);
		
		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$query->andFilterWhere(['like', 'type_activite', $this->indicateurs2]);
		//$query->andFilterWhere(['like', 'annee', $this->annee]);
		
		return $dataProvider;
	}
	//statsindicateurs($model->mois, $model->annee);
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $datax,$temoin)
	{
		if($temoin == 1){
			$query = Plans::find()->where('commune IN('.$datax.')')->andWhere(['mode' => 1]);
			
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);
		}
		else{
			$query = Plans::find()->where('commune IN('.$datax.')')->andWhere(['mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0]);
				
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);
		}
		
		$this->load($params);
	
		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
	
		$query->andFilterWhere(['like', 'mois', $this->mois]);
		$query->andFilterWhere(['ts' => $this->ts]);
		$query->andFilterWhere(['like', 'annee', $this->annee]);
	
		return $dataProvider;
	}
	
	public function search24($params, $datax, $searchModel)
	{
		$annee = date('Y');
		$plan = new Plans();
		$time = new \DateTime('now', new \DateTimeZone('UTC'));
		$mois_int = date_format($time,"n");
		$mois1 = $plan->getNumberMonth($mois_int);
		$query = "";
		
		if($searchModel->mois != '' && $searchModel->annee != ''){
			$plans = $this->getPlansSuivis($datax, $searchModel->mois, $searchModel->annee);
			if($plans != null)$query = Monitoringsousactivites::find()->where(['type_sous_activites' => 6])
				->andWhere('plan IN('.$plans.')');
		}
		elseif ($searchModel->mois != '' && $searchModel->annee == ''){
			$plans = $this->getPlansSuivis($datax, $searchModel->mois, $annee);
			if($plans != null)$query = Monitoringsousactivites::find()->where(['type_sous_activites' => 6])
				->andWhere('plan IN('.$plans.')');
		}
		elseif ($searchModel->mois == '' && $searchModel->annee != ''){
			$plans = $this->getPlansSuivis($datax, $mois1, $searchModel->annee);
			if($plans != null)$query = Monitoringsousactivites::find()->where(['type_sous_activites' => 6])
				->andWhere('plan IN('.$plans.')');
		}
		elseif ($searchModel->mois == '' && $searchModel->annee == ''){
			$plans = $this->getPlansSuivis($datax, $mois1, $annee);
			if($plans != null)$query = Monitoringsousactivites::find()->where(['type_sous_activites' => 6])
				->andWhere('plan IN('.$plans.')');
		}
		
		if($query != null){
			$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);
		}else{
			$user = User::findOne(Yii::$app->user->identity->id);
			$operateur = Operateurs::findOne($user->idOperateurs);
			
			$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
			$titles = '';
			foreach($data as $row) {
				$titles .= $row['valeur'] . ', ';
			}
			$datax = ''.rtrim($titles, ', ').'';
			$datay = '('.preg_replace("/'/", '', $datax).')';
			
			$opera = Operateurs::find()->where('commune_lie IN '.$datay.'')->asArray()->all();
			
			$titles1 = '';
			foreach($data as $row) {
				$titles1 .= $row['id'] . ', ';
			}
			$datax1 = ''.rtrim($titles, ', ').'';
			$datay1 = '('.preg_replace("/'/", '', $datax).')';
			
			$query1 = Monitoringsousactivites::find()->where(['type_sous_activites' => 6])
			  ->andWhere('isExecutedBy IN '.$datay1.'');
			
			$dataProvider = new ActiveDataProvider([
				'query' => $query1,
			]);
		}
		
	
		$this->load($params);
	
		if (!$this->validate()) {
			return $dataProvider;
		}
	
		/* $query->andFilterWhere(['like', 'mois', $this->mois]);
		$query->andFilterWhere(['ts' => $this->ts]);
		$query->andFilterWhere(['like', 'annee', $this->annee]); */
	
		return $dataProvider;
	} 
	
	private function getPlansSuivis($datax,$mois,$annee){
		$data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1,'isMonitoring' => 0,
				'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
				->andWhere('commune IN ('.$datax.')')->asArray()->all();
		
		$titles = '';
		foreach($data as $row) {
			$titles .= $row['id'] . ', ';
		}
		$dataw = ''.rtrim($titles, ', ').'';
		//$dataq = '('.preg_replace("/'/", '', $dataw).')';
		return $dataw;
	}
	
	public function getStatszone($datax,$mois,$annee_int)
	{
		$connection = Yii::$app->db;
		 
		if($mois == ''){
			$plan = new Plans();
			$time = new \DateTime('now', new \DateTimeZone('UTC'));
			$mois_int = date_format($time,"n");
			$mois = $plan->getNumberMonth($mois_int);
		}
		 
		if($annee_int == ''){
			$annee_int = date('Y');
		}
		
		$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND (`mois` = '.$mois.' or `mois` is null)
				AND (`annee` = $annee_int or `annee` is null)
				group by `commune`";
		
		$tot_g = $connection->createCommand($sql)->queryAll();
		 
		$a = json_encode($tot_g, JSON_NUMERIC_CHECK);
		 
		$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
		 
		return $json;
	}
}