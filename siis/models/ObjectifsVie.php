<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objectifs_vie".
 *
 * @property string $id
 * @property string $famille
 * @property integer $objectif
 * @property integer $plan
 * @property string $mplan
 * @property string $niveau
 * @property string $date_objectif
 * @property string $commentaire
 * @property string $createdBy
 * @property string $createdOn
 *
 * @property RefObjectifsvie $objectif0
 * @property Famille $famille0
 */
class ObjectifsVie extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'objectifs_vie';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'objectif', 'niveau', 'date_objectif', 'plan','mplan'], 'required'],
            [['id', 'famille', 'objectif', 'createdBy','plan','mplan'], 'integer'],
            [['date_objectif', 'createdOn'], 'safe'],
            [['niveau'], 'string', 'max' => 100],
            [['commentaire'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'famille' => Yii::t('app', 'Famille'),
            'objectif' => Yii::t('app', 'Objectif'),
        	'plan' => Yii::t('app', 'Plan'),
        	'mplan' => Yii::t('app', 'Mplan'),
            'niveau' => Yii::t('app', 'Niveau'),
            'date_objectif' => Yii::t('app', 'Date Objectif'),
            'commentaire' => Yii::t('app', 'Commentaire'),
            'createdBy' => Yii::t('app', 'Created By'),
            'createdOn' => Yii::t('app', 'Created On'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectif0()
    {
        return $this->hasOne(RefObjectifsvie::className(), ['id' => 'objectif']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamille0()
    {
        return $this->hasOne(Famille::className(), ['id' => 'famille']);
    }
}
