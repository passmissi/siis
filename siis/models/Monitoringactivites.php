<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "monitoringactivites".
 *
 * @property string $id
 * @property string $mplan
 * @property integer $plan
 * @property integer $activites
 * @property integer $mode
 * @property integer $type_activite
 * @property integer $isExecuted
 * @property integer $isExecutedBy
 * @property string $isExecutedOn
 * @property integer $isActif
 * @property integer $isDone
 * @property string $id_famille
 * @property integer $id_akf
 * @property integer $id_theme
 * @property string $commentaires
 * @property string $lieu
 * @property integer $nbre_participants
 * @property integer $sousactivites
 * @property string $eval_lieu
 * @property string $eval_contenu
 * @property string $eval_retroaction
 * @property string $type_rencontre
 * @property string $sujets
 * @property string $actions
 * @property string $module
 * @property integer $duree
 * @property string $remarques
 *
 * @property Monitoringplan $mplan0
 */
class Monitoringactivites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'monitoringactivites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mplan', 'plan', 'activites', 'mode'], 'required'],
            [['mplan', 'plan', 'activites', 'mode', 'type_activite', 'isExecuted', 'isExecutedBy', 'isActif', 'isDone', 'id_famille', 'id_akf', 'id_theme', 'nbre_participants', 'sousactivites', 'duree'], 'integer'],
            [['isExecutedOn'], 'safe'],
            [['commentaires', 'lieu', 'sujets', 'actions', 'remarques'], 'string', 'max' => 100],
            [['eval_lieu', 'eval_contenu', 'eval_retroaction', 'type_rencontre', 'module'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'mplan' => Yii::t('app', 'Mplan'),
            'plan' => Yii::t('app', 'Plan'),
            'activites' => Yii::t('app', 'Activites'),
            'mode' => Yii::t('app', 'Mode'),
            'type_activite' => Yii::t('app', 'Type Activite'),
            'isExecuted' => Yii::t('app', 'Is Executed'),
            'isExecutedBy' => Yii::t('app', 'Is Executed By'),
            'isExecutedOn' => Yii::t('app', 'Is Executed On'),
            'isActif' => Yii::t('app', 'Is Actif'),
            'isDone' => Yii::t('app', 'Is Done'),
            'id_famille' => Yii::t('app', 'Id Famille'),
            'id_akf' => Yii::t('app', 'Id Akf'),
            'id_theme' => Yii::t('app', 'Id Theme'),
            'commentaires' => Yii::t('app', 'Commentaires'),
            'lieu' => Yii::t('app', 'Lieu'),
            'nbre_participants' => Yii::t('app', 'Nbre Participants'),
            'sousactivites' => Yii::t('app', 'Sousactivites'),
            'eval_lieu' => Yii::t('app', 'Eval Lieu'),
            'eval_contenu' => Yii::t('app', 'Eval Contenu'),
            'eval_retroaction' => Yii::t('app', 'Eval Retroaction'),
            'type_rencontre' => Yii::t('app', 'Type Rencontre'),
            'sujets' => Yii::t('app', 'Sujets'),
            'actions' => Yii::t('app', 'Actions'),
            'module' => Yii::t('app', 'Module'),
            'duree' => Yii::t('app', 'Duree'),
            'remarques' => Yii::t('app', 'Remarques'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMplan0()
    {
        return $this->hasOne(Monitoringplan::className(), ['id' => 'mplan']);
    }
}
