<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activites".
 *
 * @property integer $id
 * @property integer $id_plan
 * @property integer $id_type_activite
 * @property string $date_activite
 * @property string $id_famille
 * @property string $lieu
 * @property string $description
 * @property integer $isDone
 * @property string $isDoneOn
 * @property integer $isDoneBy
 * @property integer $isVerified
 * @property integer $isChanged
 * @property integer $mode
 * @property integer $AKFMonitoringCode
 * @property integer $ActivitesMonitoringCode
 * @property integer $TSMonitoringCode
 * @property integer $TsActivitesMonitoringCode
 *
 * @property Activites $activitesMonitoringCode
 * @property Activites[] $activites
 * @property Activites $tsActivitesMonitoringCode
 * @property Operateurs $aKFMonitoringCode
 * @property Famille $idFamille
 * @property Mode $mode0
 * @property Plans $idPlan
 * @property TypeActivites $idTypeActivite
 * @property Operateurs $tSMonitoringCode
 */
class Activites extends \yii\db\ActiveRecord
{
	public $distribution;
	public $promotion;
	public $formation;
	public $sousactivites;
	public $libelle;
	public $formatreunion;
	public $family_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_plan', 'id_type_activite'], 'required'],
            [['id_plan', 'id_type_activite', 'id_famille', 'isDone', 'isDoneBy', 'isVerified', 'isChanged', 'mode', 'AKFMonitoringCode', 'ActivitesMonitoringCode', 'TSMonitoringCode', 'TsActivitesMonitoringCode'], 'integer'],
            [['date_activite', 'isDoneOn'], 'safe'],
            [['lieu', 'description'], 'string', 'max' => 100],
        	[['case_id'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_plan' => Yii::t('app', 'Id Plan'),
            'id_type_activite' => Yii::t('app', 'Id Type Activite'),
            'date_activite' => Yii::t('app', 'Date Activite'),
            'id_famille' => Yii::t('app', 'Id (Tapez le code Famille et cliquez 2 fois)'),
            'lieu' => Yii::t('app', 'Lieu'),
            'description' => Yii::t('app', 'Description'),
            'isDone' => Yii::t('app', 'Is Done'),
            'isDoneOn' => Yii::t('app', 'Is Done On'),
            'isDoneBy' => Yii::t('app', 'Is Done By'),
            'isVerified' => Yii::t('app', 'Is Verified'),
            'isChanged' => Yii::t('app', 'Is Changed'),
            'mode' => Yii::t('app', 'Mode'),
        	'case_id' => Yii::t('app', 'Case ID'),
            'AKFMonitoringCode' => Yii::t('app', 'Akf'),
            'ActivitesMonitoringCode' => Yii::t('app', 'Activites'),
            'TSMonitoringCode' => Yii::t('app', 'Tsmonitoring Code'),
            'TsActivitesMonitoringCode' => Yii::t('app', 'Ts Activites Monitoring Code'),
            'sousactivites' => Yii::t('app', 'Autres'),
            'formatreunion' => Yii::t('app', 'Format Reunion'),
        	'family_name' => Yii::t('app', 'Nom Chef Famille')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivitesMonitoringCode()
    {
        return $this->hasOne(Activites::className(), ['id' => 'ActivitesMonitoringCode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivites()
    {
        return $this->hasMany(Activites::className(), ['TsActivitesMonitoringCode' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTsActivitesMonitoringCode()
    {
        return $this->hasOne(Activites::className(), ['id' => 'TsActivitesMonitoringCode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAKFMonitoringCode()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'AKFMonitoringCode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFamille()
    {
        return $this->hasOne(Famille::className(), ['id' => 'id_famille']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMode0()
    {
        return $this->hasOne(Mode::className(), ['id' => 'mode']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPlan()
    {
        return $this->hasOne(Plans::className(), ['id' => 'id_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdTypeActivite()
    {
        return $this->hasOne(TypeActivites::className(), ['id' => 'id_type_activite']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTSMonitoringCode()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'TSMonitoringCode']);
    }
    
    public function getListactivites($id)
    {
    	
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$count = 0;
    	 
    	$communes = Activites::find()
    	->select('activites.*')
    	->leftJoin('plans', '`plans`.`id` = `activites`.`id_plan`')
    	->where(['plans.ts' => $operateur->id])
    	->with('plans')
    	->count();
    	 
    	if($count > 0)
    	{
    		foreach ($communes as $commune){
    			echo "<option value='".$commune->id."'>".$commune->libelle."</option>";
    		}
    	}
    	else
    	{
    		return "<option value=64>Test</option>";
    		//echo "<option>.</option>";
    	}
    }
}
