<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Membres;

/**
 * ScrMembres represents the model behind the search form about `app\models\Membres`.
 */
class ScrMembres extends Membres
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_family', 'id', 'sex', 'age', 'date_birth', 'document_type', 'b7', 'b8', 'b9', 'b10', 'b11', 'b12', 'b13', 'b14', 'b15', 'id_user', 'date_register'], 'integer'],
            [['id_person', 'firstname', 'name', 'nickname', 'document_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Membres::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_family' => $this->id_family,
            'id' => $this->id,
            'sex' => $this->sex,
            'age' => $this->age,
            'date_birth' => $this->date_birth,
            'document_type' => $this->document_type,
            'b7' => $this->b7,
            'b8' => $this->b8,
            'b9' => $this->b9,
            'b10' => $this->b10,
            'b11' => $this->b11,
            'b12' => $this->b12,
            'b13' => $this->b13,
            'b14' => $this->b14,
            'b15' => $this->b15,
            'id_user' => $this->id_user,
            'date_register' => $this->date_register,
        ]);

        $query->andFilterWhere(['like', 'id_person', $this->id_person])
            ->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'document_number', $this->document_number]);

        return $dataProvider;
    }
}
