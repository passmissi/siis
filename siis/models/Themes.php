<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "themes".
 *
 * @property integer $id
 * @property string $nom_theme
 *
 * @property Plans[] $plans
 */
class Themes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nom_theme'], 'required'],
            [['id'], 'integer'],
            [['nom_theme'], 'string', 'max' => 255],
            [['nom_theme'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nom_theme' => Yii::t('app', 'Nom Theme'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['theme' => 'id']);
    }
}
