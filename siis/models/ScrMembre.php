<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Membre;

/**
 * ScrMembre represents the model behind the search form about `app\models\Membre`.
 */
class ScrMembre extends Membre
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'memberid', 'sexe', 'age', 'annee_naissance', 'mois_naissance', 'jour_naissance', 'type_piece_indendification', 'relation_chef_famille', 'est_mere', 'est_pere', 'b10', 'b11', 'etat_civil', 'b13', 'moins_18_hors_maison', 'b15', 'savoir_lire', 'savoir_ecrire', 'c1_2', 'c1_3', 'c1_4', 'c1_5', 'niveau_education', 'c3', 'c4', 'c5', 'pouvoir_travailler', 'avoir_emploi', 'avoir_activite_economique', 'type_activite', 'd5', 'e8', 'e9', 'e10', 'e11', 'e12', 'e13', 'e14', 'e15', 'e16', 'e17', 'e18', 'e19', 'e20', 'calc_1'], 'integer'],
            [['prenom', 'nom', 'surnom', 'numero_piece'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Membre::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'famille' => $this->famille,
            'memberid' => $this->memberid,
            'sexe' => $this->sexe,
            'age' => $this->age,
            'annee_naissance' => $this->annee_naissance,
            'mois_naissance' => $this->mois_naissance,
            'jour_naissance' => $this->jour_naissance,
            'type_piece_indendification' => $this->type_piece_indendification,
            'relation_chef_famille' => $this->relation_chef_famille,
            'est_mere' => $this->est_mere,
            'est_pere' => $this->est_pere,
            'b10' => $this->b10,
            'b11' => $this->b11,
            'etat_civil' => $this->etat_civil,
            'b13' => $this->b13,
            'moins_18_hors_maison' => $this->moins_18_hors_maison,
            'b15' => $this->b15,
            'savoir_lire' => $this->savoir_lire,
            'savoir_ecrire' => $this->savoir_ecrire,
            'c1_2' => $this->c1_2,
            'c1_3' => $this->c1_3,
            'c1_4' => $this->c1_4,
            'c1_5' => $this->c1_5,
            'niveau_education' => $this->niveau_education,
            'c3' => $this->c3,
            'c4' => $this->c4,
            'c5' => $this->c5,
            'pouvoir_travailler' => $this->pouvoir_travailler,
            'avoir_emploi' => $this->avoir_emploi,
            'avoir_activite_economique' => $this->avoir_activite_economique,
            'type_activite' => $this->type_activite,
            'd5' => $this->d5,
            'e8' => $this->e8,
            'e9' => $this->e9,
            'e10' => $this->e10,
            'e11' => $this->e11,
            'e12' => $this->e12,
            'e13' => $this->e13,
            'e14' => $this->e14,
            'e15' => $this->e15,
            'e16' => $this->e16,
            'e17' => $this->e17,
            'e18' => $this->e18,
            'e19' => $this->e19,
            'e20' => $this->e20,
            'calc_1' => $this->calc_1,
        ]);

        $query->andFilterWhere(['like', 'prenom', $this->prenom])
            ->andFilterWhere(['like', 'nom', $this->nom])
            ->andFilterWhere(['like', 'surnom', $this->surnom])
            ->andFilterWhere(['like', 'numero_piece', $this->numero_piece]);

        return $dataProvider;
    }
    
    public function search11($params, $ts, $id)
    {
    	$data = FamilyAkf::find()->where(['operateur_ts' => $ts])->all();
    	$nomcomplet = array();
    	$i = 0;
    	
    	if($id == 0){
    		foreach ($data as $c){
    			$nomcomplet[$i] = $c->famille;
    			$i += 1;
    		}
    		 
    		$query = Membre::find()->where(['in', 'famille', $nomcomplet]);
    	}
    	else{
    		$query = Membre::find()->where(['famille' => $id]);
    	}
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    		'famille' => $this->famille,
    		'memberid' => $this->memberid,
    		'sexe' => $this->sexe,
    		'age' => $this->age,
    			'annee_naissance' => $this->annee_naissance,
    			'mois_naissance' => $this->mois_naissance,
    			'jour_naissance' => $this->jour_naissance,
    			'type_piece_indendification' => $this->type_piece_indendification,
    			'relation_chef_famille' => $this->relation_chef_famille,
    			'est_mere' => $this->est_mere,
    			'est_pere' => $this->est_pere,
    			'b10' => $this->b10,
    			'b11' => $this->b11,
    			'etat_civil' => $this->etat_civil,
    			'b13' => $this->b13,
    			'moins_18_hors_maison' => $this->moins_18_hors_maison,
    			'b15' => $this->b15,
    			'savoir_lire' => $this->savoir_lire,
    			'savoir_ecrire' => $this->savoir_ecrire,
    			'c1_2' => $this->c1_2,
    			'c1_3' => $this->c1_3,
    			'c1_4' => $this->c1_4,
    			'c1_5' => $this->c1_5,
    			'niveau_education' => $this->niveau_education,
    			'c3' => $this->c3,
    			'c4' => $this->c4,
    			'c5' => $this->c5,
    			'pouvoir_travailler' => $this->pouvoir_travailler,
    			'avoir_emploi' => $this->avoir_emploi,
    			'avoir_activite_economique' => $this->avoir_activite_economique,
    			'type_activite' => $this->type_activite,
    			'd5' => $this->d5,
    			'e8' => $this->e8,
    			'e9' => $this->e9,
    			'e10' => $this->e10,
    			'e11' => $this->e11,
    			'e12' => $this->e12,
    			'e13' => $this->e13,
    			'e14' => $this->e14,
    			'e15' => $this->e15,
    			'e16' => $this->e16,
    			'e17' => $this->e17,
    			'e18' => $this->e18,
    			'e19' => $this->e19,
    			'e20' => $this->e20,
    			'calc_1' => $this->calc_1,
    			]);
    
    	$query->andFilterWhere(['like', 'prenom', $this->prenom])
    	->andFilterWhere(['like', 'nom', $this->nom])
    	->andFilterWhere(['like', 'surnom', $this->surnom])
    	->andFilterWhere(['like', 'numero_piece', $this->numero_piece]);
    
    	return $dataProvider;
    }
    
    public function search14($params, $id)
    {
    	$query = Membre::find()->where(['famille' => $id]);
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'famille' => $this->famille,
    			'memberid' => $this->memberid,
    			'sexe' => $this->sexe,
    			'age' => $this->age,
    			'annee_naissance' => $this->annee_naissance,
    			'mois_naissance' => $this->mois_naissance,
    			'jour_naissance' => $this->jour_naissance,
    			'type_piece_indendification' => $this->type_piece_indendification,
    			'relation_chef_famille' => $this->relation_chef_famille,
    			'est_mere' => $this->est_mere,
    			'est_pere' => $this->est_pere,
    			'b10' => $this->b10,
    			'b11' => $this->b11,
    			'etat_civil' => $this->etat_civil,
    			'b13' => $this->b13,
    			'moins_18_hors_maison' => $this->moins_18_hors_maison,
    			'b15' => $this->b15,
    			'savoir_lire' => $this->savoir_lire,
    			'savoir_ecrire' => $this->savoir_ecrire,
    			'c1_2' => $this->c1_2,
    			'c1_3' => $this->c1_3,
    			'c1_4' => $this->c1_4,
    			'c1_5' => $this->c1_5,
    			'niveau_education' => $this->niveau_education,
    			'c3' => $this->c3,
    			'c4' => $this->c4,
    			'c5' => $this->c5,
    			'pouvoir_travailler' => $this->pouvoir_travailler,
    			'avoir_emploi' => $this->avoir_emploi,
    			'avoir_activite_economique' => $this->avoir_activite_economique,
    			'type_activite' => $this->type_activite,
    			'd5' => $this->d5,
    			'e8' => $this->e8,
    			'e9' => $this->e9,
    			'e10' => $this->e10,
    			'e11' => $this->e11,
    			'e12' => $this->e12,
    			'e13' => $this->e13,
    			'e14' => $this->e14,
    			'e15' => $this->e15,
    			'e16' => $this->e16,
    			'e17' => $this->e17,
    			'e18' => $this->e18,
    			'e19' => $this->e19,
    			'e20' => $this->e20,
    			'calc_1' => $this->calc_1,
    	]);
    
    	$query->andFilterWhere(['like', 'prenom', $this->prenom])
    	->andFilterWhere(['like', 'nom', $this->nom])
    	->andFilterWhere(['like', 'surnom', $this->surnom])
    	->andFilterWhere(['like', 'numero_piece', $this->numero_piece]);
    
    	return $dataProvider;
    }
}
