<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activites;

/**
 * ScrActivites represents the model behind the search form about `app\models\Activites`.
 */
class ScrActivites extends Activites
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_plan', 'id_type_activite', 'id_famille'], 'integer'],
            [['date_activite', 'lieu', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Activites::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'id_plan' => $this->id_plan,
            'id_type_activite' => $this->id_type_activite,
            'date_activite' => $this->date_activite,
            'id_famille' => $this->id_famille,
        ]);

        $query->andFilterWhere(['like', 'lieu', $this->lieu])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
    
    public function search14($params, $id)
    {
    	$query = Activites::find()
    	  ->joinWith('idPlan')
    	  ->where(['plans.ts' => $id, 'activites.mode' => 1])
    	  ->andWhere(['is not', 'plans.akf', null]);
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    		'id_plan' => $this->id_plan,
    		'id_type_activite' => $this->id_type_activite,
    	]);
    
    	
    
    	return $dataProvider;
    }
}
