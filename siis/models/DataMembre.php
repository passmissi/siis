<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_membre".
 *
 * @property string $famille
 * @property integer $memberid
 * @property string $case_id
 * @property string $closed
 * @property string $closed_by_username
 * @property string $closed_date
 * @property string $last_modified_by_username
 * @property string $last_modified_date
 * @property string $opened_by_username
 * @property string $opened_date
 * @property string $owner_id
 * @property string $owner_name
 * @property string $date_creation
 * @property string $source
 */
class DataMembre extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_membre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'memberid', 'case_id'], 'required'],
            [['famille', 'memberid'], 'integer'],
            [['closed_date', 'last_modified_date', 'opened_date', 'date_creation'], 'safe'],
            [['case_id'], 'string', 'max' => 100],
            [['closed', 'closed_by_username', 'last_modified_by_username', 'opened_by_username', 'owner_id', 'owner_name'], 'string', 'max' => 75],
            [['source'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'famille' => Yii::t('app', 'Famille'),
            'memberid' => Yii::t('app', 'Memberid'),
            'case_id' => Yii::t('app', 'Case ID'),
            'closed' => Yii::t('app', 'Closed'),
            'closed_by_username' => Yii::t('app', 'Closed By Username'),
            'closed_date' => Yii::t('app', 'Closed Date'),
            'last_modified_by_username' => Yii::t('app', 'Last Modified By Username'),
            'last_modified_date' => Yii::t('app', 'Last Modified Date'),
            'opened_by_username' => Yii::t('app', 'Opened By Username'),
            'opened_date' => Yii::t('app', 'Opened Date'),
            'owner_id' => Yii::t('app', 'Owner ID'),
            'owner_name' => Yii::t('app', 'Owner Name'),
            'date_creation' => Yii::t('app', 'Date Creation'),
            'source' => Yii::t('app', 'Source'),
        ];
    }
}
