<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monitoringplan;

/**
 * ScrMonitoringplan represents the model behind the search form about `app\models\Monitoringplan`.
 */
class ScrMonitoringplan extends Monitoringplan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Plan', 'isExecuted', 'isExecutedBy', 'isActif', 'isClosed', 'mode'], 'integer'],
            [['isExecutedOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monitoringplan::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'Plan' => $this->Plan,
            'isExecuted' => $this->isExecuted,
            'isExecutedBy' => $this->isExecutedBy,
            'isExecutedOn' => $this->isExecutedOn,
            'isActif' => $this->isActif,
            'isClosed' => $this->isClosed,
            'mode' => $this->mode,
        ]);

        return $dataProvider;
    }
    
    public function search11($params)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$query = Monitoringplan::find()->where(['isExecutedBy' => $operateur->id, 'mode' => 1, 'isReg' => 0]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    		'id' => $this->id,
    		'Plan' => $this->Plan,
    		'isExecuted' => $this->isExecuted,
    		'isExecutedBy' => $this->isExecutedBy,
    		'isExecutedOn' => $this->isExecutedOn,
    		'isActif' => $this->isActif,
    		'isClosed' => $this->isClosed,
    		'mode' => $this->mode,
    	]);
    
    	return $dataProvider;
    }
}
