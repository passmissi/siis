<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "chroniques".
 *
 * @property string $Id
 * @property string $famille
 * @property integer $memberid
 * @property integer $souffrir_maladie_chronique
 * @property integer $avoir_traitement
 * @property integer $duree_traitement
 * @property integer $raison_non_traitement
 * @property integer $avoir_autres_maladies
 * @property integer $MANM_HH1_HH_MALAD_81_E2_2_81
 * @property integer $MANM_HH1_HH_MALAD_81_E2_3_81
 * @property integer $MANM_HH1_HH_MALAD_81_E2_4_81
 * @property integer $MANM_HH1_HH_MALAD_81_E3_1_81
 * @property integer $MANM_HH1_HH_MALAD_81_E3_2_81
 * @property integer $MANM_HH1_HH_MALAD_81_E3_3_81
 *
 * @property Famille $famille0
 */
class Chroniques extends \yii\db\ActiveRecord
{
	public $is_ok;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chroniques';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille'], 'required'],
            [['famille', 'memberid', 'souffrir_maladie_chronique', 'avoir_traitement', 'duree_traitement', 'raison_non_traitement', 'avoir_autres_maladies', 'MANM_HH1_HH_MALAD_81_E2_2_81', 'MANM_HH1_HH_MALAD_81_E2_3_81', 'MANM_HH1_HH_MALAD_81_E2_4_81', 'MANM_HH1_HH_MALAD_81_E3_1_81', 'MANM_HH1_HH_MALAD_81_E3_2_81', 'MANM_HH1_HH_MALAD_81_E3_3_81'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => Yii::t('app', 'ID'),
            'famille' => Yii::t('app', 'Famille'),
            'memberid' => Yii::t('app', 'Memberid'),
            'souffrir_maladie_chronique' => Yii::t('app', 'Maladie (Diabete, etc..)'),
            'avoir_traitement' => Yii::t('app', 'Avoir Traitement'),
            'duree_traitement' => Yii::t('app', 'Duree Traitement'),
            'raison_non_traitement' => Yii::t('app', 'Raison Non Traitement'),
            'avoir_autres_maladies' => Yii::t('app', 'Avoir Autres Maladies'),
            'MANM_HH1_HH_MALAD_81_E2_2_81' => Yii::t('app', 'Avoir Traitement'),
            'MANM_HH1_HH_MALAD_81_E2_3_81' => Yii::t('app', 'Duree Traitement'),
            'MANM_HH1_HH_MALAD_81_E2_4_81' => Yii::t('app', 'Raison Non Traitement'),
            'MANM_HH1_HH_MALAD_81_E3_1_81' => Yii::t('app', 'Etre Handicape'),
            'MANM_HH1_HH_MALAD_81_E3_2_81' => Yii::t('app', 'Handicap'),
            'MANM_HH1_HH_MALAD_81_E3_3_81' => Yii::t('app', 'Niveau Limitation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamille0()
    {
        return $this->hasOne(Famille::className(), ['id' => 'famille']);
    }
}
