<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Plans;

/**
 * ScrPlans represents the model behind the search form about `app\models\Plans`.
 */
class ScrPlans extends Plans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'annee', 'commune', 'ts', 'akf', 'is_sync', 'dimagi'], 'integer'],
            [['mois'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Plans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'mois', $this->mois]);

        return $dataProvider;
    }
    
    public function search12($params, $id)
    {
    	$query = Plans::find()->where(['ts' => $id, 'mode' => 1]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    		'dimagi' => $this->dimagi,
    		'annee' => $this->annee,
    	]);
    	 
    	
    	$query->andFilterWhere(['like', 'mois', $this->mois]);
    
    	return $dataProvider;
    }
    
    public function search15($params, $id, $id1)
    {
    	$query = Plans::find([
    		'ts' => $id,
    	]);
    	
    	$query->andWhere(['akf' => $id1]);
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere(['like', 'mois', $this->mois]);
    
    	return $dataProvider;
    }
    
    public function search17($params, $id)
    {
    	$query = Plans::find()->where(['ts' => $id, 'mode' => 2]);
    
    	$dataProvider = new ActiveDataProvider([
    			'query' => $query,
    			]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere(['like', 'mois', $this->mois]);
    
    	return $dataProvider;
    }
    
    public function search19($params, $id)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$query = Plans::find()->where(['ts' => $id, 'mode' => 1, 'commune' => $operateur->commune_lie]);
        
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere(['like', 'mois', $this->mois]);
    
    	return $dataProvider;
    }
}
