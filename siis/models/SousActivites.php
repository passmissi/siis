<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sous_activites".
 *
 * @property integer $id
 * @property integer $id_activite
 * @property integer $id_type_sous_activite
 * @property integer $id_membre
 * @property integer $id_famille
 * @property integer $id_plan
 */
class SousActivites extends \yii\db\ActiveRecord
{
	public $ty;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sous_activites';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_activite', 'id_type_sous_activite'], 'required'],
            [['id_activite', 'id_type_sous_activite', 'id_membre', 'id_famille', 'id_plan'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_activite' => Yii::t('app', 'Id Activite'),
            'id_type_sous_activite' => Yii::t('app', 'Id Type Sous Activite'),
            'id_membre' => Yii::t('app', 'Id Membre'),
            'id_famille' => Yii::t('app', 'Id Famille'),
            'id_plan' => Yii::t('app', 'Id Plan'),
        ];
    }
    
    public function getTypeSousActivite()
    {
    	return $this->hasOne(TypeSousActivites::className(), ['id' => 'id_type_sous_activite']);
    }
    
    public function getPlan()
    {
    	return $this->hasOne(Plans::className(), ['id' => 'id_plan']);
    }
    
    public function getActivite()
    {
    	return $this->hasOne(Activites::className(), ['id' => 'id_activite']);
    }
    
}
