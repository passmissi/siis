<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "membre".
 *
 * @property string $famille
 * @property integer $memberid
 * @property string $prenom
 * @property string $nom
 * @property string $surnom
 * @property integer $sexe
 * @property integer $age
 * @property integer $annee_naissance
 * @property integer $mois_naissance
 * @property integer $jour_naissance
 * @property integer $type_piece_indendification
 * @property string $numero_piece
 * @property integer $relation_chef_famille
 * @property integer $est_mere
 * @property integer $est_pere
 * @property integer $b10
 * @property integer $b11
 * @property integer $etat_civil
 * @property integer $b13
 * @property integer $moins_18_hors_maison
 * @property integer $b15
 * @property integer $savoir_lire
 * @property integer $savoir_ecrire
 * @property integer $c1_2
 * @property integer $c1_3
 * @property integer $c1_4
 * @property integer $c1_5
 * @property integer $niveau_education
 * @property integer $c3
 * @property integer $c4
 * @property integer $c5
 * @property integer $pouvoir_travailler
 * @property integer $avoir_emploi
 * @property integer $avoir_activite_economique
 * @property integer $type_activite
 * @property integer $d5
 * @property integer $e8
 * @property integer $e9
 * @property integer $e10
 * @property integer $e11
 * @property integer $e12
 * @property integer $e13
 * @property integer $e14
 * @property integer $e15
 * @property integer $e16
 * @property integer $e17
 * @property integer $e18
 * @property integer $e19
 * @property integer $e20
 * @property integer $calc_1
 */
class Membre extends \yii\db\ActiveRecord
{
	public $date;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membre';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'memberid','prenom', 'nom','sexe', 'age', 'annee_naissance', 'mois_naissance', 'jour_naissance','type_piece_indendification', 'relation_chef_famille','etat_civil', 'b13'], 'required'],
            [['famille', 'memberid', 'sexe', 'age', 'annee_naissance', 'mois_naissance', 'jour_naissance', 'type_piece_indendification', 'relation_chef_famille', 'est_mere', 'est_pere', 'b10', 'b11', 'etat_civil', 'b13', 'moins_18_hors_maison', 'b15', 'savoir_lire', 'savoir_ecrire', 'c1_2', 'c1_3', 'c1_4', 'c1_5', 'niveau_education', 'c3', 'c4', 'c5', 'pouvoir_travailler', 'avoir_emploi', 'avoir_activite_economique', 'type_activite', 'd5', 'e8', 'e9', 'e10', 'e11', 'e12', 'e13', 'e14', 'e15', 'e16', 'e17', 'e18', 'e19', 'e20', 'calc_1'], 'integer'],
            [['prenom', 'nom', 'surnom', 'numero_piece'], 'string', 'max' => 20]
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'famille' => Yii::t('app', 'ID Famille'),
            'memberid' => Yii::t('app', 'Id'),
            'prenom' => Yii::t('app', 'Prenom'),
            'nom' => Yii::t('app', 'Nom'),
            'surnom' => Yii::t('app', 'Surnom'),
            'sexe' => Yii::t('app', 'Sexe'),
            'age' => Yii::t('app', 'Age'),
            'annee_naissance' => Yii::t('app', 'Annee Naissance'),
            'mois_naissance' => Yii::t('app', 'Mois Naissance'),
            'jour_naissance' => Yii::t('app', 'Jour Naissance'),
            'type_piece_indendification' => Yii::t('app', 'Type Piece Indendification'),
            'numero_piece' => Yii::t('app', 'Numero Piece'),
            'relation_chef_famille' => Yii::t('app', 'Relation Chef Famille'),
            'est_mere' => Yii::t('app', 'Est Mere'),
            'est_pere' => Yii::t('app', 'Est Pere'),
            'b10' => Yii::t('app', 'B10'),
            'b11' => Yii::t('app', 'B11'),
            'etat_civil' => Yii::t('app', 'Etat Civil'),
            'b13' => Yii::t('app', 'B13'),
            'moins_18_hors_maison' => Yii::t('app', 'Resident'),
            'b15' => Yii::t('app', 'B15'),
            'savoir_lire' => Yii::t('app', 'Savoir Lire'),
            'savoir_ecrire' => Yii::t('app', 'Savoir Ecrire'),
            'c1_2' => Yii::t('app', 'C1 2'),
            'c1_3' => Yii::t('app', 'C1 3'),
            'c1_4' => Yii::t('app', 'C1 4'),
            'c1_5' => Yii::t('app', 'C1 5'),
            'niveau_education' => Yii::t('app', 'Niveau Education'),
            'c3' => Yii::t('app', 'Frequente Ecole'),
            'c4' => Yii::t('app', 'C4'),
            'c5' => Yii::t('app', 'C5'),
            'pouvoir_travailler' => Yii::t('app', 'Pouvoir Travailler'),
            'avoir_emploi' => Yii::t('app', 'Avoir Emploi'),
            'avoir_activite_economique' => Yii::t('app', 'Avoir Activite Economique'),
            'type_activite' => Yii::t('app', 'Type Activite'),
            'd5' => Yii::t('app', 'D5'),
            'e8' => Yii::t('app', 'E8'),
            'e9' => Yii::t('app', 'E9'),
            'e10' => Yii::t('app', 'E10'),
            'e11' => Yii::t('app', 'E11'),
            'e12' => Yii::t('app', 'E12'),
            'e13' => Yii::t('app', 'E13'),
            'e14' => Yii::t('app', 'E14'),
            'e15' => Yii::t('app', 'E15'),
            'e16' => Yii::t('app', 'E16'),
            'e17' => Yii::t('app', 'E17'),
            'e18' => Yii::t('app', 'E18'),
            'e19' => Yii::t('app', 'E19'),
            'e20' => Yii::t('app', 'E20'),
            'calc_1' => Yii::t('app', 'Calc 1'),
        ];
    }
}
