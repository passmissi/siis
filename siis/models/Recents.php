<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recents".
 *
 * @property string $Id
 * @property string $famille
 * @property integer $memberid
 * @property integer $MANM_HH1_HH_MALAD_71_E4_71
 * @property integer $MANM_HH1_HH_MALAD_71_E5_71
 * @property integer $MANM_HH1_HH_MALAD_71_E6_71
 * @property integer $MANM_HH1_HH_MALAD_71_E7_71
 * @property integer $MANM_HH1_HH_MALAD_71_E7B_71
 */
class Recents extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille'], 'required'],
            [['famille', 'memberid', 'MANM_HH1_HH_MALAD_71_E4_71', 'MANM_HH1_HH_MALAD_71_E5_71', 'MANM_HH1_HH_MALAD_71_E6_71', 'MANM_HH1_HH_MALAD_71_E7_71', 'MANM_HH1_HH_MALAD_71_E7B_71'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id' => Yii::t('app', 'ID'),
            'famille' => Yii::t('app', 'Famille'),
            'memberid' => Yii::t('app', 'Memberid'),
            'MANM_HH1_HH_MALAD_71_E4_71' => Yii::t('app', 'Manm  Hh1  Hh  Malad 71  E4 71'),
            'MANM_HH1_HH_MALAD_71_E5_71' => Yii::t('app', 'Manm  Hh1  Hh  Malad 71  E5 71'),
            'MANM_HH1_HH_MALAD_71_E6_71' => Yii::t('app', 'Manm  Hh1  Hh  Malad 71  E6 71'),
            'MANM_HH1_HH_MALAD_71_E7_71' => Yii::t('app', 'Manm  Hh1  Hh  Malad 71  E7 71'),
            'MANM_HH1_HH_MALAD_71_E7B_71' => Yii::t('app', 'Manm  Hh1  Hh  Malad 71  E7 B 71'),
        ];
    }
}
