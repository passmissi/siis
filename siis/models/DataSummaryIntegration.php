<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_summary_integration".
 *
 * @property string $id
 * @property string $type
 * @property string $source
 * @property string $date_creation
 *
 * @property DetailsSummaryIntegration[] $detailsSummaryIntegrations
 */
class DataSummaryIntegration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_summary_integration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'source', 'date_creation'], 'required'],
            [['date_creation'], 'safe'],
            [['type', 'source'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'source' => Yii::t('app', 'Source'),
            'date_creation' => Yii::t('app', 'Date Creation'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailsSummaryIntegrations()
    {
        return $this->hasMany(DetailsSummaryIntegration::className(), ['id_integration' => 'id']);
    }
}
