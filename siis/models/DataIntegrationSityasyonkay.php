<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_integration_sityasyonkay".
 *
 * @property string $id
 * @property string $user_id
 * @property string $details_integration
 * @property string $source
 * @property string $date_creation
 * @property string $case_id_famille
 * @property string $dlo_potab
 * @property string $itlize_latrin
 * @property string $fatra
 * @property string $fatra_output_convert
 * @property string $gerer_dechets_menagers
 * @property string $jete_fatra
 * @property string $jete_fatra_output_convert
 * @property string $ou_jeter_les_ordures
 * @property string $form_id
 * @property string $form_name
 * @property double $location_longitude
 * @property double $location_latitude
 * @property double $location_precision
 * @property double $location_altitude
 * @property integer $isValid
 */
class DataIntegrationSityasyonkay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_integration_sityasyonkay';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'details_integration', 'source', 'date_creation', 'case_id_famille', 'form_id', 'form_name', 'location_longitude', 'location_latitude', 'location_precision', 'location_altitude', 'isValid'], 'required'],
            [['details_integration', 'isValid'], 'integer'],
            [['date_creation'], 'safe'],
            [['location_longitude', 'location_latitude', 'location_precision', 'location_altitude'], 'number'],
            [['user_id'], 'string', 'max' => 75],
            [['source', 'dlo_potab', 'itlize_latrin', 'fatra', 'gerer_dechets_menagers', 'jete_fatra', 'ou_jeter_les_ordures'], 'string', 'max' => 50],
            [['case_id_famille', 'fatra_output_convert', 'jete_fatra_output_convert', 'form_id', 'form_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'details_integration' => Yii::t('app', 'Details Integration'),
            'source' => Yii::t('app', 'Source'),
            'date_creation' => Yii::t('app', 'Date Creation'),
            'case_id_famille' => Yii::t('app', 'Case Id Famille'),
            'dlo_potab' => Yii::t('app', 'Dlo Potab'),
            'itlize_latrin' => Yii::t('app', 'Itlize Latrin'),
            'fatra' => Yii::t('app', 'Fatra'),
            'fatra_output_convert' => Yii::t('app', 'Fatra Output Convert'),
            'gerer_dechets_menagers' => Yii::t('app', 'Gerer Dechets Menagers'),
            'jete_fatra' => Yii::t('app', 'Jete Fatra'),
            'jete_fatra_output_convert' => Yii::t('app', 'Jete Fatra Output Convert'),
            'ou_jeter_les_ordures' => Yii::t('app', 'Ou Jeter Les Ordures'),
            'form_id' => Yii::t('app', 'Form ID'),
            'form_name' => Yii::t('app', 'Form Name'),
            'location_longitude' => Yii::t('app', 'Location Longitude'),
            'location_latitude' => Yii::t('app', 'Location Latitude'),
            'location_precision' => Yii::t('app', 'Location Precision'),
            'location_altitude' => Yii::t('app', 'Location Altitude'),
            'isValid' => Yii::t('app', 'Is Valid'),
        ];
    }
}
