<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "famille".
 *
 * @property string $id
 * @property string $localite
 * @property string $commune
 * @property string $section_communale
 * @property integer $type_milieu
 * @property integer $ID_HH_A5_1
 * @property string $ID_HH_A5_2
 * @property string $ID_HH_A5_3
 * @property integer $telephone_maison
 * @property string $ID_HH_A8
 * @property integer $ID_HH_A8_3
 * @property integer $ID_HH_A8_4
 * @property integer $ID_HH_A8_5
 * @property string $ID_HH_A9
 * @property string $precision_gps
 * @property string $altitude_gps
 * @property string $latitude_gps
 * @property string $longitude_gps
 * @property integer $ID_HH_NO_NON_RESYDAN
 * @property integer $ID_HH_NO_RESYDAN
 * @property integer $ID_HH_MANM_HH_TOTAL
 * @property integer $MANM_HH1_D6_1
 * @property integer $MANM_HH1_MALAD_QUE_CRON
 * @property integer $MANM_HH1_MALAD_NO_CRON
 * @property integer $MANM_HH1_MALAD_QUE_MWA
 * @property integer $MANM_HH1_MALAD_NO_MWA
 * @property integer $MANM_HH1_NO_MOURI
 * @property integer $BIENS_HH_F1
 * @property integer $BIENS_HH_F2
 * @property integer $BIENS_HH_F3_F3_1
 * @property integer $BIENS_HH_F3_F3_2
 * @property integer $BIENS_HH_F3_F3_3
 * @property integer $BIENS_HH_F3_F3_4
 * @property integer $BIENS_HH_F3_F3_5
 * @property integer $BIENS_HH_F3_F3_6
 * @property integer $BIENS_HH_F3_F3_7
 * @property integer $BIENS_HH_F3_F3_8
 * @property integer $BIENS_HH_F3_F3_9
 * @property integer $BIENS_HH_F3_F3_10
 * @property integer $BIENS_HH_F3_F3_11
 * @property integer $BIENS_HH_F3_F3_12
 * @property integer $BIENS_HH_F3_F3_13
 * @property integer $BIENS_HH_F3_F3_14
 * @property integer $BIENS_HH_F3_F3_15
 * @property integer $BIENS_HH_F3_F3_16
 * @property integer $BIENS_HH_F3_F3_17
 * @property integer $BIENS_HH_F3_F3_18
 * @property integer $BIENS_HH_F4
 * @property integer $BIENS_HH_F5_F5_1
 * @property integer $BIENS_HH_F5_F5_2
 * @property integer $BIENS_HH_F5_F5_3
 * @property integer $BIENS_HH_F5_F5_4
 * @property integer $BIENS_HH_F5_F5_5
 * @property integer $BIENS_HH_F5_F5_6
 * @property integer $BIENS_HH_F5_F5_7
 * @property integer $BIENS_HH_F6
 * @property integer $BIENS_HH_F7
 * @property integer $BIENS_HH_F8_1
 * @property integer $BIENS_HH_F8_2
 * @property integer $BIENS_HH_F9
 * @property integer $BIENS_HH_F9_1
 * @property integer $BIENS_HH_F10
 * @property integer $BIENS_HH_F11
 * @property integer $BIENS_HH_F12
 * @property integer $BIENS_HH_F12_0
 * @property integer $BIENS_HH_F13
 * @property integer $BIENS_HH_F14
 * @property integer $BIENS_HH_F15
 * @property integer $BIENS_HH_F15_0
 * @property integer $BIENS_HH_F16
 * @property integer $BIENS_HH_F17
 * @property integer $BIENS_HH_F18
 * @property integer $BIENS_HH_F19
 * @property integer $BIENS_HH_F20
 * @property integer $BIENS_HH_G1
 * @property integer $BIENS_HH_G2
 * @property integer $SEKALIMHHSEKALIMHH1_H1
 * @property integer $SEKALIMHHSEKALIMHH1_H2
 * @property integer $SEKALIMHHSEKALIMHH1_H3
 * @property integer $SEKALIMHHSEKALIMHH1_H4
 * @property integer $SEKALIMHHSEKALIMHH1_H5
 * @property integer $SEKALIMHHSEKALIMHH1_H6
 * @property integer $SEKALIMHHSEKALIMHH1_H7
 * @property integer $SEKALIMHHSEKALIMHH1_H8
 * @property integer $SEKALIMHHSEKALIMHH1_H9
 * @property integer $SEKALIMHHSEKALIMHH1_H10
 * @property integer $SEKALIMHHSEKALIMHH1_H11
 * @property integer $SEKALIMHHSEKALIMHH1_H12
 * @property integer $SEKALIMHHSEKALIMHH1_H13
 * @property integer $SEKALIMHHSEKALIMHH1_H14
 * @property integer $SEKALIMHHSEKALIMHH1_H15
 * @property integer $SEKALIMHHSEKALIMHH1_H16
 * @property integer $SEKALIMHHSEKALIMHH1_H17
 * @property integer $SEKALIMHHSEKALIMHH1_H18
 * @property integer $SEKALIMHHSEKALIMHH1_H19
 * @property integer $SEKALIMHHSEKALIMHH1_H20
 * @property integer $SEKALIMHHSEKALIMHH1_H21
 * @property integer $SEKALIMHHSEKALIMHH1_H22
 * @property integer $SEKALIMHHSEKALIMHH1_H23
 * @property integer $SEKALIMHHSEKALIMHH2_H1_B
 * @property integer $SEKALIMHHSEKALIMHH2_H2_B
 * @property integer $SEKALIMHHSEKALIMHH2_H3_B
 * @property integer $SEKALIMHHSEKALIMHH2_H4_B
 * @property integer $SEKALIMHHSEKALIMHH2_H5_B
 * @property integer $SEKALIMHHSEKALIMHH2_H6_B
 * @property integer $SEKALIMHHSEKALIMHH2_H7_B
 * @property integer $SEKALIMHHSEKALIMHH2_H8_B
 * @property integer $SEKALIMHHSEKALIMHH2_H9_B
 * @property integer $SEKALIMHHSEKALIMHH2_H10_B
 * @property integer $SEKALIMHHSEKALIMHH2_H11_B
 * @property integer $SEKALIMHHSEKALIMHH2_H12_B
 * @property integer $SEKALIMHHSEKALIMHH2_H13_B
 * @property integer $SEKALIMHHSEKALIMHH2_H14_B
 * @property integer $SEKALIMHHSEKALIMHH2_H15_B
 * @property integer $SEKALIMHHSEKALIMHH2_H16_B
 * @property integer $SEKALIMHHSEKALIMHH2_H17_B
 * @property integer $SEKALIMHHSEKALIMHH2_H18_B
 * @property integer $SEKALIMHHSEKALIMHH2_H19_B
 * @property integer $SEKALIMHHSEKALIMHH2_H20_B
 * @property integer $SEKALIMHHSEKALIMHH2_H21_B
 * @property integer $SEKALIMHHSEKALIMHH2_H22_B
 * @property integer $SEKALIMHHSEKALIMHH2_H23_B
 * @property integer $SEKALIMHHSEKALIMHH3_H24
 * @property integer $SEKALIMHHSEKALIMHH3_H25
 * @property integer $SEKALIMHHSEKALIMHH3_H26
 * @property integer $H24
 * @property integer $H25
 * @property integer $H26
 * @property integer $SEKALIMHHSEKALIMHH3_H27
 * @property integer $SEKALIMHHSEKALIMHH3_H28
 * @property integer $SEKALIMHHSEKALIMHH3_H29
 * @property integer $SEKALIMHHSEKALIMHH3_H30
 * @property integer $SEKALIMHHSEKALIMHH3_H31
 * @property integer $SEKALIMHHSEKALIMHH3_H32
 * @property integer $SEKALIMHHSEKALIMHH3_H33
 * @property integer $SEKALIMHHSEKALIMHH3_H34
 * @property integer $SEKALIMHHSEKALIMHH3_H35
 * @property integer $SEKALIMHHSEKALIMHH3_H36
 * @property integer $SEKALIMHHSEKALIMHH3_H37
 * @property integer $SEKALIMHHSEKALIMHH3_H38
 * @property integer $SEKALIMHHSEKALIMHH3_H39
 * @property integer $SEKALIMHHSEKALIMHH3_H40
 * @property integer $SEKALIMHHSEKALIMHH3_H41
 * @property integer $SEKALIMHHSEKALIMHH3_H42
 * @property integer $SEKALIMHHSEKALIMHH3_H43
 * @property integer $SEKALIMHHSEKALIMHH3_H44
 * @property integer $SEKALIMHHSEKALIMHH3_H45
 * @property integer $SEKALIMHHSEKALIMHH3_H46
 * @property integer $SEKALIMHHSEKALIMHH4_H47
 * @property integer $SEKALIMHHSEKALIMHH4_H48
 * @property integer $SEKALIMHHSEKALIMHH4_H49
 * @property integer $SEKALIMHHSEKALIMHH4_H50
 * @property integer $SEKALIMHHSEKALIMHH4_H51
 * @property integer $SEKALIMHHSEKALIMHH4_H52
 * @property integer $SEKALIMHHSEKALIMHH4_H53
 * @property integer $SEKALIMHHSEKALIMHH4_H54
 * @property integer $SEKALIMHHSEKALIMHH4_H55
 * @property integer $SEKALIMHHSEKALIMHH4_H56
 * @property integer $SEKALIMHHSEKALIMHH4_H57
 * @property integer $SEKALIMHHSEKALIMHH4_H58
 * @property integer $SEKALIMHHSEKALIMHH4_H59
 * @property integer $SEKALIMHHSEKALIMHH4_H60
 * @property integer $SEKALIMHHSEKALIMHH4_H61
 * @property integer $SEKALIMHHSEKALIMHH4_H62
 * @property integer $SEKALIMHHSEKALIMHH4_H63
 * @property integer $SEKALIMHHSEKALIMHH4_H64
 * @property integer $SEKALIMHHSEKALIMHH4_H65
 * @property integer $SEKALIMHHSEKALIMHH4_H66
 * @property integer $SEKALIMHHSEKALIMHH4_H67
 * @property integer $SEKALIMHHSEKALIMHH4_H68
 * @property integer $SEKALIMHHSEKALIMHH4_H69
 * @property integer $SEVIS_KONPOTMAN_HH_I1
 * @property integer $SEVIS_KONPOTMAN_HH_I2
 * @property integer $SEVIS_KONPOTMAN_HH_I3
 * @property integer $SEVIS_KONPOTMAN_HH_I4
 * @property integer $SEVIS_KONPOTMAN_HH_I5_1
 * @property integer $SEVIS_KONPOTMAN_HH_I5_2
 * @property integer $SEVIS_KONPOTMAN_HH_I6
 * @property integer $SEVIS_KONPOTMAN_HH_I7
 * @property integer $SEVIS_KONPOTMAN_HH_I8
 *
 * @property Activites[] $activites
 * @property Chroniques[] $chroniques
 * @property FamilyAkf[] $familyAkfs
 * @property Operateurs[] $operateurAkfs
 * @property SousActivites[] $sousActivites
 */
class Famille extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'famille';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['localite'], 'required'],
            [['type_milieu', 'ID_HH_A5_1', 'telephone_maison', 'ID_HH_A8_3', 'ID_HH_A8_4', 'ID_HH_A8_5', 'ID_HH_NO_NON_RESYDAN', 'ID_HH_NO_RESYDAN', 'ID_HH_MANM_HH_TOTAL', 'MANM_HH1_D6_1', 'MANM_HH1_MALAD_QUE_CRON', 'MANM_HH1_MALAD_NO_CRON', 'MANM_HH1_MALAD_QUE_MWA', 'MANM_HH1_MALAD_NO_MWA', 'MANM_HH1_NO_MOURI', 'BIENS_HH_F1', 'BIENS_HH_F2', 'BIENS_HH_F3_F3_1', 'BIENS_HH_F3_F3_2', 'BIENS_HH_F3_F3_3', 'BIENS_HH_F3_F3_4', 'BIENS_HH_F3_F3_5', 'BIENS_HH_F3_F3_6', 'BIENS_HH_F3_F3_7', 'BIENS_HH_F3_F3_8', 'BIENS_HH_F3_F3_9', 'BIENS_HH_F3_F3_10', 'BIENS_HH_F3_F3_11', 'BIENS_HH_F3_F3_12', 'BIENS_HH_F3_F3_13', 'BIENS_HH_F3_F3_14', 'BIENS_HH_F3_F3_15', 'BIENS_HH_F3_F3_16', 'BIENS_HH_F3_F3_17', 'BIENS_HH_F3_F3_18', 'BIENS_HH_F4', 'BIENS_HH_F5_F5_1', 'BIENS_HH_F5_F5_2', 'BIENS_HH_F5_F5_3', 'BIENS_HH_F5_F5_4', 'BIENS_HH_F5_F5_5', 'BIENS_HH_F5_F5_6', 'BIENS_HH_F5_F5_7', 'BIENS_HH_F6', 'BIENS_HH_F7', 'BIENS_HH_F8_1', 'BIENS_HH_F8_2', 'BIENS_HH_F9', 'BIENS_HH_F9_1', 'BIENS_HH_F10', 'BIENS_HH_F11', 'BIENS_HH_F12', 'BIENS_HH_F12_0', 'BIENS_HH_F13', 'BIENS_HH_F14', 'BIENS_HH_F15', 'BIENS_HH_F15_0', 'BIENS_HH_F16', 'BIENS_HH_F17', 'BIENS_HH_F18', 'BIENS_HH_F19', 'BIENS_HH_F20', 'BIENS_HH_G1', 'BIENS_HH_G2', 'SEKALIMHHSEKALIMHH1_H1', 'SEKALIMHHSEKALIMHH1_H2', 'SEKALIMHHSEKALIMHH1_H3', 'SEKALIMHHSEKALIMHH1_H4', 'SEKALIMHHSEKALIMHH1_H5', 'SEKALIMHHSEKALIMHH1_H6', 'SEKALIMHHSEKALIMHH1_H7', 'SEKALIMHHSEKALIMHH1_H8', 'SEKALIMHHSEKALIMHH1_H9', 'SEKALIMHHSEKALIMHH1_H10', 'SEKALIMHHSEKALIMHH1_H11', 'SEKALIMHHSEKALIMHH1_H12', 'SEKALIMHHSEKALIMHH1_H13', 'SEKALIMHHSEKALIMHH1_H14', 'SEKALIMHHSEKALIMHH1_H15', 'SEKALIMHHSEKALIMHH1_H16', 'SEKALIMHHSEKALIMHH1_H17', 'SEKALIMHHSEKALIMHH1_H18', 'SEKALIMHHSEKALIMHH1_H19', 'SEKALIMHHSEKALIMHH1_H20', 'SEKALIMHHSEKALIMHH1_H21', 'SEKALIMHHSEKALIMHH1_H22', 'SEKALIMHHSEKALIMHH1_H23', 'SEKALIMHHSEKALIMHH2_H1_B', 'SEKALIMHHSEKALIMHH2_H2_B', 'SEKALIMHHSEKALIMHH2_H3_B', 'SEKALIMHHSEKALIMHH2_H4_B', 'SEKALIMHHSEKALIMHH2_H5_B', 'SEKALIMHHSEKALIMHH2_H6_B', 'SEKALIMHHSEKALIMHH2_H7_B', 'SEKALIMHHSEKALIMHH2_H8_B', 'SEKALIMHHSEKALIMHH2_H9_B', 'SEKALIMHHSEKALIMHH2_H10_B', 'SEKALIMHHSEKALIMHH2_H11_B', 'SEKALIMHHSEKALIMHH2_H12_B', 'SEKALIMHHSEKALIMHH2_H13_B', 'SEKALIMHHSEKALIMHH2_H14_B', 'SEKALIMHHSEKALIMHH2_H15_B', 'SEKALIMHHSEKALIMHH2_H16_B', 'SEKALIMHHSEKALIMHH2_H17_B', 'SEKALIMHHSEKALIMHH2_H18_B', 'SEKALIMHHSEKALIMHH2_H19_B', 'SEKALIMHHSEKALIMHH2_H20_B', 'SEKALIMHHSEKALIMHH2_H21_B', 'SEKALIMHHSEKALIMHH2_H22_B', 'SEKALIMHHSEKALIMHH2_H23_B', 'SEKALIMHHSEKALIMHH3_H24', 'SEKALIMHHSEKALIMHH3_H25', 'SEKALIMHHSEKALIMHH3_H26', 'H24', 'H25', 'H26', 'SEKALIMHHSEKALIMHH3_H27', 'SEKALIMHHSEKALIMHH3_H28', 'SEKALIMHHSEKALIMHH3_H29', 'SEKALIMHHSEKALIMHH3_H30', 'SEKALIMHHSEKALIMHH3_H31', 'SEKALIMHHSEKALIMHH3_H32', 'SEKALIMHHSEKALIMHH3_H33', 'SEKALIMHHSEKALIMHH3_H34', 'SEKALIMHHSEKALIMHH3_H35', 'SEKALIMHHSEKALIMHH3_H36', 'SEKALIMHHSEKALIMHH3_H37', 'SEKALIMHHSEKALIMHH3_H38', 'SEKALIMHHSEKALIMHH3_H39', 'SEKALIMHHSEKALIMHH3_H40', 'SEKALIMHHSEKALIMHH3_H41', 'SEKALIMHHSEKALIMHH3_H42', 'SEKALIMHHSEKALIMHH3_H43', 'SEKALIMHHSEKALIMHH3_H44', 'SEKALIMHHSEKALIMHH3_H45', 'SEKALIMHHSEKALIMHH3_H46', 'SEKALIMHHSEKALIMHH4_H47', 'SEKALIMHHSEKALIMHH4_H48', 'SEKALIMHHSEKALIMHH4_H49', 'SEKALIMHHSEKALIMHH4_H50', 'SEKALIMHHSEKALIMHH4_H51', 'SEKALIMHHSEKALIMHH4_H52', 'SEKALIMHHSEKALIMHH4_H53', 'SEKALIMHHSEKALIMHH4_H54', 'SEKALIMHHSEKALIMHH4_H55', 'SEKALIMHHSEKALIMHH4_H56', 'SEKALIMHHSEKALIMHH4_H57', 'SEKALIMHHSEKALIMHH4_H58', 'SEKALIMHHSEKALIMHH4_H59', 'SEKALIMHHSEKALIMHH4_H60', 'SEKALIMHHSEKALIMHH4_H61', 'SEKALIMHHSEKALIMHH4_H62', 'SEKALIMHHSEKALIMHH4_H63', 'SEKALIMHHSEKALIMHH4_H64', 'SEKALIMHHSEKALIMHH4_H65', 'SEKALIMHHSEKALIMHH4_H66', 'SEKALIMHHSEKALIMHH4_H67', 'SEKALIMHHSEKALIMHH4_H68', 'SEKALIMHHSEKALIMHH4_H69', 'SEVIS_KONPOTMAN_HH_I1', 'SEVIS_KONPOTMAN_HH_I2', 'SEVIS_KONPOTMAN_HH_I3', 'SEVIS_KONPOTMAN_HH_I4', 'SEVIS_KONPOTMAN_HH_I5_1', 'SEVIS_KONPOTMAN_HH_I5_2', 'SEVIS_KONPOTMAN_HH_I6', 'SEVIS_KONPOTMAN_HH_I7', 'SEVIS_KONPOTMAN_HH_I8'], 'integer'],
            [['localite', 'commune', 'section_communale', 'ID_HH_A5_2', 'ID_HH_A5_3', 'ID_HH_A8', 'ID_HH_A9', 'precision_gps', 'altitude_gps', 'latitude_gps', 'longitude_gps'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'localite' => Yii::t('app', 'Localite'),
            'commune' => Yii::t('app', 'Commune'),
            'section_communale' => Yii::t('app', 'Section Communale'),
            'type_milieu' => Yii::t('app', 'Type Milieu'),
            'ID_HH_A5_1' => Yii::t('app', 'Id  Hh  A5 1'),
            'ID_HH_A5_2' => Yii::t('app', 'Id  Hh  A5 2'),
            'ID_HH_A5_3' => Yii::t('app', 'Id  Hh  A5 3'),
            'telephone_maison' => Yii::t('app', 'Telephone Maison'),
            'ID_HH_A8' => Yii::t('app', 'Id  Hh  A8'),
            'ID_HH_A8_3' => Yii::t('app', 'Id  Hh  A8 3'),
            'ID_HH_A8_4' => Yii::t('app', 'Id  Hh  A8 4'),
            'ID_HH_A8_5' => Yii::t('app', 'Id  Hh  A8 5'),
            'ID_HH_A9' => Yii::t('app', 'Id  Hh  A9'),
            'precision_gps' => Yii::t('app', 'Precision Gps'),
            'altitude_gps' => Yii::t('app', 'Altitude Gps'),
            'latitude_gps' => Yii::t('app', 'Latitude Gps'),
            'longitude_gps' => Yii::t('app', 'Longitude Gps'),
            'ID_HH_NO_NON_RESYDAN' => Yii::t('app', 'Id  Hh  No  Non  Resydan'),
            'ID_HH_NO_RESYDAN' => Yii::t('app', 'Id  Hh  No  Resydan'),
            'ID_HH_MANM_HH_TOTAL' => Yii::t('app', 'Id  Hh  Manm  Hh  Total'),
            'MANM_HH1_D6_1' => Yii::t('app', 'Manm  Hh1  D6 1'),
            'MANM_HH1_MALAD_QUE_CRON' => Yii::t('app', 'Manm  Hh1  Malad  Que  Cron'),
            'MANM_HH1_MALAD_NO_CRON' => Yii::t('app', 'Manm  Hh1  Malad  No  Cron'),
            'MANM_HH1_MALAD_QUE_MWA' => Yii::t('app', 'Manm  Hh1  Malad  Que  Mwa'),
            'MANM_HH1_MALAD_NO_MWA' => Yii::t('app', 'Manm  Hh1  Malad  No  Mwa'),
            'MANM_HH1_NO_MOURI' => Yii::t('app', 'Manm  Hh1  No  Mouri'),
            'BIENS_HH_F1' => Yii::t('app', 'Biens  Hh  F1'),
            'BIENS_HH_F2' => Yii::t('app', 'Biens  Hh  F2'),
            'BIENS_HH_F3_F3_1' => Yii::t('app', 'Biens  Hh  F3  F3 1'),
            'BIENS_HH_F3_F3_2' => Yii::t('app', 'Biens  Hh  F3  F3 2'),
            'BIENS_HH_F3_F3_3' => Yii::t('app', 'Biens  Hh  F3  F3 3'),
            'BIENS_HH_F3_F3_4' => Yii::t('app', 'Biens  Hh  F3  F3 4'),
            'BIENS_HH_F3_F3_5' => Yii::t('app', 'Biens  Hh  F3  F3 5'),
            'BIENS_HH_F3_F3_6' => Yii::t('app', 'Biens  Hh  F3  F3 6'),
            'BIENS_HH_F3_F3_7' => Yii::t('app', 'Biens  Hh  F3  F3 7'),
            'BIENS_HH_F3_F3_8' => Yii::t('app', 'Biens  Hh  F3  F3 8'),
            'BIENS_HH_F3_F3_9' => Yii::t('app', 'Biens  Hh  F3  F3 9'),
            'BIENS_HH_F3_F3_10' => Yii::t('app', 'Biens  Hh  F3  F3 10'),
            'BIENS_HH_F3_F3_11' => Yii::t('app', 'Biens  Hh  F3  F3 11'),
            'BIENS_HH_F3_F3_12' => Yii::t('app', 'Biens  Hh  F3  F3 12'),
            'BIENS_HH_F3_F3_13' => Yii::t('app', 'Biens  Hh  F3  F3 13'),
            'BIENS_HH_F3_F3_14' => Yii::t('app', 'Biens  Hh  F3  F3 14'),
            'BIENS_HH_F3_F3_15' => Yii::t('app', 'Biens  Hh  F3  F3 15'),
            'BIENS_HH_F3_F3_16' => Yii::t('app', 'Biens  Hh  F3  F3 16'),
            'BIENS_HH_F3_F3_17' => Yii::t('app', 'Biens  Hh  F3  F3 17'),
            'BIENS_HH_F3_F3_18' => Yii::t('app', 'Biens  Hh  F3  F3 18'),
            'BIENS_HH_F4' => Yii::t('app', 'Biens  Hh  F4'),
            'BIENS_HH_F5_F5_1' => Yii::t('app', 'Biens  Hh  F5  F5 1'),
            'BIENS_HH_F5_F5_2' => Yii::t('app', 'Biens  Hh  F5  F5 2'),
            'BIENS_HH_F5_F5_3' => Yii::t('app', 'Biens  Hh  F5  F5 3'),
            'BIENS_HH_F5_F5_4' => Yii::t('app', 'Biens  Hh  F5  F5 4'),
            'BIENS_HH_F5_F5_5' => Yii::t('app', 'Biens  Hh  F5  F5 5'),
            'BIENS_HH_F5_F5_6' => Yii::t('app', 'Biens  Hh  F5  F5 6'),
            'BIENS_HH_F5_F5_7' => Yii::t('app', 'Biens  Hh  F5  F5 7'),
            'BIENS_HH_F6' => Yii::t('app', 'Biens  Hh  F6'),
            'BIENS_HH_F7' => Yii::t('app', 'Biens  Hh  F7'),
            'BIENS_HH_F8_1' => Yii::t('app', 'Biens  Hh  F8 1'),
            'BIENS_HH_F8_2' => Yii::t('app', 'Biens  Hh  F8 2'),
            'BIENS_HH_F9' => Yii::t('app', 'Biens  Hh  F9'),
            'BIENS_HH_F9_1' => Yii::t('app', 'Biens  Hh  F9 1'),
            'BIENS_HH_F10' => Yii::t('app', 'Biens  Hh  F10'),
            'BIENS_HH_F11' => Yii::t('app', 'Biens  Hh  F11'),
            'BIENS_HH_F12' => Yii::t('app', 'Biens  Hh  F12'),
            'BIENS_HH_F12_0' => Yii::t('app', 'Biens  Hh  F12 0'),
            'BIENS_HH_F13' => Yii::t('app', 'Biens  Hh  F13'),
            'BIENS_HH_F14' => Yii::t('app', 'Biens  Hh  F14'),
            'BIENS_HH_F15' => Yii::t('app', 'Biens  Hh  F15'),
            'BIENS_HH_F15_0' => Yii::t('app', 'Biens  Hh  F15 0'),
            'BIENS_HH_F16' => Yii::t('app', 'Biens  Hh  F16'),
            'BIENS_HH_F17' => Yii::t('app', 'Biens  Hh  F17'),
            'BIENS_HH_F18' => Yii::t('app', 'Biens  Hh  F18'),
            'BIENS_HH_F19' => Yii::t('app', 'Biens  Hh  F19'),
            'BIENS_HH_F20' => Yii::t('app', 'Biens  Hh  F20'),
            'BIENS_HH_G1' => Yii::t('app', 'Biens  Hh  G1'),
            'BIENS_HH_G2' => Yii::t('app', 'Biens  Hh  G2'),
            'SEKALIMHHSEKALIMHH1_H1' => Yii::t('app', 'Sekalimhhsekalimhh1  H1'),
            'SEKALIMHHSEKALIMHH1_H2' => Yii::t('app', 'Sekalimhhsekalimhh1  H2'),
            'SEKALIMHHSEKALIMHH1_H3' => Yii::t('app', 'Sekalimhhsekalimhh1  H3'),
            'SEKALIMHHSEKALIMHH1_H4' => Yii::t('app', 'Sekalimhhsekalimhh1  H4'),
            'SEKALIMHHSEKALIMHH1_H5' => Yii::t('app', 'Sekalimhhsekalimhh1  H5'),
            'SEKALIMHHSEKALIMHH1_H6' => Yii::t('app', 'Sekalimhhsekalimhh1  H6'),
            'SEKALIMHHSEKALIMHH1_H7' => Yii::t('app', 'Sekalimhhsekalimhh1  H7'),
            'SEKALIMHHSEKALIMHH1_H8' => Yii::t('app', 'Sekalimhhsekalimhh1  H8'),
            'SEKALIMHHSEKALIMHH1_H9' => Yii::t('app', 'Sekalimhhsekalimhh1  H9'),
            'SEKALIMHHSEKALIMHH1_H10' => Yii::t('app', 'Sekalimhhsekalimhh1  H10'),
            'SEKALIMHHSEKALIMHH1_H11' => Yii::t('app', 'Sekalimhhsekalimhh1  H11'),
            'SEKALIMHHSEKALIMHH1_H12' => Yii::t('app', 'Sekalimhhsekalimhh1  H12'),
            'SEKALIMHHSEKALIMHH1_H13' => Yii::t('app', 'Sekalimhhsekalimhh1  H13'),
            'SEKALIMHHSEKALIMHH1_H14' => Yii::t('app', 'Sekalimhhsekalimhh1  H14'),
            'SEKALIMHHSEKALIMHH1_H15' => Yii::t('app', 'Sekalimhhsekalimhh1  H15'),
            'SEKALIMHHSEKALIMHH1_H16' => Yii::t('app', 'Sekalimhhsekalimhh1  H16'),
            'SEKALIMHHSEKALIMHH1_H17' => Yii::t('app', 'Sekalimhhsekalimhh1  H17'),
            'SEKALIMHHSEKALIMHH1_H18' => Yii::t('app', 'Sekalimhhsekalimhh1  H18'),
            'SEKALIMHHSEKALIMHH1_H19' => Yii::t('app', 'Sekalimhhsekalimhh1  H19'),
            'SEKALIMHHSEKALIMHH1_H20' => Yii::t('app', 'Sekalimhhsekalimhh1  H20'),
            'SEKALIMHHSEKALIMHH1_H21' => Yii::t('app', 'Sekalimhhsekalimhh1  H21'),
            'SEKALIMHHSEKALIMHH1_H22' => Yii::t('app', 'Sekalimhhsekalimhh1  H22'),
            'SEKALIMHHSEKALIMHH1_H23' => Yii::t('app', 'Sekalimhhsekalimhh1  H23'),
            'SEKALIMHHSEKALIMHH2_H1_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H1  B'),
            'SEKALIMHHSEKALIMHH2_H2_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H2  B'),
            'SEKALIMHHSEKALIMHH2_H3_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H3  B'),
            'SEKALIMHHSEKALIMHH2_H4_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H4  B'),
            'SEKALIMHHSEKALIMHH2_H5_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H5  B'),
            'SEKALIMHHSEKALIMHH2_H6_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H6  B'),
            'SEKALIMHHSEKALIMHH2_H7_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H7  B'),
            'SEKALIMHHSEKALIMHH2_H8_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H8  B'),
            'SEKALIMHHSEKALIMHH2_H9_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H9  B'),
            'SEKALIMHHSEKALIMHH2_H10_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H10  B'),
            'SEKALIMHHSEKALIMHH2_H11_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H11  B'),
            'SEKALIMHHSEKALIMHH2_H12_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H12  B'),
            'SEKALIMHHSEKALIMHH2_H13_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H13  B'),
            'SEKALIMHHSEKALIMHH2_H14_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H14  B'),
            'SEKALIMHHSEKALIMHH2_H15_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H15  B'),
            'SEKALIMHHSEKALIMHH2_H16_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H16  B'),
            'SEKALIMHHSEKALIMHH2_H17_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H17  B'),
            'SEKALIMHHSEKALIMHH2_H18_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H18  B'),
            'SEKALIMHHSEKALIMHH2_H19_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H19  B'),
            'SEKALIMHHSEKALIMHH2_H20_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H20  B'),
            'SEKALIMHHSEKALIMHH2_H21_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H21  B'),
            'SEKALIMHHSEKALIMHH2_H22_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H22  B'),
            'SEKALIMHHSEKALIMHH2_H23_B' => Yii::t('app', 'Sekalimhhsekalimhh2  H23  B'),
            'SEKALIMHHSEKALIMHH3_H24' => Yii::t('app', 'Sekalimhhsekalimhh3  H24'),
            'SEKALIMHHSEKALIMHH3_H25' => Yii::t('app', 'Sekalimhhsekalimhh3  H25'),
            'SEKALIMHHSEKALIMHH3_H26' => Yii::t('app', 'Sekalimhhsekalimhh3  H26'),
            'H24' => Yii::t('app', 'H24'),
            'H25' => Yii::t('app', 'H25'),
            'H26' => Yii::t('app', 'H26'),
            'SEKALIMHHSEKALIMHH3_H27' => Yii::t('app', 'Sekalimhhsekalimhh3  H27'),
            'SEKALIMHHSEKALIMHH3_H28' => Yii::t('app', 'Sekalimhhsekalimhh3  H28'),
            'SEKALIMHHSEKALIMHH3_H29' => Yii::t('app', 'Sekalimhhsekalimhh3  H29'),
            'SEKALIMHHSEKALIMHH3_H30' => Yii::t('app', 'Sekalimhhsekalimhh3  H30'),
            'SEKALIMHHSEKALIMHH3_H31' => Yii::t('app', 'Sekalimhhsekalimhh3  H31'),
            'SEKALIMHHSEKALIMHH3_H32' => Yii::t('app', 'Sekalimhhsekalimhh3  H32'),
            'SEKALIMHHSEKALIMHH3_H33' => Yii::t('app', 'Sekalimhhsekalimhh3  H33'),
            'SEKALIMHHSEKALIMHH3_H34' => Yii::t('app', 'Sekalimhhsekalimhh3  H34'),
            'SEKALIMHHSEKALIMHH3_H35' => Yii::t('app', 'Sekalimhhsekalimhh3  H35'),
            'SEKALIMHHSEKALIMHH3_H36' => Yii::t('app', 'Sekalimhhsekalimhh3  H36'),
            'SEKALIMHHSEKALIMHH3_H37' => Yii::t('app', 'Sekalimhhsekalimhh3  H37'),
            'SEKALIMHHSEKALIMHH3_H38' => Yii::t('app', 'Sekalimhhsekalimhh3  H38'),
            'SEKALIMHHSEKALIMHH3_H39' => Yii::t('app', 'Sekalimhhsekalimhh3  H39'),
            'SEKALIMHHSEKALIMHH3_H40' => Yii::t('app', 'Sekalimhhsekalimhh3  H40'),
            'SEKALIMHHSEKALIMHH3_H41' => Yii::t('app', 'Sekalimhhsekalimhh3  H41'),
            'SEKALIMHHSEKALIMHH3_H42' => Yii::t('app', 'Sekalimhhsekalimhh3  H42'),
            'SEKALIMHHSEKALIMHH3_H43' => Yii::t('app', 'Sekalimhhsekalimhh3  H43'),
            'SEKALIMHHSEKALIMHH3_H44' => Yii::t('app', 'Sekalimhhsekalimhh3  H44'),
            'SEKALIMHHSEKALIMHH3_H45' => Yii::t('app', 'Sekalimhhsekalimhh3  H45'),
            'SEKALIMHHSEKALIMHH3_H46' => Yii::t('app', 'Sekalimhhsekalimhh3  H46'),
            'SEKALIMHHSEKALIMHH4_H47' => Yii::t('app', 'Sekalimhhsekalimhh4  H47'),
            'SEKALIMHHSEKALIMHH4_H48' => Yii::t('app', 'Sekalimhhsekalimhh4  H48'),
            'SEKALIMHHSEKALIMHH4_H49' => Yii::t('app', 'Sekalimhhsekalimhh4  H49'),
            'SEKALIMHHSEKALIMHH4_H50' => Yii::t('app', 'Sekalimhhsekalimhh4  H50'),
            'SEKALIMHHSEKALIMHH4_H51' => Yii::t('app', 'Sekalimhhsekalimhh4  H51'),
            'SEKALIMHHSEKALIMHH4_H52' => Yii::t('app', 'Sekalimhhsekalimhh4  H52'),
            'SEKALIMHHSEKALIMHH4_H53' => Yii::t('app', 'Sekalimhhsekalimhh4  H53'),
            'SEKALIMHHSEKALIMHH4_H54' => Yii::t('app', 'Sekalimhhsekalimhh4  H54'),
            'SEKALIMHHSEKALIMHH4_H55' => Yii::t('app', 'Sekalimhhsekalimhh4  H55'),
            'SEKALIMHHSEKALIMHH4_H56' => Yii::t('app', 'Sekalimhhsekalimhh4  H56'),
            'SEKALIMHHSEKALIMHH4_H57' => Yii::t('app', 'Sekalimhhsekalimhh4  H57'),
            'SEKALIMHHSEKALIMHH4_H58' => Yii::t('app', 'Sekalimhhsekalimhh4  H58'),
            'SEKALIMHHSEKALIMHH4_H59' => Yii::t('app', 'Sekalimhhsekalimhh4  H59'),
            'SEKALIMHHSEKALIMHH4_H60' => Yii::t('app', 'Sekalimhhsekalimhh4  H60'),
            'SEKALIMHHSEKALIMHH4_H61' => Yii::t('app', 'Sekalimhhsekalimhh4  H61'),
            'SEKALIMHHSEKALIMHH4_H62' => Yii::t('app', 'Sekalimhhsekalimhh4  H62'),
            'SEKALIMHHSEKALIMHH4_H63' => Yii::t('app', 'Sekalimhhsekalimhh4  H63'),
            'SEKALIMHHSEKALIMHH4_H64' => Yii::t('app', 'Sekalimhhsekalimhh4  H64'),
            'SEKALIMHHSEKALIMHH4_H65' => Yii::t('app', 'Sekalimhhsekalimhh4  H65'),
            'SEKALIMHHSEKALIMHH4_H66' => Yii::t('app', 'Sekalimhhsekalimhh4  H66'),
            'SEKALIMHHSEKALIMHH4_H67' => Yii::t('app', 'Sekalimhhsekalimhh4  H67'),
            'SEKALIMHHSEKALIMHH4_H68' => Yii::t('app', 'Sekalimhhsekalimhh4  H68'),
            'SEKALIMHHSEKALIMHH4_H69' => Yii::t('app', 'Sekalimhhsekalimhh4  H69'),
            'SEVIS_KONPOTMAN_HH_I1' => Yii::t('app', 'Sevis  Konpotman  Hh  I1'),
            'SEVIS_KONPOTMAN_HH_I2' => Yii::t('app', 'Sevis  Konpotman  Hh  I2'),
            'SEVIS_KONPOTMAN_HH_I3' => Yii::t('app', 'Sevis  Konpotman  Hh  I3'),
            'SEVIS_KONPOTMAN_HH_I4' => Yii::t('app', 'Sevis  Konpotman  Hh  I4'),
            'SEVIS_KONPOTMAN_HH_I5_1' => Yii::t('app', 'Sevis  Konpotman  Hh  I5 1'),
            'SEVIS_KONPOTMAN_HH_I5_2' => Yii::t('app', 'Sevis  Konpotman  Hh  I5 2'),
            'SEVIS_KONPOTMAN_HH_I6' => Yii::t('app', 'Sevis  Konpotman  Hh  I6'),
            'SEVIS_KONPOTMAN_HH_I7' => Yii::t('app', 'Sevis  Konpotman  Hh  I7'),
            'SEVIS_KONPOTMAN_HH_I8' => Yii::t('app', 'Sevis  Konpotman  Hh  I8'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivites()
    {
        return $this->hasMany(Activites::className(), ['id_famille' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChroniques()
    {
        return $this->hasMany(Chroniques::className(), ['famille' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFamilyAkfs()
    {
        return $this->hasMany(FamilyAkf::className(), ['famille' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperateurAkfs()
    {
        return $this->hasMany(Operateurs::className(), ['id' => 'operateur_akf'])->viaTable('family_akf', ['famille' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSousActivites()
    {
        return $this->hasMany(SousActivites::className(), ['id_famille' => 'id']);
    }
}
