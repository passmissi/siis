<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monitoringactivites;

/**
 * ScrMonitoringactivites represents the model behind the search form about `app\models\Monitoringactivites`.
 */
class ScrMonitoringactivites extends Monitoringactivites
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mplan', 'plan', 'activites', 'mode', 'type_activite', 'isExecuted', 'isExecutedBy', 'isActif', 'isDone', 'id_famille', 'id_akf', 'id_theme', 'nbre_participants', 'sousactivites', 'duree'], 'integer'],
            [['isExecutedOn', 'commentaires', 'lieu', 'eval_lieu', 'eval_contenu', 'eval_retroaction', 'type_rencontre', 'sujets', 'actions', 'module', 'remarques'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Monitoringactivites::find();
		
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'mplan' => $this->mplan,
            'plan' => $this->plan,
            'activites' => $this->activites,
        	'mode' => $this->mode,
            'type_activite' => $this->type_activite,
            'isExecuted' => $this->isExecuted,
            'isExecutedBy' => $this->isExecutedBy,
            'isExecutedOn' => $this->isExecutedOn,
            'isActif' => $this->isActif,
            'isDone' => $this->isDone,
            'id_famille' => $this->id_famille,
            'id_akf' => $this->id_akf,
            'id_theme' => $this->id_theme,
            'nbre_participants' => $this->nbre_participants,
            'sousactivites' => $this->sousactivites,
            'duree' => $this->duree,
        ]);

        $query->andFilterWhere(['like', 'commentaires', $this->commentaires])
            ->andFilterWhere(['like', 'lieu', $this->lieu])
            ->andFilterWhere(['like', 'eval_lieu', $this->eval_lieu])
            ->andFilterWhere(['like', 'eval_contenu', $this->eval_contenu])
            ->andFilterWhere(['like', 'eval_retroaction', $this->eval_retroaction])
            ->andFilterWhere(['like', 'type_rencontre', $this->type_rencontre])
            ->andFilterWhere(['like', 'sujets', $this->sujets])
            ->andFilterWhere(['like', 'actions', $this->actions])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'remarques', $this->remarques]);

        return $dataProvider;
    }
    
    public function search11($params)
    {
    	$query = Monitoringactivites::find()->where(['mplan' => -1]);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'mplan' => $this->mplan,
    			'plan' => $this->plan,
    			'activites' => $this->activites,
    			'mode' => $this->mode,
    			'type_activite' => $this->type_activite,
    			'isExecuted' => $this->isExecuted,
    			'isExecutedBy' => $this->isExecutedBy,
    			'isExecutedOn' => $this->isExecutedOn,
    			'isActif' => $this->isActif,
    			'isDone' => $this->isDone,
    			'id_famille' => $this->id_famille,
    			'id_akf' => $this->id_akf,
    			'id_theme' => $this->id_theme,
    			'nbre_participants' => $this->nbre_participants,
    			'sousactivites' => $this->sousactivites,
    			'duree' => $this->duree,
    	]);
    
    	$query->andFilterWhere(['like', 'commentaires', $this->commentaires])
    	->andFilterWhere(['like', 'lieu', $this->lieu])
    	->andFilterWhere(['like', 'eval_lieu', $this->eval_lieu])
    	->andFilterWhere(['like', 'eval_contenu', $this->eval_contenu])
    	->andFilterWhere(['like', 'eval_retroaction', $this->eval_retroaction])
    	->andFilterWhere(['like', 'type_rencontre', $this->type_rencontre])
    	->andFilterWhere(['like', 'sujets', $this->sujets])
    	->andFilterWhere(['like', 'actions', $this->actions])
    	->andFilterWhere(['like', 'module', $this->module])
    	->andFilterWhere(['like', 'remarques', $this->remarques]);
    
    	return $dataProvider;
    }
    
    public function search12($params,$mois,$annee,$ts,$akf)
    {
    	$plan = new Plans();
    	$data = Plans::find()->where(['mois'=> $mois, 'annee'=>$annee, 'ts'=>$ts, 'akf'=>$akf, 'mode'=>1])->asArray()->all();
    	$datax = $plan->getQuerysplit($data, 'id');
    	
    	if($datax != "()"){
    		$query = Monitoringactivites::find()->where('plan IN'.$datax.'');
    	}else{
    		$query = Monitoringactivites::find()->where(['mplan' => -1]);
    	}
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$this->load($params);
    
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    
    	$query->andFilterWhere([
    			'id' => $this->id,
    			'mplan' => $this->mplan,
    			'plan' => $this->plan,
    			'activites' => $this->activites,
    			'mode' => $this->mode,
    			'type_activite' => $this->type_activite,
    			'isExecuted' => $this->isExecuted,
    			'isExecutedBy' => $this->isExecutedBy,
    			'isExecutedOn' => $this->isExecutedOn,
    			'isActif' => $this->isActif,
    			'isDone' => $this->isDone,
    			'id_famille' => $this->id_famille,
    			'id_akf' => $this->id_akf,
    			'id_theme' => $this->id_theme,
    			'nbre_participants' => $this->nbre_participants,
    			'sousactivites' => $this->sousactivites,
    			'duree' => $this->duree,
    	]);
    
    	$query->andFilterWhere(['like', 'commentaires', $this->commentaires])
    	->andFilterWhere(['like', 'lieu', $this->lieu])
    	->andFilterWhere(['like', 'eval_lieu', $this->eval_lieu])
    	->andFilterWhere(['like', 'eval_contenu', $this->eval_contenu])
    	->andFilterWhere(['like', 'eval_retroaction', $this->eval_retroaction])
    	->andFilterWhere(['like', 'type_rencontre', $this->type_rencontre])
    	->andFilterWhere(['like', 'sujets', $this->sujets])
    	->andFilterWhere(['like', 'actions', $this->actions])
    	->andFilterWhere(['like', 'module', $this->module])
    	->andFilterWhere(['like', 'remarques', $this->remarques]);
    
    	return $dataProvider;
    }
}
