<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FamilyAkf;

/**
 * ScrFamilyAkf represents the model behind the search form about `app\models\FamilyAkf`.
 */
class ScrFamilyAkf extends FamilyAkf
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['famille', 'operateur_akf', 'operateur_ts', 'commune'], 'integer'],
            [['localite', 'createdOn'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ts)
    {
        $query = FamilyAkf::find()->where(['operateur_ts' => $ts]);;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $dataProvider->pagination->pageSize = 7;
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'famille' => $this->famille,
            'operateur_akf' => $this->operateur_akf,
            'operateur_ts' => $this->operateur_ts,
            'commune' => $this->commune,
            'createdOn' => $this->createdOn,
        ]);

        $query->andFilterWhere(['like', 'localite', $this->localite]);

        return $dataProvider;
    }
    
    public function search44($params, $ts)
    {
    	$query = FamilyAkf::find()->select('commune, localite,  operateur_akf')->distinct(true)
    				->where(['operateur_ts' => $ts]);
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    
    	$dataProvider->pagination->pageSize = 7;
    	
    	$this->load($params);
    	
    	if (!$this->validate()) {
    		// uncomment the following line if you do not want to return any records when validation fails
    		// $query->where('0=1');
    		return $dataProvider;
    	}
    	
    	$query->andFilterWhere([
    		'operateur_akf' => $this->operateur_akf,
    	]);
    	
    	$query->andFilterWhere(['like', 'localite', $this->localite]);
    	
    	return $dataProvider;
    }
}
