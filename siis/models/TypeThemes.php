<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "type_themes".
 *
 * @property integer $id
 * @property string $libelle
 * @property integer $id_type_sous_activite
 * @property string $description
 */
class TypeThemes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'type_themes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['libelle'], 'required'],
            [['id_type_sous_activite'], 'integer'],
            [['libelle', 'description'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'libelle' => Yii::t('app', 'Libelle'),
            'id_type_sous_activite' => Yii::t('app', 'Id Type Sous Activite'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
    
    public static function getThemes($id){
    	$data=  static::find()->where(['id_type_sous_activite' => $id])->all();
    	$value=(count($data)==0)? [''=>'']: \yii\helpers\ArrayHelper::map($data, 'id','libelle'); //id = your ID model, name = your caption
    
    	return $value;
    }
}
