<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "operateurs".
 *
 * @property integer $id
 * @property string $nom
 * @property string $prenom
 * @property integer $sexe
 * @property string $email
 * @property string $fonction
 * @property string $role
 * @property integer $akf_lie
 * @property integer $commune_lie
 *
 * @property Operateurs $akfLie
 * @property Operateurs[] $operateurs
 * @property Plans[] $plans
 * @property User[] $users
 */
class Operateurs extends \yii\db\ActiveRecord
{
	public $nomAKF;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operateurs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nom', 'prenom', 'email', 'sexe',  'fonction'], 'required', 'on' => 'register'],
            [['nomAKF'], 'required', 'on' => 'carnet'],
            [['sexe', 'akf_lie', 'commune_lie'], 'integer'],
            [['nom', 'prenom', 'email', 'fonction'], 'string', 'max' => 255],
            [['role'], 'string', 'max' => 100],
            [['email'], 'email'],
            [['email'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nom' => Yii::t('app', 'Nom'),
            'prenom' => Yii::t('app', 'Prenom'),
            'sexe' => Yii::t('app', 'Sexe'),
            'email' => Yii::t('app', 'Email'),
            'fonction' => Yii::t('app', 'Fonction'),
            'role' => Yii::t('app', 'Role'),
            'akf_lie' => Yii::t('app', 'Akf Lie'),
            'commune_lie' => Yii::t('app', 'Commune Lie'),
        ];
    }

    public function getFullName()
    {
    	return $this->nom.' '.$this->prenom;
    }
    
    public function getOps()
    {
    	$Ops = Operateurs::model()->findAll();
    	$list    = CHtml::listData($Ops , 'id', 'fullName');
    	return $list;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkfLie()
    {
        return $this->hasOne(Operateurs::className(), ['id' => 'akf_lie']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperateurs()
    {
        return $this->hasMany(Operateurs::className(), ['akf_lie' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlans()
    {
        return $this->hasMany(Plans::className(), ['akf' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['idOperateurs' => 'id']);
    }
    
    public function getSexeName()
    {
    	$sex = $this->sexe;
    	switch ($sex)
    	{
    		case 1:
    			return "Masculin";
    			break;
    		case 2:
    			return "F�minin";
    	}
    }
    
    public function getCommuneName()
    {
    	$commune_id = $this->commune_lie;
    	$commune_name = Communes::find()->where(['id_communal'=>$commune_id])->all();
    	foreach($commune_name as $cn)
    	{
    		$c_name = $cn->n_communal;
    	}
    	return $c_name;
    }
     
    public function getNomComplet() {
    	return $this->prenom.' '.$this->nom;
    }
    
    public function  getNomTS(){
    	$id_ts = $this->akf_lie;
    	$ts_name_a = Operateurs::find()->where(['id'=>$id_ts])->all();
    	foreach($ts_name_a as $tna)
    	{
    		$ts = $tna->prenom.' '.$tna->nom;
    	}
    	return $ts;
    }
}
