<?php

namespace app\controllers;

use Yii;
use app\models\Membre;
use app\models\ScrMembre;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MembreController implements the CRUD actions for Membre model.
 */
class MembreController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Membre models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScrMembre();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Membre model.
     * @param string $famille
     * @param integer $memberid
     * @return mixed
     */
    public function actionView($famille, $memberid)
    {
        return $this->render('view', [
            'model' => $this->findModel($famille, $memberid),
        ]);
    }

    /**
     * Creates a new Membre model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Membre();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'famille' => $model->famille, 'memberid' => $model->memberid]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Membre model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $famille
     * @param integer $memberid
     * @return mixed
     */
    public function actionUpdate($famille, $memberid)
    {
        $model = $this->findModel($famille, $memberid);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'famille' => $model->famille, 'memberid' => $model->memberid]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Membre model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $famille
     * @param integer $memberid
     * @return mixed
     */
    public function actionDelete($famille, $memberid)
    {
        $this->findModel($famille, $memberid)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Membre model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $famille
     * @param integer $memberid
     * @return Membre the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($famille, $memberid)
    {
        if (($model = Membre::findOne(['famille' => $famille, 'memberid' => $memberid])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
