<?php

namespace app\controllers;

use Yii;
use app\models\Monitoringsousactivites;
use app\models\ScrMonitoringsousactivites;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\SousActivites;
use yii\data\ActiveDataProvider;

/**
 * MonitoringsousactivitesController implements the CRUD actions for Monitoringsousactivites model.
 */
class MonitoringsousactivitesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Monitoringsousactivites models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScrMonitoringsousactivites();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Monitoringsousactivites model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Monitoringsousactivites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Monitoringsousactivites();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Monitoringsousactivites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Monitoringsousactivites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id,$id2,$id3)
    {
        $this->findModel($id)->delete();
        
        /*$model = new Monitoringsousactivites();
        
        $dataProvider = new ActiveDataProvider([
        	'query' => SousActivites::find()->where(['in', 'id_activite', $listact]),
        ]);
         
        $dataProvider2 = new ActiveDataProvider([
        	'query' => Monitoringsousactivites::find()->where(['mplan' => $id2]),
        ]);
        
        return $this->render('/socialworker/suiviakf2',
        		['model' => $model, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2]);*/
        return $this->redirect(['/socialworker/suiviakf2',
        	'id' => $id2,
        	'id2' => $id3,
        	'state' => 1
        ]);
        //return $this->redirect(['index']);
    }
    
    public function actionDelete22($id, $id1, $id2)
    {
    	$this->findModel($id)->delete();
    
    	return $this->redirect(['/socialworker/suivimassive', 'id' => $id2, 'id1' => $id1]);
    }

    /**
     * Finds the Monitoringsousactivites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Monitoringsousactivites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Monitoringsousactivites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
