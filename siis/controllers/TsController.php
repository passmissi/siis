<?php

namespace app\controllers;


use Yii;
use yii\web\Controller;
use app\models\Communes;
use app\models\Familles;


use app\models\Activites;
use app\models\SrcActivites;
/*
 *  Class To control the main report 
 */
class TsController extends Controller
{
	public $layout = 'main_dashboard2';
	public $modal = 0;
        
        public $comm;
        public $total;
        public $total_f;
        public $total_m;
	
         
    public function actionIndex()
    {
        $this->comm = $this->getCommunes();
        $this->total = $this->getTotal();
        $this->total_f = $this->getTotalFeminin();
        $this->total_m = $this->getTotalMasculin();
        return $this->render('index',
                ['comm'=>$this->comm,'total'=>$this->total,
                    'total_f'=>$this->total_f,
                    'total_m'=>$this->total_m]
                );
    }
    
    public function actionTest()
    {
    	$model = new Activites();
    	 
    	return $this->render('Test', [
                'model' => $model,
            ]);
    }
    
    public function getCommunes(){
        //$sql = "SELECT * FROM communes";
        $com = Communes::find()->all();
        foreach($com as $c)
        {
           $communes[] = $c->n_communal;  
        }
        return $communes; 
                        
    }
    /*
     * @Author : Jean Came Poulard 
     * @Author : Barthelemy Montreuil
     */
    public function getTotal()
    {
        
        $sql="select count(m.id_family) as total from familles f JOIN membres m on (m.id_family = f.id_family) JOIN communes c on (c.id_communal = f.a1) group by f.a1";
        $connection = Yii::$app->db;
       
        $tot_p = $connection->createCommand($sql)->queryAll();
        
        $total = array();
        for ($i = 0; $i < sizeof($tot_p); $i++) 
        {
            $total[] = (int) $tot_p[$i]["total"];
        }
        return $total;
    }
    
    public function getTotalFeminin()
    {
        
        $sql="select count(m.id_family) as total_f from familles f JOIN membres m on (m.id_family = f.id_family) JOIN communes c on (c.id_communal = f.a1) where m.sex = 2 group by f.a1";
        $connection = Yii::$app->db;
       
        $tot_p = $connection->createCommand($sql)->queryAll();
        
        $total_f = array();
        for ($i = 0; $i < sizeof($tot_p); $i++) 
        {
            $total_f[] = (int) $tot_p[$i]["total_f"];
        }
        return $total_f;
    }
    
    public function getTotalMasculin()
    {
        
        $sql="select count(m.id_family) as total_m from familles f JOIN membres m on (m.id_family = f.id_family) JOIN communes c on (c.id_communal = f.a1) where m.sex = 1 group by f.a1";
        $connection = Yii::$app->db;
       
        $tot_p = $connection->createCommand($sql)->queryAll();
        
        $total_m = array();
        for ($i = 0; $i < sizeof($tot_p); $i++) 
        {
            $total_m[] = (int) $tot_p[$i]["total_m"];
        }
        return $total_m;
    }
}

