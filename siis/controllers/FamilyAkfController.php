<?php

namespace app\controllers;

use Yii;
use app\models\FamilyAkf;
use app\models\ScrFamilyAkf;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Operateurs;
use app\models\User;

/**
 * FamilyAkfController implements the CRUD actions for FamilyAkf model.
 */
class FamilyAkfController extends Controller
{
	public $layout = 'main_dashboard2';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all FamilyAkf models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScrFamilyAkf();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FamilyAkf model.
     * @param string $famille
     * @param integer $operateur_akf
     * @return mixed
     */
    public function actionView($famille, $operateur_akf)
    {
        return $this->render('view', [
            'model' => $this->findModel($famille, $operateur_akf),
        ]);
    }

    /**
     * Creates a new FamilyAkf model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FamilyAkf();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'famille' => $model->famille, 'operateur_akf' => $model->operateur_akf]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing FamilyAkf model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $famille
     * @param integer $operateur_akf
     * @return mixed
     */
    public function actionUpdate($famille, $operateur_akf)
    {
        $model = $this->findModel($famille, $operateur_akf);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'famille' => $model->famille, 'operateur_akf' => $model->operateur_akf]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing FamilyAkf model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $famille
     * @param integer $operateur_akf
     * @return mixed
     */
    public function actionDelete($famille, $operateur_akf)
    {
        $this->findModel($famille, $operateur_akf)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionDelete2($localite, $akf)
    {
    	//$this->findModel($famille, $operateur_akf)->delete();
    	$model1 = new Operateurs();
    	$model = new FamilyAkf();
    	$searchModel = new ScrFamilyAkf();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$dataProvider = $searchModel->search44(Yii::$app->request->queryParams, $operateur->id);
        $fam = FamilyAkf::find()->where(['localite' => $localite, 'operateur_akf' => $akf])->all();
        
        foreach ($fam as $famille){
        	$famille->delete();
        }
        
    	return $this->render('/socialworker/assignation', [
    		'model' => $model,
    		'model1' => $model1,
    		'dataProvider' => $dataProvider,
    		'searchModel' => $searchModel
    	]);
    }

    /**
     * Finds the FamilyAkf model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $famille
     * @param integer $operateur_akf
     * @return FamilyAkf the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($famille, $operateur_akf)
    {
        if (($model = FamilyAkf::findOne(['famille' => $famille, 'operateur_akf' => $operateur_akf])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
