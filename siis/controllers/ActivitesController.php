<?php

namespace app\controllers;

use Yii;
use app\models\Activites;
use app\models\ScrActivites;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\models\Operateurs;
use yii\data\ActiveDataProvider;
use app\models\Plans;
use app\models\TypeActivites;
use app\models\SousActivites;
use app\models\SousActivitesHasTheme;
use yii\db\Query;
use yii\helpers\Json;

/**
 * ActivitesController implements the CRUD actions for Activites model.
 */
class ActivitesController extends Controller
{

	public $layout = 'main_dashboard2';
	public $modal = 0;
	public $dataCalendar = array();
	public $data = array();
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionListactivites($id, $id1, $id2)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$plan = new Plans();
    	$count = 0;
    	$time=strtotime($id1);
    	$month=date("F",$time);
    	$year=date("Y",$time);
    	$mois = $plan->getNumberMonth($month);
    	
    	/*$query->select(['activites.id AS id','type_activites.libelle AS libelle'])
    		->from('activites')
    		->innerJoin('plans', 'activites.id_plan = plans.id')
    		->innerJoin('type_activites', 'activites.id_type_activite = type_activites.id')
    		->where(['plans.mois' => $mois, 'plans.mode' => 1, 'plans.annee' => $year,
	            	 'plans.akf' => $id, 'activites.id_type_activite' => 2, 'activites.date_activite' => $id1])
    	;
    	$command = $query->createCommand();
    	$data = $command->queryAll();*/
    	
    	/*$communes = Activites::find()
    	->select('`activites`.`id` AS id2, `type_activites`.`libelle` AS Libelle')
    	->inleftJoin('plans', '`plans`.`id` = `activites`.`id_plan`')
    	->where(['plans.ts' => $operateur->id])
    	->with('plans')
    	->count(); */
    	
        if($id2 == 9)
        {
        	$count = Activites::find()
        		->joinWith(['idPlan', 'idTypeActivite'])
        		->where(['plans.mode' => 1, 'plans.annee' => $year,
        			'plans.akf' => $id, 'activites.id_type_activite' => 1,
        			'activites.date_activite' => $id1])
        		->count();
        	 
        	$communes = Activites::find()
        		->joinWith(['idPlan', 'idTypeActivite'])
        		->where(['plans.mode' => 1, 'plans.annee' => $year,
        			'plans.akf' => $id, 'activites.id_type_activite' => 1,
        			'activites.date_activite' => $id1])
        		->all();
        }
        else
        {
        	$count = Activites::find()
        		->joinWith(['idPlan', 'idTypeActivite'])
        		->where(['plans.mode' => 1, 'plans.annee' => $year,
        			'plans.akf' => $id, 'activites.date_activite' => $id1])
        		->andWhere(['not in', 'activites.id_type_activite', [1]])
        		->count();
        	 
        	$communes = Activites::find()
        		->joinWith(['idPlan', 'idTypeActivite'])
        		->where(['plans.mode' => 1, 'plans.annee' => $year,
        			'plans.akf' => $id, 'activites.date_activite' => $id1])
        		->andWhere(['not in', 'activites.id_type_activite', [1]])->all();
        }
        
    	if($count > 0)
    	{
    		foreach ($communes as $commune){
    			$type = TypeActivites::findOne($commune->id_type_activite);
    			echo "<option value='".$commune->id."'>".$type->libelle."</option>";
    		}
    	}
    	else
    	{
    		return "<option></option>";
    	}
    }
    /**
     * Lists all Activites models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$model = new Plans();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
        $searchModel = new ScrActivites();
        $dataProvider = $searchModel->search14(Yii::$app->request->queryParams, $operateur->id);
        
        $this->dataBindingPage();
        
        $dataProvider->pagination->pageSize=8;
        
        if ($model->load(Yii::$app->request->post())) {
        	//print_r($model->akf);
        	$time = new \DateTime('now', new \DateTimeZone('UTC'));
        	//$model->annee = $time->format('Y');
        	$model->ts = $operateur->id;
        	$model->commune = $operateur->commune_lie;
        	$model->is_sync = 1;
        	$model->mode = 1;
        	$model->isPlanned = 1;
        	$model->isPlannedBy = $operateur->id;
        	$model->isPlannedOn = date('Y-m-d');
        	$model->isMonitoring = 1;
        	$model->isDeleted = 0;
        	$model->isClosed = 0;
        	 
        	if($model->save()){
        		return $this->redirect(['/activites/create', 'id' => $model->id]);
        	}
        	//return $this->redirect(['create', 'id' => $operateur->id]);
        } else {
        	return $this->render('index', [
        		'searchModel' => $searchModel,
        		'dataProvider' => $dataProvider,
        		'activites' => $this->data[0],
        		'plan' => $this->data[1],
        		'akf' => $this->data[2],
        		'model' => $model
        	]);
        }
    }

    /**
     * Displays a single Activites model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate2($id)
    {
    	$model = new Activites();
    	$plan = Plans::findOne($id);
    	
    	if ($model->load(Yii::$app->request->post()))
    	{
    		$model->id_plan = $id;
    		$model->mode = 2;
    		$model->case_id = Plans::v4();
    		
    		if($model->save())
    		{
    			$model = new Activites();
    			return $this->render('create2', [
    				'model' => $model,
    				'plan' => $plan
    			]);
    		}
    	}
    	else
    	{
    		$model = new Activites();
    		return $this->render('create2', [
    			'model' => $model,
    			'plan' => $plan
    		]);
    	}
    }
    /**
     * Creates a new Activites model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
	public function actionCreate($id)
    {
        $model = new Activites();
        $activities[] = new Activites();
        $plan = Plans::findOne($id);
        $sousactiviteshasthemes[] = new SousActivitesHasTheme();
        $connection = \Yii::$app->db;
        
        $model->id_plan = $id;
        
        $formPost = Yii::$app->request->post('Activites');
        
        $transaction = $connection->beginTransaction();
	    try {
	    	
	    	$model->isVerified = 1;
	    	$model->isChanged = 1;
	    	$model->mode = 1;
	    	$model->case_id = Plans::v4();
	    	
	    	if ($model->load(Yii::$app->request->post()) && $model->save()) {

	    		$attrPromotion = $formPost['promotion'];
	    		$attrDistribution = $formPost['distribution'];
	    		$attrFormation = $formPost['formation'];
	    		$attrSousactivites = $formPost['sousactivites'];
	    		
	    		if (is_array($attrSousactivites))
	    		{
	    			$arrlength = sizeof($attrSousactivites);
	    			
	    			for($i=0;$i<$arrlength;$i++)
	    			{
	    				$sac = new SousActivites();
	    				$sac->id_type_sous_activite = $attrSousactivites[$i];
	    				$sac->id_plan = $id;
	    				$sac->id_activite = $model->id;
	    				$sac->case_id = Plans::v4();
	    				$sac->save();
	    			}
	    		}
	    		
	    		if (is_array($attrPromotion))
	    		{
	    			$arrlength = sizeof($attrPromotion);
	    			$sac = new SousActivites();
	    			
	    			$sac->id_type_sous_activite = 1;
	    			$sac->id_plan = $id;
	    			$sac->id_activite = $model->id;//$ida;//$model->primaryKey();//$model->primaryKey;
	    			$sac->case_id = Plans::v4();
	    			$sac->save();
	    		
	    			for($i=0;$i<$arrlength;$i++)
	    			{
	    				$saht = new SousActivitesHasTheme();
		    			$saht->id_sous_activite = $sac->id;//$ida1;//mysql_insert_id(); $sac->primaryKey();//$sac->primaryKey;
		    			$saht->id_type_theme = $attrPromotion[$i];
		    			$saht->id_plan = $id;
		    			$saht->save();
		    			//print_r($attrPromotion[$i]);
	    			}
	    			
	    		}
	    		
	    		if (is_array($attrDistribution))
	    		{
	    			$arrlength1 = sizeof($attrDistribution);
	    			$sac = new SousActivites();
	    			
	    			$sac->id_type_sous_activite = 2;
	    			$sac->id_plan = $id;
	    			$sac->id_activite = $model->id;//$model->primaryKey;
	    			$sac->case_id = Plans::v4();
	    			$sac->save();
	    		
	    			for($i=0;$i<$arrlength1;$i++)
	    			{
	    				$saht = new SousActivitesHasTheme();
		    			$saht->id_sous_activite = $sac->id;//$sac->primaryKey;
		    			$saht->id_type_theme = $attrDistribution[$i];
		    			$saht->id_plan = $id;
		    			$saht->save();
	    			}
	    		}
	    		
	    		if (is_array($attrFormation))
	    		{
	    			$arrlength2 = sizeof($attrFormation);
	    			$sac = new SousActivites();
	    		
	    			$sac->id_type_sous_activite = 4;
	    			$sac->id_plan = $id;
	    			$sac->id_activite = $model->id;//$model->primaryKey;
	    			$sac->case_id = Plans::v4();
	    			$sac->save();
	    		
	    			for($i=0;$i<$arrlength2;$i++)
	    			{
	    				$saht = new SousActivitesHasTheme();
	    				$saht->id_sous_activite = $sac->id;//$sac->primaryKey;
	    				$saht->id_type_theme = $attrFormation[$i];
	    				$saht->id_plan = $id;
	    				$saht->save();
	    			}
	    		}
	    		$transaction->commit();
	    		$this->dataCalendar = $this->populateCalendar($id);
	    		return $this->render('create', [
	    			'model' => $model,
	    			'plan' => $plan,
	    			'modal' => $this->modal,
	    			'list' => $this->dataCalendar,
	    			'test' => $model->id_type_activite
	    		]);
	    	} else {
	    		$this->dataCalendar = $this->populateCalendar($id);
	    		 
	    		return $this->render('create', [
	    			'model' => $model,
	    			'plan' => $plan,
	    			'modal' => $this->modal,
	    			'list' => $this->dataCalendar,
	    			'test' => $model->id_type_activite
	    		]);
	    	}
	    } catch(Exception $e) {
	       $transaction->rollback();
	       print_r('No');
	    }
    }

    /**
     * Updates an existing Activites model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Activites model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $id1)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['plans/view', 'id' => $id1]);
    }

    public function actionDelete1($id)
    {
    	$this->findModel($id)->delete();
    
    	return $this->redirect(['activites/index']);
    }
    /**
     * Finds the Activites model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Activites the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Activites::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    /*public function actionCalendar($id)
    {
    	$list = Activites::find()->where(['id_plan' => $id])->asArray()->all();
    	$data = array();
    	$arrlength = count($list);
    	$color = array('Vizit Domisily�'=>"#00a65a", 'P�s Rasanbleman'=>"#f39c12", 
    			       'Kl�b manman'=>"#00c0ef", 'Vizit nan Lek�l'=>"#3c8dbc", 'Reyinyon Vwazinaj'=>"#dd4b39", 
    			       'Travay Administratif'=>"#3d9970", 'F�masyon'=>"#d81b60");	
    	
    	for($x = 0; $x < $arrlength; $x++) {
    		$TypeActivite = TypeActivites::findOne($list[$x]['id_type_activite']);
    		$nom_type_activite = $TypeActivite->libelle;
    	
    		$element = array(
    				'title' => $nom_type_activite,
    				'start' => $list[$x]['date_activite'],
    				'end' => $list[$x]['date_activite'],
    				//'backgroundColor' => $color[$nom_type_activite],
    				//'borderColor' => $color[$nom_type_activite],
    		);
    	
    		$data[$x] = $element;
    	}
    	
    	return json_encode($data);
    }*/
    
    private function populateCalendar($id)
    {
    	$list = Activites::find()->where(['id_plan' => $id])->asArray()->all();
    	$data = array();
    	$arrlength = count($list);
    	$color = array('Vizit Domisily�'=>"#00a65a", 'P�s Rasanbleman'=>"#f39c12",
    			'Kl�b manman'=>"#00c0ef", 'Vizit nan Lek�l'=>"#3c8dbc", 'Reyinyon Vwazinaj'=>"#dd4b39",
    			'Travay Administratif'=>"#3d9970", 'F�masyon'=>"#d81b60");
    	 
    	for($x = 0; $x < $arrlength; $x++) {
    		$TypeActivite = TypeActivites::findOne($list[$x]['id_type_activite']);
    		$nom_type_activite = $TypeActivite->libelle;
    		 
    		$element = array(
    				'title' => $nom_type_activite,
    				'start' => $list[$x]['date_activite'],
    				'end' => $list[$x]['date_activite'],
    				//'backgroundColor' => $color[$nom_type_activite],
    				//'borderColor' => $color[$nom_type_activite],
    		);
    		 
    		$data[$x] = $element;
    	}
    	 
    	return $data;
    }
    
    public function actionCalendar1($id)
    {
    	$list = Activites::find()->where(['id_plan' => $id])->asArray()->all();
    	$data = array();
    	$arrlength = count($list);
    	$color = array('Vizit Domisily�'=>"#00a65a", 'P�s Rasanbleman'=>"#f39c12",
    			'Kl�b manman'=>"#00c0ef", 'Vizit nan Lek�l'=>"#3c8dbc", 'Reyinyon Vwazinaj'=>"#dd4b39",
    			'Travay Administratif'=>"#3d9970", 'F�masyon'=>"#d81b60");
    
    	for($x = 0; $x < $arrlength; $x++) {
    		$TypeActivite = TypeActivites::findOne($list[$x]['id_type_activite']);
    		$nom_type_activite = $TypeActivite->libelle;
    		 
    		$element = array(
    				'title' => $nom_type_activite,
    				'start' => $list[$x]['date_activite'],
    				'end' => $list[$x]['date_activite'],
    				//'backgroundColor' => $color[$nom_type_activite],
    				//'borderColor' => $color[$nom_type_activite],
    		);
    		 
    		$data[$x] = $element;
    	}
    
    	return json_encode($data);
    }
    public function actionCalendar()
    {
    	$list = Activites::find()->asArray()->all();
    	$data = array();
    	$arrlength = count($list);
    	$color = array('Vizit Domisily�'=>"#00a65a", 'P�s Rasanbleman'=>"#f39c12",
    			'Kl�b manman'=>"#00c0ef", 'Vizit nan Lek�l'=>"#3c8dbc", 'Reyinyon Vwazinaj'=>"#dd4b39",
    			'Travay Administratif'=>"#3d9970", 'F�masyon'=>"#d81b60");
    	 
    	for($x = 0; $x < $arrlength; $x++) {
    		$TypeActivite = TypeActivites::findOne($list[$x]['id_type_activite']);
    		$nom_type_activite = $TypeActivite->libelle;
    		 
    		$element = array(
    				'title' => $nom_type_activite,
    				'start' => $list[$x]['date_activite'],
    				'end' => $list[$x]['date_activite'],
    				//'backgroundColor' => $color[$nom_type_activite],
    				//'borderColor' => $color[$nom_type_activite],
    		);
    		 
    		$data[$x] = $element;
    	}
    	 
    	return json_encode($data);
    }
    private function dataBindingPage()
    {
    	$data = array();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$this->data[0] = Activites::find()
    	->select('activites.*')
    	->leftJoin('plans', '`plans`.`id` = `activites`.`id_plan`')
    	->where(['plans.ts' => $operateur->id, 'activites.mode' => 1])
    	->andWhere(['is not', 'plans.akf', null])
    	->with('plans')
    	->count();
    	
    	$this->data[1] = (new \yii\db\Query())
    	->from('plans')
    	->where(['ts' => $operateur->id])
    	->count();
    	 
    	$this->data[2] = (new \yii\db\Query())
    	->from('operateurs')
    	->where(['akf_lie' => $operateur->id])
    	->count();
    	 
    	//return $this->data;
    }
    
    
}
