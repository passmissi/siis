<?php

namespace app\controllers;


use Yii;
use PHPExcel;
use app\models\MonitoringRegional;
use app\models\ScrMonitoringRegional;
use app\models\Config;
use app\models\Plans;
use app\models\ScrMonitoringsousactivites;
use app\models\Monitoringsousactivites;
use app\models\User;
use app\models\Operateurs;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Communes;
use app\models\Evenements;
use yii\web\ForbiddenHttpException;
use app\models\TypeSousActivites;
use app\models\Activites;
use app\models\TypeActivites;
use app\models\TypeThemes;
use app\models\Membre;
use app\models\app\models;
use app\models\ScrMonitoringactivites;
use app\models\DataIntegration;

class ControleregionalController extends \yii\web\Controller
{
	public $layout = 'main_dashboard2';
	
	public function behaviors()
	{
		return [
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['post'],
						],
				],
		];
	}
	
	public function actionMsact($id,$type)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$sous = Monitoringsousactivites::find()->where(['id'=>$id, 'type_sous_activites'=>$type])->one();
		return $sous;
	}
	
	public function actionListakfbyts($ts)
	{
		$count = Operateurs::find()
			->where(['akf_lie' => $ts])
			->count();
		
		$communes = Operateurs::find()
			->where(['akf_lie' => $ts])
			->all();
		
		if($count > 0)
		{
			echo "<option value>--Choisir un akf --</option>";
			foreach ($communes as $commune){
				echo "<option value='".$commune->id."'>".$commune->nom." ".$commune->prenom."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir un akf --</option>";
			return "<option></option>";
		}
	}
	
	public function actionValider($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$id = array();
		$sous = Monitoringsousactivites::findOne($id);
		/* $sous->isValid = 1;
		$id[0] = 0;
		if($sous->update(false))$id[0] = 1; */
		return $sous;
	}
	
	public function actionControlequalitesa($mois,$annee,$ts,$akf)
	{
		if (!Yii::$app->user->isGuest){
			if(Yii::$app->user->can('manage-admin')){
				$plan = new Plans();
				$data = Plans::find()->where(['mois'=> $mois, 'annee'=>$annee, 'ts'=>$ts, 'akf'=>$akf, 'mode'=>1])->asArray()->all();
				$datax = $plan->getQuerysplit($data, 'id');
				
				$model = new Monitoringsousactivites();	
				$searchModel = new ScrMonitoringsousactivites();
				$dataProvider = $searchModel->search55(Yii::$app->request->queryParams, $datax);
				
				$formPost = Yii::$app->request->post('Monitoringsousactivites');
				
				if($model->load(Yii::$app->request->post())){
					//print_r($model->id_famille.' '.$model->type_sous_activites.' '.$_POST['Monitoringsousactivites']['id']);
					$modif = Monitoringsousactivites::findOne($formPost['id']);
					if($modif->type_sous_activites == 1){
						$modif->id_famille = $formPost['id_famille'];
						$modif->id_membre = $formPost['id_membre'];
						$modif->typetheme = $formPost['typetheme'];
						$modif->commentaire = $formPost['commentaire'];
						$modif->date_creation = $formPost['date_creation'];
					}
					elseif ($modif->type_sous_activites == 2){
						$modif->id_famille = $formPost['id_famille'];
						$modif->id_membre = $formPost['id_membre'];
						$modif->typetheme = $formPost['typetheme'];
						$modif->commentaire = $formPost['commentaire'];
						$modif->date_creation = $formPost['date_creation'];
						$modif->quantite = $formPost['quantite'];
					}
					elseif ($modif->type_sous_activites == 3){
						$modif->id_famille = $formPost['id_famille'];
						$modif->id_membre = $formPost['id_membre'];
						$modif->secteur = $formPost['secteur'];
						$modif->institution = $formPost['institution'];
						$modif->commentaire = $formPost['commentaire'];
						$modif->date_creation = $formPost['date_creation'];
					}
					elseif ($modif->type_sous_activites == 6 || $modif->type_sous_activites == 7){
						$modif->id_famille = $formPost['id_famille'];
						$modif->id_membre = $formPost['id_membre'];
						$modif->vaccin = $formPost['vaccin'];
						$modif->poids = $formPost['poids'];
						$modif->taille = $formPost['taille'];
						$modif->rapportpt = $formPost['rapportpt'];
						$modif->pb = $formPost['pb'];
						$modif->oedeme = $formPost['oedeme'];
						$modif->en = $formPost['en'];
						$modif->commentaire = $formPost['commentaire'];
						$modif->date_creation = $formPost['date_creation'];
					}
					
					$modif->isValid = 1;
					$modif->update(false);
					
					return $this->render('/controleregional/controlequalitesa',[
						'model'=>$model,
						'searchModel'=>$searchModel,
						'dataProvider'=>$dataProvider,
					]);
					//print_r($modif->type_sous_activites);
				}else{
					return $this->render('/controleregional/controlequalitesa',[
						'model'=>$model,
						'searchModel'=>$searchModel,
						'dataProvider'=>$dataProvider,
					]);
				}
			}
			else{
				throw new ForbiddenHttpException;
			}
		}else{
			return $this->goHome();
		}
		
		//return $this->render('/controleregional/controlequalitesa',['model'=>$model]);
	}
	
	public function actionControlequalite()
	{
		if (!Yii::$app->user->isGuest){
			if(Yii::$app->user->can('manage-admin')){
				$model = new Plans(['scenario' => 'plants']);
				$searchModel = new ScrMonitoringactivites();
				$dataProvider = $searchModel->search11(Yii::$app->request->queryParams);
				$etat = 0;
				
				if ($model->load(Yii::$app->request->post()))
				{
					$etat = 1;
					$mois = $model->mois;
					$annee = $model->annee;
					$ts = $model->ts;
					$akf = $model->akf;
					
					$dataProvider = $searchModel->search12(Yii::$app->request->queryParams,$mois,$annee,$ts,$akf);
					
					return $this->render('/controleregional/controlequalite',[
						'model' => $model,
						'dataProvider' => $dataProvider,
						'etat' => $etat
					]);
				}else{
					return $this->render('/controleregional/controlequalite',[
						'model' => $model,
						'dataProvider' => $dataProvider,
						'etat' => $etat
					]);
				}
			}else{
				throw new ForbiddenHttpException;
			}
		}else{
			return $this->goHome();
		}
	}
	
	public function actionIndicateurs()
	{
		if (!Yii::$app->user->isGuest){
			if(Yii::$app->user->can('manage-admin')){
				//$data1 = array(); $data2 = array(); $data3 = array();
				$data1 = 0; $data2 = 0; $data3 = 0;
				$time = new \DateTime('now', new \DateTimeZone('UTC'));
				$mois_int = date_format($time,"n");
				$annee = date_format($time,"Y");
				
				$model = new MonitoringRegional();
				$searchModel = new ScrMonitoringRegional();
				
				if($searchModel->indicateurs2 > 0 && $searchModel->mois > 0
						&& $searchModel->annee > 0){
					print_r('ok--');
					$data1 = $this->getDatadimagi($searchModel->indicateurs2,$searchModel->mois,$searchModel->annee,1);
					$data2 = $this->getDatadimagi($searchModel->indicateurs2,$searchModel->mois,$searchModel->annee,2);
					$data3 = $this->getDatadimagi($searchModel->indicateurs2,$searchModel->mois,$searchModel->annee,3);
					$dataProvider = $searchModel->search12(Yii::$app->request->queryParams, $searchModel->mois,$searchModel->annee);
						
					print_r($searchModel->indicateurs2);
				}else{
					print_r('ok-');
					$data1 = $this->getDatadimagi(1,$mois_int,$annee,1);
					$data2 = $this->getDatadimagi(1,$mois_int,$annee,2);
					$data3 = $this->getDatadimagi(1,$mois_int,$annee,3);
					$dataProvider = $searchModel->search12(Yii::$app->request->queryParams, $mois_int,$annee);
					
				}
				
				/* $dataProvider->pagination->pageSize=12;
				
				return $this->render('indicateurs2',[
					'model' => $model,
					'dataProvider' => $dataProvider,
					'searchModel' => $searchModel,
					'data1' => $data1,
					'data2' => $data2,
					'data3' => $data3,
				]); */
			}else{
				throw new ForbiddenHttpException;
			}
		}else{
			return $this->goHome();
		}
	}
	
	public function actionDataexporting()
	{
		if (!Yii::$app->user->isGuest){
			if(Yii::$app->user->can('manage-admin')){
				$user = User::findOne(Yii::$app->user->identity->id);
				$operateur = Operateurs::findOne($user->idOperateurs);
				$mois = ''; $annee = '';
				
				$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
				$titles = '';
				foreach($data as $row) {
					$titles .= $row['valeur'] . ', ';
				}
				$datax = ''.rtrim($titles, ', ').'';
				$datay = '('.preg_replace("/'/", '', $datax).')';
				
				$model = new Monitoringsousactivites();
				$searchModel = new ScrMonitoringsousactivites();
				
				$mois = $searchModel->mois;
				$annee = $searchModel->annee;
				
				print_r($mois);
				print_r($annee);
				//$dataProvider = $searchModel->search22(Yii::$app->request->queryParams,$datay);
				//$dataProvider->pagination->pageSize=10;
				
				//$x =Yii::$app->getRequest()->getQueryParam('ScrMonitoringsousactivites[mois]');
				//print_r(/* $_POST['ScrMonitoringsousactivites']['mois'].' '. */$searchModel->mois.' ok');
			    
				/* return $this->redirect(['dataexporting', 
					'mois' => $mois,
					'annee' => $annee,
				]); */
				
				
			    return $this->render('dataexporting',[
			    	'model' => $model,
			    	'searchModel' => $searchModel,
			    	//'dataProvider' => $dataProvider,
			    ]);
			    
			    /* return $this->redirect(['/socialworker/suiviakf2',
			    		'id' => $id,
			    		'id2' => $id2,
			    		'state' => $state
			    ]); */
			}else{
				throw new ForbiddenHttpException;
			}
		}
		else{
			return $this->goHome();
		}
	}
	
    public function actionIndex()
    {
    	if (!Yii::$app->user->isGuest){
    		if(Yii::$app->user->can('manage-admin')){
    			$plan = new Plans();
    			$time = new \DateTime('now', new \DateTimeZone('UTC'));
    			$mois_int = date_format($time,"n");
    			$model1 = new Operateurs();
    			
    			$user = User::findOne(Yii::$app->user->identity->id);
    			$operateur = Operateurs::findOne($user->idOperateurs);
    			
    			$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
    			$titles = '';
    			foreach($data as $row) {
    				$titles .= $row['valeur'] . ', ';
    			}
    			$datax = ''.rtrim($titles, ', ').'';
    			$datay = '('.preg_replace("/'/", '', $datax).')';
    			 
    			$model = new MonitoringRegional();
    			$searchModel = new ScrMonitoringRegional();
    			
    			$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $datax,1);
    			$dataProvider2 = $searchModel->search(Yii::$app->request->queryParams, $datax,2);
    			$dataProvider3 = $searchModel->search24(Yii::$app->request->queryParams, $datax,$searchModel);
    			 
    			$dataProvider->pagination->pageSize=10;
    			$dataProvider2->pagination->pageSize=10;
    			$dataProvider3->pagination->pageSize=10;
    			
    			$statszone = $this->getStatszone($datay,$searchModel->mois,$searchModel->annee,1);
    			$statszone2 = $this->getStatszone2($datay,$searchModel->mois,$searchModel->annee,1);
    			
    			$stats_array = $this->getStatszone3($datay, $searchModel->mois,$searchModel->annee,1);
    			 
    			$statszone11 = $this->getStatszone($datay,$searchModel->mois,$searchModel->annee,2);
    			$statszone21 = $this->getStatszone2($datay,$searchModel->mois,$searchModel->annee,2);
    			 
    			$statszone31 = $this->getVaccination($datay,$searchModel->mois,$searchModel->annee);
    			 
    			//print_r($statszone2);
    			if (isset($_POST['Operateurs']))
    			{
    				//print_r($_POST['Operateurs']['id']);
    				return $this->redirect(['/socialworker/pdfcarnet3', 
    					'akf' => $_POST['Operateurs']['id']
    				]);
    			}else{
    				return $this->render('index',[
    					'model' => $model,
    					'model1' => $model1,
    					'searchModel' => $searchModel,
    					'dataProvider' => $dataProvider,
    					'dataProvider2' => $dataProvider2,
    					'dataProvider3' => $dataProvider3,
    					'statszone' => $statszone,
    					'statszone2' => $statszone2,
    					'statszone11' => $statszone11,
    					'statszone21' => $statszone21,
    					'statszone31' => $statszone31,
    					'stats_array' => $stats_array
    				]);
    			}
    		}else{
    			throw new ForbiddenHttpException;
    		}
    	}
    	else{
    		return $this->goHome();
    	}
    }
    
    public function actionStatszone($datax,$mois,$annee_int)
    {
    	//\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    	if($mois == ''){
    		$plan = new Plans();
    		$time = new \DateTime('now', new \DateTimeZone('UTC'));
    		$mois_int = date_format($time,"n");
    		$mois = $plan->getNumberMonth($mois_int);
    	}
    	
    	if($annee_int == ''){
    		$annee_int = date('Y');
    	}
    	
    	$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '$mois' AND `annee` = $annee_int
				group by `commune`";
    	 
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql)->queryAll();
		 
		$a = json_encode($tot_g, JSON_NUMERIC_CHECK);
		 
		$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
		 
		return $json;
		
    	//return $tot_g;
    }

    public function getStatszone($datax,$mois,$annee_int,$temoin)
    {
    	$annee = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	
    	if($temoin == 1){
    		if($mois != '' && $annee_int != ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois."' AND `annee` = ".$annee_int."
				AND `mode` = 1
				group by `commune`";
    		}
    		elseif ($mois != '' && $annee_int == ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois."' AND `annee` = ".$annee."
				AND `mode` = 1
				group by `commune`";
    		}
    		elseif ($mois == '' && $annee_int != ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois1."' AND `annee` = ".$annee_int."
				AND `mode` = 1
				group by `commune`";
    		}
    		elseif ($mois == '' && $annee_int == ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois1."' AND `annee` = ".$annee."
				AND `mode` = 1
				group by `commune`";
    		}
    	}
    	else{
    		/* 'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0 */
    		if($mois != '' && $annee_int != ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois."' AND `annee` = ".$annee_int."
				AND `mode` = 1
				AND `isMonitoring` = 0
				AND `isExecuted` = 1
				AND `isDeleted` = 0
				group by `commune`";
    		}
    		elseif ($mois != '' && $annee_int == ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois."' AND `annee` = ".$annee."
				AND `mode` = 1
				AND `isMonitoring` = 0
				AND `isExecuted` = 1
				AND `isDeleted` = 0
				group by `commune`";
    		}
    		elseif ($mois == '' && $annee_int != ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois1."' AND `annee` = ".$annee_int."
				AND `mode` = 1
				AND `isMonitoring` = 0
				AND `isExecuted` = 1
				AND `isDeleted` = 0
				group by `commune`";
    		}
    		elseif ($mois == '' && $annee_int == ''){
    			$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `plans` p
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE `commune` in ".$datax."
				AND `mois` = '".$mois1."' AND `annee` = ".$annee."
				AND `mode` = 1
				AND `isMonitoring` = 0
				AND `isExecuted` = 1
				AND `isDeleted` = 0
				group by `commune`";
    		}
    	}
    	
    	$connection = Yii::$app->db;
    	
    	$tot_g = $connection->createCommand($sql)->queryAll();
    	
    	$a = json_encode($tot_g, JSON_NUMERIC_CHECK);
    	
    	$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
    	
    	return $json;
    }
    
    public function getStatszone2($datax,$mois,$annee_int,$temoin)
    {
    	$stats_array = array();
    	$annee = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	
    	if($temoin == 1){
    		if($mois != '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee_int, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois != '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois == '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee_int, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois == '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    	}
    	else{
    		if($mois != '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee_int, 'mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois != '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois == '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee_int, 'mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		elseif ($mois == '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
    			->andWhere('commune IN '.$datax.'')->groupBy("commune")
    			->all();
    		}
    		
    		$titles = '';
    		foreach($stats_data as $row) {
    			$titles .= $row['id'] . ', ';
    		}
    		$planx = ''.rtrim($titles, ', ').'';
    		$plany = '('.preg_replace("/'/", '', $planx).')';
    	}
    	
        $o = 0;
    	foreach ($stats_data as $c){
    		$commune = Communes::findOne($c->commune);
    		$stats_array[$o]["name"] = $commune->n_communal;
    		$stats_array[$o]["id"] = $commune->n_communal;
    		
    		if($temoin == 1){
    			if($mois != '' && $annee_int != ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois."'
					AND p.`annee` = ".$annee_int."
					AND p.`mode` = 1
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois != '' && $annee_int == ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois."'
					AND p.`annee` = ".$annee."
					AND p.`mode` = 1
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois == '' && $annee_int != ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois1."'
					AND p.`annee` = ".$annee_int."
					AND p.`mode` = 1
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois == '' && $annee_int == ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois1."'
					AND p.`annee` = ".$annee."
					AND p.`mode` = 1
					GROUP BY `id_type_activite`";
    			}
    		}
    		else{
    			
    			if($mois != '' && $annee_int != ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois."'
					AND p.`annee` = ".$annee_int."
					AND p.`mode` = 1
					AND p.`id` in ".$plany."
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois != '' && $annee_int == ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois."'
					AND p.`annee` = ".$annee."
					AND p.`mode` = 1
					AND p.`id` in ".$plany."
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois == '' && $annee_int != ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois1."'
					AND p.`annee` = ".$annee_int."
					AND p.`mode` = 1
					AND p.`id` in ".$plany."
					GROUP BY `id_type_activite`";
    			}
    			elseif ($mois == '' && $annee_int == ''){
    				$sql = "SELECT count(*) as nbre, ta.`libelle` as libelle
					FROM `activites` a
					JOIN `plans` p ON (p.`id` = a.`id_plan`)
					JOIN `type_activites` ta ON (ta.`id` = a.`id_type_activite`)
					WHERE p.`commune` = ".$c->commune."
					AND p.`mois` = '".$mois1."'
					AND p.`annee` = ".$annee."
					AND p.`mode` = 1
					AND p.`id` in ".$plany."
					GROUP BY `id_type_activite`";
    			}
    		}
    		
    		
    		$connection = Yii::$app->db;
    		$tot_p = $connection->createCommand($sql)->queryAll();
    		
    		$total = array();
    		for ($i = 0; $i < sizeof($tot_p); $i++)
    		{
    			$element = array();
    			$element[0] = $tot_p[$i]["libelle"];
    			$element[1] = $tot_p[$i]["nbre"];
    			$total[$i] = $element;
    		}
    		
    		$stats_array[$o]["data"] = $total;
    		$o += 1;
    	} 
    	$a = json_encode($stats_array, JSON_NUMERIC_CHECK);
    	$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
    	return  $json/* $count */;
    }
    
    public function getStatszone3($datax,$mois,$annee_int,$temoin){
    	$stats_array = array();
    	$annee = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	$data_plans_suivis = 0;
    	$data_suivis_termines = 0;
    	$evenement2 = 0;
    	$evenement5 = 0;
    	$count = 0;
    	$count2 = 0;
    	
    	if($temoin == 1){
    		if($mois != '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee_int, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->count();
    		
    			$count = Plans::find()->where(['isMonitoring' => 0, 'mois' => $mois, 'annee' => $annee_int, 'mode' => 1,
    					'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->andWhere('commune IN '.$datax.'')
    					->count();
    		
    			$count2 = Plans::find()->where(['mois' => $mois, 'annee' => $annee_int, 'mode' => 1,
    					'isMonitoring' => 0, 'isPlanned' => 1, 'isClosed' => 1 ])
    					->andWhere('commune IN '.$datax.'')->count();
    		
    			$cond = $this->getSuivis($datax, $mois, $annee_int,1);
    			if(!$cond){
    				$evenement2 = Evenements::find()->where(['evenements' => 2])
    				->andWhere('plan IN '.$cond.'')->count();
    				$evenement5 = Evenements::find()->where(['evenements' => 5])
    				->andWhere('plan IN '.$cond.'')->count();
    			}
    		
    		}
    		elseif ($mois != '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')
    			->count();
    		
    			$count = Plans::find()->where(['isMonitoring' => 0, 'mois' => $mois, 'annee' => $annee, 'mode' => 1,
    					'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->andWhere('commune IN '.$datax.'')
    					->count();
    		
    			$count2 = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1,
    					'isMonitoring' => 0, 'isPlanned' => 1, 'isClosed' => 1 ])
    					->andWhere('commune IN '.$datax.'')->count();
    		
    			$cond = $this->getSuivis($datax, $mois, $annee,1);
    			if(!$cond){
    				$evenement2 = Evenements::find()->where(['evenements' => 2])
    				->andWhere('plan IN '.$cond.'')->count();
    				$evenement5 = Evenements::find()->where(['evenements' => 5])
    				->andWhere('plan IN '.$cond.'')->count();
    			}
    		}
    		elseif ($mois == '' && $annee_int != ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee_int, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->count();
    		
    			$count = Plans::find()->where(['isMonitoring' => 0, 'mois' => $mois1, 'annee' => $annee_int, 'mode' => 1,
    					'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->andWhere('commune IN '.$datax.'')
    					->count();
    		
    			$count2 = Plans::find()->where(['mois' => $mois1, 'annee' => $annee_int, 'mode' => 1,
    					'isMonitoring' => 0, 'isPlanned' => 1, 'isClosed' => 1 ])
    					->andWhere('commune IN '.$datax.'')->count();
    		
    			$cond = $this->getSuivis($datax, $mois1, $annee_int,1);
    			if(!$cond){
    				$evenement2 = Evenements::find()->where(['evenements' => 2])
    				->andWhere('plan IN '.$cond.'')->count();
    				$evenement5 = Evenements::find()->where(['evenements' => 5])
    				->andWhere('plan IN '.$cond.'')->count();
    			}
    		}
    		elseif ($mois == '' && $annee_int == ''){
    			$stats_data = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1])
    			->andWhere('commune IN '.$datax.'')->count();
    		
    			$count = Plans::find()->where(['isMonitoring' => 0, 'mois' => $mois1, 'annee' => $annee, 'mode' => 1,
    					'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->andWhere('commune IN '.$datax.'')
    					->count();
    		
    			$count2 = Plans::find()->where(['mois' => $mois1, 'annee' => $annee, 'mode' => 1,
    					'isMonitoring' => 0, 'isPlanned' => 1, 'isClosed' => 1 ])
    					->andWhere('commune IN '.$datax.'')->count();
    		
    			$cond = $this->getSuivis($datax, $mois1, $annee,1);
    			if(!$cond){
    				$evenement2 = Evenements::find()->where(['evenements' => 2])
    				->andWhere('plan IN '.$cond.'')->count();
    				$evenement5 = Evenements::find()->where(['evenements' => 5])
    				->andWhere('plan IN '.$cond.'')->count();
    			}
    		}
    	}
    	else{
    	
    	}
    	
    	//Yii::$app->
    	if($stats_data != 0){
    		$data_plans_suivis = ($count/$stats_data)*100;
    		$data_suivis_termines = ($count2/$stats_data)*100;
    	}
    	
    	$stats_array[0] = $data_plans_suivis;
    	$stats_array[1] = $data_suivis_termines;
    	$stats_array[2] = $evenement2;
    	$stats_array[3] = $evenement5;
    	$stats_array[4] = $count;
    	$stats_array[5] = $count2;
    	
    	return $stats_array;
    }
    
    private function getSuivis($datax,$mois,$annee,$temoin){
    	if($temoin == 1){
    		$data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1])
    		->andWhere('commune IN '.$datax.'')->asArray()->all();
    	}
    	else{
    		$data = Plans::find()->where(['mois' => $mois, 'annee' => $annee, 'mode' => 1,'isMonitoring' => 0,
    		 'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])
    		->andWhere('commune IN '.$datax.'')->asArray()->all();
    	}
    	
    	$titles = '';
    	foreach($data as $row) {
    		$titles .= $row['id'] . ', ';
    	}
    	$dataw = ''.rtrim($titles, ', ').'';
    	$dataq = '('.preg_replace("/'/", '', $dataw).')';
    	return $dataq;
    }
    
    public function getVaccination($datax,$mois,$annee_int){
    	$annee = date('Y');
    	$plan = new Plans();
    	$time = new \DateTime('now', new \DateTimeZone('UTC'));
    	$mois_int = date_format($time,"n");
    	$mois1 = $plan->getNumberMonth($mois_int);
    	
    	if($mois != '' && $annee_int != ''){
    		$data = $this->getSuivis($datax, $mois, $annee_int, 2);
    	}
    	elseif ($mois != '' && $annee_int == ''){
    		$data = $this->getSuivis($datax, $mois, $annee, 2);
    	}
    	elseif ($mois == '' && $annee_int != ''){
    		$data = $this->getSuivis($datax, $mois1, $annee_int, 2);
    	}
    	elseif ($mois == '' && $annee_int == ''){
    		$data = $this->getSuivis($datax, $mois1, $annee, 2);
    	}
    	
    	if($data != "()"){
    		$sql = "SELECT c.`n_communal` as name, count(*) as y, c.`n_communal` as drilldown
				FROM `monitoringsousactivites` msa
				JOIN `plans` p ON (p.`id` = msa.`plan`)
				JOIN `communes` c ON (p.`commune` = c.id_communal)
				WHERE msa.`plan` IN ".$data."
				AND msa.`type_sous_activites` = 6
				GROUP BY p.`commune` ";
    		 
    		$connection = Yii::$app->db;
    		
    		$tot_g = $connection->createCommand($sql)->queryAll();
    		
    		$a = json_encode($tot_g, JSON_NUMERIC_CHECK);
    		
    		$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
    	}
    	else{
    		$json = "[]";
    	}
    		
    	return $json;
    }
    
    public function actionExport($mois, $annee, $type, $type1)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
    	$titles = '';
    	foreach($data as $row) {
    		$titles .= $row['valeur'] . ', ';
    	}
    	$datax = ''.rtrim($titles, ', ').'';
    	$datay = '('.preg_replace("/'/", '', $datax).')';
    	
    	$searchModel = new ScrMonitoringsousactivites();
    	$model = $searchModel->search23($datay,$mois,$annee,$type,$type1);
    	
    	$filename = 'Suivis-'.$mois.'-'.$annee.'.xls';
    	
    	header("Content-Disposition: attachment; filename=".$filename);
    	header("Content-Type: application/vnd-ms-excel");
    	 
    	/* $flag = false;
    	foreach($data as $row) {
    		if(!$flag) {
    			// display field/column names as first row
    			echo implode("\t", array_keys($row)) . "\r\n";
    			$flag = true;
    		}
    		array_walk($row, 'cleanData');
    		echo implode("\t", array_values($row)) . "\r\n";
    	}
    	exit; */
    	 
    	/* header("Content-type: application/vnd-ms-excel");
    	header("Content-type: application/csv");
    	header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    
    	header("Content-Disposition: attachment; filename=".$filename); */
    	echo '<table border="1" width="100%">
        <thead>
            <tr>
    			<th>id</th>
                <th>Plan</th>
                <th>Mois</th>
                <th>Annee</th>
    			<th>Type Activite</th>
    			<th>Type Sous-Activites</th>
    			<th>Famille</th>
    			<th>Membre</th>
    			
    			<th>Type Theme</th>
    			<th>Quantite</th>
    			<th>vaccin</th>
    			<th>poids</th>
                    <th>taille</th>
                    <th>rapportpt</th>
                    <th>pb</th>
                    <th>oedeme</th>
                    <th>en</th>
                    		
                    <th>secteur</th>
                    <th>institution</th>
                    		
                    <th>commentaire</th>
                    <th>date_creation</th>
            </tr>
        </thead>';
    	foreach($model as $data){
    		$plan = Plans::findOne($data['plan']);
    		$tsa = TypeSousActivites::findOne($data['type_sous_activites']);
    		$activite = Activites::findOne($data['activite']);
    		$ta = TypeActivites::findOne($activite->id_type_activite);
    		
    		if($data['typetheme'] != null){
    			$tt = TypeThemes::findOne($data['typetheme']);
    			$ttheme = $tt->libelle;
    		}else{
    			$ttheme = "";
    		}
    		
    		if($data['secteur'] != null){
    			$tt = TypeThemes::findOne($data['secteur']);
    			$tts = $tt->libelle;
    		}else{
    			$tts = "";
    		}
    		
    		$mem = Membre::find()->where(['famille' => $data['id_famille'], 'memberid' => $data['id_membre']])->one();
    		if($mem != null){
    			$membre = $mem->nom.''.$mem->prenom;
    		}else{
    			$membre = "";
    		}
    		
    		echo '
                <tr>
                    <td>'.$data['id'].'</td>
                    <td>'.$data['plan'].'</td>
                    <td>'.$plan->mois.'</td>
                    <td>'.$plan->annee.'</td>
                    <td>'.$ta->libelle.'</td>
                    <td>'.$tsa->libelle.'</td>
                    
                    <td>'.$data['id_famille'].'</td>
                    <td>'.$membre.'</td>

                    <td>'.$ttheme.'</td>
                    <td>'.$data['quantite'].'</td>
                    		
                    <td>'.$data['vaccin'].'</td>
                    <td>'.$data['poids'].'</td>
                    <td>'.$data['taille'].'</td>
                    <td>'.$data['rapportpt'].'</td>
                    <td>'.$data['pb'].'</td>
                    <td>'.$data['oedeme'].'</td>
                    <td>'.$data['en'].'</td>
                    		
                    <td>'.$tts.'</td>
                    <td>'.$data['institution'].'</td>
                    		
                    <td>'.$data['commentaire'].'</td>
                    <td>'.$data['date_creation'].'</td>
                    
                </tr>
            ';
    	}
    	echo '</table>';
    }
    
    public function getDatadimagi($type,$mois,$annee,$typesac){
    	$sql = "SELECT `type_sous_activites`, COUNT(*) AS total_d
				FROM `data_integration` 
				WHERE MONTH(`date_creation`) = ".$mois." 
				AND YEAR(`date_creation`) = ".$annee." 
				AND `type_activite` = ".$type."
				AND `type_sous_activites` = ".$typesac."
				GROUP BY `type_sous_activites` ";
    	
    	/* $sql = "SELECT `type_sous_activites`, COUNT(*) AS total_d
				FROM `data_integration`
				WHERE MONTH(`date_creation`) = 6
				AND YEAR(`date_creation`) = 2016
				AND `type_activite` = 1
				AND `type_sous_activites` = 1
				GROUP BY `type_sous_activites` "; */
    	
    	$connection = Yii::$app->getDb();
    	
    	$tot_p = $connection->createCommand($sql)->queryAll();
    	
    	//$a = json_encode($tot_p, JSON_NUMERIC_CHECK);
    	 
    	//$json = preg_replace('/"([^"]+)"\s*:\s*/', '$1:', $a);
    	 
    	//return $json; 
    	$total_d = 0;
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total_d = (int) $tot_p[$i]['total_d'];
    	}
    	    	 
    	return $total_d;
    }
    
    private function cleanData(&$str){
    	$str = preg_replace("/\t/", "\\t", $str);
    	$str = preg_replace("/\r?\n/", "\\n", $str);
    	if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
    }
}
