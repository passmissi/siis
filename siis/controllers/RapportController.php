<?php

namespace app\controllers;

use app\models\Communes;
use app\models\Options;
use kartik\mpdf\Pdf;
use Yii;

class RapportController extends \yii\web\Controller
{
	public $layout = 'main_dashboard2';
	public $comm;
	public $total;
	public $total_enfantgarcon05;
	public $total_enfantfille05;
	public $total_enfantgarcon518;
	public $total_enfantfille518;
	public $total_localite;
	public $total_f;
	public $total_m;
	
	public function actionDimensions()
	{
		$model = new Options();
		
		if ($model->load(Yii::$app->request->post()))
		{
			/* print_r($model->question);
			print_r($model->commune);
			print_r($model->section_communale);
			print_r($model->localite); */
			
			$options_array = array();
			$o = 0;
			foreach ($model->question as $c){
				$obj = Options::find()->where(['id' => $c])->one();
				$options_array[$o] = $obj;
				//print_r($options_array[$o]);
				$o += 1;
			}
			
			$content = $this->renderPartial('pdfstats', [
				'data' => $options_array,
				'commune' => $model->commune,
				'section_communale' => $model->section_communale,
				'localite' => $model->localite
			]);
			
			$nom = 'Statistiques Enquetes FAES - Avril 2015';
	
			$pdf = new Pdf();
			$pdf->mode = Pdf::MODE_UTF8;
			$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
			$pdf->defaultFontSize = 10;
			$pdf->defaultFont = 'helvetica';
			$pdf->format = array(216,285);
			$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
			$pdf->destination = Pdf::DEST_BROWSER;
			$pdf->content = $content;
			$pdf->options = [
				'title' => $nom,
				'autoScriptToLang' => true,
				'ignore_invalid_utf8' => true,
				'tabSpaces' => 4
			];
			$pdf->methods = [
				'SetHeader'=>['Donnee Enquetes Avril 2015'],
				'SetFooter'=>['{PAGENO}'],
			];
		
			return $pdf->render();
			
		}else{
			return $this->render('dimensions',[
				'model' => $model
			]);
		}
	}
	
	public function actionPdfcarnet450()
	{
		
	}
	
	public function actionListquestion($dimension)
	{
		$communes = Options::find()->select('question')->distinct()->where(['dimension' => $dimension])
		->orderBy('question')->all();
	
		$count = Options::find()->select('question')->distinct()->where(['dimension' => $dimension])
		->orderBy('question')->count();
	
		if($count > 0)
		{
			echo "<option value>--Choisir une question --</option>";
			foreach ($communes as $commun){
				echo "<option value='".$commun->id."'>".$commun->question."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir une question --</option>";
			return "<option></option>";
		}
	}
	
	public function actionSanitairethomassique()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1('Thomassique');
		$edh = $this->getLumiere('Thomassique');
		$cuisines = $this->getCuisines('Thomassique');
		$maisons = $this->getMaisons('Thomassique');
		$toits = $this->getToits('Thomassique');
		$kalite = $this->getTotalkalitekay('Thomassique');
		$latrine1 = $this->getTotallatrine('Thomassique', 1);
		$latrine2 = $this->getTotallatrine('Thomassique', 2);
		$latrine3 = $this->getTotallatrine('Thomassique', 3);
		$latrine7 = $this->getTotallatrine('Thomassique', 7);
		
		return $this->render('sanitairethomassique',[
			'comm'=>$this->comm,
			'total'=>$this->total,
			'edh'=>$edh,
			'cuisines' => $cuisines,
			'maisons' => $maisons,
			'toits' => $toits,
			'kalite' => $kalite,
			'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
		]);
	}
	
	public function actionSanitaireansespitre()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1('anse_pitres');
		$edh = $this->getLumiere('anse_pitres');
		$cuisines = $this->getCuisines('anse_pitres');
		$maisons = $this->getMaisons('anse_pitres');
		$toits = $this->getToits('anse_pitres');
		$kalite = $this->getTotalkalitekay('anse_pitres');
		$latrine1 = $this->getTotallatrine('anse_pitres', 1);
		$latrine2 = $this->getTotallatrine('anse_pitres', 2);
		$latrine3 = $this->getTotallatrine('anse_pitres', 3);
		$latrine7 = $this->getTotallatrine('anse_pitres', 7);
	
		return $this->render('sanitaireansespitre',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'edh'=>$edh,
				'cuisines' => $cuisines,
				'maisons' => $maisons,
				'toits' => $toits,
				'kalite' => $kalite,
				'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
				]);
	}
	
	public function actionSanitairethiotte()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1('thiotte');
		$edh = $this->getLumiere('thiotte');
		$cuisines = $this->getCuisines('thiotte');
		$maisons = $this->getMaisons('thiotte');
		$toits = $this->getToits('thiotte');
		$kalite = $this->getTotalkalitekay('thiotte');
		$latrine1 = $this->getTotallatrine('thiotte', 1);
		$latrine2 = $this->getTotallatrine('thiotte', 2);
		$latrine3 = $this->getTotallatrine('thiotte', 3);
		$latrine7 = $this->getTotallatrine('thiotte', 7);
	
		return $this->render('sanitairethiotte',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'edh'=>$edh,
				'cuisines' => $cuisines,
				'maisons' => $maisons,
				'toits' => $toits,
				'kalite' => $kalite,
				'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
				]);
	}
	
	public function actionSanitairegosier()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1('grand_gosier');
		$edh = $this->getLumiere('grand_gosier');
		$cuisines = $this->getCuisines('grand_gosier');
		$maisons = $this->getMaisons('grand_gosier');
		$toits = $this->getToits('grand_gosier');
		$kalite = $this->getTotalkalitekay('grand_gosier');
		$latrine1 = $this->getTotallatrine('grand_gosier', 1);
		$latrine2 = $this->getTotallatrine('grand_gosier', 2);
		$latrine3 = $this->getTotallatrine('grand_gosier', 3);
		$latrine7 = $this->getTotallatrine('grand_gosier', 7);
	
		return $this->render('sanitairegosier',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'edh'=>$edh,
				'cuisines' => $cuisines,
				'maisons' => $maisons,
				'toits' => $toits,
				'kalite' => $kalite,
				'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
				]);
	}
	
	public function actionSanitaireboucan()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1('boucan_carre');
		$edh = $this->getLumiere('boucan_carre');
		$cuisines = $this->getCuisines('boucan_carre');
		$maisons = $this->getMaisons('boucan_carre');
		$toits = $this->getToits('boucan_carre');
		$kalite = $this->getTotalkalitekay('boucan_carre');
		$latrine1 = $this->getTotallatrine('boucan_carre', 1);
		$latrine2 = $this->getTotallatrine('boucan_carre', 2);
		$latrine3 = $this->getTotallatrine('boucan_carre', 3);
		$latrine7 = $this->getTotallatrine('boucan_carre', 7);
	
		return $this->render('sanitairegosier',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'edh'=>$edh,
				'cuisines' => $cuisines,
				'maisons' => $maisons,
				'toits' => $toits,
				'kalite' => $kalite,
				'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
				]);
	}
	
	public function actionSanitairesautdeau()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal1("Saut d\'eau");
		$edh = $this->getLumiere("Saut d\'eau");
		$cuisines = $this->getCuisines("Saut d\'eau");
		$maisons = $this->getMaisons("Saut d\'eau");
		$toits = $this->getToits("Saut d\'eau");
		$kalite = $this->getTotalkalitekay("Saut d\'eau");
		$latrine1 = $this->getTotallatrine("Saut d\'eau", 1);
		$latrine2 = $this->getTotallatrine("Saut d\'eau", 2);
		$latrine3 = $this->getTotallatrine("Saut d\'eau", 3);
		$latrine7 = $this->getTotallatrine("Saut d\'eau", 7);
	
		return $this->render('sanitairesautdeau',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'edh'=>$edh,
				'cuisines' => $cuisines,
				'maisons' => $maisons,
				'toits' => $toits,
				'kalite' => $kalite,
				'latrine1' => $latrine1,
				'latrine2' => $latrine2,
				'latrine3' => $latrine3,
				'latrine7' => $latrine7,
				]);
	}
	
	public function actionGrandgosier()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal("grand_gosier");
		$this->total_localite = $this->getTotalPerLocalite("grand_gosier");
		$this->total_f = $this->getTotalFeminin("grand_gosier");
		$this->total_m = $this->getTotalMasculin("grand_gosier");
		$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05('grand_gosier');
		$this->total_enfantfille05 = $this->getTotalEnfantsFille05('grand_gosier');
		$this->total_enfantgarcon518 = $this->getTotalEnfants1518('grand_gosier');
		$this->total_enfantfille518 = $this->getTotalEnfants2518('grand_gosier');
	
		return $this->render('grandgosier',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'total_localite'=>$this->total_localite,
				'total_f'=>$this->total_f,
				'total_m'=>$this->total_m,
				'total_enfantgarcon05'=>$this->total_enfantgarcon05,
				'total_enfantfille05'=>$this->total_enfantfille05,
				'total_enfantgarcon518'=>$this->total_enfantgarcon518,
				'total_enfantfille518'=>$this->total_enfantfille518,
				]);
	}
	
	//thiotte
	public function actionThiotte()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal("thiotte");
		$this->total_localite = $this->getTotalPerLocalite("thiotte");
		$this->total_f = $this->getTotalFeminin("thiotte");
		$this->total_m = $this->getTotalMasculin("thiotte");
		$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05('thiotte');
		$this->total_enfantfille05 = $this->getTotalEnfantsFille05('thiotte');
		$this->total_enfantgarcon518 = $this->getTotalEnfants1518('thiotte');
		$this->total_enfantfille518 = $this->getTotalEnfants2518('thiotte');
	
		return $this->render('thiotte',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'total_localite'=>$this->total_localite,
				'total_f'=>$this->total_f,
				'total_m'=>$this->total_m,
				'total_enfantgarcon05'=>$this->total_enfantgarcon05,
				'total_enfantfille05'=>$this->total_enfantfille05,
				'total_enfantgarcon518'=>$this->total_enfantgarcon518,
				'total_enfantfille518'=>$this->total_enfantfille518,
				]);
	}
	
	public function actionAnsespitre()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal("anse_pitres");
		$this->total_localite = $this->getTotalPerLocalite("anse_pitres");
		$this->total_f = $this->getTotalFeminin("anse_pitres");
		$this->total_m = $this->getTotalMasculin("anse_pitres");
		$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05('anse_pitres');
		$this->total_enfantfille05 = $this->getTotalEnfantsFille05('anse_pitres');
		$this->total_enfantgarcon518 = $this->getTotalEnfants1518('anse_pitres');
		$this->total_enfantfille518 = $this->getTotalEnfants2518('anse_pitres');
	
		return $this->render('ansespitre',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'total_localite'=>$this->total_localite,
				'total_f'=>$this->total_f,
				'total_m'=>$this->total_m,
				'total_enfantgarcon05'=>$this->total_enfantgarcon05,
				'total_enfantfille05'=>$this->total_enfantfille05,
				'total_enfantgarcon518'=>$this->total_enfantgarcon518,
				'total_enfantfille518'=>$this->total_enfantfille518,
		]);
	}
	
	//boucan_carre Saut d\'eau
	public function actionSautdeau()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal("Saut d\'eau");
		$this->total_localite = $this->getTotalPerLocalite("Saut d\'eau");
		$this->total_f = $this->getTotalFeminin("Saut d\'eau");
		$this->total_m = $this->getTotalMasculin("Saut d\'eau");
		$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05("Saut d\'eau");
		$this->total_enfantfille05 = $this->getTotalEnfantsFille05("Saut d\'eau");
		$this->total_enfantgarcon518 = $this->getTotalEnfants1518("Saut d\'eau");
		$this->total_enfantfille518 = $this->getTotalEnfants2518("Saut d\'eau");
		
		return $this->render('sautdeau',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'total_localite'=>$this->total_localite,
				'total_f'=>$this->total_f,
				'total_m'=>$this->total_m,
				'total_enfantgarcon05'=>$this->total_enfantgarcon05,
				'total_enfantfille05'=>$this->total_enfantfille05,
				'total_enfantgarcon518'=>$this->total_enfantgarcon518,
				'total_enfantfille518'=>$this->total_enfantfille518,
				]);
	}
	
	public function actionBoucancarre()
	{
		$this->comm = $this->getCommunes();
		$this->total = $this->getTotal('boucan_carre');
		$this->total_localite = $this->getTotalPerLocalite('boucan_carre');
		$this->total_f = $this->getTotalFeminin('boucan_carre');
		$this->total_m = $this->getTotalMasculin('boucan_carre');
		$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05('boucan_carre');
		$this->total_enfantfille05 = $this->getTotalEnfantsFille05('boucan_carre');
		$this->total_enfantgarcon518 = $this->getTotalEnfants1518('boucan_carre');
		$this->total_enfantfille518 = $this->getTotalEnfants2518('boucan_carre');
		
		return $this->render('boucancarre',[
				'comm'=>$this->comm,
				'total'=>$this->total,
				'total_localite'=>$this->total_localite,
				'total_f'=>$this->total_f,
				'total_m'=>$this->total_m,
				'total_enfantgarcon05'=>$this->total_enfantgarcon05,
				'total_enfantfille05'=>$this->total_enfantfille05,
				'total_enfantgarcon518'=>$this->total_enfantgarcon518,
				'total_enfantfille518'=>$this->total_enfantfille518,
				]);
	}
	
    public function actionIndex()
    {
    	$this->comm = $this->getCommunes();
    	$this->total = $this->getTotal('Thomassique');
    	$this->total_localite = $this->getTotalPerLocalite('Thomassique');
    	$this->total_f = $this->getTotalFeminin('Thomassique');
    	$this->total_m = $this->getTotalMasculin('Thomassique');
    	$this->total_enfantgarcon05 = $this->getTotalEnfantsGarcons05('Thomassique');
    	$this->total_enfantfille05 = $this->getTotalEnfantsFille05('Thomassique');
    	$this->total_enfantgarcon518 = $this->getTotalEnfants1518('Thomassique');
    	$this->total_enfantfille518 = $this->getTotalEnfants2518('Thomassique');
    	
        return $this->render('index',[
        	'comm'=>$this->comm,
        	'total'=>$this->total,
        	'total_localite'=>$this->total_localite,
            'total_f'=>$this->total_f,
            'total_m'=>$this->total_m,
        	'total_enfantgarcon05'=>$this->total_enfantgarcon05,
        	'total_enfantfille05'=>$this->total_enfantfille05,
        	'total_enfantgarcon518'=>$this->total_enfantgarcon518,
        	'total_enfantfille518'=>$this->total_enfantfille518,
		]);
    }

    public function getTotallatrine($communes, $id)
    {
    	if($id == 3){
    		$sql = "SELECT count(*) as latrine FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F10` in (3,4,5,6)";
    	}else{
    		$sql = "SELECT count(*) as latrine FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F10` = $id";
    	}
    	
    	
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total[] = (int) $tot_p[$i]["latrine"];
    	}
    
    	return $total;
    }
    
    public function getTotalkalitekay($communes)
    {
    	$sql = "SELECT count(*) as kalite, `BIENS_HH_F6` as nbre FROM famille WHERE commune = '".$communes."' group by `BIENS_HH_F6`";
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    	   $total[$i][0] = (int) $tot_p[$i]["kalite"];
    	   $total[$i][1] = $tot_p[$i]["nbre"];
    	}
    
        return $total;
    }
    
    public function getToits($communes)
    {
    	$sql1 = "SELECT count(*) as toits FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F8_1` in (1,2,3)";
    
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["toits"];
    	}
    
    	return $total;
    }
    
    public function getMaisons($communes)
    {
    	$sql1 = "SELECT count(*) as maisons FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F9` in (1,2)";
    
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    	$total[] = (int) $tot_g[$i]["maisons"];
    	}
    
    	return $total;
    }
    
    public function getCuisines($communes)
    {
    	$sql1 = "SELECT count(*) as cuisines FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F11` in (1,2,3)";
    	 
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["cuisines"];
    	}
    
    	return $total;
    }
    
    public function getLumiere($communes)
    {
    	$sql1 = "SELECT count(*) as limye FROM famille WHERE commune = '".$communes."' and `BIENS_HH_F9_1` = 5";
    	
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["limye"];
    	}
    
    	return $total;
    }
    
    public function getCommunes(){
    	$com = Communes::find()->all();
    	
    	foreach($com as $c)
    	{
    		$communes[] = $c->n_communal;
    	}
    	return $communes;
    }
    
    public function getTotalPerLocalite($communes)
    {
    	$sql="select f.section_communale as section_communale, count(*) as total from membre m JOIN famille f on (m.famille = f.id) where (f.commune = '".$communes."') group by f.section_communale";
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total[$i][0] = (int) $tot_p[$i]["total"];
    		$total[$i][1] = $tot_p[$i]["section_communale"];
    	}
    			 
    	return $total;
    }
    
    public function getTotalEnfantsGarcons05($communes)
    {
    	$sql1="SELECT count(*) as total_garcon05
    	FROM membre m
    	JOIN famille f on (m.famille = f.id)
    	WHERE f.commune = '".$communes."'
    	and m.sexe = 1
    	and YEAR(NOW()) - m.annee_naissance <= 5";
    	
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
        
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["total_garcon05"];
    	}
    			 
    	return $total;
    }
    
    public function getTotalEnfantsFille05($communes)
    {
    	$sql1="SELECT count(*) as total_fille05
    	FROM membre m
    	JOIN famille f on (m.famille = f.id)
    	WHERE f.commune = '".$communes."'
    	and m.sexe = 2
    	and YEAR(NOW()) - m.annee_naissance <= 5";
    	
    	$connection = Yii::$app->db;
    
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    	$total[] = (int) $tot_g[$i]["total_fille05"];
    	}
    
    	return $total;
    }
    
    public function getTotalEnfants1518($communes)
    {
    	$sql1="SELECT count(*) as total_garcon518
    	FROM membre m
    	JOIN famille f on (m.famille = f.id)
    	WHERE f.commune = '".$communes."'
    	and m.sexe = 1
    	and YEAR(NOW()) - m.annee_naissance > 5 and YEAR(NOW()) - m.annee_naissance <= 18";
    	 
    	$connection = Yii::$app->db;
    	
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    	
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["total_garcon518"];
    	}
    	
    	return $total;
    }
    
    public function getTotalEnfants2518($communes)
    {
    	$sql1="SELECT count(*) as total_fille518
    	FROM membre m
    	JOIN famille f on (m.famille = f.id)
    	WHERE f.commune = '".$communes."'
    	and m.sexe = 2
    	and YEAR(NOW()) - m.annee_naissance > 5 and YEAR(NOW()) - m.annee_naissance <= 18";
    
    	$connection = Yii::$app->db;
    	 
    	$tot_g = $connection->createCommand($sql1)->queryAll();
    	 
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_g); $i++)
    	{
    		$total[] = (int) $tot_g[$i]["total_fille518"];
    	}
    			 
    	return $total;
    }
    
    public function getTotal1($communes)
    {
    
    	$sql="select count(*) as total from famille where (commune = '".$communes."')";
    	//SELECT COUNT(*) FROM `membre` m JOIN `famille` f on (m.famille = f.id) WHERE f.`localite` = 'Thomassique'
    	$connection = Yii::$app->db;
    
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    	$total[] = (int) $tot_p[$i]["total"];
    	}
    			 
    	return $total;
    }
    
    public function getTotal($communes)
    {
    
    	$sql="select count(*) as total from membre m JOIN famille f on (m.famille = f.id) where (f.commune = '".$communes."')";
    	//SELECT COUNT(*) FROM `membre` m JOIN `famille` f on (m.famille = f.id) WHERE f.`localite` = 'Thomassique'
    	$connection = Yii::$app->db;
    	 
    	$tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total[] = (int) $tot_p[$i]["total"];
    	}
    	
    	return $total;
    }
    
    public function getTotalFeminin($communes)
    {
    
    	$sql="select count(*) as total_f from famille f 
    		  JOIN membre m on (m.famille = f.id) 
    		  where m.sexe = 2 and (f.commune = '".$communes."')";
    			
        $connection = Yii::$app->db;
    
        $tot_p = $connection->createCommand($sql)->queryAll();
    
    	$total_f = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total_f[] = (int) $tot_p[$i]["total_f"];
    	}
    	
    	return $total_f;
    }
    
    public function getTotalMasculin($communes)
    {
    
    	$sql="select count(*) as total_m from famille f 
    		  JOIN membre m on (m.famille = f.id) 
    		  where m.sexe = 1 and (f.commune = '".$communes."')";
    	$connection = Yii::$app->db;
    
        $tot_p = $connection->createCommand($sql)->queryAll();
    
        $total_m = array();
    	for ($i = 0; $i < sizeof($tot_p); $i++)
    	{
    		$total_m[] = (int) $tot_p[$i]["total_m"];
    	}
    	
    	return $total_m;
    }
}
