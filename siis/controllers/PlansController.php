<?php

namespace app\controllers;

use Yii;
use app\models\Plans;
use app\models\ScrPlans;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\helpers\Url;
use app\models\User;
use app\models\Operateurs;
use Yii\db;
use yii\data\ActiveDataProvider;
use app\models\Activites;
/**
 * PlansController implements the CRUD actions for Plans model.
 */
class PlansController extends Controller
{
	public $layout = 'main_dashboard2';
	public $data = array();
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex2()
    {
    	$this->dataBindingPage();
    	$searchModel = new ScrPlans();
    	$model = new Plans();
    	$model->scenario = 'plants';
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$dataProvider = $searchModel->search17(Yii::$app->request->queryParams, $operateur->id);
    	
    	$dataProvider->pagination->pageSize=8;
    	
    	if ($model->load(Yii::$app->request->post())) {
    		//print_r($model->akf);
    		$time = new \DateTime('now', new \DateTimeZone('UTC'));
    		//$model->annee = $time->format('Y');
    		$model->ts = $operateur->id;
    		$model->commune = $operateur->commune_lie;
    		$model->is_sync = 1;
    		$model->mode = 2;
    		$model->isPlanned = 1;
    		$model->isPlannedBy = $operateur->id;
    		$model->isPlannedOn = date('Y-m-d');
        	$model->isMonitoring = 1;
        	$model->isDeleted = 0;
        	$model->isClosed = 0;
    		 
    		if($model->save(false)){
    			//print_r('ok');
    			return $this->redirect(['/activites/create2', 'id' => $model->id]);
    		}
    		//return $this->redirect(['create', 'id' => $operateur->id]);
    	} else {
    		return $this->render('index2', [
    				'searchModel' => $searchModel,
    				'dataProvider' => $dataProvider,
    				'activites' => $this->data[0],
    				'plan' => $this->data[1],
    				'akf' => $this->data[2],
    				'model' => $model
    		]);
    	}
    }
    /**
     * Lists all Plans models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$this->dataBindingPage();
        $searchModel = new ScrPlans();
        $model = new Plans();
        
        $user = User::findOne(Yii::$app->user->identity->id);
        $operateur = Operateurs::findOne($user->idOperateurs);
        
        $dataProvider = $searchModel->search12(Yii::$app->request->queryParams, $operateur->id);
        
        $dataProvider->pagination->pageSize=8;
        
        if ($model->load(Yii::$app->request->post())) {
        	//print_r($model->dimagi);
        	$time = new \DateTime('now', new \DateTimeZone('UTC'));
        	//$model->annee = $time->format('Y');
        	$model->ts = $operateur->id;
        	$model->commune = $operateur->commune_lie;
        	$model->is_sync = 1;
        	$model->mode = 1;
        	$model->isPlanned = 1;
        	$model->isPlannedBy = $operateur->id;
        	$model->isPlannedOn = date('Y-m-d');
        	$model->isMonitoring = 1;
        	$model->isDeleted = 0;
        	$model->isClosed = 0;
        	
        	if($model->save()){
        		return $this->redirect(['/activites/create', 'id' => $model->id]);
        	}
        	//return $this->redirect(['create', 'id' => $operateur->id]);
        } else {
        	return $this->render('index', [
        			'searchModel' => $searchModel,
        			'dataProvider' => $dataProvider,
        			'activites' => $this->data[0],
        			'plan' => $this->data[1],
        			'akf' => $this->data[2],
        			'model' => $model
        	]);
        }
        /*if(Yii::$app->request->queryParams){ print_r('wi');}
        else{ print_r('wi 4');}*/
        
        /*$dataProvider = new ActiveDataProvider([
        	'query' => Plans::find()->where(['ts' => $operateur->id]),
        	'pagination' => [
        		'pageSize' => 10,
        	],
        ]);*/
    }

    public function actionSuivipdf($id) {
    	$plan = Plans::findOne($id);
    	 
    	$data = new ActiveDataProvider([
    		'query' =>Activites::find()->where(['id_plan' => $id]),
    	]);
    	 
    	$content = $this->renderPartial('suivipdf', ['activites' => $data, 'plan' => $plan]);
    	
    	$pdf = new Pdf();
    	 
    	if($plan->mode == 1){
    		$operateur = Operateurs::findOne($plan->akf);
    		$pdf->filename = 'CF'.'-'.$operateur->nom.'-'.$operateur->prenom.'/'.date('Y-m-d');
    	}
    	else{
    		$operateur = Operateurs::findOne($plan->ts);
    		$pdf->filename = 'CF'.'-'.$operateur->nom.'-'.$operateur->prenom.'/'.date('Y-m-d');
    	}
    	 
    	$pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	 
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'SIIS Report Suivi'];
    	$pdf->methods = [
	    	'SetHeader'=>['SIIS Report Suivi'],
	    	'SetFooter'=>['{PAGENO}'],
    	];
    	
    	$pdf->options = [
	    	'title' => 'Fiche de Suivi',
	    	'autoScriptToLang' => true,
	    	'ignore_invalid_utf8' => true,
	    	'tabSpaces' => 4
    	];
    	
    	// return the pdf output as per the destination setting
    	return $pdf->render();
    }
    
    // Privacy statement output demo
    public function actionPdf($id) {
    	
    	$plan = Plans::findOne($id);
    	
    	$data = new ActiveDataProvider([
    		'query' =>Activites::find()->where(['id_plan' => $id]),
    	]);
    	
    	$content = $this->renderPartial('pdf', ['activites' => $data, 'plan' => $plan]);
    	 
    	$pdf = new Pdf();
    	
    	if($plan->mode == 1){
    		$operateur = Operateurs::findOne($plan->akf);
    		$pdf->filename = 'CF'.'-'.$operateur->nom.'-'.$operateur->prenom.'/'.date('Y-m-d');
    	}
    	else{
    		$operateur = Operateurs::findOne($plan->ts);
    		$pdf->filename = 'CF'.'-'.$operateur->nom.'-'.$operateur->prenom.'/'.date('Y-m-d');
    	}
    	
    	$pdf->content = Pdf::MODE_CORE;
    	$pdf->mode = Pdf::MODE_BLANK;
    	$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
    	//$pdf->cssInline = '.kv-heading-1{font-size:4px}';
    	$pdf->defaultFontSize = 10;
    	$pdf->defaultFont = 'helvetica';
    	$pdf->format = array(216,285);
    	$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
    	$pdf->destination = Pdf::DEST_BROWSER;
    	
    	$pdf->content = $content;
    	$pdf->options = ['title' => 'SIIS Report Planning'];
    	$pdf->methods = [
    	'SetHeader'=>['SIIS Report Header'],
    	'SetFooter'=>['{PAGENO}'],
    	];
    	$pdf->options = [
    		'title' => 'Plan de Travail',
    		'autoScriptToLang' => true,
    		'ignore_invalid_utf8' => true,
    		'tabSpaces' => 4
    	];
    	 
    	// return the pdf output as per the destination setting
    	return $pdf->render();
    }
    
    /**
     * Displays a single Plans model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$plan = Plans::findOne($id);
    	$op_akf = Operateurs::findOne($plan->akf);
    	
    	$data = new ActiveDataProvider([
    		'query' => Activites::find()->where(['id_plan' => $plan->id]),
    	]);
    	
    	$data->pagination->pageSize=8;
    	
        return $this->render('view', [
            'model' => $this->findModel($id),
        	'model1' => $op_akf,
        	'dataProvider' => $data
        ]);
    }

    /**
     * Creates a new Plans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new Plans();
        $operateur1 = Operateurs::findOne($id);
        $this->dataBindingPage();
        
        $user = User::findOne(Yii::$app->user->identity->id);
        $operateur = Operateurs::findOne($user->idOperateurs);
        
        $time = new \DateTime('now', new \DateTimeZone('UTC'));
        //$model->annee = $time->format('Y');
        $model->ts = $operateur->id;
        $model->akf = $id;
        $model->commune = $operateur->commune_lie;
        $model->mode = 1;
        $model->is_sync = 1;
        $model->isPlanned = 1;
        $model->isPlannedBy = $operateur->id;
        $model->isPlannedOn = date('Y-m-d');
        $model->isMonitoring = 1;
        $model->isDeleted = 0;
        $model->isClosed = 0;
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	Yii::$app->session->setFlash('kv-detail-success', 'Saved record successfully');
        	Yii::$app->session->setFlash('kv-detail-warning', 'A last warning for completing all data.');
        	Yii::$app->session->setFlash('kv-detail-info', '<b>Note:</b> You can proceed by clicking <a href="#">this link</a>.');
        	
            return $this->redirect(['/activites/create', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'model1' => $operateur1,
            	'plan' => $this->data[1],
            	'akf' => $this->data[2],
            	'dataProvider' => $this->data[0]
            ]);
        }
        
        /*$view = 'create';
        $record = new Plans();
        
        if ($record->load($_POST) && $record->save()) {
        	$record->refresh(); // just in case of some DB logic
        	$view = 'view';
        }
        
        return $this->renderAjax($view, [
        		'model' => $record,
        		]);*/
    }

    /**
     * Updates an existing Plans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCalendar($id)
    {
    	return $this->render('calendar', ['id' => $id]);
    }
    /**
     * Deletes an existing Plans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $id1)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/plans/index', 'id'=> $id1]);
    }

    public function actionDelete2($id)
    {
    	$this->findModel($id)->delete();
    
    	return $this->redirect(['/plans/index2']);
    }
    /**
     * Finds the Plans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function dataBindingPage()
    {
    	$data = array();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$this->data[0] = Activites::find()
    	->select('activites.*')
    	->leftJoin('plans', '`plans`.`id` = `activites`.`id_plan`')
    	->where(['plans.ts' => $operateur->id])
    	->with('plans')
    	->count();
    	
    	$this->data[1] = (new \yii\db\Query())
    	->from('plans')
    	->where(['ts' => $operateur->id])
    	->count();
    	 
    	$this->data[2] = (new \yii\db\Query())
    	->from('operateurs')
    	->where(['akf_lie' => $operateur->id])
    	->count();
    }
}
