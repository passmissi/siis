<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\models\User;
use app\modules\auth\models\ResetPasswordForm;
use app\models\Operateurs;
use app\models\Plans;
use app\models\Activites;
use Yii\db;
use PHPExcel;
use PHPExcel_IOFactory;
use app\models\Communes;
use app\models\Evenements;
use kartik\mpdf\Pdf;
use app\models\ScrPlans;
use yii\data\ActiveDataProvider;
use app\models\ScrOperateurs;
use app\models\FamilyAkf;
use app\models\TypeThemes;
use app\models\Monitoringplan;
use app\models\TypeActivites;
use app\models\Monitoringactivites;
use app\models\Monitoringsousactivites;
use app\models\SousActivites;
use app\models\Membre;
use app\models\Famille;
use app\models\ScrFamilyAkf;
use app\models\ScrMembre;
use app\models\Chroniques;
use app\models\ObjectifsVie;
use app\models\Mort;
use app\models\ScrMonitoringplan;
use app\models\ScrMonitoringsousactivites;
use app\models\TypeSousActivites;
use app\models\DataFamille;
use app\models\DataMembre;
use app\models\DataIntegration;
use app\models\DataTabulation;
use app\models\DataSummaryIntegration;
use app\models\DetailsSummaryIntegration;
use app\models\DataIntegrationEvenement;
use app\models\DataIntegrationSityasyonkay;
use yii\base\Exception;

class SocialworkerController extends \yii\web\Controller
{
	public $layout = 'main_dashboard2';
	public $modif = 0;
	public $modif_famille = 0;
	public $modif_membre = 0;
	//public $countplan;
	//public $countakf;
	//public $dataProvider;
	//public $operateur;
	
	public $data = array();
	
	/*public function getLocalite($communes)
	{
		$sql="select distinct localite from famille where commune = '".$communes."' order by localite";
		//SELECT DISTINCT `localite` FROM `famille` WHERE `commune` = 'Thomassique' ORDER BY `localite` DESC
		$connection = Yii::$app->db;
	
		$tot_p = $connection->createCommand($sql)->queryAll();
	
		$total = array();
		for ($i = 0; $i < sizeof($tot_p); $i++)
		{
			$total[] = $tot_p[$i]["localite"];
    	}
	
	    return $total;
	}*/
	
	public function actionOperateurs($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$sous = Operateurs::findOne($id);
		$this->modif = $id;
		return $sous;
	}
	
	public function actionMembre($famille, $membre)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$sous = Membre::find()->where(['famille' => $famille, 'memberid' => $membre])->one();
		$modif_membre = $membre;
		return $sous;
	}
	
	public function actionVerifplan($akf, $mois, $annee, $mode)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		
		//$time = new \DateTime('now', new \DateTimeZone('UTC'));
		//$annee = $time->format('Y');
		if($mode == 1){
			$sous = Plans::find()->where(['akf' => $akf, 'mois' => $mois, 'annee' => $annee,'mode' => $mode])->count();
		}elseif($mode == 2 && $akf == 0){
			$sous = Plans::find()->where(['mois' => $mois, 'annee' => $annee,'mode' => $mode])->count();
		}
		
		return $sous;
	}
	
	public function actionChroniques($famille, $membre)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$sous = Chroniques::find()->where(['famille' => $famille, 'memberid' => $membre])->one();
		if($sous != null){ $sous->is_ok = 1;}
		else{
			$a = new Chroniques();
			$a->is_ok = 0;
			return $a;
		}
		return $sous;
	}
	
	public function actionFamilles($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$sous = Famille::findOne($id);
		$this->modif_famille = $id;
		return $sous;
	}
	
	public function actionFamilyakf($localite)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result = array();
		
		$user = User::findOne(Yii::$app->user->identity->id);
		$operateur = Operateurs::findOne($user->idOperateurs);
		$commune = Communes::findOne($operateur->commune_lie);
		
		
		$qte_famille = Famille::find()->where(['localite' => $localite, 'commune' => $commune->n_communal])->count();
		$qte_famille_assignes = FamilyAkf::find()->where(['localite' => $localite, 'commune' => $commune->id_communal])->count();
		$result[0] = $qte_famille_assignes;
		$result[1] = $qte_famille - $qte_famille_assignes;
		return $result;
	}
	
	public function actionSousact($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$result = array();
		$sous = SousActivites::findOne($id);
		$result[0] = $sous->id;
		$result[1] = $sous->id_type_sous_activite;
		return $result;
	}
	
	public function actionList($id)
	{
		$count = TypeThemes::find()
		->where(['id_type_sous_activite' => $id])
		->count();
	
		$communes = TypeThemes::find()
		->where(['id_type_sous_activite' => $id])
		->all();
	    
		if($count > 0)
		{
			echo "<option value>--Choisir un type --</option>";
			foreach ($communes as $commune){
				echo "<option value='".$commune->id."'>".$commune->libelle."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir un type --</option>";
			return "<option></option>";
		}
	}
	
	public function actionListsectioncommunale($commune)
	{
		$communes = Famille::find()->select('section_communale')->distinct()->where(['commune' => $commune])
			->orderBy('section_communale')->all();
		
		$count = Famille::find()->select('section_communale')->distinct()->where(['commune' => $commune])
			->orderBy('section_communale')->count();
		
		if($count > 0)
		{
			echo "<option value>--Choisir une section communale --</option>";
			foreach ($communes as $commun){
				echo "<option value='".$commun->section_communale."'>".$commun->section_communale."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir un membre --</option>";
			return "<option></option>";
		}
	}
	
	public function actionListlocalite($commune)
	{
		$communes = Famille::find()->select('localite')->distinct()->where(['commune' => $commune])
		->orderBy('localite')->all();
	
		$count = Famille::find()->select('localite')->distinct()->where(['commune' => $commune])
		->orderBy('localite')->count();
	
		if($count > 0)
		{
			echo "<option value>--Choisir une section communale --</option>";
			foreach ($communes as $commun){
				echo "<option value='".$commun->localite."'>".$commun->localite."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir un membre --</option>";
			return "<option></option>";
		}
	}
	
	public function actionListmembre($id)
	{
		$count = Membre::find()
		->where(['famille' => $id])
		->count();
	
		$communes = Membre::find()
		->where(['famille' => $id])
		->all();
	
		if($count > 0)
		{
			echo "<option value>--Choisir un membre --</option>";
			foreach ($communes as $commune){
				echo "<option value='".$commune->memberid."'>".$commune->nom.' '.$commune->prenom.' - '.$commune->memberid."</option>";
			}
		}
		else
		{
			echo "<option value>--Choisir un membre --</option>";
			return "<option></option>";
		}
	}
	
	public function actionListmembre3($id)
	{
		$result = "";
		$communes = Membre::find()->where(['famille' => $id, 'memberid' => 1])->one();
		
		if($communes != null)$result = $communes->nom.' '.$communes->prenom;
		
		return $result;
	}
	
	public function actionListmembre2($id)
	{
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$count = Membre::find()
		->where(['famille' => $id])
		->count();
	
		$result = array();
		$result[0] = $count + 1;
		
		return $result;
	}
	
	public function actionListplan($id)
	{
		$count = Plans::find()
			->where(['mode' => 1, 'isMonitoring' => 1,'akf' => $id])
			->count();
		
		$communes = Plans::find()
			->where(['mode' => 1, 'isMonitoring' => 1,'akf' => $id])
		    ->all();
		
		if($count > 0)
		{
			echo "<option value>--Choisir un plan --</option>";
			foreach ($communes as $commune){
				$qte = Activites::find()->where(['mode' => 1,'id_plan' => $commune->id])->count();
				if($qte > 0){
					echo "<option value='".$commune->id."'>".$commune->mois.' '.$commune->annee."</option>";
				}
			}
		}
		else
		{
			echo "<option value>--Choisir un plan --</option>";
			return "<option></option>";
		}
	}
	
	public function actionListactivites($id)
	{
		$count = Activites::find()
			->where(['mode' => 1, 'id_plan' => $id])
			->count();
	
		$communes = Activites::find()
			->where(['mode' => 1, 'id_plan' => $id])
			->all();
	
		if($count > 0)
		{
			foreach ($communes as $commune){
				$type = TypeActivites::findOne($commune->id_type_activite);
				echo "<option value='".$commune->id."'>".$type->libelle.' '.$commune->date_activite."</option>";
			}
		}
		else
		{
			return "<option></option>";
		}
	}
	
	public function actionStats($mois, $annee)
	{
		$user = User::findOne(Yii::$app->user->identity->id);
		$operateur = Operateurs::findOne($user->idOperateurs);
		
		//$total_data = $this->getDataPerType($mois, $annee, $operateur->id);
		
		$model = new Monitoringsousactivites();
		$searchModel = new ScrMonitoringsousactivites();
    	$dataProvider = $searchModel->search21(Yii::$app->request->queryParams, $operateur->id, $mois, $annee);
    	
    	$i = $dataProvider->getCount();
    	
		$model1 = new Plans();
		
		return $this->render('stats', ['model' => $model, 'model1' => $model1,
				'searchModel' => $searchModel,'dataProvider' => $dataProvider]);
	}
	
	
	
	public function actionCarnet()
	{
		$model = new Operateurs();
		$model->scenario = 'carnet';
		$famille = new Famille();
		
		/* if(isset($_POST['Operateurs']))
		{
			return $this->redirect(['/socialworker/pdfcarnet', 'akf' => $_POST['Operateurs']['nomAKF']]);
		} */
		if(isset($_POST['Operateurs']))
		{
			return $this->redirect(['/socialworker/pdfcarnet44', 'akf' => $_POST['Operateurs']['nomAKF']]);
		}elseif (isset($_POST['Famille'])){
			return $this->redirect(['/socialworker/pdflocalitecarnet', 'localite' => $_POST['Famille']['localite'], 'famille' => 0]);
		}else{
			return $this->render('carnet', ['model' => $model, 'famille' => $famille]);
		}
		
		/*if ($model->load(Yii::$app->request->post())) {
			return $this->redirect(['/socialworker/pdfcarnet', 'akf' => $_POST['Operateurs']['nomAKF']]);
		} else {
			return $this->render('carnet', ['model' => $model, 'famille' => $famille]);
		}*/
	}
	
	public function actionPdflocalitecarnet($localite, $famille)
	{
		ini_set("memory_limit","-1");
		set_time_limit(0);
	
		$a = Famille::find()->where(['localite' => $localite])->count();
		
		if($famille != 0){
			$content = $this->renderPartial('pdfcarnet2', ['nbr' => $a, 'famille' => $famille]);
		}
		else{
			$content = $this->renderPartial('pdfcarnet2', ['nbr' => $a, 'localite' => $localite]);
		}
		
		$user = User::findOne(Yii::$app->user->identity->id);
		$operateur = Operateurs::findOne($user->idOperateurs);
		$nom = 'Carnet Familial';
	
		$pdf = new Pdf();
		$pdf->mode = Pdf::MODE_UTF8;
		$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
	
		$pdf->defaultFontSize = 10;
		$pdf->defaultFont = 'helvetica';
		$pdf->format = array(216,285);
		$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
		$pdf->destination = Pdf::DEST_BROWSER;
		$pdf->content = $content;
		$pdf->options = [
			'title' => $nom,
			'autoScriptToLang' => true,
			'ignore_invalid_utf8' => true,
			//'tabSpaces' => 4
		];
		$pdf->methods = [
			'SetHeader'=>['Carnet Familial Report Header'],
			'SetFooter'=>['{PAGENO}'],
		];
	
		return $pdf->render();
	}
	
	public function actionPdfcarnet44($akf)
	{
		ini_set('memory_limit', '2048M');
		set_time_limit(0);
	
		/* $data = new ActiveDataProvider([
				'query' =>FamilyAkf::find()->where(['operateur_akf' => $akf]),
		]); */
	
		$a = FamilyAkf::find()->where(['operateur_akf' => $akf])->count();
		$content = $this->renderPartial('pdfcarnet5', ['akf' => $akf, 'nb'=> $a]);
	
		$user = User::findOne(Yii::$app->user->identity->id);
		$operateur = Operateurs::findOne($user->idOperateurs);
		$nom = 'Carnet Familial pour akf '.$operateur->nom.''.$operateur->prenom;
	
		$pdf = new Pdf();
		$pdf->mode = Pdf::MODE_UTF8;
		$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
	
		$pdf->defaultFontSize = 10;
		$pdf->defaultFont = 'helvetica';
		$pdf->format = array(216,285);
		$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
		$pdf->destination = Pdf::DEST_BROWSER;
		$pdf->content = $content;
		$pdf->options = [
			'title' => $nom,
			'autoScriptToLang' => true,
			'ignore_invalid_utf8' => true,
		];
		$pdf->methods = [
			'SetHeader'=>['Carnet Familial Report Header'],
			'SetFooter'=>['{PAGENO}'],
		];
	
		return $pdf->render();
	}
	
	public function actionPdfcarnet($akf)
	{
		//$familyakf = new FamilyAkf();
		//$model = new Operateurs();
		//print_r($_POST['Operateurs']['nomAKF']);
		
		//return $this->render('pdfcarnet');
		ini_set('memory_limit', '2048M');
        set_time_limit(0);
        
		$data = new ActiveDataProvider([
			'query' =>FamilyAkf::find()->where(['operateur_akf' => $akf]),
		]);
		
		$a = FamilyAkf::find()->where(['operateur_akf' => $akf])->count();
		//print_r($data);
		$content = $this->renderPartial('pdfcarnet', ['data' => $data, 'nb'=> $a]);
		
		$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
		$nom = 'Carnet Familial pour akf '.$operateur->nom.''.$operateur->prenom;
		
		$pdf = new Pdf();
		$pdf->content = Pdf::MODE_CORE;
		$pdf->mode = Pdf::MODE_BLANK;
		$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
		//$pdf->cssInline = '.kv-heading-1{font-size:4px}';
		
		$pdf->defaultFontSize = 10;
		$pdf->defaultFont = 'helvetica';
		$pdf->format = array(216,285);
		//$pdf->format = Pdf::FORMAT_LETTER;
		$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
		$pdf->destination = Pdf::DEST_BROWSER;
		$pdf->content = $content;
		$pdf->options = [
		      'title' => $nom,
		      'autoScriptToLang' => true,
		      'ignore_invalid_utf8' => true,
		      'tabSpaces' => 4
	    ];
		$pdf->methods = [
			  'SetHeader'=>['Carnet Familial Report Header'],
			  'SetFooter'=>['{PAGENO}'],
			];
		
		return $pdf->render();
		/*$type = TypeThemes::find()->where(['id_type_sous_activite' => 4])->all();
		foreach ($type as $c){
			$nomcomplet = $c->libelle.' '.$c->id;
			print_r($nomcomplet);
		}*/
		
	}
	
	public function actionPdfcarnet3($akf)
	{
		ini_set('memory_limit', '-1');
		set_time_limit(0);
	
		$data = new ActiveDataProvider([
			'query' =>FamilyAkf::find()->where(['operateur_akf' => $akf]),
		]);
	
		$a = FamilyAkf::find()->where(['operateur_akf' => $akf])->count();
		$content = $this->renderPartial('pdfcarnet3', ['data' => $data, 'nb'=> $a]);
	
		$operateur = Operateurs::findOne($akf);
		$nom = 'Liste Famille de '.$operateur->nom.''.$operateur->prenom;
	
		$pdf = new Pdf();
		$pdf->mode = Pdf::MODE_UTF8;
		$pdf->cssFile = '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css';
		
		$pdf->defaultFontSize = 10;
		$pdf->defaultFont = 'helvetica';
		$pdf->format = Pdf::FORMAT_LETTER;
		$pdf->orientation = Pdf::ORIENT_LANDSCAPE;
		$pdf->destination = Pdf::DEST_BROWSER;
		$pdf->content = $content;
		$pdf->options = [
			'title' => $nom,
			'autoScriptToLang' => true,
			'ignore_invalid_utf8' => true,
			'tabSpaces' => 4
		];
		$pdf->methods = [
			'SetHeader'=>['Liste des Familles associees'],
			'SetFooter'=>['{PAGENO}'],
		];
	
		return $pdf->render();
	}
	
    public function actionIndex()
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	if(!Yii::$app->user->isGuest && ($operateur->fonction != 'OS'))
    	{
    		$modal = 0;
    		$model = new Operateurs();
    		$model->scenario = 'register';
    		$user = User::findOne(Yii::$app->user->identity->id);
    		$operateur = Operateurs::findOne($user->idOperateurs);
    		
    		$searchModel = new ScrOperateurs();
    		$dataProvider = $searchModel->search13(Yii::$app->request->queryParams, $operateur->id);
    		
    		$dataProvider->pagination->pageSize=8;
    		
    		$this->dataBindingPage();
    		
    		$formPost = Yii::$app->request->post('Operateurs');
    		 
    		if(isset($_POST['creation']))
    		{
    			if ($model->load(Yii::$app->request->post()))
    			{
    				$model->fonction = 'AKF';
    				$model->akf_lie = $operateur->id;
    				$model->commune_lie = $operateur->commune_lie;
    				$model->isActive = 1;
    				
    				if($model->save()){
    					$model2 = new Operateurs();
    					$model2->scenario = 'register';
    					return $this->render('index',
    						[ 'plan' => $this->data[0],
    						'akf' => $this->data[1],
    						'activites' => $this->data[2],
    						'dataProvider' => $dataProvider,
    						'searchModel' => $searchModel,
    						'model' => $model2,
    						'modal' => $modal
    					]);
    				}else{
    					$modal = 1;
    					$model2 = new Operateurs();
    					$model2->scenario = 'register';
    					return $this->render('index',
    						['plan' => $this->data[0],
    						'akf' => $this->data[1],
    						'activites' => $this->data[2],
    						'dataProvider' => $dataProvider,
    						'searchModel' => $searchModel,
    						'model' => $model2,
    						'modal' => $modal
    					]);
    				}
    			}
    		}elseif(isset($_POST['modification']))
    		{
    			$formPost = Yii::$app->request->post('Operateurs');
    			
    			$op = Operateurs::findOne($formPost['id']);
    			$op->scenario = 'register';
    			$op->nom = $formPost['nom'];
    			$op->prenom = $formPost['prenom'];
    			$op->email = $formPost['email'];
    			$op->sexe = $formPost['sexe'];
    			$op->update();
    			
    			$model2 = new Operateurs();
    			return $this->redirect(['index',
    			    'plan' => $this->data[0],
    				'akf' => $this->data[1],
    				'activites' => $this->data[2],
    				'dataProvider' => $dataProvider,
    				'searchModel' => $searchModel,
    				'model' => $model2,
    				'modal' => $modal
    			]);
    			
    			//print_r($formPost['id'].' '.$formPost['prenom'].' '.$op->id);
    			//alert('ok');
    		}else
    		{
    			$model2 = new Operateurs();
    			$model2->scenario = 'register';
    			return $this->render('index',
    				['plan' => $this->data[0],
    				'akf' => $this->data[1],
    				'activites' => $this->data[2],
    				'dataProvider' => $dataProvider,
    				'searchModel' => $searchModel,
    				'model' => $model2,
    				'modal' => $modal
    			]);
    		}
    		
    		/*if ($model->load(Yii::$app->request->post())) {
    			
    			if($id > 0){
    				print_r('ok');
    			}
    			else{
    				$model->fonction = 'AKF';
    				$model->akf_lie = $operateur->id;
    				$model->commune_lie = $operateur->commune_lie;
    				$model->isActive = 1;
    				
    				if($model->save()){
    				
    					return $this->render('index',
    					[ 'plan' => $this->data[0],
    					  'akf' => $this->data[1],
    					  'activites' => $this->data[2],
    					  'dataProvider' => $dataProvider,
    					  'searchModel' => $searchModel,
    					  'model' => $model,
    					  'modal' => $modal
    					]);
    				}else{
    					$modal = 1;
    					return $this->render('index',
    							['plan' => $this->data[0],
    							'akf' => $this->data[1],
    							'activites' => $this->data[2],
    							'dataProvider' => $dataProvider,
    							'searchModel' => $searchModel,
    							'model' => $model,
    							'modal' => $modal
    							]);
    				}
    			}
    		}
    		else {
    			return $this->render('index',
    				['plan' => $this->data[0],
    				 'akf' => $this->data[1],
    				 'activites' => $this->data[2],
    				 'dataProvider' => $dataProvider,
    				 'searchModel' => $searchModel,
    				 'model' => $model,
    				 'modal' => $modal
    			]);
    		}*/
    	}
    }

    public function actionSuiviakf2($id, $id2, $state)
    {
    	$model = new Monitoringsousactivites();
    	$model4 = new ObjectifsVie();
    	$model5 = new Evenements();
    	$plan = Plans::findOne($id);
    	$mplan = Monitoringplan::findOne($id2);
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$mactivites = Monitoringactivites::find()->where(['mplan' => $id2])->all();
    	$listact = array();
    	$i = 0;
    	
    	foreach ($mactivites as $c){
    		$listact[$i] = $c->activites;
    		$i += 1;
    	}
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => SousActivites::find()->where(['in', 'id_activite', $listact]),
    	]);
    	
    	$dataProvider2 = new ActiveDataProvider([
    		'query' => Monitoringsousactivites::find()->where(['mplan' => $id2]),
    	]);
    	
    	$dataProvider3 = new ActiveDataProvider([
    		'query' => ObjectifsVie::find()->where(['plan' => $id, 'mplan' => $id2]),
    	]);
    	
    	$dataProvider4 = new ActiveDataProvider([
    		'query' => Evenements::find()->where(['plan' => $id,'mplan' => $id2]),
    	]);
    	
    	$dataProvider->pagination->pageSize=5;
    	$dataProvider2->pagination->pageSize=5;
    	
    	//$formPost = Yii::$app->request->post('Membre');
    	
    	if (isset($_POST['creation']))
    	{
    		if ($model->load(Yii::$app->request->post()))
    		{
    			$sous = SousActivites::findOne($model->sousactivites);
    			$act = Activites::findOne($sous->id_activite);
    			$mact = Monitoringactivites::find()->where(['mplan' => $id2, 'plan' => $id, 'activites' => $act->id, 'mode' => 1])->one();
    			
    			switch ($sous->id_type_sous_activite) {
    				case 1:
    					$model->mplan = $id2;
    					$model->mactivites = $mact->id;
    					$model->plan = $id;
    					$model->activite = $act->id;
    					$model->sousactivites = $sous->id;
    					$model->mode = 1;
    					$model->type_sous_activites = $sous->id_type_sous_activite;
    					$model->isExecuted = 1;
    					$model->isExecutedOn = date('Y-m-d');
    					$model->isExecutedBy = $operateur->id;
    					$model->isActif = 1;
    					$model->isClosed = 1;
    			
    					if($model->save(false)){
    						$model = null;
    						$state = 1;
    						//print_r($model->sousactivites);
    						//print_r($act->id);
    						//print_r($id2);
    						return $this->redirect(['/socialworker/suiviakf2',
    							'id' => $id,
    							'id2' => $id2,
    							'state' => $state
    						]);
    					}else{
    						$state = 0;
    						
    						return $this->render('suiviakf2',
    			             	['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model,'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    					}
    					break;
    				case 2:
    					$model->mplan = $id2;
    					$model->mactivites = $mact->id;
    					$model->plan = $id;
    					$model->activite = $act->id;
    					$model->sousactivites = $sous->id;
    					$model->mode = 1;
    					$model->type_sous_activites = $sous->id_type_sous_activite;
    					$model->isExecuted = 1;
    					$model->isExecutedOn = date('Y-m-d');
    					$model->isExecutedBy = $operateur->id;
    					$model->isActif = 1;
    					$model->isClosed = 1;
    			
    					if($model->save(false)){
    						$model = null;
    						$state = 1;
    						return $this->redirect(['/socialworker/suiviakf2',
    							'id' => $id,
    							'id2' => $id2,
    							'state' => $state
    						]);
    					}else{
    						$state = 0;
    						
    						return $this->render('suiviakf2',
    				            ['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5,'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    					}
    					break;
    				case 3:
    					$model->mplan = $id2;
    					$model->mactivites = $mact->id;
    					$model->plan = $id;
    					$model->activite = $act->id;
    					$model->sousactivites = $sous->id;
    					$model->mode = 1;
    					$model->type_sous_activites = $sous->id_type_sous_activite;
    					$model->isExecuted = 1;
    					$model->isExecutedOn = date('Y-m-d');
    					$model->isExecutedBy = $operateur->id;
    					$model->isActif = 1;
    					$model->isClosed = 1;
    			
    					if($model->save(false)){
    						$model = null;
    						$state = 1;
    						return $this->redirect(['/socialworker/suiviakf2',
    								'id' => $id,
    								'id2' => $id2,
    								'state' => $state
    								]);
    					}else{
    						$state = 0;
    						
    						return $this->render('suiviakf2',
    			             	['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model,'model4' => $model4,'model5' => $model5,'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    					}
    					break;
    				case 4:
    					echo "Your favorite color is green!";
    					break;
    				case 5:
    					echo "Your favorite color is green!";
    					break;
    				case 6:
    					$model->mplan = $id2;
    					$model->mactivites = $mact->id;
    					$model->plan = $id;
    					$model->activite = $act->id;
    					$model->sousactivites = $sous->id;
    					$model->mode = 1;
    					$model->type_sous_activites = $sous->id_type_sous_activite;
    					$model->isExecuted = 1;
    					$model->isExecutedOn = date('Y-m-d');
    					$model->isExecutedBy = $operateur->id;
    					$model->isActif = 1;
    					$model->isClosed = 1;
    			
    					if($model->save(false)){
    						$model = null;
    						$state = 1;
    						return $this->redirect(['/socialworker/suiviakf2',
    								'id' => $id,
    								'id2' => $id2,
    								'state' => $state
    								]);
    					}else{
    						$state = 0;
    						
    						return $this->render('suiviakf2',
    				            ['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    					}
    					break;
    				case 7:
    					$model->mplan = $id2;
    					$model->mactivites = $mact->id;
    					$model->plan = $id;
    					$model->activite = $act->id;
    					$model->sousactivites = $sous->id;
    					$model->mode = 1;
    					$model->type_sous_activites = $sous->id_type_sous_activite;
    					$model->isExecuted = 1;
    					$model->isExecutedOn = date('Y-m-d');
    					$model->isExecutedBy = $operateur->id;
    					$model->isActif = 1;
    					$model->isClosed = 1;
    			
    					if($model->save(false)){
    						$model = null;
    						$state = 1;
    						return $this->redirect(['/socialworker/suiviakf2',
    							'id' => $id,
    							'id2' => $id2,
    							'state' => $state
    						]);
    					}else{
    						$model = null;
    						$state = 0;
    						
    						return $this->render('suiviakf2',
    				            ['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    					}
    					break;
    				default:
    					echo "Your favorite color is neither red, blue, nor green!";
    			}
    		}
    		//print_r("no");
    	}
    	elseif(isset($_POST['close'])){
    		$state = 1;
    		return $this->render('suiviakf2',
    				['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    	}
    	elseif(isset($_POST['objectif'])){
    		//print_r('ok');
    		
    		$connection = \Yii::$app->db;
    		$transaction = $connection->beginTransaction();
    		try {
    			if ($model4->load(Yii::$app->request->post()))
    			{
    				//print_r($model4->famille);
    				$model4->createdOn = date('Y-m-d');
    				$model4->date_objectif = date('Y-m-d');
    				$model4->plan = $id;
    				$model4->mplan = $id2;
    				$model4->createdBy = $operateur->id;
    				
    				if($model4->save()){
	    				$transaction->commit();
	    				$state = 1;
	    				$model4 = null;
	    				$model4 = new ObjectifsVie();
	    				return $this->render('suiviakf2',
	    						['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
	    			}
    			}
    		}
    		catch(Exception $e) {
    			$transaction->rollback();
    			$state = 0;
    			$model4 = null;
    			$model4 = new ObjectifsVie();
	    		return $this->render('suiviakf2',
	    				['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
	    	}
    	}
    	elseif(isset($_POST['evenements'])){
    		$connection = \Yii::$app->db;
    		$transaction = $connection->beginTransaction();
    		try {
    			if ($model5->load(Yii::$app->request->post()))
    			{
    				//print_r($model5->famille);
    				$model5->createdOn = date('Y-m-d');
    				$model5->date_evenements = date('Y-m-d');
    				$model5->plan = $id;
    				$model5->mplan = $id2;
    				$model5->createdBy = $operateur->id;
    				
    				if($model5->save()){
    					$transaction->commit();
    					$state = 1;
    					$model5 = null;
    					$model5 = new Evenements();
    					return $this->render('suiviakf2',
    							['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    				}
    			}
    		}
    		catch(Exception $e) {
    			$transaction->rollback();
    			$state = 0;
    			$model5 = null;
    			$model5 = new Evenements();
    			return $this->render('suiviakf2',
    					['model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    		}
    	}
    	else
    	{
    		$state = 1;
    		return $this->render('suiviakf2',
    				['dataProvider4' => $dataProvider4,'dataProvider3' => $dataProvider3,'model' => $model, 'model4' => $model4,'model5' => $model5, 'dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'state' => $state]);
    	}
    	//print_r($listact);
    }
    
    public function actionFamille()
    {
    	$model = new FamilyAkf();
    	$model2 = new Famille();
    	$searchModel = new ScrFamilyAkf();
    		
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    		 
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $operateur->id);
    		
    	$dataProvider->pagination->pageSize=7;
    		
    	$formPost = Yii::$app->request->post('Famille');
    		
    	if ($model2->load(Yii::$app->request->post()))
    	{
    		//print_r($formPost['id']);
    		$modif = Famille::findOne($formPost['id']);
    		$modif->commune = $formPost['commune'];
    		$modif->section_communale = $formPost['section_communale'];
    		$modif->localite = $formPost['localite'];
    		$modif->type_milieu = $formPost['type_milieu'];
    		$modif->telephone_maison = $formPost['telephone_maison'];
    		$modif->latitude_gps = $formPost['latitude_gps'];
    		$modif->longitude_gps = $formPost['longitude_gps'];
    		$modif->update();
    		
    		return $this->render('famille',[
    			'model' => $model,
    			'model2' => $model2,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    	else
    	{
    		return $this->render('famille',[
    			'model' => $model,
    			'model2' => $model2,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    }
    
    public function actionMembres($id)
    {
    	$model = new Membre();
    	$model4 = new Membre();
    	$model2 = new Chroniques();
    	$model3 = new Mort();
    	$searchModel = new ScrMembre();
    	$connection = \Yii::$app->db;
    	
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$dataProvider = $searchModel->search11(Yii::$app->request->queryParams, $operateur->id, $id);
    
    	$dataProvider->pagination->pageSize=7;
    
    	$formPost = Yii::$app->request->post('Membre');
    	$formPost2 = Yii::$app->request->post('Membre');
    	$formPost3 = Yii::$app->request->post('Chroniques');
    	$formPost4 = Yii::$app->request->post('Mort');
    	
    	if(isset($_POST['modification'])){
    		//print_r($formPost['prenom']);
    	    $model = Membre::find()->where(['famille' => $formPost['famille'], 'memberid' => $formPost['memberid']])->one();
    		
    		$parts = explode('-', $formPost['date']);
    		$annee = (int)$parts[0];
    		$mois = (int)$parts[1];
    		$jour = (int)$parts[2];
    		
    		$model->annee_naissance = $annee;
    		$model->mois_naissance = $mois;
    		$model->jour_naissance = $jour;
    		
    		$model->savoir_lire = $formPost['savoir_lire'];
    		$model->savoir_ecrire = $formPost['savoir_ecrire'];
    		$model->prenom = $formPost['prenom'];
    		$model->nom = $formPost['nom'];
    		$model->surnom = $formPost['surnom'];
    		$model->relation_chef_famille = $formPost['relation_chef_famille'];
    		$model->date = $formPost['date'];
    		$model->niveau_education = $formPost['niveau_education'];
    		$model->c3 = $formPost['c3'];
    		$model->pouvoir_travailler = $formPost['pouvoir_travailler'];
    		$model->avoir_emploi = $formPost['avoir_emploi'];
    		$model->avoir_activite_economique = $formPost['avoir_activite_economique'];
    		$model->update(false);
    		
    		return $this->render('membres',[
    			'model' => $model,
    			'model2' => $model2,
    			'model3' => $model3,
    			'model4' => $model4,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]); 
    	}
    	elseif (isset($_POST['creation'])){
    		if ($model2->load(Yii::$app->request->post()))
    		{
    			if($model2->save()){
    				return $this->render('membres',[
    					'model' => $model,
    					'model2' => $model2,
    					'model3' => $model3,
    					'model4' => $model4,
    					'dataProvider' => $dataProvider,
    					'searchModel' => $searchModel
    				]);
    			}
    		}
    	}
    	elseif (isset($_POST['modification2'])){
    		$model55 = Chroniques::find()->where(['famille' => $formPost3['famille'], 'memberid' => $formPost3['memberid']])->one();
    		
    		$model55->souffrir_maladie_chronique = $formPost3['souffrir_maladie_chronique'];
    		$model55->avoir_traitement = $formPost3['avoir_traitement'];
    		$model55->duree_traitement = $formPost3['duree_traitement'];
    		$model55->raison_non_traitement = $formPost3['raison_non_traitement'];
    		$model55->avoir_autres_maladies = $formPost3['avoir_autres_maladies'];
    		$model55->MANM_HH1_HH_MALAD_81_E2_2_81 = $formPost3['MANM_HH1_HH_MALAD_81_E2_2_81'];
    		$model55->MANM_HH1_HH_MALAD_81_E2_3_81 = $formPost3['MANM_HH1_HH_MALAD_81_E2_3_81'];
    		$model55->MANM_HH1_HH_MALAD_81_E2_4_81 = $formPost3['MANM_HH1_HH_MALAD_81_E2_4_81'];
    		$model55->MANM_HH1_HH_MALAD_81_E3_1_81 = $formPost3['MANM_HH1_HH_MALAD_81_E3_1_81'];
    		$model55->MANM_HH1_HH_MALAD_81_E3_2_81 = $formPost3['MANM_HH1_HH_MALAD_81_E3_2_81'];
    		$model55->MANM_HH1_HH_MALAD_81_E3_3_81 = $formPost3['MANM_HH1_HH_MALAD_81_E3_3_81'];
    		$model55->update();
    		
    		return $this->render('membres',[
    			'model' => $model,
    			'model2' => $model2,
    			'model3' => $model3,
    			'model4' => $model4,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    	elseif (isset($_POST['Mort'])){
    		//$formPost4 = Yii::$app->request->post('Mort');
    		
    		//if(isset($_POST['Mort']['MANM_HH1_ID_MANM_91_B1_2_91']))
    			//print_r($_POST['Mort']['MANM_HH1_ID_MANM_91_B1_2_91']);
    		//print_r($formPost4['id_membre']);
    		$id_membre = $formPost4['id_membre'];
    		$transaction = $connection->beginTransaction();
    		try {
	    		  if ($model3->load(Yii::$app->request->post()))
	    		  {
	    		  	    //print_r($model3->famille.'-'.$model3->id_membre.'-');
	    				if($model3->save()){
	    					//print_r('ok');
	    					$model74 = Membre::find()->where(['famille' => $formPost4['famille'], 'memberid' => $formPost4['id_membre']])->one();
	    					$model74->delete();
	    				}
	    				
	    				$transaction->commit();
	    				return $this->render('membres',[
	    					'model' => $model,
	    					'model2' => $model2,
	    					'model3' => $model3,
	    					'model4' => $model4,
	    					'dataProvider' => $dataProvider,
	    					'searchModel' => $searchModel
	    				]);
	    		  }
    		}
    		catch(Exception $e) {
    			$transaction->rollback();
    			print_r('No');
    		}
    	}
    	elseif (isset($_POST['membre'])){
    		//print_r($formPost2['nom']);
    		//print_r($formPost2['famille']);
    		if ($model4->load(Yii::$app->request->post()))
    		{
    			$count = Membre::find()->where(['famille' => $model4->famille])->count() + 1;
    			$model4->memberid = $count;
    			//print_r($model2->memberid);
    			if($model4->save(false)){
    				return $this->render('membres',[
    					'model' => $model,
    					'model2' => $model2,
    					'model3' => $model3,
    					'model4' => $model4,
    					'dataProvider' => $dataProvider,
    					'searchModel' => $searchModel
    				]);
    			}
    		}
    	}
    	else
    	{
    		return $this->render('membres',[
    			'model' => $model,
    			'model2' => $model2,
    			'model3' => $model3,
    			'model4' => $model4,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    }
    
    public function actionSuiviplan3($id)
    {
    	$model = new Monitoringplan();
    	$plan = Plans::findOne($id);
    	$plan->isClosed = 0;
    	 
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    
    	$dataProvider = new ActiveDataProvider([
    		'query' => Plans::find()->where(['ts' => $operateur->id, 'mode' => 1, 'commune' => $operateur->commune_lie]),
    	]);
    
    	$dataProvider->pagination->pageSize=8;
    	 
    	if($plan->update()){
    		return $this->redirect(['suiviplan',
    				'model' => $model, 'dataProvider' => $dataProvider]);
    	}else{
    		return $this->render('suiviplan',
    				['model' => $model, 'dataProvider' => $dataProvider]);
    	}
    }
    
    public function actionSuiviplan2($id)
    {
    	$model = new Monitoringplan();
    	$plan = Plans::findOne($id);
    	$plan->isClosed = 1;
    	
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$dataProvider = new ActiveDataProvider([
    		'query' => Plans::find()->where(['ts' => $operateur->id, 'mode' => 1, 'commune' => $operateur->commune_lie]),
    	]);
    	 
    	$dataProvider->pagination->pageSize=8;
    	
    	if($plan->save()){
    		return $this->redirect(['suiviplan',
    				'model' => $model, 'dataProvider' => $dataProvider]);
    	}else{
    		return $this->render('suiviplan',
    				['model' => $model, 'dataProvider' => $dataProvider]);
    	}
    }
    
    public function actionAssignation()
    {
    	$model = new FamilyAkf();
    	$model1 = new Operateurs();
    	$searchModel = new ScrFamilyAkf();
    	
    	$connection = \Yii::$app->db;
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$commune = Communes::findOne($operateur->commune_lie);
    	
    	$dataProvider = $searchModel->search44(Yii::$app->request->queryParams, $operateur->id);
    	
    	if(isset($_POST['Operateurs']))
    	{
    		//print_r($_POST['Operateurs']['id']);
    		return $this->redirect(['/socialworker/pdfcarnet3', 'akf' => $_POST['Operateurs']['id']]);
    	}
    	elseif (isset($_POST['creation']))
    	{
    		$formPost = Yii::$app->request->post('FamilyAkf');
    		
	    	    $transaction = $connection->beginTransaction();
	    		try {
	    			if ($model->load(Yii::$app->request->post()))
	    			{
	    				$famille_assignes_array = array();
	    				$famille_assignes = FamilyAkf::find()->where(['localite' => $model->localite])->all();
	    				 
	    				$o = 0;
	    				foreach ($famille_assignes as $c){
	    					$famille_assignes_array[$o] = $c->famille;
	    					$o += 1;
	    				}
	    				 
	    				$famille_a_assignes_array = array();
	    				$famille_a_assignees = Famille::find()->where(['localite' => $model->localite])
	    				->andWhere(['NOT IN', 'id', $famille_assignes_array])->all();
	    				 
	    				$z = 0;
	    				foreach ($famille_a_assignees as $d){
	    					$famille_a_assignes_array[$z] = $d->id;
	    					$z += 1;
	    				}
	    				 
	    				 
	    				$arr_length = sizeof($famille_a_assignes_array);
	    				//print_r($arr_length);
	    				//$attrakf = $formPost['localite'];
	    				//$attrak = $formPost['quantite'];
	    				//print_r($attrak);
	    				//familyakf-quantite
	    				 
	    				for($i=0;$i<$arr_length;$i++)
	    				{
		    				$j = $i + 1;
		    				if($model->quantite >= $j){
		    					//print_r($famille_a_assignes_array[$i].'-');
		    					$data = new FamilyAkf();
		    					$data->famille = $famille_a_assignes_array[$i];
		    					$data->operateur_akf = $model->operateur_akf;
		    					$data->operateur_ts = $operateur->id;
		    					$data->commune = $commune->id_communal;
		    					$data->localite = $model->localite;
		    					$data->createdOn = date('Y-m-d');
		    					$data->save(false);
		    				}else {break;}
	    				}
	    				 
	    				$transaction->commit();
	    				return $this->render('assignation', [
		    				'model' => $model,
		    				'model1' => $model1,
		    				'dataProvider' => $dataProvider,
		    				'searchModel' => $searchModel
	    				]);
	    		}
	    	}
	    	catch(Exception $e) {
	    		 $transaction->rollback();
	    		 print_r('No');
	    	} 
    	}
    	else
    	{
    		return $this->render('assignation', [
    				'model' => $model,
    				'model1' => $model1,
    				'dataProvider' => $dataProvider,
    				'searchModel' => $searchModel
    				]);
    	}
    }
    
    public function actionDesassignation()
    {
    	$model = new FamilyAkf();
    	$searchModel = new ScrFamilyAkf();
    	 
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams, $operateur->id);
    	 
    	$dataProvider->pagination->pageSize=7;
    	 
    	$formPost = Yii::$app->request->post('FamilyAkf');
    	 
    	if ($model->load(Yii::$app->request->post()))
    	{
    		$modif = FamilyAkf::findOne($formPost['famille'], $formPost['operateur_akf']);
    		$modif->operateur_akf = $model->operateur_akf;
    		$modif->update(false);
    		
    		return $this->render('desassigner',[
    			'model' => $model,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    	else
    	{
    		return $this->render('desassigner',[
    			'model' => $model,
    			'dataProvider' => $dataProvider,
    			'searchModel' => $searchModel
    		]);
    	}
    }
    
    public function actionSuiviplan()
    {
    	$model = new Monitoringplan();
    	$searchModel = new ScrPlans();
    	
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$dataProvider = $searchModel->search19(Yii::$app->request->queryParams, $operateur->id);
    	
    	$dataProvider->pagination->pageSize=8;
    	
    	return $this->render('suiviplan',
    			['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }
    
    public function actionMassive()
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	if($operateur->fonction == 'OS'){
    		$model = new Monitoringplan();
    		$model->isExecuted = 1;
    		$model->isExecutedOn = date('Y-m-d');
    		$model->isExecutedBy = $operateur->id;
    		$model->isClosed = 1;
    		$model->mode = 1;
    		$model->isActif = 1;
    		$model->isReg = 0;
    	}
    	elseif ($operateur->fonction == 'TS'){
    		$model = new Monitoringplan();
    		$model->isExecuted = 1;
    		$model->isExecutedOn = date('Y-m-d');
    		$model->isExecutedBy = $operateur->id;
    		$model->isClosed = 1;
    		$model->mode = 1;
    		$model->isActif = 1;
    		$model->isReg = 0;
    		$model->ts_nreg = $operateur->id;
    	}
    	
    	$searchModel = new ScrMonitoringplan();
    	$dataProvider = $searchModel->search11(Yii::$app->request->queryParams);
    	 
    	$dataProvider->pagination->pageSize=8;
    	 
    	if ($model->load(Yii::$app->request->post()) && $model->save(false))
    	{
    		return $this->redirect(['suivimassive',
    				'id' => $model->id , 'id1' => $model->akf_nreg/*, 'dataProvider' => $dataProvider*/]);
    	}
    	else
    	{
    		$model = new Monitoringplan();
    		return $this->render('massive',['model' => $model, 'dataProvider' => $dataProvider]);
    	}
    }
    
    public function actionSuivimassive($id, $id1)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	 
    	$monitoringplan = Monitoringplan::findOne($id);
    	
    	$model = new Monitoringsousactivites();
    	$model->mplan = $id;
    	$model->isExecuted = 1;
    	$model->isExecutedOn = date('Y-m-d');
    	$model->isExecutedBy = $operateur->id;
    	$model->isClosed = 1;
    	$model->mode = 1;
    	$model->isActif = 1;
    	$model->isReg = 0;
    	$model->akf_reg = $monitoringplan->akf_nreg;
    	//pp
    	$searchModel = new ScrMonitoringsousactivites(); 
    	$dataProvider = $searchModel->search13(Yii::$app->request->queryParams, $id1, $id);
    	
    	$dataProvider->pagination->pageSize=8;
    	
    	
        if ($model->load(Yii::$app->request->post()) && $model->save(false))
    	{
    		return $this->render('suivimassive',['model' => $model, 'dataProvider' => $dataProvider]);
    	} 
    	else
    	{
    		return $this->render('suivimassive',['model' => $model, 'dataProvider' => $dataProvider]);
    	}
    }
    
    public function actionSuiviakf()
    {
    	$model = new Monitoringplan();
    	$list = array();
    	$connection = \Yii::$app->db;
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$prob = 0;
    	
    	$formPost = Yii::$app->request->post('Monitoringplan');
    	
    	$transaction = $connection->beginTransaction();
    	try {
    		if ($model->load(Yii::$app->request->post()))
    		{
    			$attrakf = $formPost['akf'];
    			$attrplan = $formPost['Plan'];
    			$list = $formPost['listact'];
    			$arr_length = count($list);
    			
    			if (is_array($list) && $arr_length > 0 && $attrakf != null && $attrplan != null)
    			{
    				$plan = Plans::findOne($attrplan);
    				$plan->isMonitoring = 0;
    				$plan->isExecuted = 1;
    				$plan->isExecutedOn = date('Y-m-d');
    				$plan->isExecutedBy = $attrakf;
    				$plan->save();
    				
    				$monitoringplan = new Monitoringplan();
    				$monitoringplan->Plan = $attrplan;
    				$monitoringplan->isExecuted = 1;
    				$monitoringplan->isExecutedOn = date('Y-m-d');
    				$monitoringplan->isExecutedBy = $operateur->id;
    				$monitoringplan->isActif = 1;
    				$monitoringplan->isClosed = 1;
    				$monitoringplan->mode = 1;
    				$monitoringplan->save(false);
    				
    				for($i=0;$i<$arr_length;$i++)
    				{
    					$act = Activites::findOne($list[$i]);
    					$act->isDone = 1;
    					$act->isDoneOn = date('Y-m-d');
    					$act->isDoneBy = $attrakf;
    					$act->isVerified = 1;
    					$act->isChanged = 1;
    					$act->save();
    					
    					$monitoringactivites = new Monitoringactivites();
    					$monitoringactivites->mplan = $monitoringplan->id;
    					$monitoringactivites->plan = $attrplan;
    					$monitoringactivites->activites = $list[$i];
    					$monitoringactivites->mode = 1;
    					$monitoringactivites->type_activite = $act->id_type_activite;
    					$monitoringactivites->isExecuted = 1;
    					$monitoringactivites->isExecutedOn = date('Y-m-d');
    					$monitoringactivites->isExecutedBy = $operateur->id;
    					$monitoringactivites->isActif = 1;
    					$monitoringactivites->isDone = 1;
    					$monitoringactivites->save();
    					
    				}
    				
    				$transaction->commit();
    				
    				return $this->redirect(['/socialworker/suiviakf2',
    					'id' => $attrplan, 
    					'id2' => $monitoringplan->id,
    					'state' => 0
    	            ]);
    			}
    			else
    			{
    				$prob = 1;
    				return $this->render('suiviakf', ['model' => $model, 'prob' => $prob]);
    			}
    		}
    		else
    		{
    			return $this->render('suiviakf', ['model' => $model, 'prob' => $prob]);
    		}
    	}
    	catch(Exception $e) {
    		$transaction->rollback();
    		print_r('No');
    	}
    }
    
    public function actionPlan($id)
    {
    	$operateur = Operateurs::findOne($id);
    	$this->dataBindingPage();
    	$model = new Plans();
    	
    	return $this->render('plan', [
    		'model1' => $operateur,
    		'model' => $model,
    		'plan' => $this->data[1],
    		'akf' => $this->data[2],
    		'dataProvider' => $this->data[0]
    	]);
    }
    
    public function actionDetailsplan($id)
    {
    	$this->dataBindingPage();
    	$searchModel = new ScrPlans();
    	
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$dataProvider = $searchModel->search15(Yii::$app->request->queryParams, $operateur->id, $id);
    	
    	$dataProvider->pagination->pageSize=8;
    	
    	return $this->render('detailsplan', [
    		'searchModel' => $searchModel,
    		'dataProvider' => $dataProvider,
    		'activites' => $this->data[0],
    		'plan' => $this->data[1],
    		'akf' => $this->data[2],
    	]);
    }
    
    public function actionProfile($id)
    {
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	$model = new User();
    	$response = 0;
    	
    	if(isset($_POST['creation1'])){
    		$response = Yii::$app->getSecurity()->validatePassword($_POST['User']['password'], $user->password_hash);
    		
    		if($response == 1){
    			$utilisateur = User::findOne(Yii::$app->user->identity->id);
    			$utilisateur->password_hash = Yii::$app->getSecurity()->generatePasswordHash($_POST['User']['new_password']);
    			$utilisateur->auth_key = Yii::$app->getSecurity()->generateRandomString();
    			$utilisateur->update();
    			
    			return $this->render('profile' ,[
    				'model' => $model,
    				'response' => $response
    			]);
    		}else{
    			$response = -1;
    			
    			return $this->render('profile' ,[
    				'model' => $model,
    				'response' => $response
    			]);
    		}
    		
    		
    		//print_r( Yii::$app->user->identity->id );
    		/* return $this->redirect(['/socialworker/pdfcarnet3', 'akf' => $_POST['Operateurs']['id']]);
    		$user = User::findOne(Yii::$app->user->identity->id);
    		$operateur = Operateurs::findOne($user->idOperateurs);
    		 
    		$token = 'ok';
    		$model = new ResetPasswordForm($token); */
    	}elseif (isset($_POST['creation'])){
    		$formPost = Yii::$app->request->post('Operateurs');
    		//print_r('ok');
    		/**
    		 if ($model->load(Yii::$app->request->post()))
		    	{
		    		$modif = FamilyAkf::findOne($formPost['famille'], $formPost['operateur_akf']);
		    		$modif->operateur_akf = $model->operateur_akf;
		    		$modif->update(false);
		    		
		    		return $this->render('desassigner',[
		    			'model' => $model,
		    			'dataProvider' => $dataProvider,
		    			'searchModel' => $searchModel
		    		]);
		    	}
    		 * **/
    		$modif = Operateurs::findOne($formPost['id']);
    		$modif->nom = $formPost['nom'];
    		$modif->prenom = $formPost['prenom'];
    		$modif->sexe = $formPost['sexe'];
    		$modif->email = $formPost['email'];
    		$modif->update();
    		
    		return $this->render('profile' ,[
    			'model' => $model,
    			'response' => $response
    		]);
    	}else{
    		return $this->render('profile',[
    			'model' => $model, 
    			'response' => $response
    		]);
    	}
    }
    
    public function actionDailyplanning($id)
    {
    	$time = new \DateTime('now');
    	$today = $time->format('Y-m-d');
    	
    	//$jour1 = strtotime($today."+ 4 days");
    	//$jour2 = strtotime($time."- 4 days");
    	//echo date('Y-m-d', strtotime($today. ' + 1 days'));
    	
    	//print_r(date('Y-m-d', strtotime("+2 days")));
    	
    	$query = Activites::find()
    	->joinWith('idPlan')
    	->where(['plans.akf' => $id])
    	->andWhere(['between', 'date_activite', date('Y-m-d', strtotime("-4 days")), date('Y-m-d', strtotime("+4 days"))])
    	->orderBy('date_activite');
    	
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    	]);
    	
    	/*$query->andFilterWhere([
    		'>=', 'date_activite', $today
    	]);*/
    	
    	$query->limit(7);
    	$dataProvider->pagination->pageSize=8;
    	
    	//print_r($dataProvider);
    	return $this->render('dailyplannig',
    			['id' => $id, 'dataProvider' => $dataProvider]);
    }
    
    private function dataBindingPage()
    {
    	$data = array();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$this->data[0] = (new \yii\db\Query())
    		->from('plans')
    		->where(['ts' => $operateur->id])
    		->count();
    	
    	$this->data[1] = (new \yii\db\Query())
    		->from('operateurs')
    		->where(['akf_lie' => $operateur->id])
    		->count();
    	
    	$this->data[2] = Activites::find()
    		->select('activites.*')
    		->leftJoin('plans', '`plans`.`id` = `activites`.`id_plan`')
    		->where(['plans.ts' => $operateur->id])
    		->with('plans')
    		->count();
    }
    
    public function actionExport2($id, $id2){
    	
    	$plans = Plans::findOne($id);
    	$plan = new Plans();
    	$list_file = array();
    	$user = User::findOne(Yii::$app->user->identity->id);
    	$operateur = Operateurs::findOne($user->idOperateurs);
    	
    	$start = "$plans->annee-".$plan->getMonthNumber($plans->mois)."-01";
    	$end = "$plans->annee-".$plan->getMonthNumber($plans->mois)."-28";
    	$akf = Operateurs::findOne($plans->akf);
    	$commune = Communes::findOne($akf->commune_lie);
    	$username_akf = $akf->username_dimagi;
    	$connection = \Yii::$app->db;
    
    	$i = 0;
    	if($id2 == 0){
    		$activites = Activites::find()->where(['id_plan' => $id])->all();
    	}else{
    		$activites = Activites::find()->where(['id' => $id2])->all();
    	}
    	
    	foreach ($activites as $d){
    		
    		if($d->isUploaded == 1 && $d->case_id != '')continue;
    		
    		$type = TypeActivites::findOne($d->id_type_activite);
    		$name = "temp_dimagi/".$plans->id.$plans->annee.$plans->commune.$plans->ts.$plans->akf.$i.'.xml';
    		$instance = Plans::v4();
    		$type1 = array();
    		$type1 = $this->casename($type->id, 'Promotion');
    		$nom_famille = '';
    		
    		if($type1["act"] == 'visite_domiciliare'){
    			$mem = Membre::find()->where(['famille' => $d->id_famille, 'memberid' => 1])->one();
    			if($mem != null)$nom_famille = $mem->nom.' '.$mem->prenom;
    		}
    		
    		$data = $plan->getDataHeaderXMLDimagi('Data provided by SIIS', $start,
    				$end, $username_akf, $instance);
    		
    		$adata = $plan->getDataXMLDimagi($d->case_id, 
    			    $d->date_activite, $type1["act"], 
    				$akf->location_id, 'activite', 
    				$type1["act"], $d->lieu, 
    				$d->description, $type1["sact"], 
    				$d->date_activite, $nom_famille, 
    				$d->id_famille, $akf->operateur_id, 
    				$akf->username_dimagi);
    		
    		$data .= $adata;
    		
    		$data .= $plan->getDataEndXMLDimagi();
    		
    		try {
    			$myfile = fopen($name, "w") or die("Unable to open file!");
    			fwrite($myfile, $data);
    			fclose($myfile);
    			$list_file[$i]["file"] = $name;
    			$message = $this->Upload($name);
    			
    			if($message == "Data sent. Success."){
    				$list_file[$i]["dimagi"] = 1;
    				unlink($name);
    				
    				$transaction = $connection->beginTransaction();
    				try {
    					
    					$ua = Activites::findOne($d->id);
    					//$usa = SousActivites::findOne($d->id);
    					
    					$ua->isUploaded = 1;
    					$ua->isUploadedBy = date('Y-m-d');
    					$ua->isUploadedBy = $operateur->id;
    					$ua->update();
    					
    					//$usa->isVerified = 1;
    					//$usa->isChanged = 1;
    					//$usa->update();
    					
    					$transaction->commit();
    					$list_file[$i]["server"] = 1;
    				}catch(Exception $e) {
    					$transaction->rollback();
    					$list_file[$i]["server"] = 0;
    					print_r('No');
    				}
    			}else{
    				$list_file[$i]["dimagi"] = 0;
    				$list_file[$i]["server"] = 0;
    				unlink($name);
    			}
    			
    			$data = "";
    			$instance = "";
    			$name = "";
    		}catch (Exception $e) {
    			unlink($name);
    			//$message = $e->errorMessage();
    		}
    		
    		$i += 1;
    		//print_r($type1);
    	}
    	
    	$j = $this->isOK($list_file);
    	//print_r($j);
    	//print_r($list_file);
    	$dataProvider = new ActiveDataProvider([
    		'query' => Activites::find()->where(['id_plan' => $id]),
    	]);
    	
    	$dataProvider->pagination->pageSize= 10;
    	
    	return $this->render('/socialworker/dimagi',[
    		'id' => $id,
    		'list_file' => $list_file,
    		'dataProvider' => $dataProvider,
    		'isOK' => $j
    	]);
    }
    
    public function isOK($list){
    	$result = 0;
    	$qte = sizeof($list);
    	
    	for ($i = 0; $i < $qte; $i++) {
	    	if($list[$i]["dimagi"] == 1){ 
	    		$result += 1;
	    	}
	    }
    	
    	return $result;
    }
    
    public function casename($activite, $sousactivite){
    	$result = array();
    	$a = ""; 
    	$c = "ok";
    	
    	$activity_name = ["nouvelle_enregistrement", "visite_domiciliare", "poste_rassanblement", "club_meres", 
    			"club_peres", "club_jeune", "visite_ecole", "formation", "recontre", "administration"];
    	
    	$type = ["objektif_famille", "gwo_evenman_ki_pase_nan_fanmi", "vaksinasyon_ak_antropometri", 
    			"kominikasyon_pou_chanjman_konpotman", "distribisyon_pwodui_esansyel", "referans", "fomasyon"];
    	
    	if($activite == 1){
    		$c = $activity_name[1];
    	}elseif($activite == 2){
    		$c = $activity_name[2];
    	}elseif($activite == 3){
    		$c = $activity_name[3];
    	}elseif($activite == 4){
    		$c = $activity_name[5];
    	}elseif($activite == 5){
    		$c = $activity_name[6];
    	}elseif($activite == 6){
    		$c = $activity_name[8];
    	}elseif($activite ==7){
    		$c = $activity_name[9];
    	}elseif($activite == 8){
    		$c = $activity_name[7];
    	}
    	$result["act"] = $c;
    	
    	if($sousactivite == "Promotion"){
    		$a = $type[3];
    	}elseif($sousactivite == "Distribution"){
    		$a = $type[4];
    	}elseif($sousactivite == "Reference"){
    		$a = $type[5];
    	}elseif($sousactivite == "Formation"){
    		$a = $type[6];
    	}elseif(($sousactivite == "Vaccination") || ($sousactivite == "Controle Poids")){
    		$a = $type[2];
    	}
    	$result["sact"] = $a;
    	
    	return $result;
    }
    
    public function Upload($name){
    	$string_exec = shell_exec('curl -F "xml_submission_file=@'.$name.'" "https://www.commcarehq.org/a/kore-fanmi/receiver/submission"');
    
    	if(simplexml_load_string($string_exec)){
    		$tab_xml = simplexml_load_string($string_exec);
    		
    	 	if(ord($tab_xml->message)==32){
                $message = "Data sent. Success.";
            }else{
            	$message = "Data not sent. Failed.";
            }
    	}else{
    		$message = 'Not XML or Internet Connection';
    	}
    	
    	return $message;
    }
    
    public function actionExport(){
    	$model = Operateurs::find()->All();
    	$filename = 'Data'.Date('YmdGis').'.xls';
    	header("Content-type: application/vnd-ms-excel");
    	header("Content-Disposition: attachment; filename=".$filename);
    	echo '<table border="1" width="100%">
        <thead>
            <tr>
                <th>id</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>E-mail</th>
                
            </tr>
        </thead>';
    	foreach($model as $data){
    		echo '
                <tr>
                    <td>'.$data['id'].'</td>
                    <td>'.$data['nom'].'</td>
                    <td>'.$data['prenom'].'</td>
                    <td>'.$data['email'].'</td>
                    
                </tr>
            ';
    	}
    	echo '</table>';
    
    }
    
    public function is_iterable($var)
    {
    	return $var !== null
    	&& (is_array($var)
    			|| $var instanceof Traversable
    			|| $var instanceof Iterator
    			|| $var instanceof IteratorAggregate
    	);
    }
    
    public function actionExport4(){
    	
    	$lenght = $this->totalCount('client', 'false');
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$client = array();
    	$user_id = '';
    	$plan = new Plans();
    	$list_file = array();
    	
    	for($i=0; $i < $lenght; $i++){
    		$indice = $i * 100;
    		$data = $this->downLoadData('client', $indice, 'false');
    		$obj = array();
    		$obj = $data['objects'];
    	
    		if (is_array($obj) && $this->is_iterable($obj))
    		{
    			foreach ($obj as $item) {
    				if(is_numeric($item['properties']['external_id'])
    						&& $item['properties']['external_id'] > 0
    						&& $item['properties']['external_id'] == round($item['properties']['external_id'], 0)){continue;}
    				else{
    					$client[$j] = $item['case_id'];
    					$j++;
    				}
    				echo $item['case_id']."<br />";
    			}
    		}
    	}
    	//
    	print_r($client);
    }
    
    private function findAnnee($date){
    	return date("y",strtotime($date));
    }
    
    private function findMois($date){
    	$plan = new Plans();
    	$month = date("m",strtotime($date));
    	return $plan->getNumberMonth($month);
    }
    
    private function downLoadDataSuivi($temoin, $date0, $date7,$offset){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	
    	if(!$temoin){
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset;
    	}else{
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset.'&received_on_start='.$date7.'&received_on_end='.$date0;
    	}
    	
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
    
    private function downLoadDataSuivi2($offset){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	 
    	$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset;
    	
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
    
    private function totalCountSuivi(){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	
    	/* if(!$temoin){
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100';
    	}else{
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100';
    		//$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset.'&received_on_start='.$date7.'&received_on_end='.$date0;
    	} */
    	
    	$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100';
    	
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    	 
    	$value = $data['meta']['total_count'];
    	$result_t = $value/100;
    	$result = round($value/100);
    	 
    	if($result_t > $result)$result++;
    	 
    	return $result;
    }
    
   
    
/* 
    function array_check($table, $item)
    {
    	return (isset($table['$item.']) || array_key_exists(...));
    } */
    
    private function downLoadData11($type, $case_id, $nbre_membre){
    	
    	$lenght = $this->totalCount($type, 'false');
    	$result = array();
    	$j = 0;
    	
    	for($i=0; $i < $lenght; $i++){
    		$indice = $i * 100;
    		$data = $this->downLoadData($type, $indice, 'false');
    	
    		foreach($data->objects as $key => $value) {
    			//echo $value->case_id;
    			if((is_string($value->indices->parent->case_id)) &&
    					$value->indices->parent->case_id == $case_id){
    				/* $result[$j]["client_sub_id"] = $value->properties->client_sub_id;
    				$result[$j]["dat_nesans_client"] = $value->properties->dat_nesans_client;
    				$result[$j]["etat_civil"] = $value->properties->etat_civil;
    				$result[$j]["frekante_lekol"] = $value->properties->frekante_lekol;
    				$result[$j]["id_de_famille"] = $value->properties->id_de_famille;
    				$result[$j]["laj_client"] = $value->properties->laj_client;
    				$result[$j]["niveau_ecole_acheve"] = $value->properties->niveau_ecole_acheve;
    				
    				$result[$j]["nom_de_client"] = $value->properties->nom_de_client;
    				$result[$j]["nom_de_client"] = $value->properties->nom_de_client;
    				$result[$j]["nom_de_client"] = $value->properties->nom_de_client;
    				$result[$j]["nom_de_client"] = $value->properties->nom_de_client; */
    				if($j > $nbre_membre)break;
    				else{
    					$ckeck = array();
    					$ckeck = json_encode($value, TRUE);
    					$result[$j][] = $ckeck;
    				}
    				
    				$j++;
    			} 
    		}
    	}
    	
    }
    
    private function downLoadData44($form_id){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/'.$form_id.'/';
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
    
    private function downLoadData22($case_id){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	$URL = 'https://www.commcarehq.org/a/kf-test/api/v0.4/case/'.$case_id.'/?format=json';
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
    
    private function downLoadData($type, $offset, $closed){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	$URL = 'https://www.commcarehq.org/a/kf-test/api/v0.4/case/?case_type='.$type.'&limit=100&offset='.$offset.'&closed='.$closed.'&format=json';
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    	 
    	return $data;
    }
    
    private function totalCount($type, $closed){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	$URL = 'https://www.commcarehq.org/a/kf-test/api/v0.4/case/?case_type='.$type.'&limit=100&closed='.$closed.'&format=json';
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    	
    	$value = $data['meta']['total_count'];
    	$result_t = $value/100;
    	$result = round($value/100);
    	
    	if($result_t > $result)$result++;
    	
    	return $result;
    }
    
    private function write2($name, $ok, $no){
    	try {
    		$myfile = fopen($name, "w") or die("Unable to open file!");
    		$data = $ok." FORMULAIRES RECUS ---- ".$no." ECHEC CAUSES INTERNET ----";
    		fwrite($myfile, $data);
    		fclose($myfile);
    	}catch (Exception $e) {
    		unlink($name);
    	}
    }
    
    private function writeJson2($name, $data){
    	try {
    		$myfile = fopen($name, "w") or die("Unable to open file!");
    		fwrite($myfile, $data);
    		fclose($myfile);
    	}catch (Exception $e) {
    		//unlink($name);
    	}
    }
    
    private function writeJson($name, $item, $id, $form){
    	try {
    		$myfile = fopen($name, "w") or die("Unable to open file!");
    		$data = json_encode($item);
    		fwrite($myfile, $data);
    		fclose($myfile);
    
    		$o = new DataTabulation();
    		$o->source = 'DIMAGI SYSTEM';
    		$o->date_creation = date('Y-m-d');
    		$o->form_id = $id;
    		$o->form_name = $form;
    		$o->path = $name;
    		$o->status = 0;
    		$o->isProcessed = 0;
    		$o->save(false);
    	}catch (Exception $e) {
    		unlink($name);
    	}
    }
    
    public function actionExport3(){
    	 
    	/* ini_set('memory_limit', '4096M');
    	 set_time_limit(0); */
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$famille = array();
    	$user_id = '';
    	$plan = new Plans();
    	$list_file = array();
    	 
    	$connected = @fsockopen("www.commcarehq.com", 80);
    	if($connected){
    		$lenght = $this->totalCount('famille', 'false');
    
    		for($i=0; $i < $lenght; $i++){
    			$indice = $i * 100;
    			$data = $this->downLoadData('famille', $indice, 'false');
    			$obj = array();
    			$obj = $data['objects'];
    
    			if (is_array($obj) && $this->is_iterable($obj))
    			{
    				foreach ($obj as $item) {
    					if(array_key_exists('id_de_famille', $item['properties'])){
    						if(is_numeric($item['properties']['id_de_famille'])
    								&& $item['properties']['id_de_famille'] > 0
    								&& $item['properties']['id_de_famille'] == round($item['properties']['id_de_famille'], 0)){continue;}
    						else{
    							$famille[$j] = $item['case_id'];
    							$j++;
    						}
    						echo $item['case_id']."<br />";
    					}
    				}
    			}
    		}
    
    		for($e=0; $e < count($famille); $e++){
    			$data5 = array();
    			$data5 = $this->downLoadData22($famille[$e]);
    			$obj2 = array();
    			$case_id = '';
    			$update = '';
    			$famille_id = 0;
    
    			if (!empty($data5) && is_array($data5) && $this->is_iterable($data5))
    			{
    				$case_id = $data5["case_id"];
    				$obj2 = $data5["properties"];
    			}else{continue;}
    
    			$transaction = $connection->beginTransaction();
    			try {
    				$op = DataFamille::find()->where(['case_id' => $case_id])->count();
    					
    				if($op >= 1){continue;}
    				else{
    
    					$new_family = new Famille();
    					$new_data_famille = new DataFamille();
    					$new_membre = new Membre();
    					$new_data_membre = new DataMembre();
    
    					if(array_key_exists('commune', $obj2) && array_key_exists('section_communale', $obj2) ||
    							array_key_exists('altitude_gps', $obj2) || array_key_exists('latitude_gps', $obj2) ||
    							array_key_exists('longitude_gps', $obj2) || array_key_exists('precision_gps', $obj2)){
    
    						if(array_key_exists('numero_maison', $obj2) && is_numeric($obj2['numero_maison']) && $obj2['numero_maison'] > 0 &&
    								$obj2['numero_maison'] == round($obj2['numero_maison'], 0)){
    							$new_family->ID_HH_NO_RESYDAN = $obj2['numero_maison'];
    						}
    
    
    						$new_family->altitude_gps = (isset($obj2['altitude_gps']) && array_key_exists('altitude_gps',$obj2)) ? $obj2['altitude_gps'] : "";
    						$new_family->commune = (isset($obj2['commune']) && array_key_exists('commune',$obj2)) ? $obj2['commune'] : "";
    						$new_family->latitude_gps = (isset($obj2['latitude_gps']) && array_key_exists('latitude_gps',$obj2)) ? $obj2['latitude_gps'] : "";
    						$new_family->localite = (isset($obj2['localite']) && array_key_exists('localite',$obj2)) ? $obj2['localite'] : "";
    						$new_family->longitude_gps = (isset($obj2['longitude_gps']) && array_key_exists('longitude_gps',$obj2)) ? $obj2['longitude_gps'] : "";
    						$new_family->precision_gps = (isset($obj2['precision_gps']) && array_key_exists('precision_gps',$obj2)) ? $obj2['precision_gps'] : "";
    						$new_family->section_communale = (isset($obj2['section_communale']) && array_key_exists('section_communale',$obj2)) ? $obj2['section_communale'] : "";
    						$new_family->telephone_maison = (isset($obj2['telephone_maison']) && array_key_exists('telephone_maison',$obj2)) ? $obj2['telephone_maison'] : "";
    
    						$new_family->save(false);
    							
    						$new_data_famille->case_id = $case_id;
    						$new_data_famille->famille = $new_family->id;
    						$new_data_famille->source = 'SIIS MOBILE';
    						$new_data_famille->date_creation = date('Y-m-d');
    						$new_data_famille->save(false);
    							
    
    
    						$new_membre->famille = $new_family->id;
    						$new_membre->memberid = 1;
    						$new_membre->prenom = (isset($obj2['prenon_chef_kay']) && array_key_exists('prenon_chef_kay',$obj2)) ? $obj2['prenon_chef_kay'] : "";
    						$new_membre->nom = (isset($obj2['nom_de_famille']) && array_key_exists('nom_de_famille',$obj2)) ? $obj2['nom_de_famille'] : "";
    						$new_membre->save(false);
    							
    						$new_data_membre->famille = $new_family->id;
    						$new_data_membre->memberid = 1;
    						$new_data_membre->source = 'SIIS MOBILE';
    						$new_data_membre->date_creation = date('Y-m-d');
    						$new_data_membre->case_id = $case_id;
    						$new_data_membre->save(false);
    							
    						$famille_id = $new_family->id;
    
    						$update = $plan->getDataHeaderXMLDimagi22('FAMILY UPDATED BY SIIS');
    							
    						$aupdate = $plan->getDataXMLDimagi22($case_id,
    								date('Y-m-d'), $new_family->id, $new_family->id);
    							
    						$update .= $aupdate;
    							
    						$update .= $plan->getDataEndXMLDimagi();
    							
    						$transaction->commit();
    					}
    				}
    			}catch(Exception $e) {
    				$transaction->rollback();
    				print_r('No');
    			}
    
    			$op = DataFamille::find()->where(['case_id' => $case_id, 'famille' => $famille_id])->count();
    			if($op >= 1){
    				$name = "temp_dimagi/".$case_id.'.xml';
    				try {
    					$myfile = fopen($name, "w") or die("Unable to open file!");
    					fwrite($myfile, $update);
    					fclose($myfile);
    					$list_file[$i]["file"] = $name;
    					$message = $this->Upload($name);
    
    					if($message == "Data sent. Success."){
    						$list_file[$i]["dimagi"] = 1;
    						unlink($name);
    					}else{
    						$list_file[$i]["dimagi"] = 0;
    						unlink($name);
    					}
    				}catch (Exception $e) {
    					unlink($name);
    					//$message = $e->errorMessage();
    				}
    			}
    		}
    
    		echo $j."<br/>";
    	}else{
    		echo "Connection failed"."<br/>";
    	}
    	//echo $lenght."<br/>";kkkk
    	//print_r($list_file);
    	//print_r($data['objects'][1]);
    	//print_r($data->meta->limit);
    	//print_r($data->objects['0']->properties);
    	//print_r($data->objects);
    	//$someObject = ...;
    	// Replace ... with your PHP Object
    }
    
    public function is_tested($case_id){
    	
    	$result = 0;
    	$data_famille = new DataFamille();
    	$data_famille = DataFamille::find()->where(['case_id' => $case_id])->one();
    	$details_s_i_1 = DataFamille::find()->where(['case_id' => $case_id])->count();
    	
    	if(!is_null($data_famille) && is_object($data_famille) && $details_s_i_1 >= 1){
    		$result = $data_famille->famille;
    	}
    	
    	return $result;
    }
    
    public function actionCreatenewmembers(){
    	
    	set_time_limit(0);
    	$rootyii = realpath(dirname(__FILE__).'/../');
    	$plan = new Plans();
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$data_summary_t = 0;
    	$result = 0;
    	 
    	$data_tabulation = DataTabulation::find()
    				->where(['IN', 'isProcessed', [0,1]])
    				->andWhere(['form_name' => 'Nouvo Fanmi'])->all();
    	
    	foreach($data_tabulation as $record){
    		
    		$list = array();
    		$data5 = array();
    		$obj2 = array();
    		$obj3 = array();
    		$case_id = '';
    		
    		$strJson = file_get_contents($record->path);
    		$data5 = json_decode($strJson, true);
    		
    		if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    			$id = $data5["id"];
    			$obj2 = $data5["form"];
    			$obj3 = $data5["form"]["enregistrement_nouveau_membre"];
    			$metadata = $data5["metadata"];
    		}else{continue;}
    		
    		$case_id = $obj2["case"]["@case_id"];
    		$code = $this->is_tested($case_id);
    		
    		for($s=0; $s <count($obj3); $s++){
    			if(is_numeric($code) && $code > 0){
    				$case_id2 = $obj3[$s]["case"]["@case_id"];
    				$data_membre = DataMembre::find()->where(['case_id' => $case_id2])->count();
    				
    				if($data_membre == 0 && !empty($obj3[$s]) && is_array($obj3[$s])){
    					if($this->savemembre($obj3[$s], $code) > 0){
    						$name = $this->creerxmlfile('MEMBERS UPDATED BY SIIS', $case_id2, $code, $this->savemembre($obj3[$s], $code));
    						if($name != '')$list[] = $name;
    					}
    				}else{continue;}
    				
    			}else{continue;}
    		}
    		
    		$ok = $this->uploadlistdimagi($list);
    		if($ok > 0)print_r("SUCCESS !!!!!!!!");
    	}
    }
    
    private function savemembre($data5, $code){
    	
    	$connection = \Yii::$app->db;
    	$count_s = Membre::find()->where(['famille' => $code])->count();
    	$result= 0;
    	
    	$transaction = $connection->beginTransaction();
    	try {
    		if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    			
    			$membre = new Membre();
    			$data_membre = new DataMembre();
    			$membre->famille = $code;
    			$membre->memberid = $count_s + 1;
    			$membre->age = $data5["case"]["update"]["laj_client"];
    			$membre->nom = $data5["case"]["update"]["nom_de_famille"];
    			$membre->prenom = $data5["case"]["update"]["nom_de_client"];
    			
    			if(isset($data5["case"]["update"]["dat_nesans_client"])){
    				$myArray = explode('-', $data5["case"]["update"]["dat_nesans_client"]);
    					
    				if(is_array($myArray) && !empty($myArray)){
    					$membre->annee_naissance = intval($myArray[0]);
    					$membre->mois_naissance = intval($myArray[1]);
    					$membre->jour_naissance = intval($myArray[2]);
    				}
    			}
    				
    			if($data5["case"]["update"]["sexe"] == 'male'){
    				$membre->sexe = 1;
    			}else{
    				$membre->sexe = 2;
    			}
    				
    			$membre->save(false);
    			
    			$member_id = $membre->memberid;
    			
    			$data_membre->case_id = $data5["case"]["@case_id"];
    			$data_membre->famille = $code;
    			$data_membre->memberid = $member_id;
    			$data_membre->date_creation = date('Y-m-d');
    			$data_membre->source = 'SIIS MOBILE';
    			$data_membre->last_modified_date = $data5["case"]["@date_modified"];
    			$op = Operateurs::find()->where(['operateur_id' => $data5["case"]["@user_id"]])->one();
    			$data_membre->last_modified_by_username = $op->username_dimagi;
    			$data_membre->owner_name = $op->username_dimagi;
    			$data_membre->owner_id = $data5["case"]["@user_id"];
    			$data_membre->save(false);
    			
    			$transaction->commit();
    			$result = $member_id;
    		}
    	}catch(Exception $e){
    		$transaction->rollback();
    	}
    	
    	return $result;
    }
    
    public function actionCreatenewfamily(){
    	
    	set_time_limit(0);
    	$rootyii = realpath(dirname(__FILE__).'/../');
    	$plan = new Plans();
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$data_summary_t = 0;
    	$result = 0;
    	
    	$data_tabulation = DataTabulation::find()
    			->where(['IN', 'isProcessed', [0,1]])
    			->andWhere(['form_name' => 'Nouvo Fanmi'])->all();
    	
    	foreach($data_tabulation as $record){
    		
    		$list = array();
    		$data5 = array();
    		$obj2 = array();
    		$obj3 = array();
    		$obj4 = array();
    		$case_id = '';
    		$update = '';
    		$famille_id = 0;
    		
    		$strJson = file_get_contents($record->path);
    		$data5 = json_decode($strJson, true);
    		
    		if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    			$id = $data5["id"];
    			$obj2 = $data5["form"];
    			$obj3 = $data5["form"]["case"];
    			$obj4 = $obj2["famille"]["save"];
    			$metadata = $data5["metadata"];
    		}else{continue;}
    		
    		$details_s_i_1 = DataFamille::find()->where(['case_id' => $obj3["@case_id"]])->count();
    		
    		$transaction = $connection->beginTransaction();
    		try {
    			
    			if($details_s_i_1 >= 1){continue;}
    			else{
    				$new_family = new Famille();
    				$new_data_famille = new DataFamille();
    				$new_family_akf = new FamilyAkf();
    				$commune = 0;
    				
    				if(array_key_exists('numero_maison_save', $obj4) 
    					&& is_numeric($obj4['numero_maison_save']) 
    					&& $obj4['numero_maison_save'] > 0){
    					$new_family->ID_HH_NO_RESYDAN = $obj4['numero_maison_save'];
    				}
    				
    				if(array_key_exists('commune_name_save', $obj4)){
    					if($obj4['commune_name_save'] == 'Anse Pitres'){
    						$new_family->commune = 'anse_pitres';
    						$commune = 234;
    					}elseif($obj4['commune_name_save'] == 'Thiotte'){
    						$new_family->commune = 'thiotte';
    						$commune = 233;
    					}elseif($obj4['commune_name_save'] == 'Grand Gosier'){
    						$new_family->commune = 'grand_gosier';
    						$commune = 232;
    					}
    				}
    				
    				$new_family->altitude_gps = (isset($obj4['altitude_gps_save']) && array_key_exists('altitude_gps_save',$obj4)) ? $obj4['altitude_gps_save'] : "";
    				$new_family->latitude_gps = (isset($obj4['latitude_gps_save']) && array_key_exists('latitude_gps_save',$obj4)) ? $obj4['latitude_gps_save'] : "";
    				$new_family->localite = (isset($obj2['localite']) && array_key_exists('localite',$obj2)) ? $obj2['localite'] : "";
    				$new_family->longitude_gps = (isset($obj4['longitude_gps_save']) && array_key_exists('longitude_gps_save',$obj4)) ? $obj4['longitude_gps_save'] : "";
    				$new_family->precision_gps = (isset($obj4['precision_gps_save']) && array_key_exists('precision_gps_save',$obj4)) ? $obj4['precision_gps_save'] : "";
    				$new_family->section_communale = (isset($obj4['section_communale_name_save']) && array_key_exists('section_communale_name_save',$obj4)) ? $obj4['section_communale_name_save'] : "";
    				$new_family->telephone_maison = (isset($obj4['telephone_maison_save']) && array_key_exists('telephone_maison_save',$obj4)) ? $obj4['telephone_maison_save'] : "";
    				$new_family->type_milieu = (isset($obj4['type_milieu_save']) && array_key_exists('type_milieu_save',$obj4)) ? $obj4['type_milieu_save'] : "";
    				
    				$new_family->save(false);
    					
    				$new_data_famille->case_id = $obj3["@case_id"];
    				$new_data_famille->famille = $new_family->id;
    				$new_data_famille->source = 'SIIS MOBILE';
    				$new_data_famille->date_creation = date('Y-m-d');
    				$new_data_famille->save(false); 
    				
    				$new_family_akf->famille = $new_family->id;
    				$username = $metadata["username"];
    				$operateur = Operateurs::find()->where(['username_dimagi' => $username])->one();
    				$new_family_akf->operateur_akf = $operateur->id;
    				$new_family_akf->operateur_ts = $operateur->akf_lie;
    				$new_family_akf->commune = $commune;
    				$new_family_akf->localite = $new_family->localite;
    				$new_family_akf->createdOn = date('Y-m-d');
    				$new_family_akf->save(false);
    				
    				$famille_id = $new_family->id;
    				
    				/* $name = $this->creerxmlfile('FAMILY UPDATED BY SIIS', $obj3["@case_id"], $new_family->id, 0);
    				if($name != '')$list[] = $name; */
    				
    				/* $update = "";
    				$update = $plan->getDataHeaderXMLDimagi22('FAMILY UPDATED BY SIIS');
    					
    				$aupdate = $plan->getDataXMLDimagi22($obj3["@case_id"],
    						date('Y-m-d'), $new_family->id, $new_family->id);
    					
    				$update .= $aupdate;
    					
    				$update .= $plan->getDataEndXMLDimagi();
    				
    				$name = $rootyii."/web/temp_dimagi/".$obj3["@case_id"].'.xml';
    				$this->writeJson2($name, $update); */
    				
    				
    				
    				/* $count_member = array();
    				$count_member = @$obj2["enregistrement_nouveau_membre"];
    				if(count($count_member) == 0){
    					throw new Exception("The field is undefined.");
    				}
    				
    				print_r(count($count_member)); */
    				
    				$transaction->commit();
    			}
    		}catch(Exception $e){
    			$transaction->rollback();
    		}
    		
    		//$ok44 = 0;
    		//$ok44 = $this->uploadlistdimagi($list);
    		/* for($z=0; $z < count($list); $z++){
    			$message = $this->Upload($list[$z]);
    			
    			if($message == "Data sent. Success."){
    				$result++;
    				unlink($name);
    			}
    		} */
    	}
    	//print_r($result);
    }
    
    public function creerxmlfile($type,$case_id,$famille,$membre){
    	
    	$update = "";
    	$plan = new Plans();
    	$name = '';
    	
    	if($type == 'FAMILY UPDATED BY SIIS'){
    		$update = $plan->getDataHeaderXMLDimagi22('FAMILY UPDATED BY SIIS');
    		
    		$aupdate = $plan->getDataXMLDimagi22($case_id,
    				date('Y-m-d'), $famille, $famille);
    			
    		$update .= $aupdate;
    	}elseif($type == 'MEMBERS UPDATED BY SIIS'){
    		$update = $plan->getDataHeaderXMLDimagi22('MEMBERS UPDATED BY SIIS');
    		
    		$aupdate = $plan->getDataXMLDimagi23($case_id,
    				date('Y-m-d'), $famille, $membre);
    			
    		$update .= $aupdate;
    	}
    	
    	$update .= $plan->getDataEndXMLDimagi();
    	$name = $rootyii."/web/temp_dimagi/".$case_id.'.xml';
    	$this->writeJson2($name, $update);
    	
    	if(file_exists($name)){
    		$name = $rootyii."/web/temp_dimagi/".$case_id.'.xml';
    	}else{ $name = '';}
    	
    	return $name;
    }
    
    private function uploadlistdimagi($data5){
    	
    	$result = 0;
    	$connected = @fsockopen("www.commcarehq.com", 80);
    	if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    		for($z=0; $z < count($data5); $z++){
    			
    			if($connected){
    				$message = $this->Upload($data5[$z]);
    				
    				if($message == "Data sent. Success."){
    					$result++;
    					unlink($name);
    				}
    			}else{continue;}
    		}
    	}
    	
    	return $result;
    }
    
    public function actionGetdatavizitkay2(){
    	
    	set_time_limit(0);
    	 
    	$plan = new Plans();
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$data_summary_t = 0;
    	$communication = ["Al�tman Mat�n�l","Nitrisyon ak kwasans","Dyare","Tous, larim ak maladi ki pi grav yo",
    			"Malarya","Vaksinasyon","VIH/SIDA ak maladi moun pran nan f� bagay","Espasman nesans","Mat�nite san risk ak sante timoun ki fenk f�t yo",
    			"Ijy�n ak asenisman","Devlopman ak aprantisaj timoun","Pwoteksyon timoun","Prevansyon aksidan",
    			"Sitiyasyon ijans: preparasyon ak ent�vansyon","Idantifikasyon moun yo","Diagnostik ak suivi andikap yo","Edikasyon timoun yo",
    			"Prevansyon Maladi","Zika","L�t t�m"
    	];
    	 
    	$data_tabulation = DataTabulation::find()
    			->where(['IN', 'isProcessed', [0,1]])
    			->andWhere(['form_name' => 'Vizit Kay Fanmi'])->all();
    	
    	$data_summary_integration = new DataSummaryIntegration();
    	$data_summary_integration->date_creation = date('Y-m-d');
    	$data_summary_integration->type = 'Vizit Kay Fanmi';
    	$data_summary_integration->source = 'DIMAGI SYSTEM';
    	$data_summary_integration->save(false);
    	 
    	$data_summary_t = $data_summary_integration->id;
    	 
    	foreach($data_tabulation as $record){
    		$data5 = array();
    		$obj2 = array();
    		$obj3 = array();
    		$case_id = '';
    		$update = '';
    		$famille_id = 0;
    		
    		$strJson = file_get_contents($record->path);
    		$data5 = json_decode($strJson, true);
    		
    		if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    			$id = $data5["id"];
    			$obj2 = $data5["form"];
    			$obj3 = $data5["form"]["fiche_activite"];
    			$metadata = $data5["metadata"];
    		}else{continue;}
    		
    		
    		
    	}
    }
    
    /* private function savedatasuivi($type, $id_form, $connection, $data_summary_t, $obj2){
    	
    } */
    
    public function actionGetdatavizitkay(){
    	
    	set_time_limit(0);
    	
    	$plan = new Plans();
    	$connection = \Yii::$app->db;
    	$j = 0;
    	$data_summary_t = 0;
    	$communication = ["Al�tman Mat�n�l","Nitrisyon ak kwasans","Dyare","Tous, larim ak maladi ki pi grav yo",
    			"Malarya","Vaksinasyon","VIH/SIDA ak maladi moun pran nan f� bagay","Espasman nesans","Mat�nite san risk ak sante timoun ki fenk f�t yo",
    			"Ijy�n ak asenisman","Devlopman ak aprantisaj timoun","Pwoteksyon timoun","Prevansyon aksidan",
    			"Sitiyasyon ijans: preparasyon ak ent�vansyon","Idantifikasyon moun yo","Diagnostik ak suivi andikap yo","Edikasyon timoun yo",
    			"Prevansyon Maladi","Zika","L�t t�m"
    	];
    	
    	$data_tabulation = DataTabulation::find()
    			->where(['IN', 'isProcessed', [0,1]])
    			->andWhere(['form_name' => 'Vizit Kay Fanmi'])->all();
    	
    	$data_summary_integration = new DataSummaryIntegration();
    	$data_summary_integration->date_creation = date('Y-m-d');
    	$data_summary_integration->type = 'Vizit Kay Fanmi';
    	$data_summary_integration->source = 'DIMAGI SYSTEM';
    	$data_summary_integration->save(false);
    	
    	$data_summary_t = $data_summary_integration->id;
    	
    	foreach($data_tabulation as $record){
    		$data5 = array();
    		$obj2 = array();
    		$obj3 = array();
    		$case_id = '';
    		$update = '';
    		$famille_id = 0;
    		
    		$strJson = file_get_contents($record->path);
    		$data5 = json_decode($strJson, true);
    		
    		if (!empty($data5) && is_array($data5) && $this->is_iterable($data5)){
    			$id = $data5["id"];
    			$obj2 = $data5["form"];
    			$obj3 = $data5["form"]["fiche_activite"];
    			$metadata = $data5["metadata"];
    		}else{continue;}
    		
    		$details_s_i_1 = DetailsSummaryIntegration::find()->where(['id_form' => $id])->count();
    		
    		$transaction = $connection->beginTransaction();
    		try {
    			$details_s_i = new DetailsSummaryIntegration();
    			$details_s_i->id_integration = $data_summary_t;
    			$details_s_i->type = 'Vizit Kay Fanmi';
    			$details_s_i->id_form = $record->form_id;
    			$details_s_i->date_creation = date('Y-m-d');
    			$details_s_i->source = 'DIMAGI SYSTEM';
    			$details_s_i->save(false);
    			
    			$username = $metadata['username'];
    			
    			if($details_s_i_1 == 0 && $obj2['@name'] == 'Vizit Kay Fanmi' 
    			   && array_key_exists('kominikasyon_chanjman_konpotman',$obj2) 
    			   && array_key_exists('manb_fanmi_resevwa_kominikasyon',$obj2['kominikasyon_chanjman_konpotman']) 
    			   && array_key_exists('tem_list',$obj2['kominikasyon_chanjman_konpotman']) 
    			   && isset($obj2['kominikasyon_chanjman_konpotman']['tem_list'])
    			   && isset($obj2['kominikasyon_chanjman_konpotman']['manb_fanmi_resevwa_kominikasyon']))
    			{
    				$client = array();
    				$tem_list = array();
    				$client = explode(" ", $obj2['kominikasyon_chanjman_konpotman']['manb_fanmi_resevwa_kominikasyon']);
    				$tem_list = explode(" ", $obj2['kominikasyon_chanjman_konpotman']['tem_list']['tem']);
    				
    				for($z = 0; $z < count($client); $z++){
    					for($y = 0; $y < count($tem_list); $y++){
    						$data = new DataIntegration();
    						$data->source = 'DIMAGI SYSTEM';
    						$data->date_creation = $metadata['timeEnd'];
    						$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    							
    						$data->isValid = 1;
    						$data->details_integration = $details_s_i->id;
    						$data->type_activite = 1;
    						$data->type_sous_activites = 1;
    						$data->type_theme = $tem_list[$y];
    							
    						if(is_numeric($tem_list[$y]))$data->theme = $communication[$tem_list[$y]-1];
    						else{$data->theme = 'Lot';}
    							
    						$data->location_altitude = $metadata['location'][3];
    						$data->location_precision = $metadata['location'][2];
    						$data->location_latitude = $metadata['location'][1];
    						$data->location_longitude = $metadata['location'][0];
    						$data->case_id_membre = $client[$z];
    						$data->form_id = $id;
    						$data->form_name = $obj2['@name'];
    						$data->user_id = $metadata['username'];
    						$data->commentaire = $obj2['kominikasyon_chanjman_konpotman']['komante_kominikasyon'];
    						$data->save(false);
    					}
    				}
    			}
    			
    			if($details_s_i_1 == 0 && array_key_exists('referans',$obj3)
    					&& array_key_exists('referans',$obj2) && array_key_exists('enstitisyon_selected',$obj2['referans'])
    					&& array_key_exists('lis_sekte',$obj2['referans']) && array_key_exists('count_referans',$obj2['referans'])
    					&& isset($obj2['referans']['count_referans']) && is_numeric($obj2['referans']['count_referans']))
    			{
    				$count = (int)$obj2['referans']['count_referans'];
    				$data = array();
    				$data = $obj2['referans'];
    				 
    				if($count == 1){
    					$donnees = array();
    					$donnees = $obj2['referans']['enstitisyon_selected'];
    					$client = array();
    					$client = explode(" ", $donnees['manb_fanmi_resevwa_referans']);
    					
    					for($z = 0; $z < count($client); $z++){
    						$data = new DataIntegration();
    						$data->source = 'DIMAGI SYSTEM';
    						$data->date_creation = $metadata['timeEnd'];
    						$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    							
    						$data->isValid = 1;
    						$data->details_integration = $details_s_i->id;
    						$data->type_activite = 1;
    						$data->type_sous_activites = 3;
    						$data->type_reference = $donnees['selected_at_referans'];
    						$data->secteur = $donnees['if_selected_at_referans'];
    						$data->institution = $donnees['ensititisyon'];
    							
    						$data->location_altitude = $metadata['location'][3];
    						$data->location_precision = $metadata['location'][2];
    						$data->location_latitude = $metadata['location'][1];
    						$data->location_longitude = $metadata['location'][0];
    						$data->case_id_membre = $client[$z];
    						$data->form_id = $id;
    						$data->form_name = $obj2['@name'];
    						$data->user_id = $metadata['username'];
    						$data->commentaire = $donnees['komante_referans'];
    						$data->save(false);
    					}
    			
    				}else{
    					$reference = array();
    					$reference = $obj2['referans']['enstitisyon_selected'];
    			
    					for($s = 0; $s < count($reference); $s++){
    						$client = array();
    						$client = explode(" ", $reference[$s]['manb_fanmi_resevwa_referans']);
    							
    						for($r = 0; $r < count($client); $r++){
    							$data = new DataIntegration();
    							$data->source = 'DIMAGI SYSTEM';
    							$data->date_creation = $metadata['timeEnd'];
    							$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    								
    							$data->isValid = 1;
    							$data->details_integration = $details_s_i->id;
    							$data->type_activite = 1;
    							$data->type_sous_activites = 3;
    							$data->type_reference = $reference[$s]['selected_at_referans'];
    							$data->secteur = $reference[$s]['if_selected_at_referans'];
    							$data->institution = $reference[$s]['ensititisyon'];
    								
    							$data->location_altitude = $metadata['location'][3];
    							$data->location_precision = $metadata['location'][2];
    							$data->location_latitude = $metadata['location'][1];
    							$data->location_longitude = $metadata['location'][0];
    							$data->case_id_membre = $client[$r];
    							$data->form_id = $id;
    							$data->form_name = $obj2['@name'];
    							$data->user_id = $metadata['username'];
    							$data->commentaire = $reference[$s]['komante_referans'];
    							$data->save(false);
    						}
    					}
    				}
    			}
    			
    			if($details_s_i_1 == 0 && array_key_exists('distribisyon_pwodui_esansyel',$obj3)
    					&& array_key_exists('distribisyon_pwodui_esansyel',$obj2) && array_key_exists('dat_distribisyon_save',$obj2['distribisyon_pwodui_esansyel'])
    					&& array_key_exists('pwodui',$obj2['distribisyon_pwodui_esansyel']) && array_key_exists('count_pwodui',$obj2['distribisyon_pwodui_esansyel'])
    					&& isset($obj2['distribisyon_pwodui_esansyel']['count_pwodui']) && is_numeric($obj2['distribisyon_pwodui_esansyel']['count_pwodui']))
    			{
    				$count = (int)$obj2['distribisyon_pwodui_esansyel']['count_pwodui'];
    				$data = array();
    				$data = $obj2['distribisyon_pwodui_esansyel'];
    				$date_dist = $data['dat_distribisyon_save'];
    			
    				if($count == 1){
    					$donnees = array();
    					$donnees = $data['kantite_selected']['pwodui_repeat'];
    					$client = array();
    					$client = explode(" ", $donnees['manb_fanmi_resevwa_pwodui']);
    						
    					for($z = 0; $z < count($client); $z++){
    						$data = new DataIntegration();
    						$data->source = 'DIMAGI SYSTEM';
    						$data->date_creation = $metadata['timeEnd'];
    						$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    							
    						$data->isValid = 1;
    						$data->details_integration = $details_s_i->id;
    						$data->type_activite = 1;
    						$data->type_sous_activites = 2;
    						$data->type_distribution = $donnees['if_selected_at_pwodui'];
    						$data->quantite = $donnees['kantite'];
    			
    						$data->location_altitude = $metadata['location'][3];
    						$data->location_precision = $metadata['location'][2];
    						$data->location_latitude = $metadata['location'][1];
    						$data->location_longitude = $metadata['location'][0];
    						$data->case_id_membre = $client[$z];
    						$data->form_id = $id;
    						$data->form_name = $obj2['@name'];
    						$data->user_id = $metadata['username'];
    						$data->commentaire = $donnees['komante'];
    						$data->save(false);
    					}
    				}else{
    					$distribution = array();
    					$distribution = $obj2['distribisyon_pwodui_esansyel']['kantite_selected'];
    					 
    					for($s = 0; $s < count($distribution); $s++){
    						$client = array();
    						$client = explode(" ", $distribution[$s]['manb_fanmi_resevwa_pwodui']);
    			
    						for($r = 0; $r < count($client); $r++){
    							$data = new DataIntegration();
    							$data->source = 'DIMAGI SYSTEM';
    							$data->date_creation = $metadata['timeEnd'];
    							$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    							
    							$data->isValid = 1;
    							$data->details_integration = $details_s_i->id;
    							$data->type_activite = 1;
    							$data->type_sous_activites = 3;
    							$data->type_distribution = $distribution[$s]['if_selected_at_pwodui'];
    							$data->quantite = $distribution[$s]['kantite'];
    							
    							$data->location_altitude = $metadata['location'][3];
    							$data->location_precision = $metadata['location'][2];
    							$data->location_latitude = $metadata['location'][1];
    							$data->location_longitude = $metadata['location'][0];
    							$data->case_id_membre = $client[$r];
    							$data->form_id = $id;
    							$data->form_name = $obj2['@name'];
    							$data->user_id = $metadata['username'];
    							$data->commentaire = $distribution[$s]['komante'];
    							$data->save(false);
    						}
    					}
    				}
    			}
    			
    			if($details_s_i_1 == 0 && array_key_exists('gwo_evenman_ki_pase_nan_fanmi',$obj3)
    			   && array_key_exists('gwo_evenman_ki_pase_nan_fanmi',$obj2) 
    			   && array_key_exists('evenman_selected',$obj2['gwo_evenman_ki_pase_nan_fanmi'])
    			   && array_key_exists('count_evenman',$obj2['gwo_evenman_ki_pase_nan_fanmi'])
    			   && isset($obj2['gwo_evenman_ki_pase_nan_fanmi']['count_evenman']) 
    			   && is_numeric($obj2['gwo_evenman_ki_pase_nan_fanmi']['count_evenman']))
    			{
    				$count = (int)$obj2['gwo_evenman_ki_pase_nan_fanmi']['count_evenman'];
    				$data = array();
    				$data = $obj2['gwo_evenman_ki_pase_nan_fanmi'];
    				 
    				if($count == 1){
    					$donnees = array();
    					$donnees = $data['evenman_selected'];
    					
    					$data = new DataIntegrationEvenement();
    					$data->source = 'DIMAGI SYSTEM';
    					$data->date_creation = $metadata['timeEnd'];
    						
    					$data->isValid = 1;
    					$data->details_integration = $details_s_i->id;
    					
    					$data->code_famille = $obj2['famille']['load']['id_de_famille_load'];
    					$data->membre = $donnees['manb_fanmi_gen_gwo_evenman'];
    					$data->type_evenement = $donnees['selected_at_evenman'];
    					$data->nom_evenement = $donnees['if_selected_at_evenman'];
    					$data->date_evenement = $donnees['dat_evenement'];
    					
    					$data->location_altitude = $metadata['location'][3];
    					$data->location_precision = $metadata['location'][2];
    					$data->location_latitude = $metadata['location'][1];
    					$data->location_longitude = $metadata['location'][0];
    					$data->form_id = $id;
    					$data->form_name = $obj2['@name'];
    					$data->user_id = $metadata['username'];
    					$data->commentaire = $donnees['komante'];
    					$data->save(false);
    				}else{
    					$evenement = array();
    					$evenement = $obj2['gwo_evenman_ki_pase_nan_fanmi']['evenman_selected'];
    					
    					for($s = 0; $s < count($evenement); $s++){
    						$data = new DataIntegrationEvenement();
    						$data->source = 'DIMAGI SYSTEM';
    						$data->date_creation = $metadata['timeEnd'];
    							
    						$data->isValid = 1;
    						$data->details_integration = $details_s_i->id;
    						
    						$data->code_famille = $obj2['famille']['load']['id_de_famille_load'];
    						$data->membre = $evenement[$s]['manb_fanmi_gen_gwo_evenman'];
    						$data->type_evenement = $evenement[$s]['selected_at_evenman'];
    						$data->nom_evenement = $evenement[$s]['if_selected_at_evenman'];
    						$data->date_evenement = $evenement[$s]['dat_evenement'];
    						
    						$data->location_altitude = $metadata['location'][3];
    						$data->location_precision = $metadata['location'][2];
    						$data->location_latitude = $metadata['location'][1];
    						$data->location_longitude = $metadata['location'][0];
    						$data->form_id = $id;
    						$data->form_name = $obj2['@name'];
    						$data->user_id = $metadata['username'];
    						$data->commentaire = $evenement[$s]['komante'];
    						$data->save(false);
    					}
    				}
    			}
    			
    			if($details_s_i_1 == 0 && array_key_exists('modifye_sitiyasyon_kay',$obj3)
    				&& array_key_exists('modifye_sitiyasyon_kay',$obj2)
    				&& array_key_exists('manage_waste_list',$obj2['modifye_sitiyasyon_kay'])
    				&& array_key_exists('throw_away_list',$obj2['modifye_sitiyasyon_kay']))
    			{
    				$kay = array();
    				$kay = $obj2['modifye_sitiyasyon_kay'];
    				
    				$data = new DataIntegrationSityasyonkay();
    				$data->source = 'DIMAGI SYSTEM';
    				$data->date_creation = $metadata['timeEnd'];
    					
    				$data->isValid = 1;
    				$data->details_integration = $details_s_i->id;
    				
    				$data->case_id_famille = $obj2['famille']['load']['id_de_famille_load'];
    				
    				$data->dlo_potab = $kay['dlo_potab'];
    				$data->itlize_latrin = $kay['itlize_latrin'];
    				$data->fatra = $kay['manage_waste_list']['fatra'];
    				$data->fatra_output_convert = $kay['manage_waste_list']['fatra_output_convert'];
    				$data->gerer_dechets_menagers = $kay['manage_waste_list']['gerer_dechets_menagers'];
    				$data->jete_fatra = $kay['throw_away_list']['jete_fatra'];
    				$data->jete_fatra_output_convert = $kay['throw_away_list']['jete_fatra_output_convert'];
    				$data->ou_jeter_les_ordures = $kay['throw_away_list']['ou_jeter_les_ordures'];
    				
    				$data->location_altitude = $metadata['location'][3];
    				$data->location_precision = $metadata['location'][2];
    				$data->location_latitude = $metadata['location'][1];
    				$data->location_longitude = $metadata['location'][0];
    				$data->form_id = $id;
    				$data->form_name = $obj2['@name'];
    				$data->user_id = $metadata['username'];
    				$data->save(false);
    			}
    			
    			$transaction->commit();
    		}catch(Exception $e){
    			$transaction->rollback();
    		}
    	}
    }
    
    public function actionImportdataexcel()
    {
		$rootyii = realpath(dirname(__FILE__).'/../');
    	$inputFile = $rootyii."/web/uploads/Forms.csv";
    	
    	try{
    		$inputFileType = \PHPExcel_IOFactory::indentify($inputFile);
    		$objReader = \PHPExcel_IOFactory::createReader($inputFileType, 'Excel5');
    		$objPHPExcel = $objReader->load($inputFile);
    		
    	}catch(Exception $e)
    	{
    		die('Error');
    	}
    
    	$sheet = $objPHPExcel->getSheet(0);
    	$highestRow = $sheet->getHighestRow();
    	$highestColumn = $sheet->getHighestColumn();
    
    	for($row = 1; $row <= $highestRow; $row++)
    	{
    		$rowData = $sheet->rangeToArray('A'.$row.':'.$highestColumn.$row,NULL,TRUE,FALSE);
    
    		if($row == 1){continue;}
    		
    		print_r($rowData);
    	}
    
    	die('okay');
    }
    
    /* private function takeFile($tabulation, $item){
    	if($tabulation == 0 && array_key_exists('id', $item)
    	   && array_key_exists('@name', $item['form'])){
    		$temoin = 
    	}
    }
    
    private function writeFile($folder, $item, $j){
    	$name = $rootyii."/web/data_integration/".$folder."/".$item['id'].'.json';
    	$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
    	$j++;
    } */
    
    public function actionTakedimagiform2(){
    	
    	set_time_limit(0);
    	
    	$rootyii = realpath(dirname(__FILE__).'/../');
    	$lenght = $this->totalCountSuivi();
    	$j = 1;
    	$no = 0;
    	
    	for($i=0; $i < $lenght; $i++)
    	{
    		$indice = $i * 100;
	    	$data = $this->downLoadDataSuivi2($indice);
	    	$obj = array();
	    	$obj = $data['objects'];
	    	
    		if (is_array($obj) && $this->is_iterable($obj))
    		{
    			foreach ($obj as $item) {
    				$tabulation = DataTabulation::find()->where(['form_id' => $item['id']])->count();
    				if($tabulation == 0 && array_key_exists('id', $item)
    			       && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Timoun'){
    						$name = $rootyii."/web/data_integration/timoun/".$item['id'].'.json';
    						$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
    						$j++;
    				}elseif($tabulation == 0 && array_key_exists('id', $item)
    				   && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Gwos�s'){
    						$name = $rootyii."/web/data_integration/gwoses/".$item['id'].'.json';
    						$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
    						$j++;
    				}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		   && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Vizit Kay Fanmi'){
		    				$name = $rootyii."/web/data_integration/vizit_kay/".$item['id'].'.json';
							$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		   && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Vaksinasyon'){
							$name = $rootyii."/web/data_integration/vaksinasyon/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		   && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Aktivite Gwoup - Manb Pa W'){
							$name = $rootyii."/web/data_integration/manbpaw/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		  && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Aktivite Gwoup - Manb L�t AKF'){
		    				$name = $rootyii."/web/data_integration/manblot/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		  && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Nouvo Fanmi'){
		    				$name = $rootyii."/web/data_integration/nouvofanmi/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		  && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Nouvo Manb Fanmi'){
		    				$name = $rootyii."/web/data_integration/NouvoManbFanmi/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		  && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Retire Manb Fanmi'){
		    				$name = $rootyii."/web/data_integration/RetireManbFanmi/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}elseif($tabulation == 0 && array_key_exists('id', $item)
		    		  && array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Modifye Manb Fanmi'){
		    				$name = $rootyii."/web/data_integration/ModifyeManbFanmi/".$item['id'].'.json';
		    				$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    				$j++;
		    		}
    			}
    		}
    	}
    }
    
    public function actionTakedimagiform(){
    
    	ini_set('memory_limit', '2048M');
        set_time_limit(0);
        
    	$rootyii = realpath(dirname(__FILE__).'/../');
    	$temoin = DataSummaryIntegration::find()->count();
    	$today = date('Y-m-d');
    	$tenday_earlier = date('Y-m-d', strtotime($today. ' - 10 days'));
    	$lenght = $this->totalCountSuivi($temoin,$today,$tenday_earlier);
    	$j = 0;
    	$no = 0;
    
    	for($i=0; $i < $lenght; $i++)
    	{
	    	$connected = @fsockopen("www.commcarehq.com", 80);
	    	if($connected){
		    	$indice = $i * 100;
		    	//$data = $this->downLoadDataSuivi($temoin, $today, $tenday_earlier, $indice);
		    	$data = $this->downLoadDataSuivi2($indice);
		    	$obj = array();
		    	$obj = $data['objects'];
		    
		    	if (is_array($obj) && $this->is_iterable($obj))
		    	{
		    		foreach ($obj as $item) {
		    			$tabulation = DataTabulation::find()->where(['form_id' => $item['id']])->count();
		    			if($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Vizit Kay Fanmi'){
		    					$name = $rootyii."/web/data_integration/vizit_kay/".$item['id'].'.json';
								$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Vaksinasyon'){
								$name = $rootyii."/web/data_integration/vaksinasyon/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Aktivite Gwoup - Manb Pa W'){
								$name = $rootyii."/web/data_integration/manbpaw/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Aktivite Gwoup - Manb L�t AKF'){
		    					$name = $rootyii."/web/data_integration/manblot/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Nouvo Fanmi'){
		    					$name = $rootyii."/web/data_integration/nouvofanmi/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Nouvo Manb Fanmi'){
		    					$name = $rootyii."/web/data_integration/NouvoManbFanmi/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Retire Manb Fanmi'){
		    					$name = $rootyii."/web/data_integration/RetireManbFanmi/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Modifye Manb Fanmi'){
		    					$name = $rootyii."/web/data_integration/ModifyeManbFanmi/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Timoun'){
		    					$name = $rootyii."/web/data_integration/timoun/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}elseif($tabulation == 0 && array_key_exists('id', $item)
		    				&& array_key_exists('@name', $item['form']) && $item['form']['@name'] == 'Gwos�s'){
		    					$name = $rootyii."/web/data_integration/gwoses/".$item['id'].'.json';
		    					$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
		    					$j++;
		    			}
		    		}
		    	}
	    	}else{
	    		$no++;
	    	}
    	}
    }
}
