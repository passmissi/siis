<?php

namespace app\controllers;

use Yii;
use app\models\SousActivitesHasTheme;
use app\models\ScrSousActivitesHasTheme;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SousActivitesHasThemeController implements the CRUD actions for SousActivitesHasTheme model.
 */
class SousActivitesHasThemeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SousActivitesHasTheme models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ScrSousActivitesHasTheme();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SousActivitesHasTheme model.
     * @param integer $id_sous_activite
     * @param integer $id_type_theme
     * @return mixed
     */
    public function actionView($id_sous_activite, $id_type_theme)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_sous_activite, $id_type_theme),
        ]);
    }

    /**
     * Creates a new SousActivitesHasTheme model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SousActivitesHasTheme();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_sous_activite' => $model->id_sous_activite, 'id_type_theme' => $model->id_type_theme]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SousActivitesHasTheme model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_sous_activite
     * @param integer $id_type_theme
     * @return mixed
     */
    public function actionUpdate($id_sous_activite, $id_type_theme)
    {
        $model = $this->findModel($id_sous_activite, $id_type_theme);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_sous_activite' => $model->id_sous_activite, 'id_type_theme' => $model->id_type_theme]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SousActivitesHasTheme model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_sous_activite
     * @param integer $id_type_theme
     * @return mixed
     */
    public function actionDelete($id_sous_activite, $id_type_theme)
    {
        $this->findModel($id_sous_activite, $id_type_theme)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SousActivitesHasTheme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_sous_activite
     * @param integer $id_type_theme
     * @return SousActivitesHasTheme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_sous_activite, $id_type_theme)
    {
        if (($model = SousActivitesHasTheme::findOne(['id_sous_activite' => $id_sous_activite, 'id_type_theme' => $id_type_theme])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
