<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
	    'css/css.css',
	    'css/css2.css',
	    'css/font-awesome.min.css',
	    'css/bootstrap.min.css',
        'css/mvpready-landing.css',
        'css/mvpready-flat.css',
        'css/animate.css',
        'css/stats.css',
    ];
    public $js = [
	    'js/jquery-1.10.2.min.js',
	    'js/bootstrap.min.js',
	    'js/mvpready-core.js',
	    'js/mvpready-admin.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
