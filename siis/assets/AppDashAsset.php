<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppDashAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $css = [
	    'css/fullcalendar.min.css',
	    'css/ionicons.min.css',
	    
	    'css/font-awesome.min.css',
	    'css/bootstrap.min.css',
	    
        'css/bootstrap.vertical-tabs.min.css',
        'css/AdminLTE1.css',
        'css/_all-skins.min.css',
    ];
    
    public $js = [
	   // 'js/jQuery-2.1.3.min.js',
	    'js/bootstrap.min.js',
	    'js/jquery-ui.min.js',
	    
	    //'js/jquery.slimscroll.min.js',
	    //'js/fastclick.min.js',
	    'js/app.min.js',
	    //'js/wz_tooltip.js',
	    'js/moment.min.js',
	    'js/fullcalendar.min.js',
	    //'js/lang-all.js',
        'js/fr.js',
        
    	'js/highcharts.js',
    	'js/data.js',
    	'js/drilldown.js',
    	'js/no-data-to-display.js',
    	'js/exporting.js',
    		
        'js/jquery.flot.min.js',
        'js/jquery.flot.resize.min.js',
        'js/jquery.flot.pie.min.js',
        'js/jquery.flot.categories.min.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}

