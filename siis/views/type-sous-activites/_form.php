<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TypeSousActivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-sous-activites-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <?= $form->field($model, 'libelle')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-info']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
