<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Monitoringactivites */

$this->title = Yii::t('app', 'Create Monitoringactivites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoringactivites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringactivites-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
