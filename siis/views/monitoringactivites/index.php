<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrMonitoringactivites */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Monitoringactivites');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringactivites-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Monitoringactivites'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'mplan',
            'plan',
            'activites',
            'mode',
            // 'type_activite',
            // 'isExecuted',
            // 'isExecutedBy',
            // 'isExecutedOn',
            // 'isActif',
            // 'isDone',
            // 'id_famille',
            // 'id_akf',
            // 'id_theme',
            // 'commentaires',
            // 'lieu',
            // 'nbre_participants',
            // 'sousactivites',
            // 'eval_lieu',
            // 'eval_contenu',
            // 'eval_retroaction',
            // 'type_rencontre',
            // 'sujets',
            // 'actions',
            // 'module',
            // 'duree',
            // 'remarques',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
