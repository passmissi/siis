<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrMonitoringactivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoringactivites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mplan') ?>

    <?= $form->field($model, 'plan') ?>

    <?= $form->field($model, 'activites') ?>

    <?= $form->field($model, 'mode') ?>

    <?php // echo $form->field($model, 'type_activite') ?>

    <?php // echo $form->field($model, 'isExecuted') ?>

    <?php // echo $form->field($model, 'isExecutedBy') ?>

    <?php // echo $form->field($model, 'isExecutedOn') ?>

    <?php // echo $form->field($model, 'isActif') ?>

    <?php // echo $form->field($model, 'isDone') ?>

    <?php // echo $form->field($model, 'id_famille') ?>

    <?php // echo $form->field($model, 'id_akf') ?>

    <?php // echo $form->field($model, 'id_theme') ?>

    <?php // echo $form->field($model, 'commentaires') ?>

    <?php // echo $form->field($model, 'lieu') ?>

    <?php // echo $form->field($model, 'nbre_participants') ?>

    <?php // echo $form->field($model, 'sousactivites') ?>

    <?php // echo $form->field($model, 'eval_lieu') ?>

    <?php // echo $form->field($model, 'eval_contenu') ?>

    <?php // echo $form->field($model, 'eval_retroaction') ?>

    <?php // echo $form->field($model, 'type_rencontre') ?>

    <?php // echo $form->field($model, 'sujets') ?>

    <?php // echo $form->field($model, 'actions') ?>

    <?php // echo $form->field($model, 'module') ?>

    <?php // echo $form->field($model, 'duree') ?>

    <?php // echo $form->field($model, 'remarques') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
