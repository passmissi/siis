<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Monitoringactivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoringactivites-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'mplan')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'plan')->textInput() ?>

    <?= $form->field($model, 'activites')->textInput() ?>

    <?= $form->field($model, 'mode')->textInput() ?>

    <?= $form->field($model, 'type_activite')->textInput() ?>

    <?= $form->field($model, 'isExecuted')->textInput() ?>

    <?= $form->field($model, 'isExecutedBy')->textInput() ?>

    <?= $form->field($model, 'isExecutedOn')->textInput() ?>

    <?= $form->field($model, 'isActif')->textInput() ?>

    <?= $form->field($model, 'isDone')->textInput() ?>

    <?= $form->field($model, 'id_famille')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'id_akf')->textInput() ?>

    <?= $form->field($model, 'id_theme')->textInput() ?>

    <?= $form->field($model, 'commentaires')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'lieu')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'nbre_participants')->textInput() ?>

    <?= $form->field($model, 'sousactivites')->textInput() ?>

    <?= $form->field($model, 'eval_lieu')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'eval_contenu')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'eval_retroaction')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'type_rencontre')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'sujets')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'actions')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'module')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'duree')->textInput() ?>

    <?= $form->field($model, 'remarques')->textInput(['maxlength' => 100]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
