<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\TypeSousActivites;

/* @var $this yii\web\View */
/* @var $model app\models\TypeThemes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-themes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $item_sous_activites = ArrayHelper::map(TypeSousActivites::find()->all(), 'id', 'libelle'); ?>

    <?= $form->field($model, 'libelle')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'id_type_sous_activite')->textInput()->dropDownList($item_sous_activites,['prompt'=>'-- Choisir sous activite --']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app','Cancel'),['index'],['class'=>'btn btn-info'])  ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
