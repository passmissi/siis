<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppDashAsset;
use yii\helpers\Url;

use app\models\User;
use app\models\Operateurs;

/* @var $this \yii\web\View */
/* @var $content string */

AppDashAsset::register($this);

if(!Yii::$app->user->isGuest)
{
	$user = User::findOne(Yii::$app->user->identity->id);
	$operateur = Operateurs::findOne($user->idOperateurs);
}
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="skin-blue sidebar-mini fixed">
	<?php $this->beginBody() ?>
	    <div class="wrapper">
	    	<header class="main-header">
			  <?= Html::a('<span class="fa fa-group">SIIS</span></i>', 
			      ['site/index'], ['class' => 'logo']);?>
			  <!-- Header Navbar: style can be found in header.less -->
			  <nav class="navbar navbar-static-top" role="navigation">
			    <!-- Navbar Right Menu -->
			    <a class="sidebar-toggle" role="button" data-toggle="offcanvas" href="#">
					<span class="sr-only">
						Toggle navigation
					</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
			    <div class="navbar-custom-menu">
			      <ul class="nav navbar-nav">
			        <!-- Messages: style can be found in dropdown.less-->
			        
			        <?php if (Yii::$app->user->isGuest): ?>
			        	<li class="dropdown user user-menu">
					    	<?= Html::a('<i class="fa fa-sign-in"></i><span class="hidden-xs">Se Connecter</span></i>', ['auth/default/login']);?>
			        	</li>
			        
			        <?php elseif (!Yii::$app->user->isGuest): ?>
			            <li class="dropdown messages-menu">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" 
				           role="button" aria-expanded="false"><i class="fa fa-gears"></i> <span class="caret"></span></a>
				           
				          <ul class="dropdown-menu" role="menu">
				            <li><a href="#"><i class="fa fa-bars"></i>Profil</a></li>
	            			<li><a href="#"><i class="fa fa-gears"></i>Parametres</a></li>
				          </ul>
				        </li>
			            <!-- User Account: style can be found in dropdown.less -->
				        <li class="dropdown user user-menu">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
				           <?php if ($operateur->sexe == 1): ?>
				          		<img src="dist/img/avatar5.png" class="user-image" alt="User Image"/>
				           <?php endif; ?>
				           <?php if ($operateur->sexe == 2): ?>
				          		<img src="dist/img/avatar3.png" class="user-image" alt="User Image"/>
				           <?php endif; ?>
				           <span class="hidden-xs"><?php echo $operateur->prenom.' '.$operateur->nom ?> </span>
				          </a>
				          <ul class="dropdown-menu">
				            <!-- User image -->
				            <li class="user-header">
				              <?php if ($operateur->sexe == 1): ?>
					          		<img src="dist/img/avatar5.png" class="img-circle" alt="User Image"/>
					           <?php endif; ?>
					           <?php if ($operateur->sexe == 2): ?>
					          		<img src="dist/img/avatar3.png" class="img-circle" alt="User Image"/>
					           <?php endif; ?>
				             
				              <p>
				                <?php echo $operateur->prenom.' '.$operateur->nom ?>
				                <small><?php echo $operateur->fonction ?></small>
				              </p>
				            </li>
				            <!-- Menu Body -->
				            <?php if ($operateur->fonction == 'TS'): ?>
					            <li class="user-body">
					              <div class="col-xs-4 text-center">
					              	<?= Html::a('AKF', ['socialworker/index']);?>
					              </div>
					              <div class="col-xs-4 text-center">
					                <?= Html::a('Plans', ['plans/index', 'id' => $operateur->id]);?>
					              </div>
					              <div class="col-xs-4 text-center">
					                <?= Html::a('Suivis', ['socialworker/suiviakf']);?>
					              </div>
					            </li>
				             <?php endif; ?>
				             <?php if ($operateur->fonction == 'OS'): ?>
					            <li class="user-body">
					              <div class="col-xs-6 text-center">
					                <?= Html::a('Rapport', ['rapport/index']);?>
					              </div>
					              <div class="col-xs-6 text-center">
					              	<?= Html::a('Suivis', ['socialworker/massive']);?>
					              </div>
					            </li>
				             <?php endif; ?>
				            <!-- Menu Footer-->
				            <li class="user-footer">
				              <div class="pull-left">
				                <a href="#" class="btn btn-default btn-flat">Profile</a>
				              </div>
				              <div class="pull-right">
				                <?= Html::a('Se Deconnecter', ['/auth/default/logout'],['class' => 'btn btn-default btn-flat'], ['data-method' => 'post']);?> 	
				              </div>
				            </li>
				          </ul>
				        </li>  	
			        <?php endif; ?>
			        
			      </ul>
			    </div>
			  </nav>
			</header>
	    
	        <aside class="main-sidebar">
	        	<section class="sidebar">
	        	   <?php if (!Yii::$app->user->isGuest): ?>
		        		<div class="user-panel">
		        			<div class="pull-left image">
		        			  <?php if ($operateur->sexe == 1): ?>
					          		<img src="dist/img/avatar5.png" class="img-circle" alt="User Image"/>
					           <?php endif; ?>
					           <?php if ($operateur->sexe == 2): ?>
					          		<img src="dist/img/avatar3.png" class="img-circle" alt="User Image"/>
					           <?php endif; ?>
				              
				            </div>
				            <div class="pull-left info">
				              	<p><?= $operateur->prenom.' '.$operateur->nom ?></p>
								<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				            </div>
		        		</div>
	        		<?php endif; ?>
	        		<ul class="sidebar-menu">
	        			<li class="header">MAIN NAVIGATION</li>
	        			<?php 
	        			if((!Yii::$app->user->isGuest) && ($operateur->fonction == 'TS'))
	        			{
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-home"></i><span>Accueil</span> <i class="fa fa-angle-left pull-right"></i>', ['/site/index']);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-dashboard"></i><span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/index', 'id' => 0]);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo ' <a href="#">';
	        				echo '	<i class="fa fa-bar-chart-o"></i> <span>Rapports</span> <i class="fa fa-angle-left pull-right"></i>';
	        				echo '	  <ul class="treeview-menu menu-open">';
	        				echo        '<li>'. Html::a('Thomassique', ['/rapport/index']); '.</li>';
	        				echo        '<li>'. Html::a('Boucan Carre', ['/rapport/boucancarre']); '.</li>';
	        				echo        '<li>'. Html::a('Saut d\'eau', ['/rapport/sautdeau']); '.</li>';
	        				echo        '<li>'. Html::a('Anse-a-Pitre', ['/rapport/ansespitre']); '.</li>';
	        				echo        '<li>'. Html::a('Thiotte', ['/rapport/thiotte']); '.</li>';
	        				echo        '<li>'. Html::a('Grand-Gosier', ['/rapport/grandgosier']); '.</li>';
	        				echo '	  </ul>';
	        				echo ' </a>';
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo ' <a href="#">';
	        				echo '	<i class="fa fa-users"></i> <span>Plan de Developpement</span> <i class="fa fa-angle-left pull-right"></i>';
	        				echo '	  <ul class="treeview-menu menu-open">';
	        				echo        '<li>'. Html::a('Famille', ['/socialworker/famille']); '.</li>';
	        				echo        '<li>'. Html::a('Carnet Familial', ['/socialworker/carnet']); '.</li>';
	        				echo        '<li>'. Html::a('Assignation AKF', ['/socialworker/assignation']); '.</li>';
	        				echo '	  </ul>';
	        				echo ' </a>';
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-list"></i><span>Plans de Travail</span> <i class="fa fa-angle-left pull-right"></i>', ['/plans/index', 'id' => $operateur->id]);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-paste"></i><span>Suivis</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/suiviakf']);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-cogs"></i><span>Configurations</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/profile', 'id' => $operateur->id]);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-sign-out"></i><span>Se Deconnecter</span> <i class="fa fa-angle-left pull-right"></i>', ['/auth/default/logout'],['data-method' => 'post']);
	        				echo '</li>';
	        				 
	        			}elseif ((!Yii::$app->user->isGuest) && ($operateur->fonction == 'SC'))
                        {
                        	
                        }elseif ((!Yii::$app->user->isGuest) && ($operateur->fonction == 'CR'))
                        {
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-home"></i><span>Accueil</span> <i class="fa fa-angle-left pull-right"></i>', ['/site/index']);
                        	echo '</li>';
                        	 
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-dashboard"></i><span>DashBoard</span> <i class="fa fa-angle-left pull-right"></i>', ['/controleregional/index']);
                        	echo '</li>';
                        	
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="glyphicon glyphicon-ok"></i><span>Controle Qualite</span> <i class="fa fa-angle-left pull-right"></i>', ['/controleregional/controlequalite']);
                        	echo '</li>';
                        	
                        	echo '<li class="treeview">';
                        	echo ' <a href="#">';
                        	echo '	<i class="fa fa-bar-chart-o"></i> <span>Rapports</span> <i class="fa fa-angle-left pull-right"></i>';
                        	echo '	  <ul class="treeview-menu menu-open">';
                        	echo        '<li>'. Html::a('Thomassique', ['/rapport/index']); '.</li>';
                        	echo        '<li>'. Html::a('Boucan Carre', ['/rapport/boucancarre']); '.</li>';
                        	echo        '<li>'. Html::a('Saut d\'eau', ['/rapport/sautdeau']); '.</li>';
                        	echo        '<li>'. Html::a('Anse-a-Pitre', ['/rapport/ansespitre']); '.</li>';
                        	echo        '<li>'. Html::a('Thiotte', ['/rapport/thiotte']); '.</li>';
                        	echo        '<li>'. Html::a('Grand-Gosier', ['/rapport/grandgosier']); '.</li>';
                        	echo '	  </ul>';
                        	echo ' </a>';
                        	echo '</li>';
                        	 
                        	echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-cogs"></i><span>Configurations</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/profile', 'id' => $operateur->id]);
	        				echo '</li>';
                        	
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-sign-out"></i><span>Se Deconnecter</span> <i class="fa fa-angle-left pull-right"></i>', ['/auth/default/logout'],['data-method' => 'post']);
                        	echo '</li>';
                        }elseif (((Yii::$app->user->isGuest) && ($this->title == 'Rapport')) || ($this->title == 'Dimensions'))
                        {
                        	echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-home"></i><span>Accueil</span> <i class="fa fa-angle-left pull-right"></i>', ['/site/index']);
	        				echo '</li>';
	        				
	        				echo '<li class="treeview">';
	        				echo ' <a href="#">';
	        				echo '	<i class="fa fa-bar-chart-o"></i> <span>Rapports</span> <i class="fa fa-angle-left pull-right"></i>';
	        				echo '	  <ul class="treeview-menu menu-open">';
	        				echo        '<li>'. Html::a('Thomassique', ['/rapport/index']); '.</li>';
	        				echo        '<li>'. Html::a('Boucan Carre', ['/rapport/boucancarre']); '.</li>';
	        				echo        '<li>'. Html::a('Saut d\'eau', ['/rapport/sautdeau']); '.</li>';
	        				echo        '<li>'. Html::a('Anse-a-Pitre', ['/rapport/ansespitre']); '.</li>';
	        				echo        '<li>'. Html::a('Thiotte', ['/rapport/thiotte']); '.</li>';
	        				echo        '<li>'. Html::a('Grand-Gosier', ['/rapport/grandgosier']); '.</li>';
	        				echo '	  </ul>';
	        				echo ' </a>';
	        				echo '</li>';
                        }elseif ((!Yii::$app->user->isGuest) && ($operateur->fonction == 'OS'))
                        {
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-home"></i><span>Accueil</span> <i class="fa fa-angle-left pull-right"></i>', ['/site/index']);
                        	echo '</li>';
                        	
                        	echo '<li class="treeview">';
                        	echo ' <a href="#">';
                        	echo '	<i class="fa fa-bar-chart-o"></i> <span>Rapports</span> <i class="fa fa-angle-left pull-right"></i>';
                        	echo '	  <ul class="treeview-menu menu-open">';
                        	echo        '<li>'. Html::a('Thomassique', ['/rapport/index']); '.</li>';
                        	echo        '<li>'. Html::a('Boucan Carre', ['/rapport/boucancarre']); '.</li>';
                        	echo        '<li>'. Html::a('Saut d\'eau', ['/rapport/sautdeau']); '.</li>';
                        	echo        '<li>'. Html::a('Anse-a-Pitre', ['/rapport/ansespitre']); '.</li>';
                        	echo        '<li>'. Html::a('Thiotte', ['/rapport/thiotte']); '.</li>';
                        	echo        '<li>'. Html::a('Grand-Gosier', ['/rapport/grandgosier']); '.</li>';
                        	echo '	  </ul>';
                        	echo ' </a>';
                        	echo '</li>';
                        	
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-paste"></i><span>Suivis</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/massive']);
                        	echo '</li>';
                        	 
                        	echo '<li class="treeview">';
	        				echo 	Html::a('<i class="fa fa-cogs"></i><span>Configurations</span> <i class="fa fa-angle-left pull-right"></i>', ['/socialworker/profile', 'id' => $operateur->id]);
	        				echo '</li>';
                        	 
                        	echo '<li class="treeview">';
                        	echo 	Html::a('<i class="fa fa-sign-out"></i><span>Se Deconnecter</span> <i class="fa fa-angle-left pull-right"></i>', ['/auth/default/logout'],['data-method' => 'post']);
                        	echo '</li>';
                        }
	        			?>
	        			
	        		</ul>
	        	</section>
	        </aside>
	        
	        <div class="content-wrapper">
	        	<?= $content ?>
	        </div>
	        
	        <footer class="main-footer">
	        	<div class="container">
				    <div class="row">
				      <div class="col-sm-12">
				        <p class="pull-left">&copy; PASSMISSI <?= date('Y') ?></p>
				        <p class="pull-right"><?= Yii::$app->name ?></p>
				      </div> <!-- /.col -->
				    </div> <!-- /.row -->
				</div>
	        </footer>
	    </div>
	 
	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


