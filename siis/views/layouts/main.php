<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;
use app\models\Operateurs;

/* @va$this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div id="wrapper">
	  <header class="navbar navbar-inverse" role="banner">
	    <div class="container">
	    	<div class="navbar-header">
		        <a href="#" class="masthead-title2 navbar-brand" style="padding: auto;">
		          <span class="fa fa-group"></span>SIIS
		        </a>
		        
		        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
		          <span class="sr-only">Toggle navigation</span>
		          <i class="fa fa-bars"></i>
		        </button>
		    </div> <!-- /.navbar-header -->
		     
		    <nav class="collapse navbar-collapse" role="navigation">
		     	<ul class="nav navbar-nav navbar-right mainnav-menu">
		             <?php 
		             
		             if((Yii::$app->user->isGuest) && ($this->title == 'SIIS Portal'))
                     {
                     	echo  '<li class="active">'. Html::a('Accueil', ['/site/index']); '.</li>';
                     	echo  '<li>'. Html::a('Rapports', ['/rapport/index']); '.</li>';
                     	echo  '<li>'. Html::a('Contactez-nous', ['/site/contact']); '.</li>';
                     	echo  '<li>'.Html::a('Connexion', ['/auth/default/login']);'.</li>';
                     }
                     elseif((Yii::$app->user->isGuest) && ($this->title == 'Contact'))
                     {
                     	echo  '<li>'. Html::a('Accueil', ['/site/index']); '.</li>';
                     	echo  '<li>'. Html::a('Rapports', ['/rapport/index']); '.</li>';
                     	echo  '<li class="active">'. Html::a('Contactez-nous', ['/site/contact']); '.</li>';
                     	echo  '<li>'.Html::a('Connexion', ['/auth/default/login']);'.</li>';
                     }
                     elseif((!Yii::$app->user->isGuest) && ($this->title == 'SIIS Portal'))
                     {
                     	$user = User::findOne(Yii::$app->user->identity->id);
                     	$operateurs = Operateurs::findOne($user->idOperateurs);
                     	
                     	echo  '<li class="active">'. Html::a('Accueil', ['/site/index']); '.</li>';
                     	if($operateurs->fonction == 'SC'){
                     		echo  '<li>'.Html::a('Modules', ['']);'.</li>';
                     	}elseif ($operateurs->fonction == 'TS'){
                     		echo  '<li>'.Html::a('Modules', ['socialworker/index']);'.</li>';
                     	}elseif ($operateurs->fonction == 'OS'){
                     		echo  '<li>'.Html::a('Modules', ['socialworker/massive']);'.</li>';
                     	}elseif ($operateurs->fonction == 'CR'){
                     		echo  '<li>'.Html::a('Modules', ['controleregional/index']);'.</li>';
                     	}else{
                     		echo  '<li>'.Html::a('Administration', ['']);'.</li>';
                     	}
                     	
                     	echo  '<li>'. Html::a('Rapports', ['/rapport/index']); '.</li>';
                     	echo  '<li>'. Html::a('Contactez-nous', ['/site/contact']); '.</li>';
                     	echo  '<li>'.Html::a('Deconnexion ('. Yii::$app->user->identity->username . ')', ['/auth/default/logout'],['data-method' => 'post']);'.</li>';
                     }
                     elseif ((!Yii::$app->user->isGuest) && ($this->title == 'Contact'))
                     {
                     	$user = User::findOne(Yii::$app->user->identity->id);
                     	$operateurs = Operateurs::findOne($user->idOperateurs);
                     	
                     	echo  '<li>'. Html::a('Accueil', ['/site/index']); '.</li>';
                     	if($operateurs->fonction == 'SC'){
                     		echo  '<li>'.Html::a('Modules', ['']);'.</li>';
                     	}elseif ($operateurs->fonction == 'TS'){
                     		echo  '<li>'.Html::a('Modules', ['socialworker/index']);'.</li>';
                     	}elseif ($operateurs->fonction == 'CR'){
                     		echo  '<li>'.Html::a('Modules', ['controleregional/index']);'.</li>';
                     	}else{
                     		echo  '<li>'.Html::a('Administration', ['']);'.</li>';
                     	}
                     	
                     	echo  '<li>'. Html::a('Rapports', ['/rapport/index']); '.</li>';
                     	echo  '<li class="active">'. Html::a('Contactez-nous', ['/site/contact']); '.</li>';
                     	echo  '<li>'.Html::a('Deconnexion ('. Yii::$app->user->identity->username . ')', ['/auth/default/logout'],['data-method' => 'post']);'.</li>';
                     	
                     }
                     
                     
		             ?>
		        </ul>
		        
		        
		        <ul class="nav navbar-nav navbar-social navbar-left"> 
		          <li>
		            <a data-original-title="Facebook" href="javascript:;" class="ui-tooltip" title="" data-placement="bottom">
		              <i class="fa fa-facebook"></i>
		              <span class="navbar-social-label">Facebook</span>
		            </a>
		          </li> 
		          <li>
		            <a data-original-title="Twitter" href="javascript:;" class="ui-tooltip" title="" data-placement="bottom">
		              <i class="fa fa-twitter"></i>
		              <span class="navbar-social-label">Twitter</span>
		            </a>
		          </li>
		          <li>
		            <a data-original-title="Google+" href="javascript:;" class="ui-tooltip" title="" data-placement="bottom">
		              <i class="fa fa-google-plus"></i>
		              <span class="navbar-social-label">Google+</span>
		            </a>
		          </li>  
		        </ul>
		    </nav>
	    </div>
	  </header>
	  
	  <?= $content ?>
	      
	</div>
	<footer class="copyright">
	  <div class="container">
	    <div class="row">
	      <div class="col-sm-12">
	        <p class="pull-left">&copy; PASMISSI <?= date('Y') ?></p>
	        <p class="pull-right"><?= Yii::$app->name ?></p>
	      </div> <!-- /.col -->
	    </div> <!-- /.row -->
	  </div>
	</footer>
	
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
