<?php

use app\models\Operateurs;
use app\models\Famille;
use app\models\Membre;
use app\models\FamilyAkf;
use app\models\Communes;

$x =Yii::$app->getRequest()->getQueryParam('akf');
$this->title = "Liste Famille PDF";
$famille = FamilyAkf::find()->where(['operateur_akf' => $x])->all();

$i = 0;
$operateur = Operateurs::findOne($x);
?>

   <div class="content">
		<div class="container">
	       <br/>
		   <div class="row">
		       <div class="Container">
		        	<h4 class="content-title" style="text-align:center;">
				       <span> KORE FANMI - LISTE DES FAMILLES ASSOCIEES <?php echo $nb ?> ( <?php echo $operateur->nom.' '.$operateur->prenom;?> )</span>
				    </h4>
		       </div>
		   </div>
		 </div>
	</div>
	<div class="row">
		<div class="container">
			 	 		<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
			 	 			<tr>
					          <td width="15%" align="center" bgcolor="#E5E5E5">Id<br>
					            Famille</td>
					          <td width="45%" align="center" bgcolor="#E5E5E5"><br><br>Nom Chef Fanmi</td>
					          <td width="20%" align="center" bgcolor="#E5E5E5"><br><br>Commune</td>
					          <td width="20%" align="center" bgcolor="#E5E5E5"><br><br>Localite</td>
					        </tr>
					        <?php foreach($famille as $record): ?>
					        <?php $i += 1; ?>
					        <tr>
					        	<td style="text-align:center"><br><?= $record->famille ?></td>
								<td style="text-align:center"><br><?php 
								 		$famille = Membre::find()
								 			->where(['famille' => $record->famille, 'relation_chef_famille' => 1])
								 		    ->one();
						          		if($famille != null)echo $famille->nom.' '.$famille->prenom;
						          		else{
						          			echo '---- Pas de chef de famille ----';
						          		} ?></td>
						        <td><br>&nbsp;<?php 
						        		$comm = Communes::findOne($record->commune);
						                echo $comm->n_communal; ?></td>
						        <td><br>&nbsp;<?= $record->localite ?></td>
					        </tr> 
							<?php endforeach;?>
							<tr>
						          <td bgcolor="#E5E5E5">&nbsp;</td>
						          <td bgcolor="#E5E5E5">&nbsp;</td>
						          <td bgcolor="#E5E5E5">&nbsp;</td>
						          <td bgcolor="#E5E5E5">&nbsp;</td>
						    </tr>
			 	 		</table>
			 	 	
		</div>
	</div>
	