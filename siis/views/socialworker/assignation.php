<?php


use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FamilyAkf;
use app\models\Communes;
use app\models\Famille;
use yii\helpers\ArrayHelper;

$this->title = "Assignation";

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$listlocalite = ArrayHelper::map(Famille::find()->select('localite')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('localite')->all(),
		'localite','localite');

$script1 = <<< JS
function init_click_handlers(val) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/familyakf',
			data: { localite: val },
			success: function(data){
				var divOne15= document.getElementById('familyakf-assignes');
		        divOne15.disabled = true;
	            divOne15.value = data[0];
		        var divOne16= document.getElementById('familyakf-quantite');
	            divOne16.value = data[1];
			},
			error: function() {
			},
		});
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Plan de Developpement
       <small>Assignation AKF</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Assignation</li>
    </ol>
</section>

<section class="content">


<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-body">
				<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
								<i class="fa fa-inbox"></i>
								<h3 class="box-title">PANEL DE GESTION</h3>
							</div>
							
							<!-- compose message btn -->
							<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
							<i class="fa fa-plus-square"></i>Effectuer une assignation</a>
							<!-- Navigation - folders-->
							
							<div style="margin-top: 15px;">
								<ul class="nav nav-pills nav-stacked">
									<?php
										$user = User::findOne(Yii::$app->user->identity->id);
										$operateur = Operateurs::findOne($user->idOperateurs);
										echo  '<li class="header">Actions</li>';
										echo  '<li class="active">'. Html::a('<i class="fa fa-chain"></i> Assignation AKF', ['/socialworker/assignation']); '.</li>';
										echo  '<li>'. Html::a('<i class="fa fa-chain-broken"></i> Desassignation AKF', ['/socialworker/desassignation']); '.</li>';
									?>
	                            </ul>
	                        </div>
	                    </div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
						    	<div class="col-sm-3">
						    		 <?php $form = ActiveForm::begin([
								    	'options' => ['class' => 'form-inline'],
								      ]); ?>
									    <div class="input-group">
										    <div class="input-group-btn">
										      <?= Html::submitButton('<i class="glyphicon glyphicon-book"></i>', ['class' => 'btn btn-sm btn-success', 
						    		 			'target' => '_blank']) ?>
							                </div>
							                <!-- /btn-group -->
							                 <?= $form->field($model1, 'id')->label(false)
										             ->dropDownList($listAKF, ['style'=>'width:100%', 'class' => 'form-control input-sm','prompt' => 'Choisir AKF']) ?>
									    </div>
								    <?php ActiveForm::end(); ?>
                                </div>
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                	<?php echo $this->render('/family-akf/_search', ['model' => $searchModel]); ?>
                                </div>
						    </div>
                    	    <div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Liste des Assignations</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         [ 'class' => 'yii\grid\SerialColumn'],
																[
																	'header'=>'Commune',
																	'attribute'=>'commune',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = Communes::findOne($model->commune);
																		return ''.$count->n_communal;
																	},
																],
																'localite', 
																[
																'header'=>'Nom AKF',
																'attribute'=>'operateur_akf',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Operateurs::findOne($model->operateur_akf);
																	if($count != null){return ''.$count->nom.' '.$count->prenom;}
																	else{return '--';}
																	},
																], 
																[
																	'header'=>'Assignes',
																	'attribute'=>'nombre',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = FamilyAkf::find()->where(['operateur_akf' => $model->operateur_akf, 'localite' => $model->localite])->count();
																		return $count.' '.'Familles';
																	},
																],
                                                                [
																	'class' => 'yii\grid\ActionColumn',
																	'template' => '{effacer}',
																	'buttons' => [
																	'effacer' => function ($url,$model) {
																		return Html::a(
																				'<span class="fa fa-trash-o"></span>', [
																				'family-akf/delete2', 'localite' => $model->localite, 'akf' => $model->operateur_akf],
																				['class' => 'btn btn-xs btn-danger', 
																				 'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer cet AKF?', 
																				 'onmouseover'=>'Tip("Effacer AKF")', 'onmouseout'=>'UnTip()']);
																	},
																],
                                                         ]]
					                    			 ]);
					                    		?>	    		
			                    </div>
			                </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Assigner un AKF </h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	        
	        <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'localite')->dropDownList($listlocalite, 
		    				['prompt' => 'choisir localite', 'onchange'=> 'init_click_handlers(this.value)']) ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'assignes')->textInput(['maxlength' => true]) ?>
		    	</div>
		    </div>
	        
	        <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'operateur_akf')->dropDownList($listAKF, ['prompt' => 'choisir akf']) ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'quantite')->textInput(['maxlength' => true]) ?>
		    	</div>
		    </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation', 'name' => 'creation']) ?>
	        <?= Html::submitButton('Modifier', ['class'=>'btn btn-sm btn-primary', 'id' => 'modification', 'style' => 'display :none;', 'name' => 'modification']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

