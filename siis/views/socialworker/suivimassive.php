<?php

use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use app\models\Monitoringsousactivites;
use app\models\TypeActivites;
use app\models\TypeSousActivites;
use app\models\Monitoringplan;

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$listTypeActivites = ArrayHelper::map(TypeActivites::find()->where(['mode' => 1])->asArray()->all(), 'id', 'libelle');
$listTypeSousActivites = ArrayHelper::map(TypeSousActivites::find()->asArray()->all(), 'id', 'libelle');

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$script1 = <<< JS
function init_click_handlers(val) {
	if(val == 1){
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/list',
			data: { id: val },
			success: function(data){
				$("#monitoringsousactivites-typetheme").html(data);
			},
			error: function() {
				
			},
		});
		
		var divOne288= document.getElementById('type');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('choix');
	    divOne288.style.display='none';
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}else if(val == 2){
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/list',
			data: { id: val },
			success: function(data){
				$("#monitoringsousactivites-typetheme").html(data);
			},
			error: function() {
				
			},
		});
		
		var divOne288= document.getElementById('type');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('choix');
	    divOne288.style.display='none';
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}else if(val == 3){
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/list',
			data: { id: val },
			success: function(data){
				$("#monitoringsousactivites-secteur").html(data);
			},
			error: function() {
				
			},
		});
		
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('choix');
	    divOne288.style.display='none';
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}else if(val == 4){
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/list',
			data: { id: val },
			success: function(data){
				$("#monitoringsousactivites-typetheme").html(data);
			},
			error: function() {
				
			},
		});
		
		var divOne288= document.getElementById('type');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('choix');
	    divOne288.style.display='none';
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}else if(val == 6 || val == 7){
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('choix');
	    divOne288.style.display='block';
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);


$script2 = <<< JS
function init_click_handlers1(val) {
	if(val == 1){
		
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('ok');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('poids');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('monitoringsousactivites-poids');
	    divOne288.style.display='none';
		document.getElementById("ok").className = "col-lg-12";
		
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
	}else if(val == 2){
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('ok');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ok1');
	    divOne288.style.display='block';
		document.getElementById("ok1").className = "col-lg-12";
		var divOne288= document.getElementById('poids');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('monitoringsousactivites-poids');
	    divOne288.style.display='block';
		
		
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
		
	}else if(val == 3){
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ok3');
	    divOne288.style.display='none';
		document.getElementById("ok2").className = "col-lg-12";
		var divOne288= document.getElementById('poids');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('monitoringsousactivites-poids');
	    divOne288.style.display='none';
		
		
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
		
	}else if(val == 4){
		var divOne288= document.getElementById('type');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('commentaire');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('qte');
	    divOne288.style.display='block';
		
		
		var divOne288= document.getElementById('avaccin');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('ok3');
	    divOne288.style.display='none';
		document.getElementById("ok2").className = "col-lg-12";
		var divOne288= document.getElementById('poids');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('monitoringsousactivites-poids');
	    divOne288.style.display='none';
		
		
		var divOne288= document.getElementById('ataille');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('apb');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('aen');
	    divOne288.style.display='block';
		var divOne288= document.getElementById('reference');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('quantite');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event');
	    divOne288.style.display='none';
		var divOne288= document.getElementById('event1');
	    divOne288.style.display='none';
		
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script3 = <<< JS
function init_click_handlers4(val) {
	var divOne288= document.getElementById('monitoringsousactivites-type_sous_activites');
	divOne288.disabled = false;
}
JS;
$this->registerJs($script3, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                 <i class="fa fa-inbox"></i>
                                 <h3 class="box-title">PANEL DE GESTION</h3>
                            </div>
                            <!-- compose message btn -->
                            <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal" disabled="true">
                                 <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                            <!-- Navigation - folders-->
                            <div style="margin-top: 15px;">
                                 <ul class="nav nav-pills nav-stacked">
                                      <?php 
                                                	
                                          $user = User::findOne(Yii::$app->user->identity->id);
                                          $operateur = Operateurs::findOne($user->idOperateurs);
                                          
										  if($operateur->fonction == 'OS'){
                                          	echo  '<li class="header">Actions</li>';
                                          	echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                          }
                                          elseif ($operateur->fonction == 'TS'){
                                          	echo  '<li class="header">Actions</li>';
                                          	echo  '<li>'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                          	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                          	echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                          	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats']); '.</li>';
                                          }
                                          
                                     ?>
                                </ul>
                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<h3 class="page-header">Massive Monitoring 
                    				<?php 
                    				    $x =Yii::$app->getRequest()->getQueryParam('id');
                    					$plan = Monitoringplan::findOne($x);
                    					echo '-'.$plan->mois_nreg.' '.$plan->annee_nreg.' (+)';
                                    ?>
                    			</h3>
                    			<div  class="col-md-5">
                    				<?php $form = ActiveForm::begin([]); ?>
                    					<div class="row">
											<div class="col-lg-6">
												 <?= $form->field($model, 'type_activites_reg')
									    		->dropDownList($listTypeActivites, ['prompt' => 'Choisir un type', 'onchange' => 'init_click_handlers4(this.value);'])?>
											</div>
											
											<div class="col-lg-6">
												<?= $form->field($model, 'type_sous_activites')
												->dropDownList($listTypeSousActivites, ['prompt' => 'Choisir un type', 'disabled' => 'disabled', 'onchange' => 'init_click_handlers(this.value);'])
									    		?>
											</div>
										</div>
										
										<div class="row" id="choix" style="display:none;">
											<div class="col-lg-12">
												<?= $form->field($model, 'choix')
												->dropDownList(['1' => 'Vaccin', '2' => 'Rapport PT', '3' => 'Oedeme', '4' => 'EN',
																], 
									                 ['prompt' => 'Faire un choix', 'onchange' => 'init_click_handlers1(this.value);']) 
									    		->label(null, ['id' => 'choix'])?>
											</div>
										</div>
										
										<div class="row" id="avaccin" style="display:none;">
											<div class="col-lg-6" id="ok">
												<?= $form->field($model, 'vaccin')
												->dropDownList(['1' => 'Polio 0', '2' => 'Polio 1', '3' => 'Polio 2', '4' => 'Polio 3',
																'5' => 'Polio 4', '6' => 'BCG', '7' => 'DTP 1', '8' => 'DTP 2', 
									       						'9' => 'DTP 3', '10' => 'Woujol Ribeyol', '11' => 'DT', '12' => 'Pentavalan',
																'13' => 'Completement Vaccine', '14' => 'N/A'], 
									                 ['prompt' => 'Choisir un vaccin']) 
									    		->label(null, ['id' => 'vaccin'])?>
											</div>
											<div class="col-lg-6" id="ok1">
												 <?= $form->field($model, 'poids')->textInput() 
									    		->label(null, ['id' => 'poids'])?>
											</div>
										</div>
										
										<div class="row" id="ataille" style="display:none;">
											<div class="col-lg-6">
												<?= $form->field($model, 'taille')->textInput() 
									    		->label(null, ['id' => 'taille'])?>
												
											</div>
											<div class="col-lg-6">
												<?= $form->field($model, 'rapportpt')
												->dropDownList(['1' => '< -4', '2' => '< -3', '3' => '> -3 et < -2', '4' => '>= -2'],
														['prompt' => 'Choisir un rapport pt'])
									    		->label(null, ['id' => 'rapportpt'])?>
											</div>
										</div>
									    
										<div class="row" id="apb" style="display:none;">
											<div class="col-lg-6" id="ok3">
												<?= $form->field($model, 'pb')->textInput() 
									    		->label(null, ['id' => 'pb'])?>
											</div>
											<div class="col-lg-6" id="ok2">
												<?= $form->field($model, 'oedeme')
												->dropDownList(['1' => 'Absan Edem', '2' => '+', '3' => '++', '4' => '+++'],
														['prompt' => 'Choisir un oedeme'])
									    		->label(null, ['id' => 'oedeme'])?>
												
											</div>
										</div>
									   
									    <div class="row" id="aen" style="display:none;">
											<div class="col-lg-12">
												 <?= $form->field($model, 'en')
												 ->dropDownList(['1' => 'Nomal/Sen', '2' => 'Malnitrisyon Egi Modere', 
																 '3' => 'Malnitrisyon Egi Seve san Konplikasyon', '4' => 'Malnitrisyon Egi Seve ak Konplikasyon'],
												 		['prompt' => 'Choisir EN'])
									    		->label(null, ['id' => 'en']) ?>
											</div>
										</div>
									    
									    <div class="row" id="reference" style="display:none;">
											<div class="col-lg-6">
												<?= $form->field($model, 'secteur')
												->dropDownList([], ['prompt' => 'Choisir un secteur'])
									    		->label(null, ['id' => 'secteur'])?>
											</div>
											<div class="col-lg-6">
												<?= $form->field($model, 'institution')->textInput(['maxlength' => 100]) 
									    		->label(null, ['id' => 'institution'])?>
											</div>
										</div>
										
									    <div class="row" id="type" style="display:none;">
											<div class="col-lg-12">
												<?= $form->field($model, 'typetheme')
												->dropDownList([], ['prompt' => 'Choisir un theme'])
									    		->label(null, ['id' => 'typetheme']) ?>
										    </div>
										</div>
										
									    <div class="row" id="commentaire" style="display:none;">
											<div class="col-lg-7">
												<?= $form->field($model, 'commentaire')->textArea(['rows' => '3'])
									    		->label(null, ['id' => 'commentaire']) ?>
											</div>
											<div class="col-lg-5">
												<?= $form->field($model, 'date_creation')->input('date', ['required'])
    		                                       ->label(null, ['id' => 'date_creation']) ?>
											</div>
										</div>
									    
									    <div class="row" id="quantite" style="display:none;">
											<div class="col-lg-12">
												<?= $form->field($model, 'quantite')->textInput()
									    		->label(null, ['id' => 'quantite']) ?>
											</div>
										</div>
									    
									    <div class="row" id="event" style="display:none;">
											<div class="col-lg-6">
												 <?= $form->field($model, 'evenement_type')->textInput() 
									    		->label(null, ['id' => 'evenement_type'])?>
											</div>
											<div class="col-lg-6">
												<?= $form->field($model, 'date_evenement')->textInput()
									    		->label(null, ['id' => 'date_evenement']) ?>
									    		
											</div>
										</div>
									   
										<div class="row" id="event1" style="display:none;">
											<div class="col-lg-6">
												<?= $form->field($model, 'evenement')->textInput(['maxlength' => 100])
									    		->label(null, ['id' => 'evenement']) ?>
											</div>
										</div>
									    
									    <div class="row" id="qte" style="display:none;">
											<div class="col-lg-12">
												 <?= $form->field($model, 'qteBen_reg')->textInput() 
									    		     ->label(null, ['id' => 'qteBen_reg_type'])?>
											</div>
										</div>
										
										<div class="modal-footer">
										     <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Ajouter') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name' => 'creation']) ?>
										
										</div>
                    				<?php ActiveForm::end(); ?>	
                    			</div>
                    			<div  class="col-md-7">
                    				<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         [ 'class' => 'yii\grid\SerialColumn'],
																'date_creation',
																[
																'header'=>'Activites',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
				                                                    $type = TypeActivites::findOne($model->type_activites_reg);
				                                                    $type2 = TypeSousActivites::findOne($model->type_sous_activites);
																	    return ''.$type->libelle.'/'.$type2->libelle;
																	},
																], 
																[
																	'header'=>'Periode',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$x =Yii::$app->getRequest()->getQueryParam('id');
																		$plan = Monitoringplan::findOne($x);
																		return $plan->mois_nreg.' '.$plan->annee_nreg;
																	},
																],
																'qteBen_reg',
                                                                [
																	'class' => 'yii\grid\ActionColumn',
																	'template' => '{effacer}',
																	'buttons' => [
																	'effacer' => function ($url,$model) {
																		$count = Monitoringsousactivites::find()->where(['mplan' => $model->id])->count();
																		$y =Yii::$app->getRequest()->getQueryParam('id1');
																		$x =Yii::$app->getRequest()->getQueryParam('id');
																		return !$count ? Html::a(
																			 '<span class="fa fa-trash-o"></span>',
																			 ['monitoringsousactivites/delete22', 'id' => $model->id, 'id1' => $y, 'id2' => $x],
																			 ['class' => 'btn btn-xs btn-danger',
																			  'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer cette activite?',
																			  'onmouseover'=>'Tip("Effacer Activites")', 'onmouseout'=>'UnTip()']): "";
																	},
																],
                                                         ]]
					                    			 ]);
					                    		?>
                    			</div>
                    		</div>
                       </div>
               </div>
           </div>
        </div>
    </div>
</section>