<?php

use kartik\grid\GridView;
//use yii\data\ActiveDataProvider;
use app\models\Plans;
use app\models\User;
use kartik\export\ExportMenu;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Growl;

use yii\widgets\DetailView;

$this->title = "Profile";
?>


<section class="content-header">
	<h1>
       Configurations
       <small>Profile</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Configurations</a></li>
       <li class="active">Profile</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                <i class="fa fa-inbox"></i>
                                <h3 class="box-title">PANEL DE GESTION</h3>
                            </div>
                            <!-- compose message btn -->
                            <div class="row">
                                <div class="col-md-6">
                                	<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                  		<i class="fa fa-edit"></i> Mot de Passe</a>
                                </div>
                                <div class="col-md-6">
                                	<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal1">
                                  		<i class="fa fa-edit"></i> Donnees Perso</a>
                                </div>
                            </div>
                            
                            <!-- Navigation - folders-->
                            <div style="margin-top: 15px;">
                                <ul class="nav nav-pills nav-stacked">
                                    <?php 
                                                	
                                        $user = User::findOne(Yii::$app->user->identity->id);
                                        $operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                        echo  '<li class="header">Actions</li>';
                                        echo  '<li class="active">'. Html::a('<i class="fa fa-bars"></i>Profile', ['/socialworker/profile']); '.</li>';
                                        //echo  '<li>'. Html::a('<i class="fa fa-gears"></i>Parametres', ['']); '.</li>';
                                                	 
                                    ?>
                                </ul>
                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<?php if ($response == 1): ?>
				          		<div class="row">
									<div class="col-md-12">
										<div class="alert alert-success alert-dismissable">
											 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											 Changement de Mot de passe reussi
										</div>
									</div>
								</div>
				          	<?php elseif ($response == -1): ?>
				          	 	<div class="row">
									<div class="col-md-12">
										<div class="alert alert-danger alert-dismissable">
											 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											 Changement de Mot de passe echoue / Mot de passe incorrect
										</div>
									</div>
								</div>
				            <?php endif; ?>
                    		
                    		<div class="row">
                    			<h3 class="page-header">Informations Generales</h3>
                    			<div class="col-md-6">
                    				<?= DetailView::widget([
								        'model' => $user,
								        'attributes' => [
								            'id',
								            'username',
								            'email:email',
								            'status',
								            'idOperateurs',
								        ],
								    ]) ?>
                    			</div>
                    			<div class="col-md-6">
                    				<?= DetailView::widget([
								        'model' => $operateur,
								        'attributes' => [
								            'id',
								            'nom',
								            'prenom',
								            'sexe',
								            'email:email',
								            'fonction',
								            'commune_lie',
								        ],
								    ]) ?>
                    			</div>
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Changement Mot de Passe </h4>
      </div>
      <?php $form = ActiveForm::begin([
      		'enableClientValidation' => true,
      		'validateOnSubmit' => true,
      		'enableAjaxValidation' => false,
      ]); ?>
	      <div class="modal-body">
	        <?= $form->field($user, 'id')->textInput(['maxlength' => 255, 'style' => 'display :none;',])->label(false, ['id' => 'code']) ?>
	        <div class="row">
	        	<div class="col-md-6">
	        		<?= $form->field($user, 'password')->passwordInput(['maxlength' => 255]) ?>
	        	</div>
	        	<div class="col-md-6">
	        		<?= $form->field($user, 'new_password')->passwordInput(['maxlength' => 255]) ?>
	        	</div>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $user->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation1', 'name' => 'creation1']) ?>
	        </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Modification donnees personnelles </h4>
      </div>
      <?php $form = ActiveForm::begin([
      		'enableClientValidation' => true,
      		'validateOnSubmit' => true,
      		'enableAjaxValidation' => false,
      ]); ?>
	      <div class="modal-body">
	        <?= $form->field($operateur, 'id')->textInput(['maxlength' => 255, 'style' => 'display :none;',])->label(false, ['id' => 'code']) ?>
	        <?= $form->field($operateur, 'nom')->textInput(['maxlength' => 255]) ?>
		    <?= $form->field($operateur, 'prenom')->textInput(['maxlength' => 255]) ?>
		    <?= $form->field($operateur, 'sexe')->dropDownList(
		          ['1'=>'Masculin','2'=>'Feminin'],['prompt'=>'-- Choisir un sexe --']
		    ); ?>
		    <?= $form->field($operateur, 'email')->textInput(['maxlength' => 255]) ?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($operateur->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $operateur->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation', 'name' => 'creation']) ?>
	        </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>