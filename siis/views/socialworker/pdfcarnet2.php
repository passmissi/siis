<?php
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Operateurs;
use app\models\Plans;
use app\models\Famille;
use app\models\Membre;
use app\models\Communes;
use app\models\Chroniques;
use app\models\TypeThemes;
use app\models\FamilyAkf;


$x =Yii::$app->getRequest()->getQueryParam('localite');
$y =Yii::$app->getRequest()->getQueryParam('famille');
$this->title = "Carnet Familial PDF";
?>
<?php 
     $m = 0;
     if($x != null && $y == 0){
     	$familles = Famille::find()->where(['localite' => $localite])->all();
     }
     elseif($x == null || $x == 'ok' && $y != 0){
     	$familles = Famille::find()->where(['id' => $famille])->all();
     }
     
?>
<?php foreach($familles as $record): ?>
		<?php 
	      
	      if($x != null){
	      	//$famille = Famille::findOne($record->id);
			$chef = Membre::find()->where(['famille' => $record->id, 'relation_chef_famille' => 1])->all();
			$membres = Membre::find()->where(['famille' => $record->id])->all();
			$enfants = Membre::find()->where(['famille' => $record->id])->andWhere(['<=', 'age', 5])->all(); // a refaire
			$type = TypeThemes::find()->where(['id_type_sous_activite' => 4])->all();
			 
			 
			foreach ($chef as $c){
				$nomcomplet = $c->nom.' '.$c->prenom;
			}
	      }
	      
	      $user = User::findOne(Yii::$app->user->identity->id);
	      $operateur = Operateurs::findOne($user->idOperateurs);
	      $ts = "";
	      $akf = "";
	      
		  if($x != null){
	      	$famille_akf = FamilyAkf::find()->where(['famille' => $record->id, 'localite' => $x])->one();
	      	if($famille_akf != null){
	      		$operateurakf = Operateurs::findOne($famille_akf->operateur_akf);
	      		$operateurts = Operateurs::findOne($famille_akf->operateur_ts);
	      		
	      		$ts = $operateurts->nom.' '.$operateurts->prenom;
	      		$akf = $operateurakf->nom.' '.$operateurakf->prenom;
	      	}
	      }
	      
	  ?>
	      <div class="content">
		      <div class="container">
		      <br/>
		        <div class="row">
		        	<div class="Container">
		        		<h4 class="content-title" style="text-align:center;">
				          <span> KORE FANMI - CARNET FAMILIAL <?php $m += 1; echo $m;?></span>
				        </h4>
		        	</div>
		        </div>
		        <br/>
		        <div class="row">
		        
		            <div class="container">
		            	<table width="100%" border="0">
				        <tr>
				          <td style="width:30%">
					          <table width="100%" border="1" align="right" cellpadding="2" cellspacing="2">
					            <tr>
					              <td style="text-align:center;">ID FAMILIAL<br><strong><?php 
					               if(($x != null && $x != 'ok') && $y == 0){echo $record->id;}else{ echo $record->id;} ?></strong></td>
					            </tr>
					          </table>
				          </td>
				          <td style="width:40%"></td>
				          <td style="width:30%">
					          <table width="100%" border="1" align="right" cellpadding="2" cellspacing="2">
					            <tr>
					              <td style="text-align:center;">DEGRE DE VULNERABILITE<br><strong>X</strong></td>
					            </tr>
					          </table>
							    </td>  
				        </tr>
				      </table>
		            </div>
		            
				      <br/>
				      
		              <table width="100%" border="0" style="font-family: Helvetica;font-size: 12px;">
						    <tr>
						      <td>&nbsp;</td>
						    </tr>
						    <tr>
						      <td>&nbsp;</td>
						    </tr>
						    
					      <td bordercolor="#000000"><table width="100%" border="0" cellpadding="0" cellspacing="0">
					        <tr>
					          <td><table style="border: 1px solid black;border-collapse: collapse;" width="100%" border="1"  cellpadding="0" cellspacing="0">
					            <tr style="border: 1px solid black;border-collapse: collapse;">
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%" height="30"><br/>
					                <strong>&nbsp;&nbsp;A1: Commune:</strong>&nbsp;&nbsp;<?= $record->commune ?></td>
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%"><br/>
					                <strong>&nbsp;&nbsp;A2: Section Communale:</strong>&nbsp;&nbsp;<?= $record->section_communale ?></td>
					            </tr>
					          </table></td>
					        </tr>
					        <tr>
					          <td><table style="border: 1px solid black;border-collapse: collapse;" width="100%" border="1" cellpadding="0" cellspacing="0">
					            <tr style="border: 1px solid black;border-collapse: collapse;">
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%" height="30">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A3: Nom de la Localite:</strong>&nbsp;&nbsp;<?= $record->localite ?></td>
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A4: Code Zone IACP:</strong>&nbsp;&nbsp;<?= $record->ID_HH_A5_2 ?></td>
					            </tr>
					          </table></td>
					        </tr>
					        <tr>
					          <td><table style="border: 1px solid black;border-collapse: collapse;" width="100%" border="1" cellpadding="0" cellspacing="0">
					            <tr style="border: 1px solid black;border-collapse: collapse;">
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%" height="30">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A5: Numero de la Maison :</strong>&nbsp;&nbsp;<?= $record->ID_HH_A5_3 ?></td>
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A6: Numero Telephone:</strong>&nbsp;&nbsp;<?= $record->telephone_maison ?></td>
					            </tr>
					          </table></td>
					        </tr>
					        <tr>
					          <td><table style="border: 1px solid black;border-collapse: collapse;" width="100%" border="1" cellpadding="0" cellspacing="0">
					            <tr style="border: 1px solid black;border-collapse: collapse;">
					              <td style="border: 1px solid black;border-collapse: collapse;" width="30%" height="30">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A7: Date Enquete:</strong>&nbsp;&nbsp;Avril 2015</td>
					             <td style="border: 1px solid black;border-collapse: collapse;" width="35%">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A8: Nom Agent:</strong>&nbsp;&nbsp;<?php 
					                  if($x != null && $y == 0){echo $akf;} ?></td>
					              <td style="border: 1px solid black;border-collapse: collapse;" width="35%">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A9: Nom du TS:</strong>&nbsp;&nbsp;<?php 
					                  if($x != null && $y == 0){echo $ts;} ?></td>
					            </tr>
					          </table></td>
					        </tr>
					        <tr>
					          <td><table style="border: 1px solid black;border-collapse: collapse;" width="100%" border="1" cellpadding="0" cellspacing="0">
					            <tr style="border: 1px solid black;border-collapse: collapse;">
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%" height="30">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A10: Coordonnee X:</strong>&nbsp;&nbsp;<?= $record->latitude_gps ?></td>
					              <td style="border: 1px solid black;border-collapse: collapse;" width="50%">&nbsp;<br/>
					                  <strong>&nbsp;&nbsp;A11: Coordonnee Y:</strong>&nbsp;&nbsp;<?= $record->longitude_gps ?></td>
					            </tr>
					          </table></td>
					        </tr>
					        
					      </table></td>
					    </tr>
						    <tr>
						      <td>&nbsp;<br></td>
						    </tr>
					  </table>
			    </div>
			    <br/>
			    <div class="row">
			    	<div class="Container">
			    		<table width="100%" border="0">
						    <tr>
						      <td align="center">Composition et Caracteristiques Principales de la Famille</td>
						    </tr>
						    <tr>
						      <td align="center"></td>
						    </tr>
					  </table> 
					  <table width="100%" border="0">  
					    <tr>
					      <td width="100%"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
					        <tr>
					          <td height="20px" width="5%" rowspan="2" align="center" bgcolor="#E5E5E5">Id<br>
					            Person</td>
					          <td width="9%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Prenom</td>
					          <td width="9%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Nom</td>
					          <td width="4%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Sexe</td>
					          <td width="4%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Age</td>
					          <td width="7%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Naissance</td>
					          <td colspan="2" width="26%" align="center" bgcolor="#E5E5E5">Document</td>
					          <td width="13%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Lien de Parente</td>
					          <td width="4%" rowspan="2" align="center" bgcolor="#E5E5E5">Numero<br>Mere</td>
					          <td width="4%" rowspan="2" align="center" bgcolor="#E5E5E5">Numero<br>Pere</td>
					          <td width="7%" rowspan="2" align="center" bgcolor="#E5E5E5">Numero<br>Responsable</td>
					          <td width="8%" rowspan="2" align="center" bgcolor="#E5E5E5">Situation<br>Residence</td>
					        </tr>
					        <tr>
					          <td width="13%" align="center" bgcolor="#E5E5E5">Type</td>
					          <td width="13%" align="center" bgcolor="#E5E5E5">Numero</td>
					          </tr> 
					          
					          <?php foreach($membres as $record): ?>
					          <tr>
						          <td height="12px" style="text-align:center"><br><?= $record->memberid ?></td>
						          <td><br>&nbsp;<?= iconv("iso-8859-1","UTF-8",$record->prenom) ?></td>
						          <td><br>&nbsp;<?= iconv("iso-8859-1","UTF-8",$record->nom) ?></td>
						          <td style="text-align:center"><br><?php 
						                if($record->sexe == 1){$record->sexe = 'Homme';}
						                else{$record->sexe = 'Femme';}
						          		echo iconv("iso-8859-1","UTF-8",$record->sexe)?></td>
						          <td style="text-align:center"><br><?php 
						                $time = new \DateTime('now', new \DateTimeZone('UTC'));
						                $age = $time->format('Y') - $record->annee_naissance;
						          		echo iconv("iso-8859-1","UTF-8",$age)?></td>
						          <td style="text-align:center"><br><?php 
						                if($record->jour_naissance < 10){ $record->jour_naissance = '0'.$record->jour_naissance;}
						                if($record->mois_naissance < 10){ $record->mois_naissance = '0'.$record->mois_naissance;}
						          		echo iconv("iso-8859-1","UTF-8",$record->jour_naissance.'/'.$record->mois_naissance.'/'.$record->annee_naissance)?></td>
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",$record->type_piece_indendification)?></td>
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",$record->numero_piece)?>&nbsp;</td>
						          <td style="text-align:center"><br><?php 
						                         if($record->relation_chef_famille == 1){echo iconv("iso-8859-1","UTF-8",'Ch�f Kay la');}
						                         elseif ($record->relation_chef_famille == 2){echo iconv("iso-8859-1","UTF-8",'Madanm oubyen Mouche');}
						                         elseif ($record->relation_chef_famille == 3){echo iconv("iso-8859-1","UTF-8",'Pitit fi / Pitit Gason');}
						          ?></td>
						          <td style="text-align:center"><br><?php
						                if($record->est_mere == 0){echo iconv("iso-8859-1","UTF-8",'');} 
						          		else{echo iconv("iso-8859-1","UTF-8",$record->est_mere);}?></td>
						          <td style="text-align:center"><br><?php 
						                if($record->est_pere == 0){echo iconv("iso-8859-1","UTF-8",'');}
						                else{echo iconv("iso-8859-1","UTF-8",$record->est_pere);}
						          		?></td>
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",'')?></td>
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",'')?></td>
						      </tr>
					          <?php endforeach;?>
					          
					          <tr>
						          <td height="20px">&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						          <td>&nbsp;</td>
						        </tr>
					        </table></td>
					     </tr>
					  </table>
					  <br/>
					  <br/>
					  <table width="100%" border="0">  
					    <tr>
					      <td width="100%"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
					        <tr>
					          <td height="20px" width="5%" align="center" bgcolor="#E5E5E5">Id<br>
					            Person</td>
					          <td width="9%" align="center" bgcolor="#E5E5E5"><br><br>Prenom</td>
					          <td width="9%" align="center" bgcolor="#E5E5E5"><br><br>Nom</td>
					          <td width="4%" align="center" bgcolor="#E5E5E5"><br><br>Sexe</td>
					          <td width="4%" align="center" bgcolor="#E5E5E5"><br><br>Age</td>
					          <td width="9%" align="center" bgcolor="#E5E5E5">Membres vivant en<br>dehors du foyer</td>
					          <td width="7%" align="center" bgcolor="#E5E5E5">Maladies<br>chroniques</td>
					          <td width="7%" align="center" bgcolor="#E5E5E5">Malnutrition<br>Grave</td>
					          <td width="8%" align="center" bgcolor="#E5E5E5">Severement<br>Handicape</td>
					          <td width="7%" align="center" bgcolor="#E5E5E5"><br><br>Scolarisation</td>
					          <td width="13%" align="center" bgcolor="#E5E5E5">Niveau<br>scolaire</td>
					          <td width="10%" align="center" bgcolor="#E5E5E5">Apte a<br>travailler</td>
					          <td width="8%" align="center" bgcolor="#E5E5E5">Travaille<br>actuellement</td>
					        </tr> 
					        
				              <?php foreach($membres as $record): ?>
				              <?php 
				              		$chro = Chroniques::find()->where(['famille'=> $record->famille,'memberid'=>$record->memberid])->all();
				            
				              ?>
					          <tr>
						          <td height="12px" style="text-align:center"><br><?= $record->memberid ?></td>
						          <td><br>&nbsp;<?= iconv("iso-8859-1","UTF-8",$record->prenom) ?></td>
						          <td><br>&nbsp;<?= iconv("iso-8859-1","UTF-8",$record->nom) ?></td>
						          <td style="text-align:center"><br><?php 
						                if($record->sexe == 1){$record->sexe = 'Homme';}
						                else{$record->sexe = 'Femme';}
						          		echo iconv("iso-8859-1","UTF-8",$record->sexe)?></td>
						          <td style="text-align:center"><br><?php 
						                $time = new \DateTime('now', new \DateTimeZone('UTC'));
						                $age = $time->format('Y') - $record->annee_naissance;
						          		echo iconv("iso-8859-1","UTF-8",$age)?></td>
						          
						          <td style="text-align:center"><br><?php 
						                if($record->b13 == 1){ $record->b13 = 'Present';}
						                if($record->b13 == 2){ $record->b13 = 'Absent';}
						                if($record->b13 == 3){ $record->b13 = 'Visiteur';}
						          		echo iconv("iso-8859-1","UTF-8",$record->b13)?></td>
						          
						          
						          <td style="text-align:center"><br><?php 
								        foreach ($chro as $ch){
                                            if($ch->souffrir_maladie_chronique == 1){$record->b13 = 'Oui';}
                                            elseif($ch->souffrir_maladie_chronique == 2){$record->b13 = 'Non';}
                                            else{$record->b13 = '-';}
								        }
						          		echo iconv("iso-8859-1","UTF-8",$record->b13) ?></td>
						          
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",'')?>&nbsp;</td>
						          
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",'')?>&nbsp;</td>
						          
						          <td style="text-align:center"><br><?php 
							           if($record->c1_3 == 1){ $record->c1_3 = 'Oui';}
							           elseif($record->c1_3 == 2){ $record->c1_3 = 'Non';}
							           elseif($record->c1_3 == 0){ $record->c1_3 = '-';}
						          	   echo iconv("iso-8859-1","UTF-8",$record->c1_3)?>&nbsp;</td>
						          
						          <td style="text-align:center"><br><?= iconv("iso-8859-1","UTF-8",'')?>&nbsp;</td>
						          
						          <td style="text-align:center"><br><?php 
						              if($record->pouvoir_travailler == 1){echo iconv("iso-8859-1","UTF-8",'Oui');}
						              elseif($record->pouvoir_travailler == 2){echo iconv("iso-8859-1","UTF-8",'Non');}
						              else {echo iconv("iso-8859-1","UTF-8",'');}
						          ?></td>
						          
						          <td style="text-align:center"><br><?php 
						              if($record->avoir_emploi == 1){echo iconv("iso-8859-1","UTF-8",'Oui');}
						              elseif($record->avoir_emploi == 2){echo iconv("iso-8859-1","UTF-8",'Non');}
						              else {echo iconv("iso-8859-1","UTF-8",'');}
						          ?></td>	
						          
						      </tr>
					          <?php endforeach;?>
        
					        <tr>
					          <td height="20px">&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					        </tr>
					        
					        </table></td>
					    </tr>
					  </table>
					  <br/>
					  <br/>
			    	</div>
			    </div>
			    
		      </div>
		</div> 
	    <br/>
	    <pagebreak />
	    
<?php endforeach;?>


