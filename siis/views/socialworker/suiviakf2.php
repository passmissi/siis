<?php
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Growl;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use app\models\Activites;
use app\models\TypeSousActivites;
use app\models\SousActivites;
use app\models\Monitoringsousactivites;
use app\models\TypeActivites;
use app\models\Membre;

$this->title = 'Suivis';
$x =Yii::$app->getRequest()->getQueryParam('id');
$y =Yii::$app->getRequest()->getQueryParam('id2');

$model->plan = $x;
$model->mplan = $y;

$script = <<< JS
function showUser(str) {
	if(str == ''){
		document.getElementById("monitoringsousactivites-id_membre").disabled = true;
	}
	else{
		document.getElementById("monitoringsousactivites-id_membre").disabled = false;

		$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/listmembre',
		data: { id: str },
		success: function(data){
			$("#monitoringsousactivites-id_membre").html(data);
		},
		error: function() {
		},
	});
	}
}
JS;
$this->registerJs($script, \yii\web\View::POS_BEGIN);

$script1 = <<< JS
function init_click_handlers(val) {
	$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/sousact',
		data: { id: val },
		success: function(data){
			if(data[1] == 1){
				if (confirm('Voulez-vous ajouter un autre beneficiaire?')) {
				    // Save it!
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: data[1] },
						success: function(data){
							$("#monitoringsousactivites-typetheme").html(data);
						},
						error: function() {
							aa = $("#modalTitle").val();
							alert(aa);
						},
					});
		
					var divOne288= document.getElementById('typetheme');
	            	divOne288.style.display='block';
			        var divOne448= document.getElementById('monitoringsousactivites-typetheme');
	            	divOne448.style.display='block';
		
		            var divOne35= document.getElementById('monitoringsousactivites-sousactivites');
	            	divOne35.style.display='none';
					divOne35.value = data[0];
		            var divOne36= document.getElementById('sousactivites');
	            	divOne36.style.display='none';
		
		            var divOne29= document.getElementById('ataille');
	            	divOne29.style.display='none';
		            var divOne30= document.getElementById('apb');
	            	divOne30.style.display='none';
		            var divOne31= document.getElementById('avaccin');
	            	divOne31.style.display='none';
		            var divOne32= document.getElementById('aen');
	            	divOne32.style.display='none';
					var divOne99= document.getElementById('reference');
	            	divOne99.style.display='none';
		
		            var divOne28= document.getElementById('en');
	            	divOne28.style.display='none';
			        var divOne29= document.getElementById('monitoringsousactivites-en');
	            	divOne29.style.display='none';
		
		            var divOne24= document.getElementById('pb');
	            	divOne24.style.display='none';
			        var divOne25= document.getElementById('monitoringsousactivites-pb');
	            	divOne25.style.display='none';
		
		            var divOne26= document.getElementById('oedeme');
	            	divOne26.style.display='none';
			        var divOne27= document.getElementById('monitoringsousactivites-oedeme');
	            	divOne27.style.display='none';
		
		            var divOne20= document.getElementById('taille');
	            	divOne20.style.display='none';
			        var divOne21= document.getElementById('monitoringsousactivites-taille');
	            	divOne21.style.display='none';
		
		            var divOne22= document.getElementById('rapportpt');
	            	divOne22.style.display='none';
			        var divOne23= document.getElementById('monitoringsousactivites-rapportpt');
	            	divOne23.style.display='none';
		
		            var divOne16= document.getElementById('vaccin');
	            	divOne16.style.display='none';
			        var divOne17= document.getElementById('monitoringsousactivites-vaccin');
	            	divOne17.style.display='none';
		
					var divOne18= document.getElementById('poids');
	            	divOne18.style.display='none';
			        var divOne19= document.getElementById('monitoringsousactivites-poids');
	            	divOne19.style.display='none';
		
					var divOne2= document.getElementById('quantite');
	            	divOne2.style.display='none';
			        var divOne3= document.getElementById('monitoringsousactivites-quantite');
	            	divOne3.style.display='none';
			
			        var divOne4= document.getElementById('secteur');
	            	divOne4.style.display='none';
			        var divOne5= document.getElementById('monitoringsousactivites-secteur');
	            	divOne5.style.display='none';
			
					var divOne6= document.getElementById('institution');
	            	divOne6.style.display='none';
			        var divOne7= document.getElementById('monitoringsousactivites-institution');
	            	divOne7.style.display='none';
			
			        var divOne8= document.getElementById('evenement_type');
	            	divOne8.style.display='none';
			        var divOne9= document.getElementById('monitoringsousactivites-evenement_type');
	            	divOne9.style.display='none';
			
					var divOne10= document.getElementById('date_evenement');
	            	divOne10.style.display='none';
			        var divOne11= document.getElementById('monitoringsousactivites-date_evenement');
	            	divOne11.style.display='none';
			
			        var divOne12= document.getElementById('create_date');
	            	divOne12.style.display='none';
			        var divOne13= document.getElementById('monitoringsousactivites-create_date');
	            	divOne13.style.display='none';
			
			        var divOne14= document.getElementById('evenement');
	            	divOne14.style.display='none';
			        var divOne15= document.getElementById('monitoringsousactivites-evenement');
	            	divOne15.style.display='none';
			
		            $("#myModal").find(".modal-title").html("Suivi Promotion - Code "+data[0]);
				    $("#myModal").modal("show");
				} else {
				    // Do nothing!
				}
			}else if(data[1] == 2){
				if (confirm('Voulez-vous ajouter un autre beneficiaire?')) {
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: data[1] },
						success: function(data){
							$("#monitoringsousactivites-typetheme").html(data);
						},
						error: function() {
							aa = $("#modalTitle").val();
							alert(aa);
						},
					});
		
					var divOne35= document.getElementById('monitoringsousactivites-sousactivites');
	            	divOne35.style.display='none';
					divOne35.value = data[0];
		            var divOne36= document.getElementById('sousactivites');
	            	divOne36.style.display='none';
		
		            var divOne29= document.getElementById('ataille');
	            	divOne29.style.display='none';
		            var divOne30= document.getElementById('apb');
	            	divOne30.style.display='none';
		            var divOne31= document.getElementById('avaccin');
	            	divOne31.style.display='none';
		            var divOne32= document.getElementById('aen');
	            	divOne32.style.display='none';
					var divOne99= document.getElementById('reference');
	            	divOne99.style.display='none';
		
		            var divOne28= document.getElementById('en');
	            	divOne28.style.display='none';
			        var divOne29= document.getElementById('monitoringsousactivites-en');
	            	divOne29.style.display='none';
		
		            var divOne24= document.getElementById('pb');
	            	divOne24.style.display='none';
			        var divOne25= document.getElementById('monitoringsousactivites-pb');
	            	divOne25.style.display='none';
		
		            var divOne26= document.getElementById('oedeme');
	            	divOne26.style.display='none';
			        var divOne27= document.getElementById('monitoringsousactivites-oedeme');
	            	divOne27.style.display='none';
		
		            var divOne20= document.getElementById('taille');
	            	divOne20.style.display='none';
			        var divOne21= document.getElementById('monitoringsousactivites-taille');
	            	divOne21.style.display='none';
		
		            var divOne22= document.getElementById('rapportpt');
	            	divOne22.style.display='none';
			        var divOne23= document.getElementById('monitoringsousactivites-rapportpt');
	            	divOne23.style.display='none';
		
		            var divOne16= document.getElementById('vaccin');
	            	divOne16.style.display='none';
			        var divOne17= document.getElementById('monitoringsousactivites-vaccin');
	            	divOne17.style.display='none';
		
					var divOne18= document.getElementById('poids');
	            	divOne18.style.display='none';
			        var divOne19= document.getElementById('monitoringsousactivites-poids');
	            	divOne19.style.display='none';
		
					
			        var divOne4= document.getElementById('secteur');
	            	divOne4.style.display='none';
			        var divOne5= document.getElementById('monitoringsousactivites-secteur');
	            	divOne5.style.display='none';
			
					var divOne6= document.getElementById('institution');
	            	divOne6.style.display='none';
			        var divOne7= document.getElementById('monitoringsousactivites-institution');
	            	divOne7.style.display='none';
			
			        var divOne8= document.getElementById('evenement_type');
	            	divOne8.style.display='none';
			        var divOne9= document.getElementById('monitoringsousactivites-evenement_type');
	            	divOne9.style.display='none';
			
					var divOne10= document.getElementById('date_evenement');
	            	divOne10.style.display='none';
			        var divOne11= document.getElementById('monitoringsousactivites-date_evenement');
	            	divOne11.style.display='none';
			
			        var divOne12= document.getElementById('create_date');
	            	divOne12.style.display='none';
			        var divOne13= document.getElementById('monitoringsousactivites-create_date');
	            	divOne13.style.display='none';
			
			        var divOne14= document.getElementById('evenement');
	            	divOne14.style.display='none';
			        var divOne15= document.getElementById('monitoringsousactivites-evenement');
	            	divOne15.style.display='none';
			
		            $("#myModal").find(".modal-title").html("Suivi Distribution - Code "+data[0]);
				    $("#myModal").modal("show");
				}
		        else{
					
				}
			}
		     else if(data[1] == 3){
				if (confirm('Voulez-vous ajouter un autre beneficiaire?')) {
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: data[1] },
						success: function(data){
							$("#monitoringsousactivites-secteur").html(data);
						},
						error: function() {
							aa = $("#modalTitle").val();
							alert(aa);
						},
					});
		
		            var divOne99= document.getElementById('reference');
	            	divOne99.style.display='block';
		
		            var divOne94= document.getElementById('secteur');
	            	divOne94.style.display='block';
			        var divOne95= document.getElementById('monitoringsousactivites-secteur');
	            	divOne95.style.display='block';
			
					var divOne96= document.getElementById('institution');
	            	divOne96.style.display='block';
			        var divOne97= document.getElementById('monitoringsousactivites-institution');
	            	divOne97.style.display='block';
		
					var divOne35= document.getElementById('monitoringsousactivites-sousactivites');
	            	divOne35.style.display='none';
					divOne35.value = data[0];
		            var divOne36= document.getElementById('sousactivites');
	            	divOne36.style.display='none';
		
					var divOne28= document.getElementById('typetheme');
	            	divOne28.style.display='none';
			        var divOne44= document.getElementById('monitoringsousactivites-typetheme');
	            	divOne44.style.display='none';
					var divOne45= document.getElementById('type');
	            	divOne45.style.display='none';
		
		            var divOne29= document.getElementById('ataille');
	            	divOne29.style.display='none';
		            var divOne30= document.getElementById('apb');
	            	divOne30.style.display='none';
		            var divOne31= document.getElementById('avaccin');
	            	divOne31.style.display='none';
		            var divOne32= document.getElementById('aen');
	            	divOne32.style.display='none';
					
					var divOne2= document.getElementById('quantite');
	            	divOne2.style.display='none';
			        var divOne3= document.getElementById('monitoringsousactivites-quantite');
	            	divOne3.style.display='none';
			
			        var divOne8= document.getElementById('evenement_type');
	            	divOne8.style.display='none';
			        var divOne9= document.getElementById('monitoringsousactivites-evenement_type');
	            	divOne9.style.display='none';
			
					var divOne10= document.getElementById('date_evenement');
	            	divOne10.style.display='none';
			        var divOne11= document.getElementById('monitoringsousactivites-date_evenement');
	            	divOne11.style.display='none';
			
			        var divOne12= document.getElementById('create_date');
	            	divOne12.style.display='none';
			        var divOne13= document.getElementById('monitoringsousactivites-create_date');
	            	divOne13.style.display='none';
			
			        var divOne14= document.getElementById('evenement');
	            	divOne14.style.display='none';
			        var divOne15= document.getElementById('monitoringsousactivites-evenement');
	            	divOne15.style.display='none';
		
		            $("#myModal").find(".modal-title").html("Suivi Reference - Code "+data[0]);
				    $("#myModal").modal("show");
				}
			 }
		     else if(data[1] == 4){}
		     else if(data[1] == 5){}
		     else if(data[1] == 6 || data[1] == 7){
				if (confirm('Voulez-vous ajouter un autre beneficiaire?')) {
					var divOne28= document.getElementById('typetheme');
	            	divOne28.style.display='none';
			        var divOne44= document.getElementById('monitoringsousactivites-typetheme');
	            	divOne44.style.display='none';
		
		  			var divOne45= document.getElementById('type');
	            	divOne45.style.display='none';
		        	var divOne29= document.getElementById('ataille');
	            	divOne29.style.display='block';
		            var divOne30= document.getElementById('apb');
	            	divOne30.style.display='block';
		            var divOne31= document.getElementById('avaccin');
	            	divOne31.style.display='block';
		            var divOne32= document.getElementById('aen');
	            	divOne32.style.display='block';
					var divOne99= document.getElementById('reference');
	            	divOne99.style.display='none';
		
					var divOne35= document.getElementById('monitoringsousactivites-sousactivites');
	            	divOne35.style.display='none';
					divOne35.value = data[0];
		            var divOne36= document.getElementById('sousactivites');
	            	divOne36.style.display='none';
		
				var divOne2= document.getElementById('quantite');
            	divOne2.style.display='none';
		        var divOne3= document.getElementById('monitoringsousactivites-quantite');
            	divOne3.style.display='none';
		
		        var divOne4= document.getElementById('secteur');
            	divOne4.style.display='none';
		        var divOne5= document.getElementById('monitoringsousactivites-secteur');
            	divOne5.style.display='none';
		
				var divOne6= document.getElementById('institution');
            	divOne6.style.display='none';
		        var divOne7= document.getElementById('monitoringsousactivites-institution');
            	divOne7.style.display='none';
		
		        var divOne8= document.getElementById('evenement_type');
            	divOne8.style.display='none';
		        var divOne9= document.getElementById('monitoringsousactivites-evenement_type');
            	divOne9.style.display='none';
		
				var divOne10= document.getElementById('date_evenement');
            	divOne10.style.display='none';
		        var divOne11= document.getElementById('monitoringsousactivites-date_evenement');
            	divOne11.style.display='none';
		
		        var divOne12= document.getElementById('create_date');
            	divOne12.style.display='none';
		        var divOne13= document.getElementById('monitoringsousactivites-create_date');
            	divOne13.style.display='none';
		
		        var divOne14= document.getElementById('evenement');
            	divOne14.style.display='none';
		        var divOne15= document.getElementById('monitoringsousactivites-evenement');
            	divOne15.style.display='none';
		
				$("#myModal").find(".modal-title").html("Suivi Vaccin/Controle Poids - Code "+data[0]);
			    $("#myModal").modal("show");
				}
				else{
					
				}
			}
		     else {
			 }
		},
		dataType:"json",
		error: function() {
			aa = $("#modalTitle").val();
			alert(aa);
		},
	});
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);

?>

<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal9">
                                                <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats']); '.</li>';
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    	    <div class="row">
						    	<div class="btn-group col-md-8">
						    		<?php 
						    			echo Html::a('<span class="glyphicon glyphicon-plus-sign"></span> Autres Activites', [''], ['class' => 'btn btn-default btn-flat btn-sm']);
						    			echo Html::a('<span class="glyphicon glyphicon-qrcode"></span> Evenements', [''], ['class' => 'btn btn-default btn-flat btn-sm','data-toggle'=>'modal', 'data-target'=>'#myModal3']);
						    			 
						    			echo Html::a('<span class="glyphicon glyphicon-road"></span> Objectifs de Vie', [''], ['class' => 'btn btn-default btn-flat btn-sm', 'data-toggle'=>'modal', 'data-target'=>'#myModal2']);
						    			echo Html::a('<span class="glyphicon glyphicon-lock"></span> Terminer Suivi', ['/socialworker/suiviplan2', 'id' => $x], ['class' => 'btn btn-info btn-flat btn-sm']);
						    		?>
						    		
                                </div>
                                <div class="col-md-4">
                                	
                                </div>
						    </div>
						    <br/>
						    <?php if ($state == 0): ?>
						    	<div class="row">
							     	<div class="col-md-12">
							     		<div class="alert alert-danger alert-dismissable">
						                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						                    <h4><i class="icon fa fa-ban"></i> Danger!</h4>
						                    Data is not inserted. Format Date empty or incorrect. Please enter a correct format date. Thx.
						                </div>
							     	</div>
							    </div>
						    <?php endif; ?>
						    
						    <div class="row">
						    	<div class="col-md-7">
						    		<div class="box">
                                	<div class="box-header with-border">
                    	    		    <h3 class="box-title">Suivi des Sous-Activites</h3>
                    	    		</div>
                    	    		<div class="box-body table-responsive no-padding">
			                    	   			<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
														'rowOptions' => function ($model, $key, $index, $grid) {
 															if($model->isDone == 0 && $model->isDone != null){
																return ['id' => $model['id'],'class' => 'success'];
															}
															else{
																$Op = Monitoringsousactivites::find()->where(['sousactivites' => $model->id])->count();
 																if($Op > 0){
 																	return ['id' => $model['id'],
																		'onclick' => 'init_click_handlers(this.id);',
																		'class' => 'info'];
 																}
 																else{
 																	return ['id' => $model['id'],
																		'onclick' => 'init_click_handlers(this.id);',
																		'class' => 'warning'];
 																}
															}
														},
                                                        'columns' => [
	                                                         ['class' => 'yii\grid\SerialColumn'],
                                                             'id', 
                                                              [
																'attribute'=>'id_activite',
																'header'=>'Activite',
																'value'=>function ($model, $key, $index, $widget) {
																	$act = Activites::findOne($model->id_activite);
																	$type = TypeActivites::findOne($act->id_type_activite);
																	return $type->libelle;
																},
                                                              ],'typeSousActivite.libelle',
                                                              [
																'header'=>'Date',
																'value'=>function ($model, $key, $index, $widget) {
																	$Op = Activites::findOne($model->id_activite);
																	return $Op->date_activite;
																},
                                                              ], 
                                                              [
                                                              'header'=>'Beneficiaire',
                                                              'value'=>function ($model, $key, $index, $widget) {
                                                              	$Op = Monitoringsousactivites::find()->where(['sousactivites' => $model->id])->count();
                                                              	return $Op;
                                                              },
                                                              ],
																
                                                            ]
					                    			 ]);
					                    		?>
			                    	   		</div>
                                 </div>
						    	</div>
						    	<div class="col-md-5">
						    		<div class="box">
                                	<div class="box-header with-border">
                    	    		    <h3 class="box-title">Liste des Beneficiaires</h3>
                    	    		</div>
                    	    		<div class="box-body table-responsive no-padding">
			                    	   			<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider2,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         ['class' => 'yii\grid\SerialColumn'],
															 [
																'header'=>'Nom',
																'value'=>function ($model, $key, $index, $widget) {
																	//$Op = Monitoringsousactivites::find()->where(['sousactivites' => $model->id])->count();
																	//return $model->id_membre;
																	$mem = Membre::find()->where(['famille' => $model->id_famille, 'memberid' => $model->id_membre])->one();
																	if($mem != null){return $mem->nom.' '.$mem->prenom;}
																	else{return '---- '.' '.'----- ';}
																	
																	
																},
															],[
																'header'=>'Type',
																'value'=>function ($model, $key, $index, $widget) {
																	//$Op = Monitoringsousactivites::find()->where(['sousactivites' => $model->id])->count();
																	$mem = TypeSousActivites::findOne($model->type_sous_activites);
																	if($mem != null){return $mem->libelle;}
																	else{return $mem->libelle;}
																	
																},
															],
															[
															'class' => 'yii\grid\ActionColumn',
															'template' => '{editer}',
															'buttons' => [
															'editer' => function ($url,$model) {
																$x =Yii::$app->getRequest()->getQueryParam('id');
																$y =Yii::$app->getRequest()->getQueryParam('id2');
																
																return Html::a(
																		'<span class="fa fa-trash-o"></span>', ['/monitoringsousactivites/delete', 'id' => $model->id,'id2' => $x,'id3' => $y ],
																		
																	['class' => 'btn btn-xs btn-danger', 'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer ce beneficiaire?', 'onmouseover'=>'Tip("Effacer AKF")', 'onmouseout'=>'UnTip()'])
															;},
															],
															]
                                                         ]
					                    			 ]);
					                    		?>
			                    	   		</div>
                                 </div>
						    	</div>
						    </div>
						    <div class="row">
						    	<div class="col-md-6">
						    		<div class="box box-default collapsed-box">
							            <div class="box-header with-border">
							              <h3 class="box-title">Evenements</h3>
							
							              <div class="box-tools pull-right">
							                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
							                </button>
							              </div>
							              <!-- /.box-tools -->
							            </div>
							            <!-- /.box-header -->
							            <div class="box-body table-responsive no-padding" style="display: none;">
							              <?php 
							              echo GridView::widget([
							              		'dataProvider' => $dataProvider4,
							              		'export' => false,
							              		'showPageSummary' => false,
							              		'bordered' => false,
							              		'responsive' => true,
							              		'hover' => true,
							              		'columns' => [
							              			['class' => 'yii\grid\SerialColumn'],
							              			'id','famille','evenements0.libelle','date_evenements'
							              		]
							              	]);
							              ?>
							            </div>
							            <!-- /.box-body -->
							        </div>
						    	</div>
						    	<div class="col-md-6">
						    		<div class="box box-default collapsed-box">
							            <div class="box-header with-border">
							              <h3 class="box-title">Objectifs de Vie</h3>
							
							              <div class="box-tools pull-right">
							                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
							                </button>
							              </div>
							              <!-- /.box-tools -->
							            </div>
							            <!-- /.box-header -->
							            <div class="box-body table-responsive no-padding" style="display: none;">
							              <?php 
								              echo GridView::widget([
								              		'dataProvider' => $dataProvider3,
								              		'export' => false,
								              		'showPageSummary' => false,
								              		'bordered' => false,
								              		'responsive' => true,
								              		'hover' => true,
								              		'columns' => [
								              			['class' => 'yii\grid\SerialColumn'],
								              			'id','famille','objectif0.libelle','date_objectif',
								              			[
								              			 'class' => 'yii\grid\ActionColumn',
								              			 'template' => '{effacer}',
								              			 'buttons' => [
								              				'effacer' => function ($url,$model) {
								              				    $x =Yii::$app->getRequest()->getQueryParam('id');
								              					$y =Yii::$app->getRequest()->getQueryParam('id2');
								              				
								              					return Html::a(
								              									'<span class="fa fa-trash-o"></span>', ['/objectifsVie/delete', 'id' => $model->id,'id2' => $x,'id3' => $y ],
								              				
								              									['class' => 'btn btn-xs btn-danger', 'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer ce beneficiaire?', 'onmouseover'=>'Tip("Effacer Objetifs de Vie")', 'onmouseout'=>'UnTip()'])
								              					;},
								              			  ],
								              			]
								              		]
								              ]);
							              ?>
							            </div>
							            <!-- /.box-body -->
							        </div>
						    	</div>
						    </div>
			             </div>
                    	</div>
                    </div>
                   </div>
                  </div>
                 </div>
                </section>
                
                
                
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Suivi - Sous-Activite </h4>
      </div>
      <?= $this->render('/monitoringsousactivites/_form', ['model' => $model]) ?>
    </div>
  </div>
</div>

<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Suivi - Objectifs de Vie </h4>
      </div>
      <?= $this->render('/objectifs-vie/_form', ['model' => $model4]) ?>
    </div>
  </div>
</div>

<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Suivi - Evenements </h4>
      </div>
      <?= $this->render('/evenements/_form', ['model' => $model5]) ?>
    </div>
  </div>
</div>