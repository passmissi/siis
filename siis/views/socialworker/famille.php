<?php
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FamilyAkf;
use app\models\Communes;
use app\models\Famille;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
use app\models\Membre;
use app\models\ScrMembre;
use kartik\select2\Select2;

$this->title = "Famille";

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listcommune = ArrayHelper::map(Famille::find()->select('commune')->distinct()
		->asArray()->orderBy('commune')->all(),
		'commune','commune');

$listsectioncommunale = ArrayHelper::map(Famille::find()->select('section_communale')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('section_communale')->all(),
		'section_communale','section_communale');

$listlocalite = ArrayHelper::map(Famille::find()->select('localite')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('localite')->all(),
		'localite','localite');

$script1 = <<< JS
function init_click_handlers2(val) {
	if (confirm('Voulez-vous vraiment modifier les donnees de cette famille?')) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/familles',
			data: { id: val },
			success: function(data){
				var divOne15= document.getElementById('famille-id');
				document.getElementById('famille-id').style.display = 'none';
				document.getElementById('label-key').style.display = 'none';
				document.getElementById('key').style.display = 'none';
	            divOne15.value = data["id"];
		
				var divOne15= document.getElementById('famille-localite');
	            divOne15.value = data["localite"];
		
				var divOne16= document.getElementById('famille-commune');
		        document.getElementById('famille-commune').value =  data["commune"];
		        
				var divOne15= document.getElementById('famille-section_communale');
	            divOne15.value = data["section_communale"];
		
				var divOne15= document.getElementById('famille-type_milieu');
	            divOne15.value = data["type_milieu"];
		
				var divOne15= document.getElementById('famille-telephone_maison');
	            divOne15.value = data["telephone_maison"];
		
				var divOne15= document.getElementById('famille-latitude_gps');
	            divOne15.value = data["latitude_gps"];
		
				var divOne15= document.getElementById('famille-longitude_gps');
	            divOne15.value = data["longitude_gps"];
		
				$("#myModal").find(".modal-title").html("Modification Famille - Code "+ data["id"]);
				$('#myModal').modal('show');
			},
			error: function() {
			},
		});
	}
	else{

    }
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);


$script2 = <<< JS
function init_click_handlers3(val) {
	$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/listsectioncommunale',
		data: { commune: val },
		success: function(data){
			$("select#famille-section_communale").html(data);
		
			$.ajax({
				type: 'GET',
				url: 'index.php?r=socialworker/listlocalite',
				data: { commune: val },
				success: function(data){
					$("select#famille-localite").html(data);
				},
				error: function() {
					alert('no1');
				},
			});
		},
		error: function() {
			alert('no2');
		},
	});
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

?>

<section class="content-header">
	<h1>
       Plan de Developpement
       <small>Famille</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
		<li>Plan de Developpement</li>
       <li class="active">Famille</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body">
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
								<i class="fa fa-inbox"></i>
								<h3 class="box-title">PANEL DE GESTION</h3>
							</div>
							<div style="margin-top: 5px;">
								<ul class="nav nav-pills nav-stacked">
									<?php
										$user = User::findOne(Yii::$app->user->identity->id);
										$operateur = Operateurs::findOne($user->idOperateurs);
										echo  '<li class="header">Actions</li>';
										echo  '<li class="active">'. Html::a('<i class="fa fa-users"></i> Familles', ['/socialworker/famille']); '.</li>';
										echo  '<li>'. Html::a('<i class="fa fa-user"></i> Membres', ['/socialworker/membres', 'id' => 0]); '.</li>';
									?>
	                            </ul>
	                        </div>
						</div>
						<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
						    	<div class="col-sm-5">
                                </div>
                                <div class="col-sm-7">
                                	<?php echo $this->render('/family-akf/_search3', ['model' => $searchModel]); ?>
                                </div>
						    </div>
                    	    <div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Liste des Familles</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         [ 'class' => 'yii\grid\SerialColumn'],
                                                        		[
	                                                        		'class' => 'kartik\grid\ExpandRowColumn',
	                                                        		'value' => function ($model, $key, $index, $column) {
	                                                        			return GridView::ROW_COLLAPSED;
	                                                        	},
	                                                        		'enableRowClick' => false,
	                                                        		'detail'=>function ($model, $key, $index, $column) {
			                                                        	  $dataProvider = new ActiveDataProvider([
			                                                        		  'query' => Membre::find()->where(['famille' => $model->famille]),
			                                                        	  ]);
	                                                        			  return Yii::$app->controller->renderPartial('details_famille', [
	                                                        			  		'dataProvider' => $dataProvider
	                                                        			  ]);
	                                                        		},
                                                        		],
																[
																	'header'=>'Famille',
																	'attribute'=>'famille',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = Membre::find()->where(['famille' => $model->famille, 'memberid' => 1])->one();
																		if($count != null){return ''.$count->nom.' '.$count->prenom;}
																		else{return '--';}
																		
																	},
																],
																[
																	'header'=>'Commune',
																	'attribute'=>'commune',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = Communes::findOne($model->commune);
																		return ''.$count->n_communal;
																	},
																],
																'localite',
																[
																'header'=>'Type Milieu',
																'attribute'=>'type_milieu',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$data = Famille::findOne($model->famille);
																	if($data->type_milieu == 1){ return 'Rural';}
																	elseif($data->type_milieu == null){ return '--';}
																	else{ return 'Urbain';}	
																},
																],
																[
																'header'=>'Nom AKF',
																'attribute'=>'operateur_akf',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Operateurs::findOne($model->operateur_akf);
																	    return ''.$count->nom.' '.$count->prenom;
																	},
																],
																[
																'header'=>'Membres',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Membre::find()->where(['famille' => $model->famille])->count();
																	return ''.$count.' '.'Membres';
																},
																],
                                                                [
																	'class' => 'yii\grid\ActionColumn',
																	'template' => '{editer} {membres} {carnet}',
																	'buttons' => [
																	'editer' => function ($url,$model) {
																		return Html::a(
																				'<span class="glyphicon glyphicon-edit"></span>', '#', 
																				['class' => 'btn btn-xs btn-default', 'onclick' => 'init_click_handlers2('.$model->famille.');',
																				 'onmouseover'=>'Tip("editer une famille")', 'onmouseout'=>'UnTip()']);
																	},
																	'membres' => function ($url,$model) {
																		return Html::a(
																				'<span class="fa fa-user"></span>', ['socialworker/membres', 'id' => $model->famille],
																				['class' => 'btn btn-xs btn-default',
																				'onmouseover'=>'Tip("Voir les membres")', 'onmouseout'=>'UnTip()']);
																	},
																	'carnet' => function ($url,$model) {
																		return Html::a(
																				'<span class="fa fa-file-text"></span>', ['socialworker/pdflocalitecarnet', 'localite' => 'ok', 'famille' => $model->famille],
																				['class' => 'btn btn-xs btn-warning', 'target' => '_blank', 
																				'onmouseover'=>'Tip("Carnet Familial")', 'onmouseout'=>'UnTip()']);
																	},
																],
                                                         ]]
					                    			 ]);
					                    		?>	    		
			                    </div>
			                </div>
                    	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Modification Famille </h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	        
	        <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model2, 'commune') 
		    		     ->dropDownList($listcommune, ['prompt' => 'Choisir une commune', 
							'onchange' => 'init_click_handlers3(this.value)'])?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model2, 'section_communale') 
		    			->dropDownList($listsectioncommunale, ['prompt' => 'Choisir une section communale'])?>
		    	</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col-lg-6">
		    		 <?= $form->field($model2, 'localite')
		    		 	->dropDownList($listlocalite, ['prompt' => 'Choisir une localite'])?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model2, 'type_milieu')
		    			->dropDownList(['1' => 'Rural', '2' => 'Urbain'], ['prompt' => 'Choisir un type'])?>
		    	</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col-lg-12">
		    		<?= $form->field($model2, 'telephone_maison')->textInput() ?>
		    	</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model2, 'latitude_gps')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model2, 'longitude_gps')->textInput(['maxlength' => true]) ?>
		    	</div>
		    </div>
		    
		    <div class="row" id="key">
		    	<div class="col-lg-12">
		    		 <?= $form->field($model2, 'id')
			          	->textInput()->label(null, ['id' => 'label-key'])?>
		    	</div>
		    </div>
		    
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton('Modifier', ['class'=>'btn btn-sm btn-primary', 'id' => 'modification', 'name' => 'modification']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>