<?php


use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Growl;
use yii\helpers\ArrayHelper;
use app\models\Communes;
use app\models\Monitoringplan;
use app\models\ScrPlans;

?>

<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats']); '.</li>';
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
						    	<div class="col-sm-9">
                                </div>
                                <div class="col-sm-3">
                                	<?php 
                                	
                                	echo $this->render('/plans/_search3', ['model' => $searchModel, 'id' => $operateur->id]); ?>
                                </div>
						    </div>
                    		<div class="box">
                    	    		        <div class="box-header with-border">
                    	    		        	<h3 class="box-title">Resume des Plans 
			                    	          	</h3>
                    	    		        </div>
			                    	   		<div class="box-body table-responsive no-padding">
			                    	   			<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'rowOptions' => function($model){
									                           if($model->isMonitoring == 0 && $model->isPlanned == 1 && $model->isClosed == 1){
									                           		return ['class' => 'success'];
									                           }
									                           elseif($model->isMonitoring == 0 && $model->isPlanned == 1 && $model->isClosed == 0
																	){
									                           		return ['class' => 'info'];
									                           }
									                           elseif($model->isMonitoring == 1 && $model->isDeleted == 0){
									                           		return ['class' => 'warning'];
									                           }
                                                         },
                                                        'columns' => [
	                                                         ['class' => 'yii\grid\SerialColumn'],
                                                             'id', 'mois', 'annee', 
                                                              [
																'attribute'=>'commune',
																'header'=>'Commune',
																'value'=>function ($model, $key, $index, $widget) {
																	$Op = Communes::findOne($model->commune);
																	return $Op->n_communal;
																},
																], 
                                                             [
																'attribute'=>'akf',
																'header'=>'AKF',
																'value'=>function ($model, $key, $index, $widget) {

																	$Op = Operateurs::findOne($model->akf);
																	if($Op!=null)
																	return $Op->nom.' '.$Op->prenom;
																	else
																		return null;
																},
                                                             ],
                                                             [
																'class' => 'yii\grid\ActionColumn',
																'template' => '{activite} {delete}',
																'buttons' => [
																'activite' => function ($url,$model) {
																	if($model->isMonitoring == 0 && $model->isPlanned == 1 && $model->isClosed == 1){
																		return '';
																	}
																	elseif($model->isMonitoring == 0 
																			&& $model->isPlanned == 1 && $model->isClosed == 0){
																		$mplan = Monitoringplan::find()->where(['Plan' => $model->id])->one();
																		return Html::a('<span class="glyphicon glyphicon glyphicon-share"></span>', 
																			['/socialworker/suiviakf2', 'id' => $model->id, 'id2' => $mplan->id, 'state' => 1],
																			['class'=>'btn btn-xs btn-default', ]);
																	}
																	elseif($model->isMonitoring == 1 && $model->isDeleted == 0){
																		return '';
																	}
																},
																'delete' => function ($url,$model) {
																	if($model->isMonitoring == 0 && $model->isPlanned == 1 && $model->isClosed == 1){
																		return Html::a('<span class="fa fa-unlock"></span>',
																				['/socialworker/suiviplan3', 'id' => $model->id],
																				['class'=>'btn btn-xs btn-default', 'data-confirm'=>'Voulez vous vraiment rouvrir ce suivi?',]);
																	}
																	elseif($model->isMonitoring == 0
																			&& $model->isPlanned == 1 && $model->isClosed == 0){
																		$mplan = Monitoringplan::find()->where(['Plan' => $model->id])->one();
																		return Html::a('<span class="glyphicon glyphicon-lock"></span>',
																				['/socialworker/suiviplan2', 'id' => $model->id],
																				['class'=>'btn btn-xs btn-default', 'data-confirm'=>'Voulez vous vraiment clore le suivi?',]);
																	}
																	elseif($model->isMonitoring == 1 && $model->isDeleted == 0){
																		return '';
																	}
																},
															],
                                                         ]]
					                    			 ]);
					                    		?>
			                    	   		</div>
			                    	   	</div>
                    	</div>
                    	</div></div></div></div></div>
</section>