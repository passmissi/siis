<?php
/* @var $this yii\web\View */
use kartik\grid\GridView;
use app\models\Plans;
use app\models\User;
use kartik\export\ExportMenu;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Growl;
use yii\helpers\Url;

$this->title = "Dashboard";

$script1 = <<< JS
function init_click_handlers(val) {
	if (confirm('Voulez-vous modifier les parametres de cet AKF?')) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/operateurs',
			data: { id: val },
			success: function(data){
				var divOne15= document.getElementById('operateurs-nom');
	            divOne15.value = data["nom"];
				var divOne25= document.getElementById('operateurs-prenom');
	            divOne25.value = data["prenom"];
		
				var divOne35= document.getElementById('operateurs-email');
	            divOne35.value = data["email"];
		
		        var divOne5 = document.getElementById('operateurs-id');
				divOne5.value = data["id"];
            	divOne5.style.display='none';
		       
		
				var divOne16= document.getElementById('creation');
	            divOne16.style.display='none';
				var divOne17= document.getElementById('modification');
	            divOne17.style.display='inline';
		        document.getElementById("operateurs-sexe").selectedIndex = data["sexe"];
		
				$("#myModal").find(".modal-title").html("Modification AKF - Code "+ data["id"]);
				$('#myModal').modal('show');
			},
			error: function() {
			},
		});
	}
	else{
		
    }
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
    
    
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-square"></i> Ajouter un AKF</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-ellipsis-v"></i> AKF', ['/socialworker/index']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-rss-square"></i> Notification', ['']); '.</li>';
                                                	 
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    	<div class="row pad">
						    	<div class="col-sm-6">
						    		<?php 
						    		
						    		//echo Html::a('<i class="fa fa-rss-square"></i> Notification', ['/socialworker/export2']);
						    		
						    		/*$gridColumns = ['id', 'nom', 'prenom', 'email'];
						    		?>
							    	<?=	ExportMenu::widget([
							    			'dataProvider' => $dataProvider,
							    			'columns' => $gridColumns,
							    			'fontAwesome' => true,
							    			'asDropdown' => true,
							    			'exportConfig'=> [
							    				ExportMenu::FORMAT_TEXT => true,
							    				ExportMenu::FORMAT_PDF => true
							    			],
										    'dropdownOptions' => [
										        'label' => 'Export All',
										        'class' => 'btn btn-default'
										    ]
							    		]);*/
						    		?>
                                </div>
                                <div class="col-sm-6">
                                	<?php echo $this->render('/operateurs/_search', ['model' => $searchModel, 'id' => $operateur->id]); ?>
                                </div>
						    </div>
                    	    		 <div class="box">
                    	    		        <div class="box-header with-border">
                    	    		        	<h3 class="box-title">Liste des AKF 
			                    	          	</h3>
                    	    		        </div>
			                    	   		<div class="box-body table-responsive no-padding">
			                    	    		<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
														'rowOptions' => function($model1){
															$count = Plans::find()->where(['akf' => $model1->id])->count();
															if($model1->isActive == 0){
																return ['class' => 'danger',];
															}
															elseif($count > 0 && $model1->isActive == 1){
																return ['class' => 'success'];
															}
															elseif($count <= 0 && $model1->isActive == 1){
																return ['class' => 'warning',];
															}
														},
                                                        'columns' => [
	                                                         ['class' => 'yii\grid\SerialColumn'],
                                                             'id', 'nom', 'prenom', 'email',
																[
																'header'=>'Statut',
																'attribute'=>'isActive',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Plans::find()->where(['akf' => $model->id])->count();
																	if($model->isActive == 0){
                                                                        return '<span class="label label-danger">bloque</span>';
																	}
																	elseif($count > 0 && $model->isActive == 1){ return '<span class="label label-success">En service</span>';}
																	elseif($count <= 0 && $model->isActive == 1){ return '<span class="label label-warning">Inactif</span>';}
																},
																],
                                                             [
																'class' => 'yii\grid\ActionColumn',
																'template' => '{modifier} {plan} {detailsplan} {detailsakf} {activation} {effacer}',
																'buttons' => [
																'modifier' => function ($url,$model) {
																	return Html::a(
																			'<span class="glyphicon glyphicon-edit"></span>', '#',
																			['class' => 'btn btn-xs btn-default', 'onclick' => 'init_click_handlers('.$model->id.');',
																			 'onmouseover'=>'Tip("Modifier donnees")', 'onmouseout'=>'UnTip()']);
																},
																'plan' => function ($url,$model) {
																	return $model->isActive ? Html::a(
																			'<span class="glyphicon glyphicon glyphicon-share"></span>',
																			['plans/create', 'id' => $model->id],
                                                                            ['class' => 'btn btn-xs btn-default', 'onmouseover'=>'Tip("Creer Plan")', 'onmouseout'=>'UnTip()']) : "";
																},
																'detailsplan' => function ($url,$model) {
																	return $model->isActive ? Html::a( 																		
																			'<span class="glyphicon glyphicon-folder-open"></span>', 
																			['socialworker/detailsplan', 'id' => $model->id],
                                                                            ['class' => 'btn btn-xs btn-warning', 'onmouseover'=>'Tip("Dossier Plans")', 'onmouseout'=>'UnTip()']) : "";
																},
																'detailsakf' => function ($url,$model) {
																	return $model->isActive ? Html::a(
																			'<span class="glyphicon glyphicon glyphicon-th"></span>',
																			['socialworker/dailyplanning', 'id' => $model->id],['class' => 'btn btn-xs btn-success', 'onmouseover'=>'Tip("Planning Hebdo")', 'onmouseout'=>'UnTip()']) : "";
																},
																'activation' => function ($url,$model) {
																	return !$model->isActive ? Html::a(
																			'<span class="fa fa-unlock"></span>',
																			['operateurs/reactivate', 'id' => $model->id],['class' => 'btn btn-xs btn-danger', 'data-confirm'=>'Voulez vous vraiment reactiver cet AKF?', 'onmouseover'=>'Tip("Activer AKF")', 'onmouseout'=>'UnTip()']) : "";
																},
																'effacer' => function ($url,$model) {
																	$count = Plans::find()->where(['akf' => $model->id])->count();
																	return !$count ? Html::a(
																			'<span class="fa fa-trash-o"></span>',
																			['operateurs/delete', 'id' => $model->id],['class' => 'btn btn-xs btn-danger', 'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer cet AKF?', 'onmouseover'=>'Tip("Effacer AKF")', 'onmouseout'=>'UnTip()']) : "";
																},
																],
                                                         ]]
					                    			 ]);
					                    		?>
		
		
			                    	    	</div>
			                    	 </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Ajouter un AKF </h4>
      </div>
      <?php $form = ActiveForm::begin([
      		'enableClientValidation' => true,
      		'validateOnSubmit' => true,
      		'enableAjaxValidation' => false,
      ]); ?>
	      <div class="modal-body">
	        <?= $form->field($model, 'id')->textInput(['maxlength' => 255, 'style' => 'display :none;',])->label(false, ['id' => 'code']) ?>
	        <?= $form->field($model, 'nom')->textInput(['maxlength' => 255]) ?>
		    <?= $form->field($model, 'prenom')->textInput(['maxlength' => 255]) ?>
		    <?= $form->field($model, 'sexe')->dropDownList(
		          ['1'=>'Masculin','2'=>'Feminin'],['prompt'=>'-- Choisir un sexe --']
		    ); ?>
		    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation', 'name' => 'creation']) ?>
	        <?= Html::submitButton('Modifier', ['class'=>'btn btn-sm btn-primary', 'id' => 'modification', 'style' => 'display :none;', 'name' => 'modification']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

