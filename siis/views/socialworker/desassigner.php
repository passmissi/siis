<?php
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\FamilyAkf;
use app\models\Communes;
use app\models\Famille;
use yii\helpers\ArrayHelper;
use app\models\Membre;

$this->title = "Desassignation";

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$listlocalite = ArrayHelper::map(Famille::find()->select('localite')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('localite')->all(),
		'localite','localite');

$script1 = <<< JS
function init_click_handlers(val) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/familyakf',
			data: { localite: val },
			success: function(data){
				var divOne15= document.getElementById('familyakf-assignes');
		        divOne15.disabled = true;
	            divOne15.value = data[0];
		        var divOne16= document.getElementById('familyakf-quantite');
	            divOne16.value = data[1];
			},
			error: function() {
			},
		});
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);

$script2 = <<< JS
function init_click_handlers2(val) {
	if (confirm('Voulez-vous desassigner cet AKF?')) {
		var divOne = document.getElementById('familyakf-famille');
        divOne.style.display='none'; 
		divOne.value = val; 
		var divOne1 = document.getElementById('famille');
        divOne1.style.display='none'; 
		$("#myModal2").find(".modal-title").html("Desassigner AKF - Code Famille "+ val);
		$('#myModal2').modal('show');
    }
	else{}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Plan de Developpement
       <small>Desassignation AKF</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Desassignation</li>
    </ol>
</section>

<section class="content">


<div class="row">
	<div class="col-xs-12">
		<div class="box box-solid">
			<div class="box-body">
				<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
								<i class="fa fa-inbox"></i>
								<h3 class="box-title">PANEL DE GESTION</h3>
							</div>
							
							<div style="margin-top: 15px;">
								<ul class="nav nav-pills nav-stacked">
									<?php
										$user = User::findOne(Yii::$app->user->identity->id);
										$operateur = Operateurs::findOne($user->idOperateurs);
										echo  '<li class="header">Actions</li>';
										echo  '<li>'. Html::a('<i class="fa fa-chain"></i> Assignation AKF', ['/socialworker/assignation']); '.</li>';
										echo  '<li class="active">'. Html::a('<i class="fa fa-chain-broken"></i> Desassignation AKF', ['/socialworker/desassignation']); '.</li>';
											
									?>
	                            </ul>
	                        </div>
	                    </div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
						    	<div class="col-sm-6">
                                </div>
                                <div class="col-sm-6">
                                	<?php echo $this->render('/family-akf/_search2', ['model' => $searchModel]); ?>
                                </div>
						    </div>
                    	    <div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Liste des Assignations</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         [ 'class' => 'yii\grid\SerialColumn'],
																[
																	'header'=>'Famille',
																	'attribute'=>'famille',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = Membre::find()->where(['famille' => $model->famille, 'memberid' => 1])->one();
																		return ''.$count->nom.' '.$count->prenom;
																	},
																],
																[
																	'header'=>'Commune',
																	'attribute'=>'commune',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		$count = Communes::findOne($model->commune);
																		return ''.$count->n_communal;
																	},
																],
																'localite', 
																[
																'header'=>'Nom AKF',
																'attribute'=>'operateur_akf',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Operateurs::findOne($model->operateur_akf);
																	    return ''.$count->nom.' '.$count->prenom;
																	},
																], 
																'createdOn',
                                                                [
																	'class' => 'yii\grid\ActionColumn',
																	'template' => '{effacer}',
																	'buttons' => [
																	'effacer' => function ($url,$model) {
																		return Html::a(
																				'<span class="fa fa-chain-broken"></span>', '#', 
																				['class' => 'btn btn-xs btn-warning', 'onclick' => 'init_click_handlers2('.$model->famille.');',
																				 'onmouseover'=>'Tip("Desassigner AKF")', 'onmouseout'=>'UnTip()']);
																	},
																],
                                                         ]]
					                    			 ]);
					                    		?>	    		
			                    </div>
			                </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>



<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Desassigner un AKF </h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	        <div class="row">
		    	<div class="col-lg-12">
		    		<?= $form->field($model, 'operateur_akf')->dropDownList($listAKF, ['prompt' => 'choisir akf']) ?>
		    	</div>
		    	 <?= $form->field($model, 'famille')
		          ->textInput()
		          ->label(null, ['id' => 'famille'])?>
		    </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton('Desassigner', ['class' => 'btn btn-sm btn-success', 'id' => 'creation', 'name' => 'creation']) ?>
	       </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
