<?php
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\MaskedInput;
use yii\widgets\ActiveForm;
use app\models\FamilyAkf;
use app\models\Communes;
use app\models\Famille;
use yii\helpers\ArrayHelper;
use app\models\Membre;
use yii\helpers\Url;
use app\models\ScrMembre;
use app\models\Chroniques;

$this->title = "Membres";

$x =Yii::$app->getRequest()->getQueryParam('id');

if($x != 0){
$script1 = <<< JS
	document.getElementById("scrmembre-nom").disabled = true;
	document.getElementById("scrmembre-famille").disabled = true;
	document.getElementById("scrmembre-prenom").disabled = true;
	document.getElementById("scrmembre-sexe").disabled = true;
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
}

$script2 = <<< JS
function init_click_handlers(val, val2) {
	if (confirm('Voulez-vous modifier les parametres de cette personne?')) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/membre',
			data: { famille : val,  membre : val2},
			success: function(data){
				var divOne16= document.getElementById('membre-famille');
		        divOne16.value = data["famille"];
	            divOne16.style.display='none';
		
		        var divOne18= document.getElementById('membre-memberid');
				divOne18.value = data["memberid"];
	            divOne18.style.display='none';
		
				var divOne15= document.getElementById('membre-prenom');
	            divOne15.value = data["prenom"];
				var divOne25= document.getElementById('membre-nom');
	            divOne25.value = data["nom"];

				var divOne35= document.getElementById('membre-surnom');
	            divOne35.value = data["surnom"];
		
				var divOne36= document.getElementById('membre-relation_chef_famille');
	            divOne36.value = data["relation_chef_famille"];
                document.getElementById("membre-relation_chef_famille").selectedIndex = data["relation_chef_famille"];
		
				var divOne37= document.getElementById('membre-niveau_education');
	            divOne37.value = data["niveau_education"];
		
		        var divOne38= document.getElementById('membre-c3');
	            divOne38.value = data["c3"];
				document.getElementById("membre-c3").selectedIndex = data["c3"];
		
				var divOne39= document.getElementById('membre-pouvoir_travailler');
	            divOne39.value = data["pouvoir_travailler"];
		        document.getElementById("membre-pouvoir_travailler").selectedIndex = data["pouvoir_travailler"];
		
		        var divOne39= document.getElementById('membre-savoir_lire');
	            divOne39.value = data["savoir_lire"];
		        document.getElementById("membre-savoir_lire").selectedIndex = data["savoir_lire"];
		
		        var divOne39= document.getElementById('membre-savoir_ecrire');
	            divOne39.value = data["savoir_ecrire"];
		        document.getElementById("membre-savoir_ecrire").selectedIndex = data["savoir_ecrire"];
		
				var divOne40= document.getElementById('membre-avoir_emploi');
	            divOne40.value = data["avoir_emploi"];
		        document.getElementById("membre-avoir_emploi").selectedIndex = data["avoir_emploi"];
		
		        var divOne35= document.getElementById('membre-avoir_activite_economique');
	            divOne35.value = data["avoir_activite_economique"];
		        document.getElementById("membre-avoir_activite_economique").selectedIndex = data["avoir_activite_economique"];
		
		        var divOne152= document.getElementById('membre-date');
		        var mois = data["mois_naissance"];
		        var jour = data["jour_naissance"];
		
		        if(data["mois_naissance"] >= 10){ mois = data["mois_naissance"];}
		        else{ mois = "0"+data["mois_naissance"];}
		        if(data["jour_naissance"] >= 10){ jour = data["jour_naissance"];}
		        else{ jour = "0"+data["jour_naissance"];}
		
		        divOne152.value = data["annee_naissance"]+"-"+mois+"-"+jour;
		
				var divOne100= document.getElementById('creation');
	            divOne100.style.display='none';
				var divOne101= document.getElementById('modification');
	            divOne101.style.display='inline';
		
				$("#myModal").find(".modal-title").html("Modification Membre - Code "+ data["memberid"]);
				$('#myModal').modal('show');
			},
			error: function() {
			},
		});
	}
	else{

    }
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);


$script3 = <<< JS
function init_click_handlers2(val, val2) {
	if (confirm('Voulez-vous ajouter une maladie chronique?')) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/chroniques',
			data: { famille : val,  membre : val2},
			success: function(data){
				if(data["famille"] == null && data["memberid"] == null){
					var divOne16= document.getElementById('chroniques-famille');
					divOne16.value = val;
		            divOne16.style.display='none';
			
			        var divOne18= document.getElementById('chroniques-memberid');
					divOne18.value = val2;
		            divOne18.style.display='none';
		
		
		            $('#myModal2').modal('show');
				}
		        else{
					var divOne16= document.getElementById('chroniques-famille');
			        divOne16.value = data["famille"];
		            divOne16.style.display='none';
			
			        var divOne18= document.getElementById('chroniques-memberid');
					divOne18.value = data["memberid"];
		            divOne18.style.display='none';
		
					var divOne18= document.getElementById('chroniques-souffrir_maladie_chronique');
		            if(data["souffrir_maladie_chronique"] != null)
						divOne18.value = data["souffrir_maladie_chronique"];
		           
		            var divOne18= document.getElementById('chroniques-avoir_traitement');
					if(data["avoir_traitement"] != null)
						divOne18.value = data["avoir_traitement"];
		
					var divOne18= document.getElementById('chroniques-duree_traitement');
					if(data["duree_traitement"] != null)
						divOne18.value = data["duree_traitement"];
		
					var divOne18= document.getElementById('chroniques-raison_non_traitement');
					if(data["raison_non_traitement"] != null)
						divOne18.value = data["raison_non_traitement"];
		
		            var divOne18= document.getElementById('chroniques-avoir_autres_maladies');
					if(data["avoir_autres_maladies"] != null)
						divOne18.value = data["avoir_autres_maladies"];
		
		            var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e2_2_81');
					if(data["MANM_HH1_HH_MALAD_81_E2_2_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E2_2_81"];
		
					var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e2_3_81');
					if(data["MANM_HH1_HH_MALAD_81_E2_3_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E2_3_81"];
		
					var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e2_4_81');
					if(data["MANM_HH1_HH_MALAD_81_E2_4_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E2_4_81"];
		
		            var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e3_1_81');
					if(data["MANM_HH1_HH_MALAD_81_E3_1_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E3_1_81"];
		
		            var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e3_2_81');
					if(data["MANM_HH1_HH_MALAD_81_E3_2_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E3_2_81"];
		
					var divOne18= document.getElementById('chroniques-manm_hh1_hh_malad_81_e3_3_81');
					if(data["MANM_HH1_HH_MALAD_81_E3_3_81"] != null)
						divOne18.value = data["MANM_HH1_HH_MALAD_81_E3_3_81"];
		
					var divOne100= document.getElementById('creation2');
	            	divOne100.style.display='none';
					var divOne100= document.getElementById('modification2');
	            	divOne100.style.display='inline';
					$('#myModal2').modal('show');
                }
			},
			error: function() {
			},
		});
	}
	else{

    }
}
JS;
$this->registerJs($script3, \yii\web\View::POS_END);

$script4 = <<< JS
function init_click_handlers3(val, val2) {
	if (confirm('Voulez-vous vraiment ajouter cette personne a la liste des morts?')) {
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/membre',
			data: { famille : val,  membre : val2},
			success: function(data){
				var divOne16= document.getElementById('mort-famille');
			    divOne16.value = data["famille"];
		        divOne16.style.display='none';
				
				var divOne16= document.getElementById('mort-id_membre');
			    divOne16.value = val2;
		        divOne16.style.display='none';
		
		
		        var divOne16= document.getElementById('mort-manm_hh1_id_manm_91_b1_1_91');
			    divOne16.value = data["prenom"];
		
		        var divOne16= document.getElementById('mort-manm_hh1_id_manm_91_b1_2_91');
			    divOne16.value = data["nom"];
		
		        var divOne16= document.getElementById('mort-manm_hh1_id_manm_91_b2_91');
			    divOne16.value = data["sexe"];
		        document.getElementById("mort-manm_hh1_id_manm_91_b2_91").selectedIndex = data["sexe"];
		
		        var divOne16= document.getElementById('mort-manm_hh1_id_manm_91_b7_91');
			    divOne16.value = data["relation_chef_famille"];
		        document.getElementById("mort-manm_hh1_id_manm_91_b7_91").selectedIndex = data["relation_chef_famille"];
		
		        var date = new Date();
                y = date.getFullYear();
		
		        var divOne16= document.getElementById('mort-manm_hh1_id_manm_91_b3_91');
			    divOne16.value = y - data["annee_naissance"];
		
				$('#myModal3').modal('show');
			},
			error: function() {
			},
		});
	}
	else{

    }
}
JS;
$this->registerJs($script4, \yii\web\View::POS_END);


?>

<section class="content-header">
	<h1>
		Plan de Developpement
		<small>Membres</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
		<li>Plan de Developpement</li>
		<li class="active">Membres</li>
	</ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body">
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
								<i class="fa fa-inbox"></i>
								<h3 class="box-title">PANEL DE GESTION</h3>
							</div>
							<a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal4">
                                  <i class="fa fa-plus-square"></i> Ajouter un membre</a>
							<div style="margin-top: 5px;">
								<ul class="nav nav-pills nav-stacked">
									<?php
									$user = User::findOne(Yii::$app->user->identity->id);
									$operateur = Operateurs::findOne($user->idOperateurs);
									echo  '<li class="header">Actions</li>';
									echo  '<li>'. Html::a('<i class="fa fa-users"></i> Familles', ['/socialworker/famille']); '.</li>';
									echo  '<li class="active">'. Html::a('<i class="fa fa-user"></i> Membres', ['/socialworker/membres','id' => 0]); '.</li>';
									?>
	                            </ul>
	                        </div>
						</div>
						<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
						    	<div class="col-sm-6">
                                </div>
                                <div class="col-sm-6">
                                	<?php echo $this->render('/membre/_search', ['model' => $searchModel]); ?>
                                </div>
						    </div>
                    	    <div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Liste des Membres</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php 
					                    echo GridView::widget([
					                    		'dataProvider' => $dataProvider,
												//'filterModel' => $searchModel,
                                                 'export' => false,
												 'showPageSummary' => false,
												 'bordered' => false,
												 'responsive' => true,
												 'hover' => true,
                                                 'columns' => [
	                                                   [ 'class' => 'yii\grid\SerialColumn'],
														'famille','memberid','prenom', 'nom',
							                    		[
								                    		'header'=>'Age',
								                    		'attribute'=>'annee_naissance',
								                    		'format'=>'raw',
								                    		'value'=> function ($model, $key, $index, $widget) {
								                    			$time = new \DateTime('now', new \DateTimeZone('UTC'));
								                    			$age = $time->format('Y') - $model->annee_naissance;
								                    			return $age.' ans';
								                            },
							                    		],
													   [
															'header'=>'Lire',
															'attribute'=>'savoir_lire',
															'format'=>'raw',
															'value'=> function ($model, $key, $index, $widget) {
																if($model->savoir_lire == 0){
																	return '-';
																}
																elseif($model->savoir_lire == 1){ return '<span class="label label-success">Yes</span>';}
																elseif($model->savoir_lire == 2){ return '<span class="label label-warning">No</span>';}
															},
													   ],
													   [
														   'header'=>'Ecrire',
														   'attribute'=>'savoir_ecrire',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	if($model->savoir_ecrire == 0){
														   		return '-';
														   	}
														   	elseif($model->savoir_ecrire == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($model->savoir_ecrire == 2){ return '<span class="label label-warning">No</span>';}
														   },
													   ],
													   [
														   'header'=>'Capacite',
														   'attribute'=>'pouvoir_travailler',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	if($model->pouvoir_travailler == 0){
														   		return '-';
														   	}
														   	elseif($model->pouvoir_travailler == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($model->pouvoir_travailler == 2){ return '<span class="label label-warning">No</span>';}
														   },
													   ],
													   [
														   'header'=>'Emploi',
														   'attribute'=>'avoir_emploi',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	if($model->avoir_emploi == 0){
														   		return '-';
														   	}
														   	elseif($model->avoir_emploi == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($model->avoir_emploi == 2){ return '<span class="label label-warning">No</span>';}
														   },
													   ],
													   [
														   'header'=>'MCH',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	$ch = Chroniques::find()->where(['famille' => $model->famille, 'memberid' => $model->memberid])->one();
														   	
														   	if($ch != null && $ch->souffrir_maladie_chronique == 0){
														   		return '-';
														   	}
														   	elseif($ch != null && $ch->souffrir_maladie_chronique == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($ch != null && $ch->souffrir_maladie_chronique == 2){ return '<span class="label label-warning">No</span>';}
														   	elseif($ch == null){ return '-';}	
														   },
													   ],
													   [
														   'header'=>'HAD',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	$ch = Chroniques::find()->where(['famille' => $model->famille, 'memberid' => $model->memberid])->one();
														   
														   	if($ch == null || ($ch != null && $ch->MANM_HH1_HH_MALAD_81_E3_1_81 == 0)){
														   		return '-';
														   	}
														   	elseif($ch != null && $ch->MANM_HH1_HH_MALAD_81_E3_1_81 == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($ch != null && $ch->MANM_HH1_HH_MALAD_81_E3_1_81 == 2){ return '<span class="label label-warning">No</span>';}
														   		
														   },
													   ],
													   [
														'class' => 'yii\grid\ActionColumn',
														'template' => '{editer} {malades} {mort}',
														'buttons' => [
														'editer' => function ($url,$model) {
															return Html::a(
																	'<span class="glyphicon glyphicon-edit"></span>', '#',
																	['class' => 'btn btn-xs btn-default', 'onclick' => 'init_click_handlers('.$model->famille.','.$model->memberid.');',
																	'onmouseover'=>'Tip("editer une famille")', 'onmouseout'=>'UnTip()']);
														},
														'malades' => function ($url,$model) {
															return Html::a(
																	'<span class="fa fa-wheelchair"></span>', '#',
																	['class' => 'btn btn-xs btn-warning', 'onclick' => 'init_click_handlers2('.$model->famille.','.$model->memberid.');',
																	'onmouseover'=>'Tip("Malades")', 'onmouseout'=>'UnTip()']);
														},
														'mort' => function ($url,$model) {
															return Html::a(
																	'<span class="fa fa-times"></span>', '#',
																	['class' => 'btn btn-xs btn-danger', 'onclick' => 'init_click_handlers3('.$model->famille.','.$model->memberid.');',
																	'onmouseover'=>'Tip("Mort")', 'onmouseout'=>'UnTip()']);
														},
														],
														]
                                                  ]
					                    ]);
					                ?>	    		
			                    </div>
			                </div>
                    	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modification Membre</h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	      
	      	 <?= $form->field($model, 'famille')->label(false)->textInput(['maxlength' => true]) ?>

   			 <?= $form->field($model, 'memberid')->label(false)->textInput() ?>
   			 
	      	 <div class="row">
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'prenom')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'nom')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'surnom')->textInput(['maxlength' => true]) ?>
		    	</div>
		     </div>
		    
	        <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'relation_chef_famille')->dropDownList(
		          		['1'=>'Chef kay la','2'=>'Madanm / Mari','3'=>'Pitit fi / Pitit gason','4'=>'Mennaj pitit gason/mennaj pitit fi','5'=>'Pitit-pitit','6'=>'Manman/papa'
						 ,'7'=>'Tonton / matant','8'=>'Kouzen / Kouzin','9'=>'Fre / se','10'=>'Gran papa / Grann','11'=>'Neve / nyes','12'=>'Pitit adoptif'
                         ,'13'=>'Lot kalite relasyon','14'=>'Travay peye','15'=>'Travay ki pa peye','16'=>'Moun ou konnen / Zanmi'],['prompt'=>'-- Relation Chef Famille --']); ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'date')->textInput() ?>
		    	</div>
		     </div>
		     
		     <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'savoir_lire')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Savoir Lire --']); ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'savoir_ecrire')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Savoir Ecrire --']); ?>
		    	</div>
		     </div>
		     
		     <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'niveau_education')->dropDownList(
		          		['1'=>'Pa gen Nivo','2'=>'prime','3'=>'seconde','4'=>'profesyonel','5'=>'univesite','6'=>'lot'
						 ],['prompt'=>'-- Niveau Education --']); ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'c3')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Niveau Education --']); ?>
		    	</div>
		     </div>
		    
    		
    		<div class="row">
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'pouvoir_travailler')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Pouvoir Travailler --']); ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'avoir_emploi')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Emploi --']); ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model, 'avoir_activite_economique')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Activite Economique --']); ?>
		    	</div>
		     </div>
		     
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation', 'name' => 'creation']) ?>
	        <?= Html::submitButton('Modifier', ['class'=>'btn btn-sm btn-primary', 'id' => 'modification', 'style' => 'display :none;', 'name' => 'modification']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Maladies Chroniques</h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	      
	      	 <?= $form->field($model2, 'famille')->label(false)->textInput(['maxlength' => true]) ?>

   			 <?= $form->field($model2, 'memberid')->label(false)->textInput() ?>
   			 
	      	 <div class="row">
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'souffrir_maladie_chronique')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Maladie Chronique --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'avoir_traitement')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Traitement --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'duree_traitement')->dropDownList(
		          		['1'=>'Mwens ke 3 mwa','2'=>'Ant 3 et 6 mwa','3'=>'Ant 6 e 12 mwa','4'=>'Plis ke 12 mwa'
						 ],['prompt'=>'-- Duree Traitement --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'raison_non_traitement')->dropDownList(
		          		['1'=>'Pas de ressources','2'=>'Pas de traitement disponible','3'=>' Le traitement est trop loin','4'=>'Le traitement est trop cher','5'=>'Maladie surnaturelle (Magie)','6'=>'Pas d\'espoir'
						 ],['prompt'=>'-- Raison Non Traitement --', 'empty' => '',]); ?>
		    	</div>
		     </div>
		    
		     
    		<div class="row">
    			<div class="col-lg-3">
		    		 <?= $form->field($model2, 'avoir_autres_maladies')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Maladie Chronique --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E2_2_81')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Traitement --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E2_3_81')->dropDownList(
		          		['1'=>'Mwens ke 3 mwa','2'=>'Ant 3 et 6 mwa','3'=>'Ant 6 e 12 mwa','4'=>'Plis ke 12 mwa'
						 ],['prompt'=>'-- Duree Traitement --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-3">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E2_4_81')->dropDownList(
		          		['1'=>'Pas de ressources','2'=>'Pas de traitement disponible','3'=>' Le traitement est trop loin','4'=>'Le traitement est trop cher','5'=>'Maladie surnaturelle (Magie)','6'=>'Pas d\'espoir'
						 ],['prompt'=>'-- Raison Non Traitement --', 'empty' => '',]); ?>
		    	</div>
		     </div>
		     
		     <div class="row">
		    	<div class="col-lg-4">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E3_1_81')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Handicap --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E3_2_81')->dropDownList(
		          		['1'=>'Pwoblem pou m we, malgre m pote ve','2'=>'Pwoblem pou m tande, malgre m gen pwotez','3'=>'Pwoblem pou m pale','4'=>'Pwoblem pou m okipe tet mwen (met rad sou mwen)'],['prompt'=>'-- Handicap --', 'empty' => '',]); ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model2, 'MANM_HH1_HH_MALAD_81_E3_3_81')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Limitation --', 'empty' => '',]); ?>
		    	</div>
		     </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" id="" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model2->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model2->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation2', 'name' => 'creation']) ?>
	        <?= Html::submitButton('Modifier', ['class'=>'btn btn-sm btn-primary', 'id' => 'modification2', 'style' => 'display :none;', 'name' => 'modification2']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>


<!-- Modal -->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Mort</h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	      
	      	 <?= $form->field($model3, 'famille')->label(false)->textInput(['maxlength' => true]) ?>
	      	 <?= $form->field($model3, 'id_membre')->label(false)->textInput(['maxlength' => true]) ?>

	      	 <div class="row">
		    	<div class="col-lg-4">
		    		<?= $form->field($model3, 'MANM_HH1_ID_MANM_91_B1_1_91')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model3, 'MANM_HH1_ID_MANM_91_B1_2_91')->textInput(['maxlength' => true]) ?>
		    	</div>
		    	<div class="col-lg-4">
		    		<?= $form->field($model3, 'MANM_HH1_ID_MANM_91_B2_91')->dropDownList(
		          		['1'=>'Masculin','2'=>'Feminin'],['prompt'=>'-- Sexe --']); ?>
		    	</div>
		     </div>
		     
		     <div class="row">
		    	<div class="col-lg-6">
		    		<?= $form->field($model3, 'MANM_HH1_ID_MANM_91_B3_91')->textInput() ?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model3, 'MANM_HH1_ID_MANM_91_B7_91')->dropDownList(
		          		['1'=>'Chef kay la','2'=>'Madanm / Mari','3'=>'Pitit fi / Pitit gason','4'=>'Mennaj pitit gason/mennaj pitit fi','5'=>'Pitit-pitit','6'=>'Manman/papa'
						 ,'7'=>'Tonton / matant','8'=>'Kouzen / Kouzin','9'=>'Fre / se','10'=>'Gran papa / Grann','11'=>'Neve / nyes','12'=>'Pitit adoptif'
                         ,'13'=>'Lot kalite relasyon','14'=>'Travay peye','15'=>'Travay ki pa peye','16'=>'Moun ou konnen / Zanmi'],['prompt'=>'-- Relation Chef Famille --']); ?>
		    	</div>
		     </div>
		    
	      <div class="modal-footer">
	        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model3->isNewRecord ? Yii::t('app', 'Ajouter') : Yii::t('app', 'Update'), ['class' => $model3->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation18', 'name' => 'Mort3']) ?>
	      </div>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nouveau Membre</h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	        <div>
			  <!-- Nav tabs -->
			  <ul class="nav nav-tabs" role="tablist">
			    <li role="presentation" class="active"><a href="#infodebase" aria-controls="home" role="tab" data-toggle="tab">Informations de Base</a></li>
			    <li role="presentation"><a href="#autresinfos" aria-controls="profile" role="tab" data-toggle="tab">Autres Infos</a></li>
			  </ul>
			
			  <!-- Tab panes -->
			  <div class="tab-content">
			    <div role="tabpanel" class="tab-pane active" id="infodebase">
			    	<div class="row">
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'famille')->textInput(['ondblclick' => 'showUser(this.value)']) ?>
				    	</div>
				    </div>
				    
				     <div class="row">
				    	<div class="col-lg-4">
				    		 <?= $form->field($model4, 'prenom')->textInput(['maxlength' => true]) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'nom')->textInput(['maxlength' => true]) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'surnom')->textInput(['maxlength' => true]) ?>
				    	</div>
				    </div>
				    
				    <div class="row">
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'sexe')->dropDownList(
		          		   ['1'=>'Masculin','2'=>'Feminin'],['prompt'=>'-- Sexe --']) ?>
				    	</div>
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'age')->textInput() ?>
				    	</div>
				    </div>
				    
				    <div class="row">
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'annee_naissance')->textInput() ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'mois_naissance')->dropDownList(
					          ['1'=>'Janvier','2'=>'Fevrier', '3'=>'Mars','4'=>'Avril',
			                   '5'=>'Mai','6'=>'Juin', '7'=>'Juillet','8'=>'Aout',
			                   '9'=>'Septembre', '10'=>'Octobre', '11'=>'Novembre',
			                   '12'=>'Decembre'], ['prompt'=>'--Choisir un mois --']
					    ) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'jour_naissance')->textInput() ?>
				    	</div>
				    </div>
				    
				    <div class="row">
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'type_piece_indendification')->dropDownList(
					          ['1'=>'Kat elektoral','2'=>'Matrikil fiskal', '3'=>'Batiste','4'=>'Paspo',
			                   '5'=>'Lot pyes','6'=>'Moun nan pa gen pyes'], ['prompt'=>'--Type Piece Indendification --']
					    )  ?>
				    	</div>
				    	<div class="col-lg-6">
				    		 <?= $form->field($model4, 'numero_piece')->textInput(['maxlength' => true]) ?>
				    	</div>
				    </div>
				    
				    <div class="row">
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'relation_chef_famille')->dropDownList(
		          		['1'=>'Chef kay la','2'=>'Madanm / Mari','3'=>'Pitit fi / Pitit gason','4'=>'Mennaj pitit gason/mennaj pitit fi','5'=>'Pitit-pitit','6'=>'Manman/papa'
						 ,'7'=>'Tonton / matant','8'=>'Kouzen / Kouzin','9'=>'Fre / se','10'=>'Gran papa / Grann','11'=>'Neve / nyes','12'=>'Pitit adoptif'
                         ,'13'=>'Lot kalite relasyon','14'=>'Travay peye','15'=>'Travay ki pa peye','16'=>'Moun ou konnen / Zanmi'],['prompt'=>'-- Relation Chef Famille --']);  ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'est_mere')->dropDownList(
		          		   ['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Est Mere --']) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'est_pere')->dropDownList(
		          		   ['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Est Pere --']) ?>
				    	</div>
				    </div>
				    
			    </div>
			    <div role="tabpanel" class="tab-pane" id="autresinfos">
			        <div class="row">
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'etat_civil')->dropDownList(
					          ['1'=>'Selibate','2'=>'Marye', '3'=>'Plase','4'=>'Divose',
			                   '5'=>'Ou ansanm ak yon moun','6'=>'Separe apre maryaj', '7'=>'Separe apre plasay','8'=>' Vef / Vev'], ['prompt'=>'--Etat Civil--']
					    ) ?>
				    	</div>
				    	<div class="col-lg-6">
				    		<?= $form->field($model4, 'moins_18_hors_maison')->dropDownList(
					          ['1'=>'Residan prezan','2'=>'Residan absan', '3'=>'Visite'], ['prompt'=>'--Resident--']
					    ) ?>
				    	</div>
				    </div>
				    
			    	<div class="row">
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'savoir_lire')->dropDownList(
		          				['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Savoir Lire --']) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'savoir_ecrire')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Savoir Ecrire --']) ?>
				    	</div>
				    	<div class="col-lg-4">
				    		<?= $form->field($model4, 'niveau_education')->dropDownList(
		          		['1'=>'Pa gen Nivo','2'=>'prime','3'=>'seconde','4'=>'profesyonel','5'=>'univesite','6'=>'lot'
						 ],['prompt'=>'-- Niveau Education --']) ?>
				    	</div>
				    </div>
				    
				
				    <div class="row">
				    	<div class="col-lg-3">
				    		<?= $form->field($model4, 'pouvoir_travailler')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Pouvoir Travailler --']); ?>
				    	</div>
				    	<div class="col-lg-3">
				    		 <?= $form->field($model4, 'avoir_emploi')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Emploi --']); ?>
				    	</div>
				    	<div class="col-lg-3">
				    		<?= $form->field($model4, 'avoir_activite_economique')->dropDownList(
		          		['1'=>'Wi','2'=>'Non'],['prompt'=>'-- Avoir Activite Economique --']) ?>
				    	</div>
				    	<div class="col-lg-3">
				    		 <?= $form->field($model4, 'type_activite')->dropDownList(
					          ['1'=>'M t ap f� biznis','2'=>'M te travay', '3'=>'M pa t travay, men, m t ap travay anvan','4'=>'M t ap chache travay e m te travay anvan',
			                   '5'=>'M te chache travay, men m pa t ap travay anvan','6'=>'M pa t chache travay, paske m te dekouraje', '7'=>'M te etidye s�lman','8'=>'M t ap etidye epi m t ap travay alafwa',
			                   '9'=>'M pran pansyon, m gen aktivite k ap rap�te m', '10'=>'M okipe fwaye a s�lman', '11'=>'Mwen enfim',
			                   '12'=>'L�t sitiyasyon, di kisa'], ['prompt'=>'--Type Activite --']
					    ) ?>
				    	</div>
				    </div>
			    </div>
			  </div>
			</div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
		        <?= Html::submitButton($model4->isNewRecord ? Yii::t('app', 'Ajouter') : Yii::t('app', 'Update'), ['class' => $model4->isNewRecord ? 'btn btn-sm btn-success' : 'btn btn-sm btn-primary', 'id' => 'creation5', 'name' => 'membre']) ?>
		      </div>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>
