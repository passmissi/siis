<?php

use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use app\models\Monitoringsousactivites;

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

if($operateur->fonction == 'OS'){
	$listAKF = ArrayHelper::map(Operateurs::find()->where(['commune_lie' => $operateur->commune_lie, 'fonction' => 'AKF'])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
	});
}elseif ($operateur->fonction == 'TS'){
	$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
	});
}

?>

<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                 <i class="fa fa-inbox"></i>
                                 <h3 class="box-title">PANEL DE GESTION</h3>
                            </div>
                            <!-- compose message btn -->
                            <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal" disabled="true">
                                 <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                            <!-- Navigation - folders-->
                            <div style="margin-top: 15px;">
                                 <ul class="nav nav-pills nav-stacked">
                                      <?php    	
                                          $user = User::findOne(Yii::$app->user->identity->id);
                                          $operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                          if($operateur->fonction == 'OS'){
                                          	echo  '<li class="header">Actions</li>';
                                          	echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                          }
                                          elseif ($operateur->fonction == 'TS'){
                                          	echo  '<li class="header">Actions</li>';
                                          	echo  '<li>'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                          	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                          	echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                          	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats']); '.</li>';
                                          }
                                      ?>
                                </ul>
                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<h3 class="page-header">Massive Monitoring</h3>
                    			<div  class="col-md-6">
                    				<?php $form = ActiveForm::begin([]); ?>
                    					<div class="row">
                    						<div  class="col-md-12">
                    							<?= $form->field($model, 'akf_nreg')->dropDownList($listAKF, ['prompt'=>'--Choisir un AKF --']) ?>
                    						</div>
                    					</div>
                    					<div class="row">
                    						<div  class="col-md-6">
                    							<?= $form->field($model, 'mois_nreg')->dropDownList(
											          ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
									                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
									                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
									                   'Decembre'=>'Decembre'], ['prompt'=>'--Choisir un mois --']
											    );?>
                    						</div>
                    						<div  class="col-md-6">
                    							 <?= $form->field($model, 'annee_nreg')
											        ->textInput()?>
                    						</div>
                    					</div>
                    					<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    				<?php ActiveForm::end(); ?>	
                    			</div>
                    			<div  class="col-md-6">
                    				<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
                                                        'columns' => [
	                                                         [ 'class' => 'yii\grid\SerialColumn'],
																 
																[
																'header'=>'Nom AKF',
																'attribute'=>'akf_nreg',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Operateurs::findOne($model->akf_nreg);
																	    if($count != null) return ''.$count->nom.' '.$count->prenom;
																	},
																], 
																[
																	'header'=>'Periode',
																	'format'=>'raw',
																	'value'=> function ($model, $key, $index, $widget) {
																		return $model->mois_nreg.' '.$model->annee_nreg;
																	},
																],
																[
																'header'=>'Nbre Activites',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
																	$count = Monitoringsousactivites::find()->where(['mplan' => $model->id])->count();
																	return ''.$count.' Activites';
																},
																],
                                                                [
																	'class' => 'yii\grid\ActionColumn',
																	'template' => '{editer} {effacer}',
																	'buttons' => [
																	'effacer' => function ($url,$model) {
																		$count = Monitoringsousactivites::find()->where(['mplan' => $model->id])->count();
																			
																		return !$count ? Html::a(
																			 '<span class="fa fa-trash-o"></span>',
																			 ['monitoringplan/delete', 'id' => $model->id],
																			 ['class' => 'btn btn-xs btn-danger',
																			  'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer ce suivi?',
																			  'onmouseover'=>'Tip("Effacer Suivi")', 'onmouseout'=>'UnTip()']): "";
																	},
																	'editer' => function ($url,$model) {
																		return Html::a(
																				'<span class="glyphicon glyphicon-edit"></span>',
																				['socialworker/suivimassive', 'id' => $model->id, 'id1' => $model->akf_nreg],
																				['class' => 'btn btn-xs btn-default',
																				'data-confirm'=>'Voulez vous vraiment editer ce suivi?',
																				'onmouseover'=>'Tip("Editer Suivi")', 'onmouseout'=>'UnTip()']);
																	},
																],
                                                         ]]
					                    			 ]);
					                    		?>
                    			</div>
                    		</div>
                    		<br />
                       </div>
               </div>
           </div>
        </div>
    </div>
</section>