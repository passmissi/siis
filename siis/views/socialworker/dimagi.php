<?php


use app\models\Plans;
use yii\helpers\Html;
use app\models\Operateurs;
use app\models\User;
use kartik\grid\GridView;
use app\models\TypeSousActivites;
use app\models\Activites;
use app\models\TypeActivites;

$this->title = 'Data Dimagi';
?>


<section class="content-header">
	<h1>
       Sharing Data (Dimagi)
       <small>Plan #<strong><?php echo $id ?></strong> pour <strong>
       <?php 
           $plan = Plans::findOne($id);
           $op = Operateurs::findOne($plan->akf);
           if($op!=null)
           echo ' '.$op->nom.' '.$op->prenom ?></strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dimagi Transfer</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                 <i class="fa fa-inbox"></i>
                                 <h3 class="box-title">PANEL DE GESTION</h3>
                            </div>
                            <?= Html::a('  <i class="glyphicon glyphicon-duplicate"></i>  PDF', 
                                               ['/plans/pdf', 'id' => $id], ['class' => 'btn btn-block btn-primary', 'type' => 'button','target' => '_blank']);?>                
                           
                                            
                                      
                                            <!-- Navigation - folders-->
                           <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans', ['']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-rss-square"></i> Notification', ['']); '.</li>';
                                                	 
                                                	?>
                                                </ul>
                         </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<h3 class="page-header">Centre de Partage des donnees (DIMAGI)</h3>
                    			<div class="col-xs-2"> <!-- required for floating -->
								    <!-- Nav tabs -->
								    <ul class="nav nav-tabs tabs-left">
								      <li class="active"><a href="#home" data-toggle="tab">Resume</a></li>
								      <li><a href="#profile" data-toggle="tab">Data</a></li>
								      <li><a href="#messages" data-toggle="tab">Statistiques</a></li>
								      <li><a href="#settings" data-toggle="tab">Map</a></li>
								    </ul>
								</div>
								
								<div class="col-xs-10">
								    <!-- Tab panes -->
								    <div class="tab-content">
								      <div class="tab-pane active" id="home">
								      	<div class="row">
								      		<div class="col-md-12">
								      			<?php 
								      				$x = Yii::$app->getRequest()->getQueryParam('id');
								      				$count = Activites::find()->where(['id_plan' => $x])->count();
								      			?>
								      			<?php if ($isOK == sizeof($list_file) && sizeof($list_file) > 1): ?>
								      				<div class="alert alert-success">
								      				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													  <strong>Success!</strong> toutes les donnees de ce plan ont ete transferees.
													</div>
												<?php elseif ($isOK > 0 && $isOK < sizeof($list_file)):?>
								      				<div class="alert alert-warning">
								      				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													  <strong>Warning!</strong> Certaines donnees n'ont pas ete transferees.
													</div>
								      			<?php elseif ($isOK == 0):?>
								      				<div class="alert alert-danger">
								      				  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
													  <strong>Danger!</strong> Aucune donnee n'a ete envoyee.
													</div>
								      			<?php endif;?>
								      			
								      			<?php 
					                    echo GridView::widget([
					                    		'dataProvider' => $dataProvider,
                                                 'export' => false,
												 'showPageSummary' => false,
					                    		 'summary' => '',
					                    		 'layout' => '{summary}{items}{pager}',
												 'bordered' => false,
												 'responsive' => true,
												 'hover' => true,
                                                 'columns' => [
	                                                   'id', 
                                                 		[
                                                 		'header'=>'TA',
                                                 		'attribute'=>'id_activite',
                                                 		'format'=>'raw',
                                                 		'value'=> function ($model, $key, $index, $widget) {
                                                 			$type = TypeActivites::findOne($model->id_type_activite);
                                                 			return $type->libelle.'';
                                                 		},
                                                 		],'description', 'lieu',
							                    		[
							                    		'header'=>'Date',
							                    		'format'=>'raw',
							                    		'value'=> function ($model, $key, $index, $widget) {
							                    			return $model->date_activite.'';
							                    		},
							                    		],
							                    		[
							                    		'format'=>'raw',
							                    		'header'=>'Dimagi',
							                    		'value'=>function ($model, $key, $index, $widget) {
							                    			if($model->isUploaded == 1)
							                    				return '<span class="fa fa-check-circle"></span>';
							                    			else
							                    				return '<span class="fa fa-minus-circle"></span>';
							                    		},
							                    		],
							                    		[
							                    				'class' => 'yii\grid\ActionColumn',
							                    				'template' => '{dimagi}',
							                    				'buttons' => [
							                    						'dimagi' => function ($url,$model) {
							                    							if ($model->isUploaded == 1){
							                    								return '';
							                    							}else{
							                    								return Html::a(
							                    										'<span class="fa fa-cloud-upload"></span>',
							                    										['/socialworker/export2', 'id' => $model->id_plan, 'id2' => $model->id],
							                    										['class' => 'btn btn-xs btn-default']);
							                    							}
							                    						},
							                    		]]
                                                  ]
					                    ]);
					                ?>	 
								      		</div>
								      	</div>
								      </div>
								      
								      <div class="tab-pane" id="profile">Data.</div>
								      <div class="tab-pane" id="messages">Statistiques.</div>
								      <div class="tab-pane" id="settings">Map.</div>
								      
								    </div>
								</div> 
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>