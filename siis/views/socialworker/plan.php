<?php 
use yii\helpers\Html;
use app\models\plans;
use yii\widgets\ActiveForm;
use yii\base\View;

$this->title = 'Plan';
?>

<section class="content-header">
	<h1>
       Plan
       <small>Creation de Plan pour <strong><?php echo $model1->nom.' '.$model1->prenom ?></strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Plan</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-ellipsis-v"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nombre d'AKF</span>
                  <span class="info-box-number"><?php echo $akf ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-retweet"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nombre de Plans</span>
                  <span class="info-box-number"><?php echo $plan ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->


            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa  fa-angle-up"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nombre d'Activites</span>
                  <span class="info-box-number">0</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-folder-open-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Rapports</span>
                  <span class="info-box-number"><?php echo $plan ?></span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
	</div>
	
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <li class="header">Actions</li>
                                                     <li><a href="../SPM/DashBoard.aspx"><i class="fa fa-inbox"></i> Notifications</a></li>
                                                    <li><a href="../SPM/GetPPM.aspx?user=1"><i class="fa  fa-ellipsis-v"></i> AKF</a></li>
                                                    <li class="active"><a href="../SPM/MarchesUser.aspx"><i class="fa fa-retweet"></i>Plans</a></li>
                                                    <li><a href="../SPM/Requetes.aspx"><i class="fa  fa-angle-up"></i>Activites</a></li>
                                                </ul>
                                            </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<div  class="col-md-7">
                    				 
                    				 	<?= $this->render( '//plans/_form', ['model' => $model]); ?>
                    				 
                    			</div>
                    		
                    		    <div  class="col-md-5">
                    				<div class="box box-solid box-default">
						                <div class="box-header with-border">
						                  <i class="fa fa-text-width"></i>
						                  <h3 class="box-title">Infos A.K.F.</h3>
						                </div><!-- /.box-header -->
						                <div class="box-body">
						                  <dl class="dl-horizontal">
						                    <dt>Nom</dt>
						                    <dd><?php echo $model1->nom ?></dd>
						                    <dt>Prenom</dt>
						                    <dd><?php echo $model1->prenom ?></dd>
						                    <dt>E-mail</dt>
						                    <dd><?php echo $model1->email ?></dd>
						                    <dt>Fonction</dt>
						                    <dd><?php echo $model1->fonction ?></dd>
						                    <dt>Commune</dt>
						                    <dd><?php echo $model1->communeName ?></dd>
						                  </dl>
						                </div><!-- /.box-body -->
						              </div><!-- /.box -->
                    			</div>
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>