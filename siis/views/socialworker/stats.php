<?php

use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Plans;
use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use app\models\Monitoringsousactivites;
?>

<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	$plan = new Plans();
                                                	$time = new \DateTime('now', new \DateTimeZone('UTC'));
                                                	
                                                	$mois_int = date_format($time,"n");
                                                	$mois = $plan->getNumberMonth($mois_int);
                                                	$annee_int = date('Y');
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats', 'mois' => $mois, 'annee' => $annee_int]); '.</li>';
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    	    <div class="row pad">
							    	<div class="col-sm-7">
	                                </div>
	                                <div class="col-sm-5">
	                                	<?php $form = ActiveForm::begin([
	                                			'action' => ['/socialworker/stats'],
	                                			'method' => 'get',
	                                			'enableClientValidation' => false,
	                                			'enableAjaxValidation' => false,
	                                			'options' => ['class' => 'form-inline'],]); ?>
											    <div class="input-group">
											      <?= $form->field($model, 'annee')->label(false)
											      	->textInput(['style'=>'width:100%', 'class' => 'form-control input-sm','placeholder' => 'annee']) ?>
												  <span class="input-group-btn" style="width:0px;"></span>
											       <?= $form->field($model, 'mois')->dropDownList(
													  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
										                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
										                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
										                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm'])->label(false)
											      ?>
											      <span class="input-group-btn" style="width:0px;"></span>
												  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
												</div>
										<?php ActiveForm::end(); ?>
	                                </div>
							    </div>
                    		<div class="row">
                    			<div class="col-md-6">
                    				<div class="box box-success">
		                                <div class="box-header">
		                                    <i class="fa fa-bar-chart-o"></i>
		                                    <h3 class="box-title">Rapport - Suivi</h3>
		                                </div>
		                                <div class="box-body">
		                                	<?php 
			                                	$ma = new Monitoringsousactivites();
			                                	$x =Yii::$app->getRequest()->getQueryParam('mois');
			                                	$y =Yii::$app->getRequest()->getQueryParam('annee');
		                                	
			                                	$sact_1 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 1);
			                                	$sact_2 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 2);
			                                	$sact_3 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 3);
			                                	$sact_4 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 4);
			                                	$sact_6 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 6);
			                                	$sact_7 = $ma->getDataSousActivitesPerType($x, $y, $operateur->id, 7);
		                                	
			                                	print_r($sact_7[0].'ok');
		                                	?>
		                                	
		                                	<?php 
		                                	
		                                	echo Highcharts::widget([
			                                	'options' => [
				                                	'exporting' =>['enabled' => true],
				                                	'title' => ['text' => 'Donnees Statistiques'],
				                                	'xAxis' => [
				                                		'categories' => ['Suivi du Plan de Travail']
				                                	],
				                                	'yAxis' => [
				                                		'title' => ['text' => 'value']
				                                	],
				                                	'series' => [
					                                	['name' => 'PR',
					                                	'type' => 'column',
					                                	'data' => $sact_1,
					                                	'color' => new JsExpression('Highcharts.getOptions().colors[0]'),],
														['name' => 'DI',
'type' => 'column',
'data' => $sact_2,
'color' => new JsExpression('Highcharts.getOptions().colors[1]'),],
['name' => 'RF',
'type' => 'column',
'data' => $sact_3,
'color' => new JsExpression('Highcharts.getOptions().colors[2]'),],
['name' => 'FR',
'type' => 'column',
'data' => $sact_4,
'color' => new JsExpression('Highcharts.getOptions().colors[3]'),],
['name' => 'VA',
'type' => 'column',
'data' => $sact_6,
'color' => new JsExpression('Highcharts.getOptions().colors[4]'),],
['name' => 'CP',
'type' => 'column',
'data' => $sact_7,
'color' => new JsExpression('Highcharts.getOptions().colors[5]'),],
				                                	]
			                                	
		                                	]]);
		                                	?>
		                                </div>
		                            </div>
                    			</div>
                    			
                    			<div class="col-md-6">
                    				<div class="box box-info">
		                                <div class="box-header">
		                                    <i class="fa fa-bar-chart-o"></i>
		                                    <h3 class="box-title">Rapport - Plan</h3>
		                                </div>
		                                <div class="box-body">
		                                	<?php 
		                                		$ma = new Monitoringsousactivites();
		                                		$x =Yii::$app->getRequest()->getQueryParam('mois');
		                                		$y =Yii::$app->getRequest()->getQueryParam('annee');
		                                	
		                                		$act_1 = $ma->getDataPerType($x, $y, $operateur->id, 1);
		                                		$act_2 = $ma->getDataPerType($x, $y, $operateur->id, 2);
		                                		$act_3 = $ma->getDataPerType($x, $y, $operateur->id, 3);
		                                		$act_4 = $ma->getDataPerType($x, $y, $operateur->id, 4);
		                                		$act_5 = $ma->getDataPerType($x, $y, $operateur->id, 5);
		                                		$act_6 = $ma->getDataPerType($x, $y, $operateur->id, 6);
		                                		//print_r($act_1[0]);
		                                	?>
		                                	
		                                	<?php 
		                                	echo Highcharts::widget([
			                                	'options' => [
				                                	'exporting' =>['enabled' => true],
				                                	'title' => ['text' => 'Donnees Statistiques'],
				                                	'xAxis' => [
				                                		'categories' => ['Plan de Travail']
				                                	],
				                                	'yAxis' => [
				                                		'title' => ['text' => 'value']
				                                	],
				                                	'series' => [
					                                	['name' => 'VD',
					                                	'type' => 'column',
					                                	'data' => $act_1,
					                                	'color' => new JsExpression('Highcharts.getOptions().colors[0]'),],
					                                	 
														['name' => 'PO',
'type' => 'column',
'data' => $act_2,
'color' => new JsExpression('Highcharts.getOptions().colors[1]'),],

['name' => 'CM',
'type' => 'column',
'data' => $act_3,
'color' => new JsExpression('Highcharts.getOptions().colors[2]'),],

['name' => 'CJ',
'type' => 'column',
'data' => $act_4,
'color' => new JsExpression('Highcharts.getOptions().colors[3]'),],

['name' => 'VE',
'type' => 'column',
'data' => $act_5,
'color' => new JsExpression('Highcharts.getOptions().colors[4]'),],
['name' => 'RE',
'type' => 'column',
'data' => $act_6,
'color' => new JsExpression('Highcharts.getOptions().colors[5]'),],

				                                	]
			                                	
		                                	]]);
		                                	?>
		                                </div>
		                            </div>
                    			</div>
                    			
                    		</div>
                    	</div>
                    	</div></div></div></div></div></section>