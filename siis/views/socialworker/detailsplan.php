<?php 

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Operateurs;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrPlans */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Plans";

?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body">
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                 <!-- compose message btn -->
                                 <a class="btn btn-block btn-primary">
                                      <i class="fa fa-plus-square"></i> Creer un plan de travail</a>
                                 <!-- Navigation - folders-->
                                 <div style="margin-top: 15px;">
                                      <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li>'. Html::a('<i class="fa  fa-ellipsis-v"></i> AKF', ['/socialworker/index']); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans'); '.</li>';
                                                	
                                                	?>
                                                </ul>
                                            </div>
						</div>
						
						<div class="col-md-9 col-sm-8">
						
						    <div class="row pad">
						    	<div class="col-sm-9">
						    		
                                </div>
                                <div class="col-sm-3">
                                	<?php 
	                                	$id = Yii::$app->getRequest()->getQueryParam('id');
                                	    echo $this->render('/socialworker/_search', ['model' => $searchModel, 'id' => $id]); ?>
                                </div>
						    </div>
						    
							<div class="box">
                    	          <div class="box-header with-border">
                    	          	<h3 class="box-title">Liste des plans de l'AKF 
                    	          	      <?php 
                    	          	        $op = Operateurs::findOne($id);
                    	          	      	echo '<span>'.$op->nom.' '.$op->prenom.'</span>'
                    	          	      ?>
                    	          	</h3>
                    	          </div>
			                      <div class="box-body table-responsive no-padding">
			                      	<?php
				                      echo	GridView::widget([
					                      	'dataProvider' => $dataProvider,
					                      	//'filterModel' => $searchModel,
                                            'export' => false,
											'showPageSummary' => false,
											'bordered' => false,
											'responsive' => true,
											'hover' => true,
					                      	'columns' => [
						                      	['class' => 'yii\grid\SerialColumn'],
						                      	'id',
						                      	'mois',
						                      	'annee',
						                      	'akf0.nom',
						                      	[
													'class' => 'yii\grid\ActionColumn',
													'template' => '{activites} {calendrier}',
													'buttons' => [
													'activites' => function ($url,$model) {
														return Html::a(
																'<span class="glyphicon glyphicon glyphicon-share"></span>',
																['plans/view', 'id' => $model->id],
																['class' => 'btn btn-xs btn-default']);
													},
													'calendrier' => function ($url,$model) {
														return Html::a(
																'<span class="glyphicon glyphicon-calendar"></span>',
																['plans/calendar', 'id' => $model->id],
																['class' => 'btn btn-xs btn-warning']);
													},
													
													],
													]
					                      	],
				                      	]);
			                      	
			                      	?>
			                      </div>
			                </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>