<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Plans;
use app\models\User;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Growl;
use yii\helpers\ArrayHelper;

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$script1 = <<< JS
function getState(val) {
	 if(val == ''){ 
		document.getElementById("monitoringplan-plan").disabled = true;
		document.getElementById("monitoringplan-listact").disabled = true;
		$("#monitoringplan-plan").html("");
		$("#monitoringplan-listact").html("");
     }
	 else{ 
		document.getElementById("monitoringplan-plan").disabled = false;
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/listplan',
			data: { id: val },
			success: function(data){
				$("#monitoringplan-plan").html(data);
			},
			error: function() {
				aa = $("#modalTitle").val();
				alert(aa);
			},
		});
     }
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);

$script2 = <<< JS
function getState1(val) {
	 if(val == ''){
		document.getElementById("monitoringplan-listact").disabled = true;
		$("#monitoringplan-listact").html("");
     }
	 else{
		document.getElementById("monitoringplan-listact").disabled = false;
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/listactivites',
			data: { id: val },
			success: function(data){
				$("#monitoringplan-listact").html(data);
			},
			error: function() {
				aa = $("#modalTitle").val();
				alert(aa);
			},
		});
     }
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);
?>


<section class="content-header">
	<h1>
       Suivi
       <small>Centre de Suivi du Plan de Travail</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Suivi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-square"></i> Creer un plan de Travail</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	$plan = new Plans();
                                                	$time = new \DateTime('now', new \DateTimeZone('UTC'));
                                                	
                                                	$mois_int = date_format($time,"n");
                                                	$mois = $plan->getNumberMonth($mois_int);
                                                	$annee_int = date('Y');
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-ellipsis-v"></i> Suivi AKF', ['/socialworker/suiviakf']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plan', ['/socialworker/suiviplan']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-fire"></i> Massive Monitoring', ['/socialworker/massive']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i>Stats. Monitoring', ['/socialworker/stats', 'mois' => $mois, 'annee' => $annee_int]); '.</li>';
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
	                    		<?php $form = ActiveForm::begin([]); ?>
	                    			<div  class="col-md-6">
	                    				<div class="box box-primary">
	                    					<div class="box-header with-border">
							                  <h3 class="box-title">Suivi Plan de Travail</h3>
							                </div><!-- /.box-header -->
							                
							                <div class="box-body">
							                	<?= $form->field($model, 'akf')->dropDownList($listAKF, ['prompt'=>'--Choisir un AKF --', 'onchange' =>'getState(this.value);']) ?>
		    
		                                        <?= $form->field($model, 'Plan')->dropDownList([], ['prompt'=>'--Choisir un plan --', 'onchange' =>'getState1(this.value);', 'disabled' => 'disabled']) ?>
							                </div>
							            </div>
	                    			</div>
	                    			<div  class="col-md-6">
	                    			    <div class="box box-primary">
	                    			    	<div class="box-body">
	                    			    		<?= $form->field($model, 'listact')->listBox
	                    			    		([], ['prompt'=>'Select','size'=>12, 'disabled' => 'disabled', 'multiple' => 'multiple']); ?>
	                    			    	</div>
	                    			    	 <div class="box-footer">
	                    			    	 	<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	                    			    	 </div>
	                    			    </div>
	                    			</div>
	                    	   <?php ActiveForm::end(); ?>	
                    	   
                    		</div>
                    	</div>
                    	</div></div></div></div></div>
</section>