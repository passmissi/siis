<?php
use kartik\grid\GridView;

?>
<div class="row">
	<div class="col-lg-12">
		 <div class="box">
                    	    	<div class="box-header">
                    	    		<h3 class="box-title">Liste des Membres</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
		<?php 
					                    echo GridView::widget([
					                    		'dataProvider' => $dataProvider,
                                                 'export' => false,
												 'showPageSummary' => false,
					                    		 'summary' => '',
					                    		 'layout' => '{summary}{items}{pager}',
												 'bordered' => false,
												 'responsive' => true,
												 'hover' => true,
                                                 'columns' => [
	                                                   'memberid','prenom', 'nom',
							                    		[
								                    		'header'=>'Age',
								                    		'attribute'=>'annee_naissance',
								                    		'format'=>'raw',
								                    		'value'=> function ($model, $key, $index, $widget) {
								                    			$time = new \DateTime('now', new \DateTimeZone('UTC'));
								                    			$age = $time->format('Y') - $model->annee_naissance;
								                    			return $age.' ans';
								                            },
							                    		],
													   [
															'header'=>'Lire',
															'attribute'=>'savoir_lire',
															'format'=>'raw',
															'value'=> function ($model, $key, $index, $widget) {
																if($model->savoir_lire == 0){
																	return '-';
																}
																elseif($model->savoir_lire == 1){ return '<span class="label label-success">Yes</span>';}
																elseif($model->savoir_lire == 2){ return '<span class="label label-warning">No</span>';}
															},
													   ],
													   [
														   'header'=>'Ecrire',
														   'attribute'=>'savoir_ecrire',
														   'format'=>'raw',
														   'value'=> function ($model, $key, $index, $widget) {
														   	if($model->savoir_ecrire == 0){
														   		return '-';
														   	}
														   	elseif($model->savoir_ecrire == 1){ return '<span class="label label-success">Yes</span>';}
														   	elseif($model->savoir_ecrire == 2){ return '<span class="label label-warning">No</span>';}
														   },
													   ],
                                                  ]
					                    ]);
					                ?>	    		
	</div>
	</div></div>