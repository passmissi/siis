<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
//use app\models\plans;
use app\models\User;
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Famille;
use app\models\Communes;
use kartik\select2\Select2;

$this->title = "Carnet";

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(), 
		'id', 
		function($element) {
        	return $element['nom'] ." ". $element['prenom'] ;
		});

$listlocalite = ArrayHelper::map(Famille::find()->select('localite')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('localite')->all(),
		'localite','localite');
?>

<section class="content-header">
	<h1>
       Plan de Developpement
       <small>Carnet Familial</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Carnet</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                <i class="fa fa-inbox"></i>
                                <h3 class="box-title">PANEL DE GESTION</h3>
                            </div>
                                            
                                            <div style="margin-top: 5px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa  fa-ellipsis-v"></i> Carnet Familial / AKF', ['']); '.</li>';
                                                	
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<div  class="col-md-6">
	                    			<?php $form = ActiveForm::begin() ?>
	                    				<div class="box box-primary">
	                    					<div class="box-header with-border">
							                  <h3 class="box-title">Carnet Familial / AKF</h3>
							                </div><!-- /.box-header -->
										    <div class="box-body">
										        <?= $form->field($model, 'nomAKF')->dropDownList($listAKF, [
													'prompt'=>'Select AKF']);?>
										    </div>
										    <div class="box-footer">
										        <?= Html::submitButton('Generer Carnet', ['class' => 'btn btn-success', 'formtarget' => '_blank']) ?>
										    </div>
	                    				</div>
	                    			<?php ActiveForm::end() ?>	
                    			</div>
                    			<div  class="col-md-6">	
                    		     	<?php $form = ActiveForm::begin() ?>
	                    				<div class="box box-primary">
	                    					<div class="box-header with-border">
							                  <h3 class="box-title">Carnet Familial / Localite</h3>
							                </div><!-- /.box-header -->
										    <div class="box-body">
										       <?= $form->field($famille, 'localite')->widget(Select2::classname(), [
													'data' => $listlocalite,
													'language' => 'en',
													'options' => ['placeholder' => 'Select localite','style'=>'width:100%',],
										    		'size' => 'md',
										    		'theme' => Select2::THEME_BOOTSTRAP,
													'pluginOptions' => [
														'allowClear' => true,
													],]);?>
										    </div>
										    <div class="box-footer">
										        <?= Html::submitButton('Generer Carnet', ['class' => 'btn btn-success', 'formtarget' => '_blank']) ?>
										    </div>
	                    				</div>
	                    			<?php ActiveForm::end() ?>
                    			</div>
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>