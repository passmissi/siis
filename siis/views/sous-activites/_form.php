<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SousActivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sous-activites-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'id_activite')->textInput() ?>

    <?= $form->field($model, 'id_type_sous_activite')->textInput() ?>

    <?= $form->field($model, 'id_membre')->textInput() ?>

    <?= $form->field($model, 'id_famille')->textInput() ?>

    <?= $form->field($model, 'id_plan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
