<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrSousActivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sous-activites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_activite') ?>

    <?= $form->field($model, 'id_type_sous_activite') ?>

    <?= $form->field($model, 'id_membre') ?>

    <?= $form->field($model, 'id_famille') ?>

    <?php // echo $form->field($model, 'id_plan') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
