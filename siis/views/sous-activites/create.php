<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SousActivites */

$this->title = Yii::t('app', 'Create Sous Activites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sous Activites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sous-activites-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
