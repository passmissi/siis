<?php

use yii\helpers\Html;
use kartik\grid\GridView;


use app\models\User;
use app\models\Operateurs;
use app\models\Activites;
use app\models\TypeActivites;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrSousActivites */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sous Activites');
$this->params['breadcrumbs'][] = $this->title;

$activite = Activites::findOne($id);
?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary">
                                                <i class="fa fa-plus-square"></i> Creer un plan de travail</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	if($plan->mode == 1){
                                                		echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';

                                                	}elseif ($plan->mode == 2){
                                                		echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li class="active">'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
														
	                                                }
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    	   <div class="row">
                    	   		<div class="col-md-5">
                    	   			<div class="box box-solid">
						                <div class="box-header with-border">
						                  <i class="fa fa-text-width"></i>
						                  <h3 class="box-title">Details Activites <?= $id ?></h3>
						                </div><!-- /.box-header -->
						                <div class="box-body">
						                  <dl>
						                    <dt>Date</dt>
						                    <dd><?= $activite->date_activite ?></dd>
						                    <dt>AKF</dt>
						                    <dd><?php 
						                          $op = Operateurs::findOne($plan->akf);
						                          echo $op->nom.' '.$op->prenom; ?></dd>
						                    <dt>Plan</dt>
						                    <dd><?= $plan->nomplan ?></dd>
						                    <dt>Activite</dt>
						                    <dd><?php 
						                    	$act = Activites::findOne($id);
						                    	$type = TypeActivites::findOne($act->id_type_activite);
						                    	echo $type->libelle;
						                    ?></dd>
						                  </dl>
						                </div><!-- /.box-body -->
						              </div><!-- /.box -->
                    	   		</div>
                    	   		<div class="col-md-7">
                    	   			<div class="box">
                    	    		        <div class="box-header with-border">
			                    	          	<h3 class="box-title">Liste des sous-activites 
			                    	          	</h3>
			                    	          </div>
			                    	   		<div class="box-body table-responsive no-padding">
			                    	   			<?= GridView::widget([
											        'dataProvider' => $dataProvider,
											        //'filterModel' => $searchModel,
											        'export' => false,
											        'showPageSummary' => false,
											        'bordered' => false,
											        'responsive' => true,
											        'hover' => true,
											        'columns' => [
											            ['class' => 'yii\grid\SerialColumn'],
											            'id',
											            'plan.nomplan',
											            'typeSousActivite.libelle',
											        ],
											    ]); ?>
			                    	   </div></div>
                    	   		</div>
                    	   </div>
                    	</div>
                    </div>
               </div>
           </div>
       </div>
    </div>
 </section>