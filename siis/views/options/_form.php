<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Famille;
use app\models\Options;
use kartik\export\ExportMenu;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Options */
/* @var $form yii\widgets\ActiveForm */

$listcommune = ArrayHelper::map(Famille::find()->select('commune')->distinct()
		->where('commune not in ("thiotte", "anse_pitres", "grand_gosier")')->asArray()->orderBy('commune')->all(),
		'commune','commune');

/* $listquestion = ArrayHelper::map(Options::find()->select('question')->distinct()
		->asArray()->orderBy('question')->all(),
		'question','question'); */

$listquestion = ArrayHelper::map(Options::find()
		->asArray()->orderBy('question')->all(),
		'id','question','dimension');

$script2 = <<< JS
function init_click_handlers3(val) {
	$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/listsectioncommunale',
		data: { commune: val },
		success: function(data){
			$("select#options-section_communale").html(data);
			console.log(data);
		},
		error: function() {
			alert('no2');
		},
	});
		
	$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/listlocalite',
		data: { commune: val },
		success: function(data){
			$("select#options-localite").html(data);
			console.log(data);
		},
		error: function() {
			alert('no2');
		},
	});
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script3 = <<< JS
function init_click_handlers4(val) {
	$.ajax({
		type: 'GET',
		url: 'index.php?r=rapport/listquestion',
		data: { dimension: val },
		success: function(data){
			$("select#options-question").html(data);
			console.log(data);
		},
		error: function() {
			alert('no2');
		},
	});
}
JS;
$this->registerJs($script3, \yii\web\View::POS_END);
?>

<div class="options-form">
    <?php $form = ActiveForm::begin(); ?>
		<div class="row">
			<div class="col-lg-4">
				<?= $form->field($model, 'commune')->dropDownList($listcommune, ['prompt' => 'Choisir une commune', 
							'onchange' => 'init_click_handlers3(this.value)'])?>
			</div>
			<div class="col-lg-4">
				<?= $form->field($model, 'section_communale')
					->dropDownList([], ['prompt' => 'Choisir une section communale']) ?>
			</div>
			<div class="col-lg-4">
				<?= $form->field($model, 'localite')
					->dropDownList([], ['prompt' => 'Choisir une localite']) ?>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<?= $form->field($model, 'question')->widget(Select2::classname(), [
					'data' => $listquestion,
					'language' => 'en',
					'options' => ['placeholder' => 'Choisir question','style'=>'width:100%','multiple' => true, 'rows' => 3],
		    		'size' => 'sm',
		    		'theme' => Select2::THEME_BOOTSTRAP,
					'pluginOptions' => [
						'allowClear' => true,
					],]); ?>
			</div>
		</div>
	    <div class="form-group">
	        <?= Html::submitButton(Yii::t('app', 'Generer Data'), ['class' => 'btn btn-success']) ?>
	    </div>
    <?php ActiveForm::end(); ?>
</div>
