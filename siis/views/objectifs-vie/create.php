<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ObjectifsVie */

$this->title = Yii::t('app', 'Create Objectifs Vie');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Objectifs Vies'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objectifs-vie-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
