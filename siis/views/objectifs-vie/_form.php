<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\ObjectifsVie;
use app\models\RefObjectifsvie;

/* @var $this yii\web\View */
/* @var $model app\models\ObjectifsVie */
/* @var $form yii\widgets\ActiveForm */

$objectif = ArrayHelper::map(RefObjectifsvie::find()->asArray()->all(), 'id', 'libelle');
?>

<div class="objectifs-vie-form">

    <?php $form = ActiveForm::begin(); ?>
	<div class="modal-body">
		<div class="row">
			<div class="col-lg-6">
				<?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>
				
			</div>
			<div class="col-lg-6">
				<?= $form->field($model, 'objectif')->dropDownList($objectif, ['prompt' => 'Choisir un objectif']) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<?= $form->field($model, 'niveau')->dropDownList(['1' => 'Pou Travay', '2' => 'Pou Kenbe', '3' => 'N/A', '4' => 'Komplete',
							], 
                 ['prompt' => 'Niveau'])  ?>
				
			</div>
			<div class="col-lg-6">
				<?= $form->field($model, 'date_objectif')->input('date', ['required']) ?>
				
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<?= $form->field($model, 'commentaire')->textArea(['rows' => '3']) ?>
				
			</div>
		</div>
		<div class="modal-footer">
			 <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name' => 'objectif']) ?>
   
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
