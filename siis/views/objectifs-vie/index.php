<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrObjectifsVie */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Objectifs Vies');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objectifs-vie-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Objectifs Vie'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'famille',
            'objectif',
            'niveau',
            'date_objectif',
            // 'commentaire',
            // 'createdBy',
            // 'createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
