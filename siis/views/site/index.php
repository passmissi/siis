<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'SIIS Portal';
?>
  <div class="masthead">
    <div class="container">
       <div id="masthead-carousel" class="carousel masthead-carousel slide">
       		<div class="carousel-inner">
       		
       			<div class="item active">
	              <div class="row">
	                <div class="col-md-6 masthead-text animated fadeInDownBig">
	                  <h1 class="masthead-title">KORE FANMI - PASMISSI.</h1>
	                  <p class="masthead-">
	                  	Sous la direction du Gouvernement d'Haiti, Kore Fanmi est le r&eacute;sultat d'une proche 
	                  	collaboration entre de nombreux acteurs (Banque Mondiale, Agences ONU et Organisations 
	                  	Non-Gouvernementales) visant &#224; harmoniser et am&eacute;liorer la provision de services de base.
	                  </p>
	                  <br>
	                  <div class="masthead-actions">
						
	                    <?php
	                    echo Html::a('Voir les rapports', ['/rapport/index'], ['class' => 'btn btn-transparent btn-jumbo']);
	                    ?>
	                    <?php
	                    echo Html::a('Se connecter', ['auth/default/login'], ['class' => 'btn btn-primary btn-jumbo']);
	                    ?>
	                    
	                  </div> <!-- /.masthead-actions -->
	                </div> <!-- /.masthead-text -->
	
	                <div class="col-md-6 masthead-img animated fadeInUpBig">
	                  <img src="img/photo.jpg.png" alt="slide2" class="img-responsive">     
	                </div> <!-- /.masthead-img -->
	              </div> <!-- /.row -->
	            </div> <!-- /.item -->
       		</div>
       		
       		 <div class="carousel-controls">
		        <a class="carousel-control left" href="#masthead-carousel" data-slide="prev"><span class="fa  fa-caret-square-o-left"></span></a>
		        <a class="carousel-control right" href="#masthead-carousel" data-slide="next"><span class="fa fa-caret-square-o-right"></span></a>
		     </div>
       </div>
    </div>
  </div>
  
  
  <div class="content">
      <div class="container">
      	<h4 class="content-title">
          <span>Principes Directeurs</span>
        </h4>
        <br>
        
        <div class="row">
        	 <div class="mini-feature">
	            <i class="fa fa-cloud-upload mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Gestion de l'information</h5>            
	              <p>Kore Fanmi repose sur une connaissance d&eacute;taill&eacute;e des conditions et besoins des familles (demande) 
                     et des services et programmes disponibles dans la zone d'intervention (offre). </p>
	            </div>
	          </div> <!-- /.mini-feature -->
	
	          <div class="mini-feature">
	            <i class="fa fa-puzzle-piece mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Renforcement Institutionnel</h5>
	              <p>Kore Fanmi vise &#224; am&eacute;liorer les capacit&eacute;s des administrations publiques &#224; &eacute;laborer, g&eacute;rer et 
                     superviser l'offre de services de protection sociale. L'appui se fait au niveau national et communal. </p>
	            </div>
	          </div> <!-- /.mini-feature -->
	
	          <div class="mini-feature">
	            <i class="fa fa-refresh mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Approche centr&eacute;e sur la famille</h5>
	              <p>La famille dans son individualit&eacute; est au centre de Kore Fanmi et en est l'acteur principal.
                     Kore Fanmi cr&eacute;e une relation claire et une responsabilit&eacute; bien d&eacute;finie entre chaque famille 
                     et son ACP et TS.</p>
	            </div>
	          </div> <!-- /.mini-feature -->
        
        
        </div>
        
        <div class="row">
        	<div class="mini-feature">
	            <i class="fa fa-link mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Adaptation du service aux besoins des familles</h5>            
	              <p>Kore Fanmi, au travers de sa strat&eacute;gie int&eacute;gr&eacute;e et centr&eacute;e sur la famille, vise &#224; adapter les 
					 services de protection sociale en fonction du degr&eacute; de pauvret&eacute; et de vuln&eacute;rabilit&eacute; de 
					 chaque famille.</p>
	            </div>
	          </div> <!-- /.mini-feature -->
	
	          <div class="mini-feature">
	            <i class="fa fa-arrows-alt mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Polyvalence</h5>
	              <p>Kore Fanmi est con&#231;ue pour prendre en compte l'int&eacute;gralit&eacute; des droits de la famille dans ses 
					 domaines sociaux essentiels.</p>
	            </div>
	          </div> <!-- /.mini-feature -->
	
	          <div class="mini-feature">
	            <i class="fa fa-credit-card mini-feature-icon text-secondary"></i>
	            <div class="mini-feature-text">
	              <h5 class="mini-feature-title">Paiement bas&eacute; sur la performance</h5>
	              <p>Kore Fanmi pr&eacute;voit un syst&#232;me de paiement bas&eacute; sur les performances autour d'indicateurs 
					 clairement d&eacute;finis ainsi que des crit&#232;res de satisfaction des familles.
				     .</p>
	            </div>
	          </div> <!-- /.mini-feature -->
        </div>
      </div>
  </div>