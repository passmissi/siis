<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrFamille */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="famille-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'localite') ?>

    <?= $form->field($model, 'commune') ?>

    <?= $form->field($model, 'section_communale') ?>

    <?= $form->field($model, 'type_milieu') ?>

    <?php // echo $form->field($model, 'ID_HH_A5_1') ?>

    <?php // echo $form->field($model, 'ID_HH_A5_2') ?>

    <?php // echo $form->field($model, 'ID_HH_A5_3') ?>

    <?php // echo $form->field($model, 'telephone_maison') ?>

    <?php // echo $form->field($model, 'ID_HH_A8') ?>

    <?php // echo $form->field($model, 'ID_HH_A8_3') ?>

    <?php // echo $form->field($model, 'ID_HH_A8_4') ?>

    <?php // echo $form->field($model, 'ID_HH_A8_5') ?>

    <?php // echo $form->field($model, 'ID_HH_A9') ?>

    <?php // echo $form->field($model, 'precision_gps') ?>

    <?php // echo $form->field($model, 'altitude_gps') ?>

    <?php // echo $form->field($model, 'latitude_gps') ?>

    <?php // echo $form->field($model, 'longitude_gps') ?>

    <?php // echo $form->field($model, 'ID_HH_NO_NON_RESYDAN') ?>

    <?php // echo $form->field($model, 'ID_HH_NO_RESYDAN') ?>

    <?php // echo $form->field($model, 'ID_HH_MANM_HH_TOTAL') ?>

    <?php // echo $form->field($model, 'MANM_HH1_D6_1') ?>

    <?php // echo $form->field($model, 'MANM_HH1_MALAD_QUE_CRON') ?>

    <?php // echo $form->field($model, 'MANM_HH1_MALAD_NO_CRON') ?>

    <?php // echo $form->field($model, 'MANM_HH1_MALAD_QUE_MWA') ?>

    <?php // echo $form->field($model, 'MANM_HH1_MALAD_NO_MWA') ?>

    <?php // echo $form->field($model, 'MANM_HH1_NO_MOURI') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F2') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_2') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_3') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_4') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_5') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_6') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_7') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_8') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_9') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_10') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_11') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_12') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_13') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_14') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_15') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_16') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_17') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F3_F3_18') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F4') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_2') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_3') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_4') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_5') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_6') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F5_F5_7') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F6') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F7') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F8_1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F8_2') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F9') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F9_1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F10') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F11') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F12') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F12_0') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F13') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F14') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F15') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F15_0') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F16') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F17') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F18') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F19') ?>

    <?php // echo $form->field($model, 'BIENS_HH_F20') ?>

    <?php // echo $form->field($model, 'BIENS_HH_G1') ?>

    <?php // echo $form->field($model, 'BIENS_HH_G2') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H1') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H2') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H3') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H4') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H5') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H6') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H7') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H8') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H9') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H10') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H11') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H12') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H13') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H14') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H15') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H16') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H17') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H18') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H19') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H20') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H21') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H22') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH1_H23') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H1_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H2_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H3_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H4_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H5_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H6_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H7_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H8_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H9_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H10_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H11_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H12_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H13_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H14_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H15_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H16_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H17_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H18_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H19_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H20_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H21_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H22_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH2_H23_B') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H24') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H25') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H26') ?>

    <?php // echo $form->field($model, 'H24') ?>

    <?php // echo $form->field($model, 'H25') ?>

    <?php // echo $form->field($model, 'H26') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H27') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H28') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H29') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H30') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H31') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H32') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H33') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H34') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H35') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H36') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H37') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H38') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H39') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H40') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H41') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H42') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H43') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H44') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H45') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH3_H46') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H47') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H48') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H49') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H50') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H51') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H52') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H53') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H54') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H55') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H56') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H57') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H58') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H59') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H60') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H61') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H62') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H63') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H64') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H65') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H66') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H67') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H68') ?>

    <?php // echo $form->field($model, 'SEKALIMHHSEKALIMHH4_H69') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I1') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I2') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I3') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I4') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I5_1') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I5_2') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I6') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I7') ?>

    <?php // echo $form->field($model, 'SEVIS_KONPOTMAN_HH_I8') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
