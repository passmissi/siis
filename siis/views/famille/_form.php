<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Famille */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="famille-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'localite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'commune')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'section_communale')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_milieu')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A5_1')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A5_2')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_HH_A5_3')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone_maison')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A8')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_HH_A8_3')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A8_4')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A8_5')->textInput() ?>

    <?= $form->field($model, 'ID_HH_A9')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'precision_gps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'altitude_gps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latitude_gps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'longitude_gps')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ID_HH_NO_NON_RESYDAN')->textInput() ?>

    <?= $form->field($model, 'ID_HH_NO_RESYDAN')->textInput() ?>

    <?= $form->field($model, 'ID_HH_MANM_HH_TOTAL')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_D6_1')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_MALAD_QUE_CRON')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_MALAD_NO_CRON')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_MALAD_QUE_MWA')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_MALAD_NO_MWA')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_NO_MOURI')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F2')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_2')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_3')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_4')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_5')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_6')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_7')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_8')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_9')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_10')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_11')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_12')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_13')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_14')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_15')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_16')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_17')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F3_F3_18')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F4')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_2')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_3')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_4')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_5')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_6')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F5_F5_7')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F6')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F7')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F8_1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F8_2')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F9')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F9_1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F10')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F11')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F12')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F12_0')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F13')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F14')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F15')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F15_0')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F16')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F17')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F18')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F19')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_F20')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_G1')->textInput() ?>

    <?= $form->field($model, 'BIENS_HH_G2')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H1')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H2')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H3')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H4')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H5')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H6')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H7')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H8')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H9')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H10')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H11')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H12')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H13')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H14')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H15')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H16')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H17')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H18')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H19')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H20')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H21')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H22')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH1_H23')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H1_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H2_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H3_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H4_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H5_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H6_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H7_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H8_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H9_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H10_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H11_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H12_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H13_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H14_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H15_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H16_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H17_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H18_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H19_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H20_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H21_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H22_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH2_H23_B')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H24')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H25')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H26')->textInput() ?>

    <?= $form->field($model, 'H24')->textInput() ?>

    <?= $form->field($model, 'H25')->textInput() ?>

    <?= $form->field($model, 'H26')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H27')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H28')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H29')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H30')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H31')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H32')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H33')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H34')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H35')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H36')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H37')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H38')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H39')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H40')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H41')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H42')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H43')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H44')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H45')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH3_H46')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H47')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H48')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H49')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H50')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H51')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H52')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H53')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H54')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H55')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H56')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H57')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H58')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H59')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H60')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H61')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H62')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H63')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H64')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H65')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H66')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H67')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H68')->textInput() ?>

    <?= $form->field($model, 'SEKALIMHHSEKALIMHH4_H69')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I1')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I2')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I3')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I4')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I5_1')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I5_2')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I6')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I7')->textInput() ?>

    <?= $form->field($model, 'SEVIS_KONPOTMAN_HH_I8')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
