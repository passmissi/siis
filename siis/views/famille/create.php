<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Famille */

$this->title = Yii::t('app', 'Create Famille');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Familles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="famille-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
