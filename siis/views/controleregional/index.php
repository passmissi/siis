<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use app\models\User;
use app\models\Config;
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Plans;
use app\models\Communes;
use app\models\Activites;
use app\models\SousActivites;
use app\models\ScrMonitoringRegional;
use app\models\Membre;

$this->title = 'Dashboard';

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);


$plan = new Plans();
$time = new \DateTime('now', new \DateTimeZone('UTC'));
$mois_int = date_format($time,"n");
$mois = $plan->getNumberMonth($mois_int);
$annee_int = date('Y');

$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
$titles = '';
foreach($data as $row) {
	$titles .= $row['valeur'] . ', ';
}
$datax = ''.rtrim($titles, ', ').'';
$datay = '('.preg_replace("/'/", '', $datax).')';

/* $searchModel2 = new ScrMonitoringRegional();
$statszone2 = $searchModel2->getStatszone($datay,$searchModel->mois,$searchModel->annee);
echo $statszone2;
 */
$listAKF = ArrayHelper::map(Operateurs::find()
		->where('commune_lie IN('.$datax.')')
		->andWhere(['fonction' => 'AKF'])
		->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'];
		});

if($searchModel->mois != '')$mois=$searchModel->mois;
if($searchModel->annee != '')$annee_int=$searchModel->annee; 

$script5 = <<< JS
$(function () {
    // Create the chart
    
    $('#container').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Statistics Zone - PLANS. $mois, $annee_int'
        },
        subtitle: {
            text: 'Click the slices to view versions.'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },
        series: [{
            name: 'Stats Plans',
            colorByPoint: true,
            data: $statszone
        }],
        drilldown: {
            series: $statszone2
        }
    });
            		
    $('#container2').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Statistics Zone - PLANS - SUIVIS. $mois, $annee_int'
        },
        subtitle: {
            text: 'Click the slices to view versions.'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },
        series: [{
            name: 'Stats Suivis',
            colorByPoint: true,
            data: $statszone11
        }],
        drilldown: {
            series: $statszone21
        }
    });
            		
    $('#container3').highcharts({
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Statistics Zone - VACCINATION. $mois, $annee_int'
        },
        subtitle: {
            text: 'Click the slices to view versions.'
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
        },
        series: [{
            name: 'Stats Vaccination',
            colorByPoint: true,
            data: $statszone31
        }],
        drilldown: {
            series: []
        }
    });
});
JS;
$this->registerJs($script5, \yii\web\View::POS_END);

$script2 = <<< JS
function showUser2(str) {
	if(str == ''){
	}
	else{
		if(str == 'plans'){
			$('.nav-tabs a[href="#tab_1-1"]').tab('show');
		}else if(str == 'suivis'){
			$('.nav-tabs a[href="#tab_2-2"]').tab('show');
		}else if(str == 'vaccination'){
			$('.nav-tabs a[href="#tab_3-2"]').tab('show');
		}
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script3 = <<< JS
	$(document).ready(function(){
		$('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
			var currentTab = $(e.target).text(); // get current tab
			
			if(currentTab == 'Plans'){
				document.getElementById("scrmonitoringregional-type").selectedIndex = "1";
			}else if(currentTab == 'Suivis'){
				document.getElementById("scrmonitoringregional-type").selectedIndex = "2";
			}else if(currentTab == 'Vaccination'){
				document.getElementById("scrmonitoringregional-type").selectedIndex = "3";
			}
		});
	});
JS;
$this->registerJs($script3, \yii\web\View::POS_READY);
?>
<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion Nationale</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Plans</span>
              <span class="info-box-number"><?= $stats_array[4] ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo round($stats_array[0]).'%' ?>"></div>
              </div>
                  <span class="progress-description">
                    <?php echo round($stats_array[0]).'%' ?> plans suivis
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-paste"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Suivis</span>
              <span class="info-box-number"><?= $stats_array[5] ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: <?php echo round($stats_array[1]).'%' ?>"></div>
              </div>
                  <span class="progress-description">
                    <?php echo round($stats_array[1]).'%' ?> suivis termines
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Naissance</span>
              <span class="info-box-number"><?= $stats_array[2] ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Evenements
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="glyphicon glyphicon-bed"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Grossesse</span>
              <span class="info-box-number"><?= $stats_array[3] ?></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Evenements
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
	</div>

	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<?php $form = ActiveForm::begin([
								'options' => ['class' => 'form-inline'],
							]); ?>
								<div class="input-group">
									<div class="input-group-btn">
										<?= Html::submitButton('<i class="glyphicon glyphicon-book"></i>', ['class' => 'btn btn-md btn-success', 
						    		 		'target' => '_blank', 'name' => 'famille']) ?>
							        </div>
							            <!-- /btn-group -->
							            <?= $form->field($model1, 'id')->label(false)
										     ->dropDownList($listAKF, ['style'=>'width:100%', 'class' => 'form-control input-md','prompt' => 'Choisir AKF']) ?>
								</div>
							<?php ActiveForm::end(); ?>
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-stats"></i> Stats Zone', ['/controleregional/index']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="fa fa-cloud-download"></i> Data Exporting', ['/controleregional/dataexporting']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="glyphicon glyphicon-level-up"></i> Donnees AKF (DIMAGI)', ['/controleregional/indicateurs']); '.</li>';
                                       
                                       
                                   ?>
                               </ul>
                           </div>
                           
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    		<?php $form = ActiveForm::begin([
									    'action' => ['index'],
									    'method' => 'get',
										'options' => ['class' => 'form-inline'],
									]); ?>
                    			<div class="col-sm-4">
                    				<?= $form->field($searchModel, 'type')->dropDownList(
										['plans'=>'plans','suivis'=>'suivis', 'vaccination'=>'vaccination'], ['prompt' => 'Choisir un type','class'=>'form-control input-sm','onchange' => 'showUser2(this.value)'])->label(false)
									?>
                                </div>
                                <div class="col-sm-8 pull-right">
                                	
									    <div class="input-group">
									       <?= $form->field($searchModel, 'ts')->dropDownList(
											   $listAKF, ['prompt' => 'Choisir un TS','class'=>'form-control input-sm'])->label(false, ['id' => 'type_activite'])
									       ?>
									       <span class="input-group-btn" style="width:0px;"></span>
									       <?= $form->field($searchModel, 'mois')->dropDownList(
											  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
								                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
								                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
								                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm'])->label(false)
									      ?>
									      <span class="input-group-btn" style="width:0px;"></span>
									      <?= $form->field($searchModel,'annee')
								          	 ->dropDownList(
										       ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
								                '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','class'=>'form-control input-sm']
										   )->label(false)?>
										  <span class="input-group-btn" style="width:0px;"></span>
										  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
										</div>
								<?php ActiveForm::end(); ?>
                                </div>
						    </div>
                    		
                    		<div class="nav-tabs-custom">
                    			<ul class="nav nav-tabs pull-right">
					              <li class="active"><a aria-expanded="true" href="#tab_1-1" data-toggle="tab">Plans</a></li>
					              <li class=""><a aria-expanded="false" href="#tab_2-2" data-toggle="tab">Suivis</a></li>
					              <li class=""><a aria-expanded="false" href="#tab_3-2" data-toggle="tab">Vaccination</a></li>
					              <li class="pull-left header"><i class="glyphicon glyphicon-stats"></i> Statistics Zone</li>
					            </ul>
					            
					            <div class="tab-content">
					             	<div class="tab-pane active" id="tab_1-1">
					             		<div class="row">
					             			<div class="col-md-4">
					             				<div class="table-responsive no-padding">
					             			<?php 
						             			echo	GridView::widget([
						             					'dataProvider' => $dataProvider,
						             					//'filterModel' => $searchModel,
						             					'export' => false,
						             					'showPageSummary' => false,
						             					'bordered' => false,
						             					'responsive' => true,
						             					'hover' => true,
						             					'rowOptions' => function($model){
						             						$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             								'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             			
						             						if($count <= 0){
						             							return ['class' => 'warning',];
						             						}
						             						elseif($count > 0){
						             							return ['class' => 'success'];
						             						}
						             					}, 
						             					'columns' => [
						             						/*['class' => 'yii\grid\SerialColumn'],
						             						  'id', 
						             						 'mois',
						             						 'annee',*/
						             							[
						             							'header'=>'Periode',
						             							'value'=>function ($model, $key, $index, $widget) {
						             								return $model->mois.' '.$model->annee;
						             								/* $Op = Communes::findOne($model->commune);
						             								if($Op!=null)
						             									return $Op->n_communal;
						             								else
						             									return null; */
						             							},
						             							],
						             						  /* [
						             							'attribute'=>'commune',
						             							'header'=>'Commune',
						             							'value'=>function ($model, $key, $index, $widget) {
						             								$Op = Communes::findOne($model->commune);
						             								if($Op!=null)
						             									return $Op->n_communal;
						             								else
						             									return null;
						             							},
						             						  ], */
						             						  [
						             							'attribute'=>'akf',
						             							'header'=>'AKF',
						             							'value'=>function ($model, $key, $index, $widget) {
						             								$Op = Operateurs::findOne($model->akf);
						             								if($Op!=null)
						             									return $Op->prenom[0].'. '.$Op->nom;
						             								else
						             									return null;
						             							},
						             						  ],
						             						  /* [
							             						  'attribute'=>'ACT',
							             						  'header'=>'Nbre ACT',
							             						  'value'=>function ($model, $key, $index, $widget) {
							             						  	$Op = Activites::find()->where(['id_plan' => $model->id])->count();
							             						  	if($Op > 0)
							             						  		return $Op.' Activites';
							             						  	else
							             						  		return null;
							             						  },
						             						  ],
						             						  [
							             						  'attribute'=>'Sous ACT',
							             						  'header'=>'Nbre Sous ACT',
							             						  'value'=>function ($model, $key, $index, $widget) {
							             						  	$Op = SousActivites::find()->where(['id_plan' => $model->id])->count();
							             						  	if($Op > 0)
							             						  		return $Op.' Sous-Activites';
							             						  	else
							             						  		return null;
							             						  },
						             						  ], */
						             						  [
						             									'header'=>'Suivi',
						             									'format'=>'raw',
						             									'value'=> function ($model, $key, $index, $widget) {
						             										$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             												'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             										if($count <= 0){
						             											return '<span class="label label-warning"><i class="fa fa-minus"></i></span>';
						             										}
						             										else{ return '<span class="label label-success"><i class="fa fa-check"></span>';}
						             											
						             									},
						             						   ],
						             					],
						             				]);
					             			?>
					             		</div>
					             			</div>
					             			<div class="col-md-8">
					             				<div id="container" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto">
					             				</div>
					             			</div>
					             		</div>
					             	</div>
					             	
					             	<div class="tab-pane" id="tab_2-2">
					             		<div class="row">
					             			<div class="col-md-4">
					             				<div class="table-responsive no-padding">
					             					<?php
						             					echo	GridView::widget([
						             							'dataProvider' => $dataProvider2,
						             							'export' => false,
						             							'showPageSummary' => false,
						             							'bordered' => false,
						             							'responsive' => true,
						             							'hover' => true,
						             							'rowOptions' => function($model){
						             								$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             										'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             					
						             								if($count <= 0){
						             									return ['class' => 'warning',];
						             								}
						             								elseif($count > 0){
						             									return ['class' => 'success'];
						             								}
						             							},
						             							'columns' => [
						             									[
						             										'header'=>'Periode',
						             										'value'=>function ($model, $key, $index, $widget) {
						             											return $model->mois.' '.$model->annee;},
						             									],
						             									[
						             										'attribute'=>'akf',
						             										'header'=>'AKF',
						             										'value'=>function ($model, $key, $index, $widget) {
						             												$Op = Operateurs::findOne($model->akf);
						             												if($Op!=null)
						             													return $Op->prenom[0].'. '.$Op->nom;
						             												else
						             													return null;},
						             									],
						             									[
						             										'header'=>'Suivi',
						             										'format'=>'raw',
						             										'value'=> function ($model, $key, $index, $widget) {
						             												$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             														'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             												if($count <= 0){
						             													return '<span class="label label-warning"><i class="fa fa-minus"></i></span>';
						             												}
						             												else{ return '<span class="label label-success"><i class="fa fa-check"></span>';}},
						             									],
						             							],
						             						]);
					             					?>
					             				</div>
					             			</div>
					             			<div class="col-md-8">
					             				<div id="container2" style="width: auto;height: 400px; margin: 0 auto">
					             				</div>
					             			</div>
					             		</div>
					             	</div>
					             	
					             	<div class="tab-pane" id="tab_3-2">
					             		
					             		<div class="row">
					             			<div class="col-md-4">
					             				<div class="table-responsive no-padding">
					             					<?php
						             					echo	GridView::widget([
						             							'dataProvider' => $dataProvider3,
						             							'export' => false,
						             							'showPageSummary' => false,
						             							'bordered' => false,
						             							'responsive' => true,
						             							'hover' => true,
						             							/* 'rowOptions' => function($model){
						             								$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             										'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             					
						             								if($count <= 0){
						             									return ['class' => 'warning',];
						             								}
						             								elseif($count > 0){
						             									return ['class' => 'success'];
						             								}
						             							}, */
						             							'columns' => [
						             									'id',
						             									 [
						             										'header'=>'id_membre',
						             										'value'=>function ($model, $key, $index, $widget) {
						             											$mem = Membre::find()->where(['famille' => $model->id_famille, 'memberid' => $model->id_membre])->one();
						             											if($mem!=null)
						             													return $mem->prenom[0].'. '.$mem->nom;
						             												else
						             													return "---";},
						             									],
						             									[
						             										'attribute'=>'vaccin',
						             										'header'=>'vaccin',
						             										'value'=>function ($model, $key, $index, $widget) {
						             												switch ($model->vaccin) {
						             													case 1:
						             														return "Polio 0";
						             														break;
						             													case 2:
						             														return "Polio 1";
						             														break;
						             													case 3:
						             														return "Polio 2";
						             														break;
						             													case 4:
						             														return "Polio 3";
						             														break;
						             													case 5:
						             														return "Polio 4";
						             														break;
						             													case 6:
						             														return "BCG";
						             														break;
						             													case 7:
						             														return "DTP 1";
						             														break;
						             													case 8:
						             														return "DTP 2";
						             														break;
						             													case 9:
						             														return "DTP 3";
						             														break;
						             													case 10:
						             														return "Woujol Ribeyol";
						             														break;
						             													case 11:
						             														return "DT";
						             														break;
						             													case 12:
						             														return "Pentavalan";
						             														break;
						             													case 13:
						             														return "Completement Vaccine";
						             														break;
						             													default:
						             														echo "N/A";
						             												}
						             										},
						             									]
						             									/*[
						             										'header'=>'Suivi',
						             										'format'=>'raw',
						             										'value'=> function ($model, $key, $index, $widget) {
						             												$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
						             														'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
						             												if($count <= 0){
						             													return '<span class="label label-warning"><i class="fa fa-minus"></i></span>';
						             												}
						             												else{ return '<span class="label label-success"><i class="fa fa-check"></span>';}},
						             									], */
						             							],
						             						]);
					             					?>
					             				</div>
					             			</div>
					             			<div class="col-md-8">
					             				<div id="container3" style="width: auto;height: 400px; margin: 0 auto">
					             				</div>
					             			</div>
					             		</div>
					             	</div>
					            </div>
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>