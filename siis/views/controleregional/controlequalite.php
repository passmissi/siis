<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Config;
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Communes;
use app\models\ScrMonitoringRegional;
use app\models\Membre;
use app\models\Plans;
use app\models\TypeActivites;
use app\models\SousActivites;

$this->title = 'Controle Qualite';

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$plan = new Plans();
$time = new \DateTime('now', new \DateTimeZone('UTC'));
$mois_int = date_format($time,"n");
$mois = $plan->getNumberMonth($mois_int);
$annee_int = date('Y');

$data = Config::find()->where(['operateur' => $operateur->id, 'cle' => 'COMM', 'isValid' => 1])->asArray()->all();
$datax = $plan->getQuerysplit($data, 'valeur');

$listTS = ArrayHelper::map(Operateurs::find()
		->where('commune_lie IN'.$datax.'')
		->andWhere(['fonction' => 'TS'])
		->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$listAKF = ArrayHelper::map(Operateurs::find()
		->where('commune_lie IN'.$datax.'')
		->andWhere(['fonction' => 'AKF'])
		->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$script2 = <<< JS
function showUser2(str) {
	if(str == ''){
		document.getElementById("plans-annee").disabled = true;
		document.getElementById("plans-ts").disabled = true;
		document.getElementById("plans-akf").disabled = true;
	}
	else{
		document.getElementById("plans-annee").disabled = false;
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script3 = <<< JS
function showUser3(str) {
	if(str == ''){
		document.getElementById("plans-ts").disabled = true;
		document.getElementById("plans-akf").disabled = true;
	}
	else{
		document.getElementById("plans-ts").disabled = false;
	}
}
JS;
$this->registerJs($script3, \yii\web\View::POS_END);

$script4 = <<< JS
function showUser4(str) {
	if(str == ''){
		document.getElementById("plans-akf").disabled = true;
		document.getElementById("recherche").disabled = true;
		document.getElementById("valider").disabled = true;
	}
	else{
		document.getElementById("plans-akf").disabled = false;
		document.getElementById("recherche").disabled = false;
		document.getElementById("valider").disabled = false;
		
		$.ajax({
			type: 'GET',
			url: 'index.php?r=controleregional/listakfbyts',
			data: { ts : str},
			success: function(data){
				$("#plans-akf").html(data);
			},
			error: function() {
			},
		});
	}
}
JS;
$this->registerJs($script4, \yii\web\View::POS_END);


$script5 = <<< JS
$(function () {
    // Create the chart
    var etat = $etat;
	if(etat = 1){
		document.getElementById("plans-annee").disabled = false;
		document.getElementById("plans-ts").disabled = false;
		document.getElementById("plans-akf").disabled = false;
    	document.getElementById("valider").disabled = false;
    }else{
    	
	}
});
JS;
$this->registerJs($script5, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Controle Qualite
       <small>Centre du Controle de Qualite</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Controle Qualite</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i> Plans', ['']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="fa fa-ellipsis-v"></i> Sous-Activites', [''],['disabled' => true,]); '.</li>';
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    			<?php $form = ActiveForm::begin(['options' => ['class' => 'form-inline'], 'enableAjaxValidation' => false]); ?>
                    				
                    				<div class="col-sm-12">
                    					<div class="input-group">
									       <?= $form->field($model, 'mois')->dropDownList(
											  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
								                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
								                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
								                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm', 'onchange' => 'showUser2(this.value)'])->label(false)
									      ?>
									      <span class="input-group-btn" style="width:0px;"></span>
									      <?= $form->field($model,'annee')
								          	 ->dropDownList(
										       ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
								                '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','class'=>'form-control input-sm','disabled' => true,'onchange' => 'showUser3(this.value)']
										   )->label(false)?>
										  <span class="input-group-btn" style="width:0px;"></span>
										  <?= $form->field($model, 'ts')->dropDownList(
											   $listTS, ['prompt' => 'Choisir un TS','class'=>'form-control input-sm','disabled' => true,'onchange' => 'showUser4(this.value)'])->label(false, ['id' => 'type_activite'])
									       ?>
									       <span class="input-group-btn" style="width:0px;"></span>
									       <?= $form->field($model, 'akf')->dropDownList(
											   [], ['prompt' => 'Choisir un AKF','class'=>'form-control input-sm','disabled' => true])->label(false, ['id' => 'type_activite'])
									       ?>
									       <span class="input-group-btn" style="width:0px;"></span>
										 <div class="btn-group">
										 	<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary','disabled' => true, 'id' => 'recherche']) ?>
										  <?php ActiveForm::end(); ?>
										  <?php 
										  	  $mois = $model->mois;
										  	  $annee = $model->annee;
										  	  $ts = $model->ts;
										  	  $akf = $model->akf;
										  	  if($etat == 1){
										  	  	echo Html::a('Verification',
										  	  			['/controleregional/controlequalitesa', 'mois'=>$mois, 'annee'=>$annee,'ts'=>$ts,'akf'=>$akf],
										  	  			['class' => 'btn btn-sm btn-warning','name' => 'valider','id' => 'valider']);
										  	  }
										  	  else{
										  	  	echo Html::a('Verification',
										  	  			['/controleregional/controlequalitesa', 'mois'=>$mois, 'annee'=>$annee,'ts'=>$ts,'akf'=>$akf],
										  	  			['class' => 'btn btn-sm btn-warning','name' => 'valider','disabled' => true,'id' => 'valider']);
										  	  }
										  ?>
										 </div>
										  
										</div>
                    				</div>
                    			
                    		</div>
                    		
                    		<div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Plans 
                    	    			<?php 
                    	    				if($etat == 1){
                    	    					$personne = Operateurs::findOne($model->akf);
                    	    					$result = $model->mois.'-'.$model->annee.' de '.$personne->nom.' '.$personne->prenom;
                    	    					echo $result;
                    	    				}
                    	    			?>
                    	    		</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php 
			                    	echo	GridView::widget([
			                    			'dataProvider' => $dataProvider,
			                    			'export' => false,
			                    			'showPageSummary' => false,
			                    			'bordered' => false,
			                    			'responsive' => true,
			                    			'hover' => true,
			                    			'columns' => [
			                    				['class' => 'yii\grid\SerialColumn'],
			                    				'plan',
			                    				[
			                    					'attribute'=>'type_activite',
			                    					'header'=>'Type',
			                    					'value'=>function ($model, $key, $index, $widget) {
			                    						$Op = TypeActivites::findOne($model->type_activite);
			                    						if($Op!=null)
			                    							return $Op->libelle;
			                    						else
			                    							return null;
			                    					},
			                    			    ],
			                    			    [
			                    			    'header'=>'Promotion',
			                    			    'format'=>'raw',
			                    			    'value'=>function ($model, $key, $index, $widget) {
			                    			    	$Op = SousActivites::find()->where(['id_activite'=>$model->activites, 'id_type_sous_activite'=>1])->one();
			                    			    	if($Op!=null)
			                    			    		return '<span class="label label-success"><i class="fa fa-check"></span>';
			                    			    	else
			                    			    		return '-';
			                    			    },
			                    			    ],
			                    			    [
			                    			    'header'=>'Distribution',
			                    			    'format'=>'raw',
			                    			    'value'=>function ($model, $key, $index, $widget) {
			                    			    	$Op = SousActivites::find()->where(['id_activite'=>$model->activites, 'id_type_sous_activite'=>2])->one();
			                    			    	if($Op!=null)
			                    			    		return '<span class="label label-success"><i class="fa fa-check"></span>';
			                    			    	else
			                    			    		return '-';
			                    			    },
			                    			    ],
			                    			    [
			                    			    'header'=>'Reference',
			                    			    'format'=>'raw',
			                    			    'value'=>function ($model, $key, $index, $widget) {
			                    			    	$Op = SousActivites::find()->where(['id_activite'=>$model->activites, 'id_type_sous_activite'=>3])->one();
			                    			    	if($Op!=null)
			                    			    		return '<span class="label label-success"><i class="fa fa-check"></span>';
			                    			    	else
			                    			    		return '-';
			                    			    },
			                    			    ],
			                    			    [
			                    			    'header'=>'Formation',
			                    			    'format'=>'raw',
			                    			    'value'=>function ($model, $key, $index, $widget) {
			                    			    	$Op = SousActivites::find()->where(['id_activite'=>$model->activites, 'id_type_sous_activite'=>4])->one();
			                    			    	if($Op!=null)
			                    			    		return '<span class="label label-success"><i class="fa fa-check"></span>';
			                    			    	else
			                    			    		return '-';
			                    			    },
			                    			    ],
			                    			    [
			                    			    'header'=>'Vacc et Controle Poids',
			                    			    'format'=>'raw',
			                    			    'value'=>function ($model, $key, $index, $widget) {
			                    			    	$Op = SousActivites::find()->where(['id_activite'=>$model->activites])->andWhere('id_type_sous_activite IN (6,7)')->one();
			                    			    	if($Op!=null)
			                    			    		return '<span class="label label-success"><i class="fa fa-check"></span>';
			                    			    	else
			                    			    		return '-';
			                    			    },
			                    			    ],
			                    			],
			                    		]);
			                    	?>
			                    </div>
			                </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>
