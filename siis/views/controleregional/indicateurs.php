<?php

use yii\helpers\Html;
/* use app\models\User;
 use app\models\Config; */
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
 use app\models\Plans;
 /*use app\models\Communes;
 use app\models\Activites;
 use app\models\SousActivites; */
use app\models\ScrMonitoringRegional;

$this->title = 'Indicateurs';

$plan = new Plans();
$time = new \DateTime('now', new \DateTimeZone('UTC'));
$mois_int = date_format($time,"n");
$mois = $plan->getNumberMonth($mois_int);
$annee_int = date('Y');

$indicateurs = array();
$indicateurs = ['1'=>'Enfants de moins de un an ayant recu 3 doses de Penta 3',
		'2'=>'Enfants de 6-59 mois ayant recu deux doses de vitamine A', 
		'3'=>'Enfants vus dans un programme de controle de croissance',
		'4'=>'Enfants de 6-59 atteints de malnutrition',
		'5'=>'Enfants de moins de moins de 5 ans ayant un acte de naissance',
		'6'=>'Famille qui utilisent les latrines',
		'7'=>'Nombre total de famille',
		'8'=>'Enfants enregistres pendant les mois qui suivent la naissance'];
?>

<section class="content-header">
	<h1>
       Indicateurs
       <small>Data <?php echo $model->mois.' '.$model->annee?></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Indicateurs</li>
    </ol>
</section>

<section class="content">
	<div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Bookmarks</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Events</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Comments</span>
              <span class="info-box-number">41,410</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i> Stats Zone', ['/controleregional/index']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="fa fa-cloud-download"></i> Data Exporting', ['/controleregional/dataexporting', ]); '.</li>';
                                       echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-level-up"></i> Indicateurs', ['/controleregional/indicateurs']); '.</li>';
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    			<?php $form = ActiveForm::begin([
                    					'options' => ['class' => 'form-inline'],
								    ]); ?>
								
                    			<div class="col-md-12">
								    <div class="input-group">
								    	<?= $form->field($model, 'mois')->dropDownList(
											  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
								                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
								                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
								                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm','onchange' =>'showUser2(this.value);'])->label(false) ?>
								    	<span class="input-group-btn" style="width:0px;"></span>
								    	<?= $form->field($model, 'annee')->dropDownList(
										       ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
								                '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','class'=>'form-control input-sm']
										   )->label(false) ?>
	      								<span class="input-group-btn" style="width:0px;"></span>
	      								<?= $form->field($model, 'communes')->dropDownList(
										       ['642'=>'Anse-a-Pitres','233'=>'Thiotte', '232'=>'Grand Gosier'], ['prompt'=>'--Choisir une commune --','class'=>'form-control input-sm']
										   )->label(false) ?>
	      								<span class="input-group-btn" style="width:0px;"></span>
	      								<?= $form->field($model, 'indicateurs2')->dropDownList(
										       $indicateurs, ['prompt'=>'--Choisir un indicateur --','class'=>'form-control input-sm'
										       		, 'style' => 'width: 200px !important; min-width: 200px; max-width: 200px;'
	      								]
										   )->label(false) ?>
	      								<span class="input-group-btn" style="width:0px;"></span>
								    	<div class="btn-group">
								          <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary', 'name' => 'indicateurs','id' => 'indicateurs']) ?>
			                    		</div>
			                    <?php ActiveForm::end(); ?>  
                    	          </div>
                               </div>
              			 	</div>
              			 	
              			 	<div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Donnees des indicateurs <?php echo $model->mois.' '.$model->annee?></h3>
                    	    	
                    	    		<div class="box-tools pull-right">
						                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						                </button>
						                <div class="btn-group">
						                  <button aria-expanded="true" type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
						                    <i class="fa fa-wrench"></i></button>
						                  <ul class="dropdown-menu" role="menu">
						                    <li><a href="#">Export</a></li>
						                    <li class="divider"></li>
						                    <li><a href="#">CSV</a></li>
						                    <li><a href="#">PDF</a></li>
						                  </ul>
						                </div>
						            </div>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	<?php
			                    	 // $a = $model->indicateurs2;
			                    	  //print_r($indicateurs2);
				                      echo	GridView::widget([
					                      	'dataProvider' => $dataProvider,
                                            'export' => false,
											'showPageSummary' => false,
											'bordered' => false,
											'responsive' => true,
											'hover' => true,
					                      	'columns' => [
						                      	['class' => 'yii\grid\SerialColumn'],
						                      	'plan',
					                      		'mode',
					                      		'id_famille',
					                      		'id_membre',
					                      	],
				                      	]);
			                      	?>
			                    </div>
			                </div>
			           </div>
			       </div>
			   </div>
           </div>
        </div>
    </div>
</section>