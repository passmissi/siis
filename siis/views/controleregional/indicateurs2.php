<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Config;
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Plans;
use app\models\Communes;
use app\models\Activites;
use app\models\SousActivites;
use app\models\ScrMonitoringRegional;
use app\models\Membre;
use yii\web\JsExpression;
use miloschuman\highcharts\Highcharts;
use app\models\DataIntegration;

$this->title = 'Data Dimagi';

$script5 = <<< JS
$(function () {
    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Donnees provenant de DIMAGI'
        },
        subtitle: {
            text: 'Activites Terrain'
        },
      	xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Nombre de Beneficiaires'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><br/>'
        },

        series: [{
            name: 'Suivis',
            colorByPoint: true,
            data: [{
                name: 'Promotion',
                y: $data1,
				drilldown: 'Promotion'
            }, {
                name: 'Distribution',
                y: $data2,
				drilldown: 'Distribution'
            }, {
                name: 'Reference',
                y: $data3,
				drilldown: 'Reference'
            }]
        }],
    });
});
JS;
$this->registerJs($script5, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Donnees AKF (DIMAGI)</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li>Dashboard</li>
       <li class="active">Donnees Dimagi</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-list"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Formulaires</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    traites
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-paste"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Beneficiaires</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Ben.
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Nbre AKF</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    AKF
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="glyphicon glyphicon-bed"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">ACAT</span>
              <span class="info-box-number"></span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                  <span class="progress-description">
                    Nouvelles familles
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
	</div>
	
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i> Stats Zone', ['/controleregional/index']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="fa fa-cloud-download"></i> Data Exporting', ['/controleregional/dataexporting']); '.</li>';
                                       echo  '<li class="active">'. Html::a('<i class="glyphicon glyphicon-level-up"></i> Donnees AKF (DIMAGI)', ['/controleregional/indicateurs']); '.</li>';
                                       
                                       
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    			<?php $form = ActiveForm::begin([
									    'action' => ['indicateurs'],
									    'method' => 'get',
										'options' => ['class' => 'form-inline'],
									]); ?>
									<div class="col-sm-4"></div>
									<div class="col-sm-8 pull-right">
										<div class="input-group">
										   <?= $form->field($searchModel, 'indicateurs2')->dropDownList(
											  ['1'=>'Vizit kay','2'=>'Pos Rasanbleman', '3'=>'Kleb Manman','4'=>'Kleb Jen',
								               '5'=>'Vizit Nan Lekol','6'=>'Reyinyon Vwazinaj'], ['prompt' => 'Choisir un type', 'class'=>'form-control input-sm'])->label(false)
									      ?>
									      <span class="input-group-btn" style="width:0px;"></span>
									       <?= $form->field($searchModel, 'mois')->dropDownList(
											  ['1'=>'Janvier','2'=>'Fevrier', '3'=>'Mars','4'=>'Avril',
								                   '5'=>'Mai','6'=>'Juin', '7'=>'Juillet','8'=>'Aout',
								                   '9'=>'Septembre', '10'=>'Octobre', '11'=>'Novembre',
								                   '12'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm'])->label(false)
									      ?>
									      <span class="input-group-btn" style="width:0px;"></span>
									      <?= $form->field($searchModel,'annee')
								          	 ->dropDownList(
										       ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
								                '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','class'=>'form-control input-sm']
										   )->label(false)?>
										  <span class="input-group-btn" style="width:0px;"></span>
										  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
										</div>
									</div>
								<?php ActiveForm::end(); ?>
                    		</div>
                    		
                    		<div class="nav-tabs-custom">
                    			<ul class="nav nav-tabs pull-right">
					              <li class="active"><a aria-expanded="true" href="#tab_1-1" data-toggle="tab">Data</a></li>
					              <li class="pull-left header"><i class="glyphicon glyphicon-stats"></i> Data Zone</li>
					            </ul>
					            
					            <div class="tab-content">
					             	<div class="tab-pane active" id="tab_1-1">
					             		<div class="row">
					             			<div class="col-md-4">
					             				<?php 
						             				echo  GridView::widget([
						             					'dataProvider' => $dataProvider,
						             					'export' => false,
						             					'showPageSummary' => false,
						             					'bordered' => false,
						             					'responsive' => true,
						             					'hover' => true, 
						             					'columns' => [
						             						
						             					],
						             				]);
					             			?>
					             			</div>
					             			<div class="col-md-8">
					             				<div id="container" style="min-width: 310px; max-width: 600px; height: 400px; margin: 0 auto">
					             				</div>
					             			</div>
					             		</div>
					             	</div>
					            </div>
					        </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>