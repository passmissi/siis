<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Config;
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Communes;
use app\models\ScrMonitoringRegional;
use app\models\Membre;
use app\models\Plans;
use app\models\TypeActivites;
use app\models\SousActivites;
use app\models\TypeSousActivites;
use app\models\TypeThemes;

$this->title = 'Suivi Controle Qualite';
$id = 0;
$listAKF = ArrayHelper::map(TypeSousActivites::find()->asArray()->all(),
		'id','libelle');

$listTypeActivites = ArrayHelper::map(TypeActivites::find()->where(['mode' => 1])->asArray()->all(), 'id', 'libelle');
$listTypeSousActivites = ArrayHelper::map(TypeSousActivites::find()->asArray()->all(), 'id', 'libelle');
$listTypeTheme = ArrayHelper::map(TypeThemes::find()->asArray()->all(), 'id', 'libelle');

$w =Yii::$app->getRequest()->getQueryParam('mois');
$x =Yii::$app->getRequest()->getQueryParam('annee');
$y =Yii::$app->getRequest()->getQueryParam('ts');
$z =Yii::$app->getRequest()->getQueryParam('akf');

$script1 = <<< JS
function init_click_handlers(val,val1) {
	if (confirm('Voulez-vous modifier ces donnees de suivis?')) {
		if(val1 == 1){
			$.ajax({
				type: 'GET',
				url: 'index.php?r=controleregional/msact',
				data: { id: val, type:  val1},
				success: function(data){
					
					var fam = data['id_famille'];
					var mem = data['id_membre'];
					var type = data['typetheme'];
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/listmembre',
						data: { id: fam },
						success: function(data){
							$("#monitoringsousactivites-id_membre").html(data);
							$("#monitoringsousactivites-id_membre").val(mem);
						},
						error: function() {
						},
					});
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: val1 },
						success: function(data){
							$("#monitoringsousactivites-typetheme").html(data);
							$("#monitoringsousactivites-typetheme").val(type);
						},
						error: function() {
							
						},
					});
		
					var divOne288= document.getElementById('type');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('type');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					
					var divOne288= document.getElementById('avaccin');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('ataille');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('apb');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('aen');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('reference');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('quantite');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('monitoringsousactivites-id');
				    divOne288.value = data['id'];
					var divOne288= document.getElementById('quantite1');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('monitoringsousactivites-id_famille');
				    divOne288.value = data['id_famille'];
				    var divOne288= document.getElementById('monitoringsousactivites-commentaire');
				    divOne288.value = data['commentaire'];
					var divOne288= document.getElementById('monitoringsousactivites-date_creation');
				    divOne288.value = data['date_creation'];
					
		
					$('#myModal').modal('show');
				},
				error: function() {
					
				},
			});
		}
		else if(val1 == 2){
			$.ajax({
				type: 'GET',
				url: 'index.php?r=controleregional/msact',
				data: { id: val, type:  val1},
				success: function(data){
					
					var fam = data['id_famille'];
					var mem = data['id_membre'];
					var type = data['typetheme'];
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/listmembre',
						data: { id: fam },
						success: function(data){
							$("#monitoringsousactivites-id_membre").html(data);
							$("#monitoringsousactivites-id_membre").val(mem);
						},
						error: function() {
						},
					});
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: val1 },
						success: function(data){
							$("#monitoringsousactivites-typetheme").html(data);
							$("#monitoringsousactivites-typetheme").val(type);
						},
						error: function() {
							
						},
					});
		
					var divOne288= document.getElementById('type');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('type');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('quantite');
				    divOne288.style.display='block';
					
					var divOne288= document.getElementById('avaccin');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('ataille');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('apb');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('aen');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('reference');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('quantite1');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('monitoringsousactivites-id_famille');
				    divOne288.value = data['id_famille'];
				    var divOne288= document.getElementById('monitoringsousactivites-commentaire');
				    divOne288.value = data['commentaire'];
					var divOne288= document.getElementById('monitoringsousactivites-date_creation');
				    divOne288.value = data['date_creation'];
					var divOne288= document.getElementById('monitoringsousactivites-quantite');
				    divOne288.value = data['quantite'];
					var divOne288= document.getElementById('monitoringsousactivites-id');
				    divOne288.value = data['id'];
		
					$('#myModal').modal('show');
				},
				error: function() {
					
				},
			});
		}
		else if(val1 == 3){
			$.ajax({
				type: 'GET',
				url: 'index.php?r=controleregional/msact',
				data: { id: val, type:  val1},
				success: function(data){
					
					var fam = data['id_famille'];
					var mem = data['id_membre'];
					var type = data['secteur'];
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/listmembre',
						data: { id: fam },
						success: function(data){
							$("#monitoringsousactivites-id_membre").html(data);
							$("#monitoringsousactivites-id_membre").val(mem);
						},
						error: function() {
						},
					});
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/list',
						data: { id: val1 },
						success: function(data){
							$("#monitoringsousactivites-secteur").html(data);
							$("#monitoringsousactivites-secteur").val(type);
						},
						error: function() {
							
						},
					});
		
					var divOne288= document.getElementById('type');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('quantite');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('avaccin');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('ataille');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('apb');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('aen');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('reference');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('quantite1');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('monitoringsousactivites-id_famille');
				    divOne288.value = data['id_famille'];
				    var divOne288= document.getElementById('monitoringsousactivites-commentaire');
				    divOne288.value = data['commentaire'];
					var divOne288= document.getElementById('monitoringsousactivites-date_creation');
				    divOne288.value = data['date_creation'];
					var divOne288= document.getElementById('monitoringsousactivites-institution');
				    divOne288.value = data['institution'];
					var divOne288= document.getElementById('monitoringsousactivites-id');
				    divOne288.value = data['id'];
		
					$('#myModal').modal('show');
				},
				error: function() {
					
				},
			});
		}
		else if(val1 == 4){}
		else if(val1 == 6 || val1 == 7){
			$.ajax({
				type: 'GET',
				url: 'index.php?r=controleregional/msact',
				data: { id: val, type:  val1},
				success: function(data){
					
					var fam = data['id_famille'];
					var mem = data['id_membre'];
					var type = data['secteur'];
		
					$.ajax({
						type: 'GET',
						url: 'index.php?r=socialworker/listmembre',
						data: { id: fam },
						success: function(data){
							$("#monitoringsousactivites-id_membre").html(data);
							$("#monitoringsousactivites-id_membre").val(mem);
						},
						error: function() {
						},
					});
		
					var divOne288= document.getElementById('type');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('commentaire');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('quantite');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('avaccin');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('ataille');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('apb');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('aen');
				    divOne288.style.display='block';
					var divOne288= document.getElementById('reference');
				    divOne288.style.display='none';
					var divOne288= document.getElementById('quantite1');
				    divOne288.style.display='none';
					
					var divOne288= document.getElementById('monitoringsousactivites-id_famille');
				    divOne288.value = data['id_famille'];
				    var divOne288= document.getElementById('monitoringsousactivites-commentaire');
				    divOne288.value = data['commentaire'];
					var divOne288= document.getElementById('monitoringsousactivites-date_creation');
				    divOne288.value = data['date_creation'];
					var divOne288= document.getElementById('monitoringsousactivites-vaccin');
				    divOne288.value = data['vaccin'];
					var divOne288= document.getElementById('monitoringsousactivites-poids');
				    divOne288.value = data['poids'];
					var divOne288= document.getElementById('monitoringsousactivites-taille');
				    divOne288.value = data['taille'];
					var divOne288= document.getElementById('monitoringsousactivites-rapportpt');
				    divOne288.value = data['rapportpt'];
					var divOne288= document.getElementById('monitoringsousactivites-pb');
				    divOne288.value = data['pb'];
					var divOne288= document.getElementById('monitoringsousactivites-oedeme');
				    divOne288.value = data['oedeme'];
					var divOne288= document.getElementById('monitoringsousactivites-en');
				    divOne288.value = data['en'];
					var divOne288= document.getElementById('monitoringsousactivites-id');
				    divOne288.value = data['id'];
		
					$('#myModal').modal('show');
				},
				error: function() {
					
				},
			});
		}
	}
	else{

    }
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);

$script1 = <<< JS
function init_click_handlers2(val) {
	if (confirm('Voulez-vous valider ces donnees?')) {
		$.ajax({
						type: 'GET',
						url: 'index.php?r=controleregional/valider',
						data: { id: val },
						success: function(data){
							
						},
						error: function() {
						},
					});
	}
	else{
    }
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Controle Qualite
       <small>Suivi des Sous-Activites</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Suivi Controle Qualite</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li>'. Html::a('<i class="fa fa-retweet"></i> Plans', ['/controleregional/controlequalite']); '.</li>';
                                       echo  '<li class="active">'. Html::a('<i class="fa fa-ellipsis-v"></i> Sous-Activites', ['']); '.</li>';
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    			<?php $form = ActiveForm::begin([
									'action' => ['/controleregional/controlequalitesa', 'mois'=>$w,'annee'=>$x,'ts'=>$y,'akf'=>$z],
									'method' => 'get',
									'options' => ['class' => 'form-inline'],
								]); ?>
								<div class="col-sm-9"></div>
								<div class="col-sm-3">
									<div class="input-group">
										 <?= $form->field($searchModel, 'type_sous_activites')->dropDownList(
											   $listAKF, ['prompt' => 'sous-activites','class'=>'form-control input-sm'])->label(false, ['id' => 'sous_activite'])
									     ?>
										<span class="input-group-btn" style="width:0px;"></span>
										<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
									</div>
								</div>
								<?php ActiveForm::end(); ?>
                    		</div>
                    		
                    		<div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Sous-Activites 
                    	    			<?php 
	                    	    			
                    	    				$personne = Operateurs::findOne($z);
                    	    				$result = $w.'-'.$x.' de '.$personne->nom.' '.$personne->prenom;
                    	    				echo $result;
                    	    			?>
                    	    		</h3>
                    	    	</div>
                    	    	
                    	    	<div class="box-body table-responsive no-padding">
                    	    	 	<?php 
			                    	echo	GridView::widget([
			                    			'dataProvider' => $dataProvider,
			                    			'export' => false,
			                    			'showPageSummary' => false,
			                    			'bordered' => false,
			                    			'responsive' => true,
			                    			'hover' => true,
			                    			'rowOptions' => function($model){
				                    			if($model->isValid == 1){
				                    				return ['class' => 'success'];
				                    			}
			                    			},
			                    			'columns' => [
			                    				['class' => 'yii\grid\SerialColumn'],
			                    				 'id','plan',
			                    					[
			                    					'attribute'=>'type_sous_activites',
			                    					'header'=>'Type SA',
			                    					'value'=>function ($model, $key, $index, $widget) {
			                    						$Op = TypeSousActivites::findOne($model->type_sous_activites);
			                    						if($Op!=null)
			                    							return $Op->libelle;
			                    						else
			                    							return null;
			                    					},
			                    					],
			                    					[
			                    					'attribute'=>'typetheme',
			                    					'header'=>'Theme',
			                    					'value'=>function ($model, $key, $index, $widget) {
			                    						$Op = TypeThemes::findOne($model->typetheme);
			                    						if($Op!=null)
			                    							return $Op->libelle;
			                    						else
			                    							return null;
			                    					},
			                    					],'id_famille',
			                    					[
			                    					'header'=>'Membre',
			                    					'attribute'=>'id_membre',
			                    					'format'=>'raw',
			                    					'value'=> function ($model, $key, $index, $widget) {
			                    						$type = Membre::find()->where(['famille' => $model->id_famille, 'memberid' => $model->id_membre])->one();
			                    						if($type != null){return $model->id_famille.'-'.$type->nom.' '.$type->prenom;}
			                    						else{return '---';}
			                    					},
			                    					],'date_creation',
			                    					[
				                    					'class' => 'yii\grid\ActionColumn',
				                    					'template' => '{modifier} {valider}',
				                    					'buttons' => [
				                    							'modifier' => function ($url,$model) {
				                    								
				                    								return Html::a(
				                    										'<span class="glyphicon glyphicon-edit"></span>', '#',
				                    										['class' => 'btn btn-xs btn-default', 
				                    										 'onclick' => 'init_click_handlers('.$model->id.','.$model->type_sous_activites.');',
				                    										 'onmouseover'=>'Tip("Editer data suivis")', 
				                    										 'onmouseout'=>'UnTip()']);
				                    							},
				                    							'valider' => function ($url,$model) {
				                    								return Html::a(
				                    										'<span class="fa fa-check"></span>',[''],
				                    										['class' => 'btn btn-xs btn-success', 
				                    										 'onclick' => 'init_click_handlers2('.$model->id.');',
				                    										 'onmouseover'=>'Tip("valider data suivis")', 
				                    										 'onmouseout'=>'UnTip()']);
				                    							},
				                    					 ],
			                    				    ]
			                    			],
			                    		]);
			                    	?>
                    	    	</div>
                    	    </div>
                    	</div>
                    </div>
               </div>
           </div>
       </div>
	</div>
</section>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Suivi - Sous-Activite </h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
<div class="modal-body">

	<div class="row" id="famille">
		<div class="col-lg-4">
			<?= $form->field($model, 'id')->textInput(['maxlength' => 100])
    		->label(null, ['id' => 'said']) ?>
		</div>
		
		<div class="col-lg-4">
			 <?= $form->field($model, 'id_famille')->textInput(['ondblclick' => 'showUser(this.value)']) 
    		->label(null, ['id' => 'id_famille'])?>
		</div>
		
		<div class="col-lg-4">
			<?= $form->field($model, 'id_membre')->textInput(['maxlength' => 20])
			->dropDownList([], ['prompt' => 'Choisir un membre'])
    		->label(null, ['id' => 'id_membre'])?>
		</div>
		
	</div>
	

	<div class="row" id="avaccin">
		<div class="col-lg-6">
			<?= $form->field($model, 'vaccin')
			->dropDownList(['1' => 'Polio 0', '2' => 'Polio 1', '3' => 'Polio 2', '4' => 'Polio 3',
							'5' => 'Polio 4', '6' => 'BCG', '7' => 'DTP 1', '8' => 'DTP 2', 
       						'9' => 'DTP 3', '10' => 'Woujol Ribeyol', '11' => 'DT', '12' => 'Pentavalan',
							'13' => 'Completement Vaccine', '14' => 'N/A'], 
                 ['prompt' => 'Choisir un vaccin']) 
    		->label(null, ['id' => 'vaccin'])?>
		</div>
		<div class="col-lg-6">
			 <?= $form->field($model, 'poids')->textInput() 
    		->label(null, ['id' => 'poids'])?>
		</div>
	</div>
	
	<div class="row" id="ataille">
		<div class="col-lg-6">
			<?= $form->field($model, 'taille')->textInput() 
    		->label(null, ['id' => 'taille'])?>
			
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'rapportpt')
			->dropDownList(['1' => '< -4', '2' => '< -3', '3' => '> -3 et < -2', '4' => '>= -2'],
					['prompt' => 'Choisir un rapport pt'])
    		->label(null, ['id' => 'rapportpt'])?>
		</div>
	</div>
    
	<div class="row" id="apb">
		<div class="col-lg-6">
			<?= $form->field($model, 'pb')->textInput() 
    		->label(null, ['id' => 'pb'])?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'oedeme')
			->dropDownList(['1' => 'Absan Edem', '2' => '+', '3' => '++', '4' => '+++'],
					['prompt' => 'Choisir un oedeme'])
    		->label(null, ['id' => 'oedeme'])?>
			
		</div>
	</div>
   
    <div class="row" id="aen">
		<div class="col-lg-12">
			 <?= $form->field($model, 'en')
			 ->dropDownList(['1' => 'Nomal/Sen', '2' => 'Malnitrisyon Egi Modere', 
							 '3' => 'Malnitrisyon Egi Seve san Konplikasyon', '4' => 'Malnitrisyon Egi Seve ak Konplikasyon'],
			 		['prompt' => 'Choisir EN'])
    		->label(null, ['id' => 'en']) ?>
		</div>
	</div>
    
    <div class="row" id="reference">
		<div class="col-lg-6">
			<?= $form->field($model, 'secteur')
			->dropDownList([], ['prompt' => 'Choisir un secteur'])
    		->label(null, ['id' => 'secteur'])?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'institution')->textInput(['maxlength' => 100]) 
    		->label(null, ['id' => 'institution'])?>
		</div>
	</div>
	
    <div class="row" id="type">
		<div class="col-lg-12">
			<?= $form->field($model, 'typetheme')
			->dropDownList([], ['prompt' => 'Choisir un theme'])
    		->label(null, ['id' => 'typetheme']) ?>
	    </div>
	</div>
	
    <div class="row" id="commentaire">
		<div class="col-lg-7">
			<?= $form->field($model, 'commentaire')->textArea(['rows' => '3'])
    		->label(null, ['id' => 'commentaire']) ?>
		</div>
		<div class="col-lg-5">
			<?= $form->field($model, 'date_creation')->input('date', ['required'])
    		->label(null, ['id' => 'date_creation']) ?>
    		
		</div>
	</div>
    
    <div class="row" id="quantite">
		<div class="col-lg-12">
			<?= $form->field($model, 'quantite')->textInput()
    		->label(null, ['id' => 'quantite']) ?>
		</div>
	</div>
    
    <div class="row" id="quantite1">
    	<?= $form->field($model, 'evenement_type')->textInput() 
    		->label(null, ['id' => 'evenement_type'])?>

    <?= $form->field($model, 'date_evenement')->textInput()
    		->label(null, ['id' => 'date_evenement']) ?>

    <?= $form->field($model, 'create_date')->textInput()
    		->label(null, ['id' => 'create_date']) ?>

    <?= $form->field($model, 'evenement')->textInput(['maxlength' => 100])
    		->label(null, ['id' => 'evenement']) ?>
    		
     <?= $form->field($model, 'sousactivites')->textInput(['maxlength' => 100])
    		->label(null, ['id' => 'sousactivites']) ?>
    		
    
    </div>
    
    
	<div class="modal-footer">
	     
	     <?= Html::submitButton(Yii::t('app', 'Update'), ['class' => 'btn btn-primary', 'name' => 'creation']) ?>
	
	</div>
</div>
 <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>