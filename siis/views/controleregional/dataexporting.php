<?php

use yii\helpers\Html;
/* use app\models\User;
use app\models\Config; */
use yii\widgets\ActiveForm;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Plans;
/*use app\models\Communes;
use app\models\Activites;
use app\models\SousActivites; */
use app\models\ScrMonitoringRegional;
use app\models\TypeSousActivites;
use app\models\TypeThemes;
use app\models\Membre;

$this->title = 'Data Exporting';

$listTSA = ArrayHelper::map(TypeSousActivites::find()->asArray()->all(),'id','libelle');
$listTT = ArrayHelper::map(TypeThemes::find()->asArray()->all(),'id','libelle','Mode');


$plan = new Plans();
$time = new \DateTime('now', new \DateTimeZone('UTC'));
$mois_int = date_format($time,"n");
$mois = $plan->getNumberMonth($mois_int);
$annee_int = date('Y');

/* $x =Yii::$app->getRequest()->getQueryParam('mois');
$y =Yii::$app->getRequest()->getQueryParam('annee'); */
/* $searchModel->mois = $x;
$searchModel->annee = $y; */


if($searchModel->mois != '')$mois=$searchModel->mois;
if($searchModel->annee != '')$annee_int=$searchModel->annee;

$script2 = <<< JS
function showUser2(str) {
	if(str == ''){
	}
	else{
		
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Data Exporting
       <small>Suivis </small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Data Exporting</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                       echo  '<li class="header">Actions</li>';
                                       echo  '<li>'. Html::a('<i class="glyphicon glyphicon-stats"></i> Stats Zone', ['/controleregional/index']); '.</li>';
                                       echo  '<li class="active">'. Html::a('<i class="fa fa-cloud-download"></i> Data Exporting', ['/controleregional/dataexporting']); '.</li>';
                                       echo  '<li>'. Html::a('<i class="glyphicon glyphicon-level-up"></i> Indicateurs', ['/controleregional/indicateurs']); '.</li>';
                                       
                                       
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row pad">
                    			<?php $form = ActiveForm::begin([
								        'action' => ['dataexporting'],
								        'method' => 'get',
                    					'options' => ['class' => 'form-inline'],
								    ]); ?>
									<div class="row">
										<div class="col-md-12">
								    <div class="input-group">
								    	<?= $form->field($searchModel, 'mois')->dropDownList(
											  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
								               'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
								               'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
								               'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm','onchange' =>'showUser2(this.value);'])->label(false) ?>
								    	<span class="input-group-btn" style="width:0px;"></span>
								    	<?= $form->field($searchModel, 'annee')->dropDownList(
										      ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
								               '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','class'=>'form-control input-sm']
										   )->label(false) ?>
								    	<span class="input-group-btn" style="width:0px;"></span>
								    	 <?= $form->field($searchModel, 'type_sous_activites')->dropDownList(
										      $listTSA, ['prompt'=>'--Sous Activites --','class'=>'form-control input-sm']
										   )->label(false) ?>
								    	<span class="input-group-btn" style="width:0px;"></span>
								    	<?= $form->field($searchModel, 'typetheme')->dropDownList(
										      $listTT, ['prompt'=>'--Type Theme --','class'=>'form-control input-sm']
										   )->label(false) ?>
	      								<span class="input-group-btn" style="width:0px;"></span>
	      								<?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary', 'name' => 'search','id' => 'search']) ?>
								    	<span class="input-group-btn" style="width:0px;"></span>
								    	<?php
											   if($searchModel->type_sous_activites != '' && $searchModel->typetheme != ''){
												   	echo Html::a('Export',
												   		['/controleregional/export', 'mois' => $mois, 'annee' => $annee_int,'type' => $searchModel->type_sous_activites, 'type1' => $searchModel->typetheme],
												   		['class' => 'btn btn-sm btn-warning','name' => 'export','id' => 'export']);
											   }
											   elseif ($searchModel->type_sous_activites != '' && $searchModel->typetheme == ''){
												   	echo Html::a('Export',
												   		['/controleregional/export', 'mois' => $mois, 'annee' => $annee_int,'type' => $searchModel->type_sous_activites, 'type1' => 0],
												   		['class' => 'btn btn-sm btn-warning','name' => 'export','id' => 'export']);
											   }
											   elseif ($searchModel->type_sous_activites == '' && $searchModel->typetheme != ''){
												   	echo Html::a('Export',
												   			['/controleregional/export', 'mois' => $mois, 'annee' => $annee_int,'type' => 0, 'type1' => $searchModel->typetheme],
												   			['class' => 'btn btn-sm btn-warning','name' => 'export','id' => 'export']);
											   }
											   elseif ($searchModel->type_sous_activites == '' && $searchModel->typetheme == ''){
												   	echo Html::a('Export',
												   			['/controleregional/export', 'mois' => $mois, 'annee' => $annee_int,'type' => 0, 'type1' => 0],
												   			['class' => 'btn btn-sm btn-warning','name' => 'export','id' => 'export']);
											   }
					                      ?>
								    </div>
                    			</div>
									</div>
                    			<?php ActiveForm::end(); ?>  
                    		</div>
                    		
                    		<div class="box">
                    	    	<div class="box-header with-border">
                    	    		<h3 class="box-title">Suivis</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
			                    	
			                    </div>
			               </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
    </div>
</section>