<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Chroniques */

$this->title = Yii::t('app', 'Create Chroniques');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chroniques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chroniques-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
