<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrChroniques */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Chroniques');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chroniques-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Chroniques'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'famille',
            'memberid',
            'souffrir_maladie_chronique',
            'avoir_traitement',
            // 'duree_traitement',
            // 'raison_non_traitement',
            // 'avoir_autres_maladies',
            // 'MANM_HH1_HH_MALAD_81_E2_2_81',
            // 'MANM_HH1_HH_MALAD_81_E2_3_81',
            // 'MANM_HH1_HH_MALAD_81_E2_4_81',
            // 'MANM_HH1_HH_MALAD_81_E3_1_81',
            // 'MANM_HH1_HH_MALAD_81_E3_2_81',
            // 'MANM_HH1_HH_MALAD_81_E3_3_81',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
