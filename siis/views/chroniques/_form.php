<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Chroniques */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chroniques-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memberid')->textInput() ?>

    <?= $form->field($model, 'souffrir_maladie_chronique')->textInput() ?>

    <?= $form->field($model, 'avoir_traitement')->textInput() ?>

    <?= $form->field($model, 'duree_traitement')->textInput() ?>

    <?= $form->field($model, 'raison_non_traitement')->textInput() ?>

    <?= $form->field($model, 'avoir_autres_maladies')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_2_81')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_3_81')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_4_81')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_1_81')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_2_81')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_3_81')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
