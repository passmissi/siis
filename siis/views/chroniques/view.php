<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Chroniques */

$this->title = $model->Id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Chroniques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chroniques-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'famille',
            'memberid',
            'souffrir_maladie_chronique',
            'avoir_traitement',
            'duree_traitement',
            'raison_non_traitement',
            'avoir_autres_maladies',
            'MANM_HH1_HH_MALAD_81_E2_2_81',
            'MANM_HH1_HH_MALAD_81_E2_3_81',
            'MANM_HH1_HH_MALAD_81_E2_4_81',
            'MANM_HH1_HH_MALAD_81_E3_1_81',
            'MANM_HH1_HH_MALAD_81_E3_2_81',
            'MANM_HH1_HH_MALAD_81_E3_3_81',
        ],
    ]) ?>

</div>
