<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrChroniques */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="chroniques-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'famille') ?>

    <?= $form->field($model, 'memberid') ?>

    <?= $form->field($model, 'souffrir_maladie_chronique') ?>

    <?= $form->field($model, 'avoir_traitement') ?>

    <?php // echo $form->field($model, 'duree_traitement') ?>

    <?php // echo $form->field($model, 'raison_non_traitement') ?>

    <?php // echo $form->field($model, 'avoir_autres_maladies') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_2_81') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_3_81') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E2_4_81') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_1_81') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_2_81') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_81_E3_3_81') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
