<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrMembre */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Membres');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membre-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Membre'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'famille',
            'memberid',
            'prenom',
            'nom',
            'surnom',
            // 'sexe',
            // 'age',
            // 'annee_naissance',
            // 'mois_naissance',
            // 'jour_naissance',
            // 'type_piece_indendification',
            // 'numero_piece',
            // 'relation_chef_famille',
            // 'est_mere',
            // 'est_pere',
            // 'b10',
            // 'b11',
            // 'etat_civil',
            // 'b13',
            // 'moins_18_hors_maison',
            // 'b15',
            // 'savoir_lire',
            // 'savoir_ecrire',
            // 'c1_2',
            // 'c1_3',
            // 'c1_4',
            // 'c1_5',
            // 'niveau_education',
            // 'c3',
            // 'c4',
            // 'c5',
            // 'pouvoir_travailler',
            // 'avoir_emploi',
            // 'avoir_activite_economique',
            // 'type_activite',
            // 'd5',
            // 'e8',
            // 'e9',
            // 'e10',
            // 'e11',
            // 'e12',
            // 'e13',
            // 'e14',
            // 'e15',
            // 'e16',
            // 'e17',
            // 'e18',
            // 'e19',
            // 'e20',
            // 'calc_1',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
