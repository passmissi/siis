<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Membre */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Membre',
]) . ' ' . $model->famille;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->famille, 'url' => ['view', 'famille' => $model->famille, 'memberid' => $model->memberid]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="membre-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
