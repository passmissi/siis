<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Operateurs;
use app\models\Communes;
use app\models\FamilyAkf;
use app\models\Membre;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\ScrMembre */
/* @var $form yii\widgets\ActiveForm */
$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listfamille = ArrayHelper::map(FamilyAkf::find()->where(['operateur_ts' => $operateur->id])->asArray()->all(),
		'famille',
		function($element) {
			$mem = Membre::find()->where(['famille' => $element['famille'], 'memberid' => 1])->one();
			if($mem != null){return $mem->nom.' '.$mem->prenom;}
			else{return "--";}
		});
?>

    <?php $form = ActiveForm::begin([
        'action' => ['/socialworker/membres', 'id' => 0],
        'method' => 'get',
    	'options' => ['class' => 'form-inline'],
    ]); ?>

    	<div class="input-group">
	    	 <?= $form->field($model, 'famille')->widget(Select2::classname(), [
					'data' => $listfamille,
					'language' => 'en',
					'options' => ['placeholder' => 'Choisir Famille','style'=>'width:100%',],
		    		'size' => 'sm',
		    		'theme' => Select2::THEME_BOOTSTRAP,
					'pluginOptions' => [
						'allowClear' => true,
					],])->label(false);?>
	    	<span class="input-group-btn" style="width:0px;"></span>
		    <?= $form->field($model, 'nom')->label(false) 
		    	->textInput(['style'=>'width:100%', 'class' => 'form-control input-sm','placeholder' => 'Nom'])?>
			<span class="input-group-btn" style="width:0px;"></span>
		    <?= $form->field($model, 'prenom')->label(false) 
		    	->textInput(['style'=>'width:100%', 'class' => 'form-control input-sm','placeholder' => 'Prenom'])?>
			<span class="input-group-btn" style="width:0px;"></span>
		    <?= $form->field($model, 'sexe')->label(false)
		    	 ->dropDownList(['1' => 'Masculin', '2' => 'Feminin'], ['style'=>'width:100%', 'class' => 'form-control input-sm','prompt' => 'Choisir Sexe']) ?>
			<span class="input-group-btn" style="width:0px;"></span>
		    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
	    </div>
	<?php ActiveForm::end(); ?>
