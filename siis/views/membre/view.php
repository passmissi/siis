<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Membre */

$this->title = $model->famille;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membre-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'famille' => $model->famille, 'memberid' => $model->memberid], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'famille' => $model->famille, 'memberid' => $model->memberid], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'famille',
            'memberid',
            'prenom',
            'nom',
            'surnom',
            'sexe',
            'age',
            'annee_naissance',
            'mois_naissance',
            'jour_naissance',
            'type_piece_indendification',
            'numero_piece',
            'relation_chef_famille',
            'est_mere',
            'est_pere',
            'b10',
            'b11',
            'etat_civil',
            'b13',
            'moins_18_hors_maison',
            'b15',
            'savoir_lire',
            'savoir_ecrire',
            'c1_2',
            'c1_3',
            'c1_4',
            'c1_5',
            'niveau_education',
            'c3',
            'c4',
            'c5',
            'pouvoir_travailler',
            'avoir_emploi',
            'avoir_activite_economique',
            'type_activite',
            'd5',
            'e8',
            'e9',
            'e10',
            'e11',
            'e12',
            'e13',
            'e14',
            'e15',
            'e16',
            'e17',
            'e18',
            'e19',
            'e20',
            'calc_1',
        ],
    ]) ?>

</div>
