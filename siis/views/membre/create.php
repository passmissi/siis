<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Membre */

$this->title = Yii::t('app', 'Create Membre');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membre-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
