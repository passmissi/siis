<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Membre */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membre-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memberid')->textInput() ?>

    <?= $form->field($model, 'prenom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surnom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sexe')->textInput() ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?= $form->field($model, 'annee_naissance')->textInput() ?>

    <?= $form->field($model, 'mois_naissance')->textInput() ?>

    <?= $form->field($model, 'jour_naissance')->textInput() ?>

    <?= $form->field($model, 'type_piece_indendification')->textInput() ?>

    <?= $form->field($model, 'numero_piece')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'relation_chef_famille')->textInput() ?>

    <?= $form->field($model, 'est_mere')->textInput() ?>

    <?= $form->field($model, 'est_pere')->textInput() ?>

    <?= $form->field($model, 'b10')->textInput() ?>

    <?= $form->field($model, 'b11')->textInput() ?>

    <?= $form->field($model, 'etat_civil')->textInput() ?>

    <?= $form->field($model, 'b13')->textInput() ?>

    <?= $form->field($model, 'moins_18_hors_maison')->textInput() ?>

    <?= $form->field($model, 'b15')->textInput() ?>

    <?= $form->field($model, 'savoir_lire')->textInput() ?>

    <?= $form->field($model, 'savoir_ecrire')->textInput() ?>

    <?= $form->field($model, 'c1_2')->textInput() ?>

    <?= $form->field($model, 'c1_3')->textInput() ?>

    <?= $form->field($model, 'c1_4')->textInput() ?>

    <?= $form->field($model, 'c1_5')->textInput() ?>

    <?= $form->field($model, 'niveau_education')->textInput() ?>

    <?= $form->field($model, 'c3')->textInput() ?>

    <?= $form->field($model, 'c4')->textInput() ?>

    <?= $form->field($model, 'c5')->textInput() ?>

    <?= $form->field($model, 'pouvoir_travailler')->textInput() ?>

    <?= $form->field($model, 'avoir_emploi')->textInput() ?>

    <?= $form->field($model, 'avoir_activite_economique')->textInput() ?>

    <?= $form->field($model, 'type_activite')->textInput() ?>

    <?= $form->field($model, 'd5')->textInput() ?>

    <?= $form->field($model, 'e8')->textInput() ?>

    <?= $form->field($model, 'e9')->textInput() ?>

    <?= $form->field($model, 'e10')->textInput() ?>

    <?= $form->field($model, 'e11')->textInput() ?>

    <?= $form->field($model, 'e12')->textInput() ?>

    <?= $form->field($model, 'e13')->textInput() ?>

    <?= $form->field($model, 'e14')->textInput() ?>

    <?= $form->field($model, 'e15')->textInput() ?>

    <?= $form->field($model, 'e16')->textInput() ?>

    <?= $form->field($model, 'e17')->textInput() ?>

    <?= $form->field($model, 'e18')->textInput() ?>

    <?= $form->field($model, 'e19')->textInput() ?>

    <?= $form->field($model, 'e20')->textInput() ?>

    <?= $form->field($model, 'calc_1')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
