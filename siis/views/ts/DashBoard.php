<?php 

//$this->registerJsFile(Yii::$app->request->baseUrl.'/js/calendar.js', 
//		['depends' => [\yii\web\JqueryAsset::className()]],
//		\yii\web\View::POS_END);


use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Activites */
/* @var $form yii\widgets\ActiveForm */

$script = <<< JS

$(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#calendar').fullCalendar({
        
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
            
          },
          buttonText: {
            today: 'Aujourd\'hui',
            month: 'mois',
            week: 'semaine',
            day: 'jour'
          },
          //Random default events
          events: [
            
          ],
		  
		  dayClick: function(date, jsEvent, view) {

			alert('Clicked on: ' + date.format());

					//alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

					//alert('Current view: ' + view.name);

					// change the day's background color just for fun
					//$(this).css('background-color', 'red');

		 },
				
          editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
		
		  eventDrop: function(event, delta, revertFunc) {

				//alert(event.title + " was dropped on " + event.start.format());

				if (!confirm("Are you sure about this change?")) {
					revertFunc();
				}

			},
			
			eventClick:  function(event, jsEvent, view) {
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
            $('#fullCalModal').modal('show');
			
			//$("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
            //$("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
            //$("#eventInfo").html(event.description);
            //$("#eventLink").attr('href', event.url);
            //$("#eventContent").dialog({ modal: true, title: event.title, width:350});
			
			//alert('hi');
        },
		
		  eventDragStop: function( event, jsEvent, ui, view ) {
              alert(event.title + " was dropped on " + event.start.format());
		  },
		  
		  eventReceive: function( event ) {
			
			//$("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
            //$("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
            //$("#eventInfo").html(event.description);
            //$("#eventLink").attr('href', event.url);
            //$("#eventContent").dialog({ modal: true, title: event.title, width:350});
			            
		    var title = event.title;
			$('#modalTitle').html(event.title);
            //$('#modalBody').html('Reussi');
			//$('#modalBody').html(event.description);
            $('#eventUrl').attr('href','www.google.fr');
            $('#fullCalModal').modal('show');
						//alert('hi');
			  // $('#myModal').modal('show');  
               // $('#dialog').modal('show');  
               
               			   
		  },
		  
          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
          e.preventDefault();
          //Save color
          currColor = $(this).css("color");
          //Add color effect to button
          $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
          e.preventDefault();
          //Get value and make sure it is not null
          var val = $("#new-event").val();
          if (val.length == 0) {
            return;
          }

          //Create events
          var event = $("<div />");
          event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
          event.html(val);
          $('#external-events').prepend(event);

          //Add draggable funtionality
          ini_events(event);

          //Remove event from text input
          $("#new-event").val("");
        });
      });
JS;
$this->registerJs($script, \yii\web\View::POS_END);


?>

<?php
if($modal = 1){
    $script2 = <<< JS
        $('#modalTitle').html('poze');
        $('#fullCalModal').modal('show'); 
JS;
    $this->registerJs($script2, \yii\web\View::POS_LOAD);
}
?>


	 
<section class="content-header">
	<h1>
       Calendar
       <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Calendar</li>
    </ol>
</section>


<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Draggable Events</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id='external-events'>
                    <div class='external-event bg-green' data-event='{"title":"Lunch"}' data-duration='03:00'>Lunch</div>
                    <div class='external-event bg-yellow' data-event='{"title":"Go home"}' data-duration='03:00'>Go home</div>
                    <div class='external-event bg-aqua' data-event='{"title":"Do homework"}' data-duration='03:00'>Do homework</div>
                    <div class='external-event bg-light-blue' data-event='{"title":"Work on UI design"}' data-duration='03:00'>Work on UI design</div>
                    <div class='external-event bg-red' data-event='{"title":"Sleep tight"}' data-duration='03:00'>Sleep tight</div>
                    <div class="checkbox">
                      <label for='drop-remove'>
                        <input type='checkbox' id='drop-remove' />
                        remove after drop
                      </label>
                    </div>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
		</div>
		<div class="col-md-9">
			<div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div><!-- /.box-body -->
              </div><!-- /. box -->
		</div>
	</div>
</section>

<div id="fullCalModal" class="modal fade">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span> <span class="sr-only">close</span></button>
						<h4 id="modalTitle" class="modal-title"></h4>
					</div>
					<?php $form = ActiveForm::begin([
							'id' => 'frm-login',
							//'withAjaxSubmit' => true,
							//'action' => ['login'],
							//'enableClientValidation'=>true
							]); ?>
					<div id="modalBody" class="modal-body">

					<?php $form = ActiveForm::begin(); ?>
					
					    <?php echo $this->render('//activites/_form', [
				             'model' => $model,
				        ]); ?>
					
					    
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
					   
					</div>
					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
		
		
