<?php
/* @var $this yii\web\View */
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use app\models\Familles;
use app\models\Membres;
use app\models\Communes;
?>

<div class="col-md-9">
	<div class="portlet">
		<h4 class="portlet-title">
		   <u>
		       Stats Generales
		   </u>
		</h4>
		<div class="portlet-body">
			<div class="row">
				<div class="col-sm-6 col-md-3">
					<div class="row-stat">
					    <p class="row-stat-label">
					        Nombre de familles
					    </p>
					    <h3 class="row-stat-value">
					       <?php
                              $number_famille = Familles::find()->count(); 
                              echo $number_famille; 
                           ?>
					    </h3>
					    <span class="label label-success row-stat-badge">
					        Enqueté
					    </span>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="row-stat">
					    <p class="row-stat-label">
					         Nombre d'habitants
					    </p>
					    <h3 class="row-stat-value">
					        <?php        
                               $nombre_pers = Membres::find()->count();
                               echo $nombre_pers;
                            ?>
					    </h3>
					    <span class="label label-success row-stat-badge">
					        Enqueté
					    </span>
					</div>
				</div>
				<div class="col-sm-6 col-md-3">
					<div class="row-stat">
					    <p class="row-stat-label">
					        Total hommes
                                            </p>
					    <h3 class="row-stat-value">
					        <?php              
                              $nombre_homme = Membres::find()->where(['sex'=>1])->count();
                              echo $nombre_homme;
                            ?> 
					    </h3>
					    <span class="label label-success row-stat-badge">
					        <?php if($nombre_pers!=0) echo round(100*$nombre_homme/$nombre_pers,2).'%' ?>
					    </span>
					</div>
				</div>
                                <div class="col-sm-6 col-md-3">
					<div class="row-stat">
					    <p class="row-stat-label">
					        Total hommes
                                            </p>
					    <h3 class="row-stat-value">
					        <?php       
                              $nombre_femme = Membres::find()->where(['sex'=>2])->count();
                              echo $nombre_femme;
                            ?>
                                                
					    </h3>
					    <span class="label label-success row-stat-badge">
					        <?php if($nombre_pers!=0) echo round(100*$nombre_femme/$nombre_pers,2).'%' ?>
					    </span>
					</div>
				</div>
                            
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 col-sm-3">
			<div class="portlet">
			    <h4 class="portlet-title"><u>Rapports</u></h4>
				<div class="portlet-body">
						 	 
			<?php
                       
                                  
						 		echo Highcharts::widget([
	                            	'options' => [
                                            'exporting' =>['enabled' => true],
                                    'xAxis' => [
										'categories' =>$comm,
										],
									'yAxis' => [
										'min' =>0,
										'title' => ['text', 'Age (annee)'],
									],
                                    'series' => [
												[
													'name' => 'Total',
                                                                                                        'type' => 'column',
													'data' =>$total,
													'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
												],
												[
													'name' => 'Féminin',
													'type' => 'column',
													'data' =>$total_f,//[12920, 14400, 17600],
													'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
												],
												[
													'name' => 'Masculin',
													'type' => 'column',
													'data' =>$total_m,//[21649, 19415, 9560],
													'color' => new JsExpression('Highcharts.getOptions().colors[2]'), // Joe's color
												],
										   ],
                                     ],
                                     
                                ]);
							?>
					
				
			</div>
	</div>
		
		
	</div>
</div>

