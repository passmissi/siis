<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Mort */

$this->title = Yii::t('app', 'Create Mort');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Morts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mort-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
