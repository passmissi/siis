<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mort */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mort-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B1_1_91')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B1_2_91')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B2_91')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B3_91')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B7_91')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
