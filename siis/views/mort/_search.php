<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrMort */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mort-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'famille') ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B1_1_91') ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B1_2_91') ?>

    <?= $form->field($model, 'MANM_HH1_ID_MANM_91_B2_91') ?>

    <?php // echo $form->field($model, 'MANM_HH1_ID_MANM_91_B3_91') ?>

    <?php // echo $form->field($model, 'MANM_HH1_ID_MANM_91_B7_91') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
