<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Mort */

$this->title = $model->Id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Morts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mort-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'famille',
            'MANM_HH1_ID_MANM_91_B1_1_91',
            'MANM_HH1_ID_MANM_91_B1_2_91',
            'MANM_HH1_ID_MANM_91_B2_91',
            'MANM_HH1_ID_MANM_91_B3_91',
            'MANM_HH1_ID_MANM_91_B7_91',
        ],
    ]) ?>

</div>
