<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrMort */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Morts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mort-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Mort'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'famille',
            'MANM_HH1_ID_MANM_91_B1_1_91',
            'MANM_HH1_ID_MANM_91_B1_2_91',
            'MANM_HH1_ID_MANM_91_B2_91',
            // 'MANM_HH1_ID_MANM_91_B3_91',
            // 'MANM_HH1_ID_MANM_91_B7_91',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
