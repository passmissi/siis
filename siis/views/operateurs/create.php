<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Operateurs */

$this->title = Yii::t('app', 'Create Operateurs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Operateurs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operateurs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
