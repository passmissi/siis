<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrOperateurs */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
	    'action' => ['index'],
	    'method' => 'get',
		'options' => ['class' => 'form-inline'],
	]); ?>
	    <div class="input-group">
	      <?= $form->field($model, 'nom')->label(false)
	      		->textInput(['style'=>'width:100%', 'class' => 'form-control input-sm','placeholder' => 'Nom']) ?>
		  <span class="input-group-btn" style="width:0px;"></span>
		 
	      <?= $form->field($model, 'prenom')->label(false)
	      		->textInput(['style'=>'width:100%', 'class' => 'form-control input-sm','placeholder' => 'Prenom']) ?>
		  
	      <span class="input-group-btn" style="width:0px;"></span>
		  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
		</div>
<?php ActiveForm::end(); ?>

