<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrOperateurs */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Operateurs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="operateurs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Operateurs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nom',
            'prenom',
            'sexe',
            'email:email',
            // 'fonction',
            // 'role',
            // 'akf_lie',
            // 'commune_lie',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
