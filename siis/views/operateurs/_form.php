<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Operateurs */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	        <?= $form->field($model, 'nom')->textInput(['maxlength' => 255]) ?>

		    <?= $form->field($model, 'prenom')->textInput(['maxlength' => 255]) ?>
		
		    <?= $form->field($model, 'sexe')->textInput() ?>
		
		    <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
		
		    <?= $form->field($model, 'fonction')->textInput(['maxlength' => 255]) ?>
		
		    
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    		
	      </div>
      <?php ActiveForm::end(); ?>
