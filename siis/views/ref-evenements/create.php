<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefEvenements */

$this->title = Yii::t('app', 'Create Ref Evenements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ref Evenements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-evenements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
