<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Plans */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
    		]); ?>
	    <div class="box-body">
	    	<div id="msg" class="row" style="display : none;">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable">
						 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						 Duplicate Planning not allowed. Choose a another month.
					</div>
				</div>
			</div>
		    <div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'mois')->dropDownList(
				          ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
		                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
		                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
		                   'Decembre'=>'Decembre'], ['prompt'=>'--Choisir un mois --', 'onchange' => 'showUser2(this.value)']
				    );?>
				</div>
				<div class="col-md-6">
					 <?= $form->field($model,'annee')
		          		->dropDownList(
				          ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
		                   '2016'=>'2016'], ['prompt'=>'--Choisir une annee --','disabled' => true,'onchange' => 'showUser(this.value)']
				    )?>
				</div>
			</div>	
			<div class="row">
				<div class="col-md-6">
					 <?= $form->field($model, 'dimagi')->checkbox(array('label'=>''))
							->label('Will upload to Dimagi'); ?> 
				</div>
			</div>	
	    </div>
	    <div class="box-footer">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'ok']) ?>
	    </div>
    <?php ActiveForm::end(); ?>


