<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Activites;
use app\models\User;
use app\models\Operateurs;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Plans;
use app\models\TypeThemes;
use app\models\TypeActivites;

/* @var $this yii\web\View */
/* @var $model app\models\Plans */

$this->title = 'Details Plan';
$x =Yii::$app->getRequest()->getQueryParam('id');
$plan = Plans::findOne($x);
?>

<section class="content-header">
	<h1>
       Plan
       <small>Details du Plan <strong>
       <?php 
            if($plan!=null && $plan->mode == 1){
				if($model1!=null && $model!=null)
            	echo ' de '.$model1->nom.' '.$model1->prenom.' ( '.$model->mois.' '.$model->annee.' )';
            }
       ?>
       </strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Details Plan</li>
    </ol>
</section>

<section class="content">
    <div class="row">
	
	
	</div>
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                               
                                            <?php
                                               if($plan->mode == 1){
                                               		echo Html::a('<i class="fa fa-plus-square"></i> Ajouter Activites (AKF)',
                                                    ['/activites/create', 'id' => $model->id], ['class' => 'btn btn-block btn-primary']);
                                               }elseif ($plan->mode == 2){
                                               		echo Html::a('<i class="fa fa-plus-square"></i> Ajouter Activites (TS)',
		                                            ['/activites/create2', 'id' => $model->id], ['class' => 'btn btn-block btn-primary']);
                                               }
                                               
                                            	
                                            ?>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	if($plan->mode == 1){
                                                		echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';

                                                	}elseif ($plan->mode == 2){
                                                		echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li class="active">'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
														
	                                                }
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    	    <div class="row">
                    	       <div class="col-md-12">
                    	          <?php 
                    	          
                    	            $activites = Activites::find()
                    	            	->where(['id_plan' => $model->id])
                    	                ->count();
                    	            	
                    	          	if($activites == 0)
                                    {
                                    	echo '<div class="alert alert-warning">';
                                    		echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
                                    		echo '<span class="sr-only">Error:</span>';
                                    		echo 'ATTENTION, CE PLAN NE CONTIENT AUCUN ELEMENT';
                                    	echo '</div>';
                                    }
                    	          ?>
                    	    </div></div>
                    		<div class="row">
                    			<div  class="col-md-12">
                    				<h3 class="page-header">Liste des Activites associees</h3>
                    				
                    				<?php 
					                    			echo GridView::widget([
					                    				'dataProvider' => $dataProvider,
                                                        'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
														'rowOptions' => function($model1){
															//$count = Plans::find()->where(['akf' => $model1->id])->count();
															if($model1->isDone != 0 && $model1->isDone != null){
																return ['class' => 'success',];
															}
															elseif($model1->isDone == 0 || $model1->isDone == null){
																return ['class' => 'warning'];
															}
														},
                                                        'columns' => [
	                                                         ['class' => 'yii\grid\SerialColumn'],
                                                             
															[
																'header'=>'Type Activite',
																'format'=>'raw',
																'value'=> function ($model, $key, $index, $widget) {
	                                                                $type = TypeActivites::findOne($model->id_type_activite);
																	return $type->libelle;
																},
															], 'date_activite', 'lieu', 'description',
															[
															'header'=>'Etat',
															'format'=>'raw',
															'value'=> function ($model1, $key, $index, $widget) {
																if($model1->isDone != 0 && $model1->isDone != null){
																	return '<span class="label label-success">realise</span>';
																}
																elseif($model1->isDone == 0 || $model1->isDone == null){
																	return '<span class="label label-warning">non realise</span>';
																}
															},
															],
                                                             [
																'class' => 'yii\grid\ActionColumn',
																'template' => '{activites} {details} {effacer}',
																'buttons' => [
																'activites' => function ($url,$model) {
																	return Html::a(
																			'<span class="glyphicon glyphicon-edit"></span>',
																			['activites/view', 'id' => $model->id],
                                                                            ['class' => 'btn btn-xs btn-default']);
																},
																'details' => function ($url,$model) {
																	return Html::a(
																			'<span class="glyphicon glyphicon-folder-open"></span>',
																			['sous-activites/index', 'id' => $model->id],
																			['class' => 'btn btn-xs btn-warning']);
																},
																'effacer' => function ($url,$model) {
																	$x =Yii::$app->getRequest()->getQueryParam('id');
																	$count = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
																			'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0, 'mode' => 1])->count();
																	
																	$count1 = Plans::find()->where(['id' => $model->id, 'isMonitoring' => 0,
																			'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 1, 'mode' => 1])->count();
																	
																	if($count == 0 && $count1 == 0){
																		return "";
																	}
																	else{ 
																		return Html::a(
																				'<span class="fa fa-trash-o"></span>',
																				['activites/delete', 'id' => $model->id, 'id1' => $x],
																				['class' => 'btn btn-xs btn-danger', 'data-method' => 'post',
																						'data-confirm'=>'Voulez vous vraiment effacer cette activite?',
																						'onmouseover'=>'Tip("Effacer AKF")', 'onmouseout'=>'UnTip()']);
																	}
																	
																	
																},
																],
                                                         ]]
					                    			 ]);
					                    		?> 
                    			</div>
                    			
                    			
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>