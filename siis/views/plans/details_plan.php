<?php
use kartik\grid\GridView;
use app\models\TypeSousActivites;
use app\models\Activites;
use app\models\TypeActivites;
use yii\helpers\Html;
?>
<div class="row">
	<div class="col-lg-12">
		 <div class="box">
                    	    	<div class="box-header">
                    	    		<h3 class="box-title">Activites</h3>
                    	    	</div>
			                    <div class="box-body table-responsive no-padding">
		<?php 
		echo GridView::widget([
				'dataProvider' => $dataProvider,
				'export' => false,
				'showPageSummary' => false,
				'summary' => '',
				'layout' => '{summary}{items}{pager}',
				'bordered' => false,
				'responsive' => true,
				'hover' => true,
				'columns' => [
						'id',
						[
								'header'=>'TA',
								'attribute'=>'id_activite',
								'format'=>'raw',
								'value'=> function ($model, $key, $index, $widget) {
									$type = TypeActivites::findOne($model->id_type_activite);
									return $type->libelle.'';
								},
								],'description', 'lieu',
								[
										'header'=>'Date',
										'format'=>'raw',
										'value'=> function ($model, $key, $index, $widget) {
											return $model->date_activite.'';
										},
										],
										[
												'format'=>'raw',
												'header'=>'Dimagi',
												'value'=>function ($model, $key, $index, $widget) {
													if($model->isUploaded == 1)
														return '<span class="fa fa-check-circle"></span>';
													else
														return '<span class="fa fa-minus-circle"></span>';
												},
												],
												[
														'class' => 'yii\grid\ActionColumn',
														'template' => '{dimagi}',
														'buttons' => [
																'dimagi' => function ($url,$model) {
																	if ($model->isUploaded == 1){
																		return '';
																	}else{
																		return Html::a(
																				'<span class="fa fa-cloud-upload"></span>',
																				['/socialworker/export2', 'id' => $model->id_plan, 'id2' => $model->id],
																				['class' => 'btn btn-xs btn-default']);
																	}
																},
																]]
																]
																]);
		?>
					                		
	</div>
	</div></div>
