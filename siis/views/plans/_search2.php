<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\ScrPlans */
/* @var $form yii\widgets\ActiveForm */
?>



<?php $form = ActiveForm::begin([
	    'action' => ['index2'],
	    'method' => 'get',
		'options' => ['class' => 'form-inline'],
	]); ?>
	    <div class="input-group">
	      
	       <?= $form->field($model, 'mois')->dropDownList(
			  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm'])->label(false)
			  
	      ?>
	      <span class="input-group-btn" style="width:0px;"></span>
		  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
		</div>
<?php ActiveForm::end(); ?>
