<?php

use yii\helpers\Html;
use app\models\User;
use app\models\Operateurs;
$x =Yii::$app->getRequest()->getQueryParam('id');
$this->title = 'Plan';

$script2 = <<< JS
function showUser2(str) {
	if(str == ''){
		document.getElementById("plans-annee").disabled = true;
	}
	else{
		document.getElementById("plans-annee").disabled = false;
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script1 = <<< JS
function showUser(str) {
	if(str == ''){

	}
	else{
		
		var ann = document.getElementById("plans-mois").value;
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/verifplan',
			data: { akf: $x, mois: ann, annee : str, mode : 1},
			success: function(data){
				if(data >= 1){
					var divOne448= document.getElementById('msg');
	            	divOne448.style.display='block';
					document.getElementById("ok").disabled = true;
					$("#myModal").modal("show");
				}else{
					var divOne448= document.getElementById('msg');
	            	divOne448.style.display='none';
					document.getElementById("ok").disabled = false;
					$("#myModal").modal("show");
				}
			},
			error: function() {
			},
		});
	}
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Plan
       <small>Creation de Plan pour <strong><?php echo $model1->nom.' '.$model1->prenom ?></strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Plan</li>
    </ol>
</section>

<section class="content">
	
	
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li>'. Html::a('<i class="fa  fa-ellipsis-v"></i> AKF', ['/socialworker/index']); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Nouveau Plan'); '.</li>';
                                                	
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<div  class="col-md-6">
                    				<div class="box box-primary">
                    					<div class="box-header with-border">
						                  <h3 class="box-title">Creation de Plan</h3>
						                </div><!-- /.box-header -->
                    				     <?= $this->render('_form', [
									        	'model' => $model,
									   		 ]) 
                    				     ?>
                    				</div>
                    			</div>
                    		
                    		    <div  class="col-md-6">
                    		        <h3>Sommaire du Plan</h3>
                    				<ul class="todo-list">
                    					<li>
                    						  <!-- drag handle -->
						                      <span class="handle">
						                        <i class="fa fa-ellipsis-v"></i>
						                        <i class="fa fa-ellipsis-v"></i>
						                      </span>
						                      
						                      <!-- todo text -->
						                      <span class="text"><?php echo 'Fait pour : '.$model1->prenom.' '.$model1->nom.',' ?></span>
						                      <!-- Emphasis label -->
						                      <small class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo $model1->fonction.', '.$model1->communeName ?></small>
						                      <!-- General tools such as edit or delete-->
						                      <div class="tools">
						                        <i class="fa fa-edit"></i>
						                        <i class="fa fa-trash-o"></i>
						                      </div>
                    					</li>
                    					<li>
                    						  <!-- drag handle -->
						                      <span class="handle">
						                        <i class="fa fa-ellipsis-v"></i>
						                        <i class="fa fa-ellipsis-v"></i>
						                      </span>
						                     
						                      <!-- todo text -->
						                      <span class="text"><?php echo $model1->email ?></span>
						                      <!-- Emphasis label -->
						                      <small class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo 'e-mail de '.$model1->prenom ?></small>
						                      <!-- General tools such as edit or delete-->
						                      <div class="tools">
						                        <i class="fa fa-edit"></i>
						                        <i class="fa fa-trash-o"></i>
						                      </div>
                    					</li>
                    					<li>
                    						  <!-- drag handle -->
						                      <span class="handle">
						                        <i class="fa fa-ellipsis-v"></i>
						                        <i class="fa fa-ellipsis-v"></i>
						                      </span>
						                     
						                      <!-- todo text -->
						                      <span class="text">
						                      	<?php 
						                      		$user = User::findOne(Yii::$app->user->identity->id);
						                      		$operateur = Operateurs::findOne($user->idOperateurs);
						                      		echo 'Fait par : '.$operateur->prenom.' '.$operateur->nom.','
						                      	?></span>
						                      <!-- Emphasis label -->
						                      <small class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo $operateur->fonction.', '.$operateur->communeName ?></small>
						                      <!-- General tools such as edit or delete-->
						                      <div class="tools">
						                        <i class="fa fa-edit"></i>
						                        <i class="fa fa-trash-o"></i>
						                      </div>
                    					</li>
                    					<li>
                    						  <!-- drag handle -->
						                      <span class="handle">
						                        <i class="fa fa-ellipsis-v"></i>
						                        <i class="fa fa-ellipsis-v"></i>
						                      </span>
						                     
						                      <!-- todo text -->
						                      <span class="text"><?php echo $operateur->email ?></span>
						                      <!-- Emphasis label -->
						                      <small class="label label-warning"><i class="fa fa-clock-o"></i> <?php echo 'e-mail de '.$operateur->prenom ?></small>
						                      <!-- General tools such as edit or delete-->
						                      <div class="tools">
						                        <i class="fa fa-edit"></i>
						                        <i class="fa fa-trash-o"></i>
						                      </div>
                    					</li>
                    				</ul>
                    			</div>
                    		</div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>