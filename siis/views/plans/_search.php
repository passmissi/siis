<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Operateurs;
/* @var $this yii\web\View */
/* @var $model app\models\ScrPlans */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

?>

<?php $form = ActiveForm::begin([
	    'action' => ['index', 'id' => $operateur->id],
	    'method' => 'get',
		'options' => ['class' => 'form-inline'],
	]); ?>
	    <div class="input-group">
	       <?= $form->field($model, 'mois')->dropDownList(
			  ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
                   'Decembre'=>'Decembre'], ['prompt' => 'Choisir un mois', 'class'=>'form-control input-sm'])->label(false)
	      ?>
	      <span class="input-group-btn" style="width:0px;"></span>
	      <?= $form->field($model, 'annee')->dropDownList(
			  ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015','2016'=>'2016',
			  		'2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020'
	      ], ['prompt' => 'Choisir une annee', 'class'=>'form-control input-sm'])->label(false)
	      ?>
	      <span class="input-group-btn" style="width:0px;"></span>
	      <?= $form->field($model, 'dimagi')->dropDownList(
			  ['1'=>'Oui','0'=>'Non'], ['prompt' => 'Dimagi', 'class'=>'form-control input-sm'])->label(false)
	      ?>
	      <span class="input-group-btn" style="width:0px;"></span>
		  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
		</div>
<?php ActiveForm::end(); ?>