<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\Models\Activites;
use app\Models\SousActivites;
use app\Models\Themes; 
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\Plans */
/* @var $form yii\widgets\ActiveForm */
?>

<?php 
        $item_activite = ArrayHelper::map(Activites::find()->all(), 'id', 'nom_activite');
        $item_sous_activite = ArrayHelper::map(SousActivites::find()->all(), 'id', 'nom_sous_activite');
        $item_theme = ArrayHelper::map(Themes::find()->all(), 'id', 'nom_theme');
        $item_status = ['Planifié'=>'Planifié','Réalisé'=>'Réalisé','Non réalisé'=>'Non réalisé'];
    ?>


<div class="container col-lg-9">
                      
      <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a data-toggle="tab" href="#activites">Activités</a></li>
        <li><a data-toggle="tab" href="#sousactivites">Sous Activités</a></li>
        <li><a data-toggle="tab" href="#themes">Thèmes</a></li>
            
      </ul>
    <div class="tab-content">
        <div id="activites" class="tab-pane fade in active">
            <?php $form1 = ActiveForm::begin(); ?>
            <?= $form1->field($modelActivite, 'activite')->textInput()->dropDownList($item_activite, ['prompt'=>'-- Choisir une activité --']) ?>
            <?= $form1->field($modelActivite, 'lieu')->textInput(['maxlength' => 255]) ?>
            <?= $form1->field($modelActivite, 'date')->textInput() ?>
            <?= $form1->field($modelActivite, 'famille')->textInput() ?>
            <?= Html::submitButton('Ajouter activités', ['name'=>'addActivite','class'=>'btn btn-success'])?>
            <?php ActiveForm::end(); ?>
             <?=   GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'code_plan',
            //'ts',
            'communeName',
            //'akf',
             'activite',
             //'sous_activite',
             //'theme',
             'lieu',
            'date',
             'famille',
            // 'status',
            // 'commentaires:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); 
      ?>
        </div>
        <div id="sousactivites" class="tab-pane fade">
             <?php $form2 = ActiveForm::begin(); ?>
            <?= $form2->field($modelSousActivite, 'sous_activite')->textInput()->dropDownList($item_sous_activite, ['prompt'=>'-- Choisir une sous activité --']) ?>
            <?= $form2->field($modelSousActivite, 'lieu')->textInput(['maxlength' => 255]) ?>
            <?= $form2->field($modelSousActivite, 'date')->textInput() ?>
            <?= Html::submitButton('Ajouter sous-activités', ['name'=>'addSousActivite','class'=>'btn btn-success'])?>
            <?php ActiveForm::end(); ?>
        </div>
        <div id="themes" class="tab-pane fade">
            <?php $form3 = ActiveForm::begin(); ?>
            <?= $form3->field($modelTheme, 'theme')->textInput()->dropDownList($item_theme, ['prompt'=>'-- Choisir un thème --']) ?>
            <?= $form3->field($modelTheme, 'lieu')->textInput(['maxlength' => 255]) ?>
            <?= $form3->field($modelTheme, 'date')->textInput() ?>
            <?= Html::submitButton('Ajouter thèmes', ['name'=>'addTheme','class'=>'btn btn-success'])?>
             <?php ActiveForm::end(); ?>
        </div>
    </div>
    
  


<div class="row">
    

    

    

    <?php //$form->field($model, 'ts')->textInput() ?>

    <?php // $form->field($model, 'commune')->textInput() ?>

    <?php // $form->field($model, 'akf')->textInput() ?>
    

    
    
    <?php // $form->field($model, 'commentaires')->textarea(['rows' => 6]) ?>

    <div class="form-group span9">
        <?php // Html::submitButton($model->isNewRecord ? 'Ajouter nouveau' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php // Html::submitButton('Terminé plan' , ['class' =>'btn btn-secondary']) ?>
    </div>

   

</div>
    
</div>
