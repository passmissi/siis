<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Operateurs;
use app\models\Plans;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrPlans */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Plans";
$x =Yii::$app->getRequest()->getQueryParam('id');
$titre = '';
$plan = Plans::findOne($x);
$a = new Plans();
$date = "'$plan->annee-".$a->getMonthNumber($plan->mois)."-01'";

$script = <<< JS
$(function () {

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        m = 'December',
        y = date.getFullYear();


		$('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },

          buttonText: {
            today: 'Aujourd\'hui',
            month: 'mois',
            week: 'semaine',
            day: 'jour'
          },

          events: '?r=activites/calendar1&id=$x',

		  dayClick: function(date, jsEvent, view) {
				alert('Clicked on: ' + date.format());
		 	},

          viewRender: function(view, element) {
		    $('#calendar').fullCalendar( 'gotoDate', $date );
		  },
		    		
          editable: false,
          droppable: false, // this allows things to be dropped onto the calendar !!!

		  eventDrop: function(event, delta, revertFunc) {
			if (!confirm("Are you sure about this change?")){
				revertFunc();
			}
		  },
		
		  eventClick:  function(event, jsEvent, view) {
            //$('#modalTitle').html(event.title);
           // $('#modalBody').html(event.description);
            //$('#eventUrl').attr('href',event.url);
            //$('#fullCalModal').modal('show');
          },

		  eventDragStop: function( event, jsEvent, ui, view ) {
             // alert(event.title + " was dropped on " + event.start.format());
		  },

		  eventReceive: function( event ) {
			
		  },

          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }

        });

		function custom_sort(a, b) {
		    return new Date(a.start).getTime() - new Date(b.start).getTime();
		}

		events_array.sort(custom_sort);

    	$('#calendar').fullCalendar('gotoDate', "2016-11-10");
      });
JS;
$this->registerJs($script, \yii\web\View::POS_END);

?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-solid">
				<div class="box-body">
					<div class="row">
						<div class="col-md-3 col-sm-4">
							<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                 <!-- compose message btn -->
                                 <a class="btn btn-block btn-primary">
                                      <i class="fa fa-plus-square"></i> Creer un plan de travail</a>
                                 <!-- Navigation - folders-->
                                 <div style="margin-top: 15px;">
                                      <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	if($plan->mode == 1){
                                                		echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';

                                                	}elseif ($plan->mode == 2){
                                                		echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li class="active">'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
														
	                                                }
                                                	?>
                                                </ul>
                                            </div>
						</div>
						
						<div class="col-md-9 col-sm-8">
							<div class="box box-primary">
						                <div class="box-body no-padding">
						                  <!-- THE CALENDAR -->
						                  <div id="calendar"></div>
						                </div><!-- /.box-body -->
						              </div><!-- /. box -->
						</div>
					</div></div></div></div></div>
</section>