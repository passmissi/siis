<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Operateurs;
use app\models\Plans;
use app\models\TypeActivites;
use app\models\SousActivites;
use app\models\TypeSousActivites;
use app\models\Monitoringactivites;
use app\models\Monitoringsousactivites;
use app\models\Membre;
use app\models\TypeThemes;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrPlans */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Fiche Suivi PDF";
?>
                    	          	
<div class="content">
	<div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
					<span> FICHE DE SUIVI DU PLAN DE TRAVAIL 
						
					</span>
				</h4>
		   	</div>
		</div>
		<br/>
		<div class="row">
			<div class="container">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica;font-size: 10px;">
				  <tr>
				     <td align="left" width="70%">Rezume Plan Travay la</td>
				     <td align="right" width="30%">
				     <?php 
                    	          	   $id = Yii::$app->getRequest()->getQueryParam('id');
                    	          	   $plan = Plans::findOne($id);
                    	          	   if($plan->mode == 1){
                    	          	   		$op = Operateurs::findOne($plan->akf);
                                            echo 'AKF :'.$op->nom.''.$op->prenom.'________________________________';
                    	          	   }
                    	          	   else{
                    	          	   	    $op1 = Operateurs::findOne($plan->ts);
                                            echo 'TS :'.$op1->nom.''.$op1->prenom.'________________________________';
                    	          	   }
                    	          	   
                    	?></td>
				  </tr>
			   </table>
				
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica;font-size: 10px;">
					 <tr>
					  	<td align="left" width="10%"><?php echo 'Code Plan : '.$plan->id.' /'?></td>
					  	<td align="left" width="15%"><?php echo 'Periode : '.$plan->mois.' '.$plan->annee.' /'?></td>
					  	<td align="right" width="75%"></td>
					  </tr>
				</table>
				
				<br/><br/>
			   <table width="100%" border="0">
			   
				   <?php if($plan->mode == 1):?>
				   		<tr>
			      <td width="100%"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
			        <tr>
			          <td width="8%" align="center" bgcolor="#E5E5E5"><br><br><br>Dat</td>
			          <td width="17%" align="center" bgcolor="#E5E5E5"><br><br><br>Aktivite</td>
			          <td width="4%" align="center" bgcolor="#E5E5E5"><br><br>Dire</td>
			          <td width="4%" align="center" bgcolor="#E5E5E5"><br><br>Id<br>Fanmi</td>
			          <td width="13%" align="center" bgcolor="#E5E5E5"><br><br><br>Ki kote</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5">Fomasyon<br>(*)</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Distribisyon</td>
			          <td width="6%" align="center" bgcolor="#E5E5E5"><br><br><br>Pwomosyon</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Referens</td>
			          <td width="6%" align="center" bgcolor="#E5E5E5"><br><br><br>Vaksinasyon</td>
			          <td width="3%" align="center" bgcolor="#E5E5E5"><br><br><br>Pwa</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Apwi TS la</td>
			          <td width="16%" align="center" bgcolor="#E5E5E5"><br><br><br>Komante</td>
			          <td width="3%" align="center" bgcolor="#E5E5E5"><br><br><br>Realise</td>
			        </tr>
			        <?php $nbre = 0; ?>
			        <?php foreach($activites->getModels() as $record): ?>
			            <?php $type = TypeActivites::findOne($record->id_type_activite);
			                  $sous = SousActivites::find()->where(['id_activite' => $record->id])->all();
			                  $nomcomplet = array();
			                  
			                  $mo = Monitoringactivites::find()->where(['activites' => $record->id])->one();
			                  $i = 0;
			                  $a = 0;
			                  
			                  foreach ($sous as $c){
			                  	$nomcomplet[$i] = $c->id_type_sous_activite;
			                  	$i += 1;
			                  }
			            ?>
			        	<?php $nbre += 1;?>
			        	<tr>
				          <td height="19px">&nbsp;<?= $record->date_activite?></td>
				          <td>&nbsp;<?= $record->id.' - '.$type->libelle?></td>
				          <td>&nbsp;</td>
				          <td>&nbsp;<?= 
				                 $record->id_famille
				          		//if($type->libelle == 'Vizit Domisily�'){echo '$record->id_famille';}
				          ?></td>
				          <td>&nbsp;<?= $record->lieu?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          		for ($j=0;$j<$i;$j++){
				          			if($nomcomplet[$j] == 4){ echo 'X';break;}
				          		}
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
						        for ($j=0;$j<$i;$j++){
						          	if($nomcomplet[$j] == 2){ echo 'X';break;}
						        }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
						        for ($j=0;$j<$i;$j++){
						          	if($nomcomplet[$j] == 1){ echo 'X';break;}
						        }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 3){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 6){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 7){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 8){ echo 'X';break;}
				          }
				          ?></td>
				          <td>&nbsp;</td>
				          <td>&nbsp;<?php 
				          		if($mo != null){echo 'X';}
				          ?></td>
			           </tr>
			        <?php endforeach;?>
			        
			        <?php if($nbre < 24):?>
			        	<?php $j = 24 - $nbre;?>
			        	
			        	<?php for ($i=1;$i<$j;$i++):?>
			        	<tr>
				          <td height="19px">&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
			           </tr>
			        <?php endfor;?>
			        	
			        <?php endif;?>
			        
			        <tr>
				      <td height="12px" colspan="14" align="left" bgcolor="#E5E5E5"><br><strong> (*) Aktivite:</strong> &nbsp; Vizit Domisilye&nbsp; /&nbsp; Pos Rasanbleman&nbsp; /&nbsp; Kleb Manman&nbsp; /&nbsp; Kleb Jenn&nbsp; /&nbsp; Vizit nan Lekol&nbsp; /&nbsp; Reyinyon Vwazinaj&nbsp; /&nbsp; Travay Administratif&nbsp; /&nbsp; Fomasyon</td>
				   </tr>   
				      </table></td>
				    </tr> 
				      </table>
				   <?php endif;?>
				   <pagebreak />  
			       <br/>
			       	   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
							<tr>
							   <td align="center" width="100%">Fomile swivi  vaksinasyon ak pwa timoun yo / Fanm ki nan laj pou fe pitit</td>
							</tr>
					   </table>
			       	   <br/>
				       <table width="100%" border="0">
					    <tr>
					      <td width="100%">
					      	<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
					      		<tr>
						          <td width="5%" align="center" bgcolor="#E5E5E5">Id Timoun / Fanm </td>
						          <td width="16%" align="center" bgcolor="#E5E5E5"><br><br>Non Timoun / Fanm </td>
						          <td width="6%" align="center" bgcolor="#E5E5E5"><br><br>Dat</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>Vaksen<br>(*)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>Pwa<br>(kg)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>Tay<br>(cm)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>Rapo P/T<br>(*)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>PB<br>(cm)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5">Prezans<br>Edem<br>(*)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>EN<br>(*)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5">Tip<br>Referans<br>(*)</td>
						          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br>Aktivite<br>(*)</td>
						          <td width="28%" align="center" bgcolor="#E5E5E5"><br><br>Komante</td>
						        </tr>
						        <?php $nbre = 0; ?>
						        
						        <?php foreach($activites->getModels() as $record): ?>
						            <?php 
						                  //$type = TypeActivites::findOne($record->id_type_activite);
						                  $sous = SousActivites::find()->where(['id_activite' => $record->id])->all();
						                  $nomcomplet = array();
						                  $nomcomplet1 = array();
						                  $i = 0;
						                  $z = 0;
						                  $a = Monitoringsousactivites::find()->where(['activite' => $record->id])->all();
						                  
						                  foreach ($a as $b){
						                  	$nomcomplet1[$z][0] = $b->type_sous_activites;
						                  	$nomcomplet1[$z][1] = $b;
						                  	$z += 1;
						                  }
						                  
						                  foreach ($sous as $c){
						                  	$nomcomplet[$i] = $c->id_type_sous_activite;
						                  	$i += 1;
						                  }
						            ?>
						            
						           <?php for ($j=0;$j<$i;$j++):?>
						           		<?php if($nomcomplet[$j] == 6 && sizeof($nomcomplet1) == 0):?>
						           			<tr>
									          <td height="19px">&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;<?php echo $record->date_activite;?></td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          <td>&nbsp;<?php echo $record->id;?></td>
									          <td>&nbsp;</td>
									        </tr>
									        <?php break; ?>
						           		<?php endif;?>
						           <?php endfor;?>
						           
						           <?php for ($o=0;$o<$z;$o++):?>
						           		<?php if($nomcomplet1[$o][0] == 6):?>
						           			<tr>
									          <td height="19px">&nbsp;<?php echo $nomcomplet1[$o][1]->id_membre; ?></td>
									          <td>&nbsp;<?php 
									              $mem = Membre::find()->where(['memberid' => $nomcomplet1[$o][1]->id_membre, 'famille' => $nomcomplet1[$o][1]->id_famille])->one();
									              echo $mem->prenom.' '.$mem->nom;
									          ?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->date_creation;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->vaccin;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->poids;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->taille;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->rapportpt;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->pb;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->oedeme;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->en;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->type_reference;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->activite;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->commentaire;?></td>
									        </tr>
						           		<?php endif;?>
						           <?php endfor;?>
						           
						        <?php endforeach;?>
						        
						        <?php if($nbre < 24):?>
						        	<?php $j = 24 - $nbre;?>
						        	
						        	<?php for ($i=1;$i<23;$i++):?>
						        	<tr>
							          <td height="19px">&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          </tr>
			                    <?php endfor;?>
						        <?php endif;?>
						        
						        <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) Vaksen:</strong> &nbsp;1. Polio 0&nbsp; /&nbsp; 2. Polio 1&nbsp; /&nbsp; 3. Polio 2&nbsp; /&nbsp; 4. Polio 3&nbsp; /&nbsp; 5. Polio 4&nbsp; /&nbsp; 6. BCG&nbsp; /&nbsp; 7. DTP 1&nbsp; /&nbsp; 8. DTP 2&nbsp; /&nbsp; 9. DTP 3&nbsp; /&nbsp; 10. Wouj�l Ribey�l&nbsp; /&nbsp; 11. DT&nbsp; /&nbsp; 12. Pentavalan&nbsp; /&nbsp; 98. Compl�tement Vaccin�&nbsp; /&nbsp; 99. Non Applicable</td>
							   </tr>   
							   <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) Rapo P/T:</strong> &nbsp;0: &lt; -4 ET&nbsp; /&nbsp;&nbsp;1: &lt; -3 ET&nbsp; /&nbsp;&nbsp;2: &gt; -3 ET ak &lt; -2 ET&nbsp; /&nbsp;&nbsp;3: &gt;= -2 ET</td>
							   </tr>   
							   <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) Prezans Edem:</strong> &nbsp;0: Absan Edem&nbsp; /&nbsp;1: +&nbsp; /&nbsp;2: ++&nbsp; /&nbsp;3: +++</td>
							   </tr>   
							   <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) EN:</strong> &nbsp;0: N�mal/Sen&nbsp; /&nbsp;&nbsp;1: Malnitrisyon Egi Modere&nbsp; /&nbsp;2: Malnitrisyon Egi Sev� san Konplikasyon&nbsp; /&nbsp;3: Malnitrisyon Egi Sev� ak Konplikasyon</td>
							   </tr>   
							   <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) Tip referans:</strong> &nbsp;0: Referans nan Swivi-Pwomosyon Kwasans&nbsp; /&nbsp;1: Pwogram Nitrisyon�l Siplemantasyon (PNS)&nbsp; /&nbsp; 2: Pwogwam Tr�tman Ambilatwa (PTA)&nbsp; /&nbsp; 3: Init� Estabilizasyon Nitrisyon�l(USN)</td>
							   </tr>   
							   <tr>
							      <td height="12px" colspan="13" align="left" bgcolor="#E5E5E5"><br><strong> (*) Aktivite:</strong> &nbsp;1. Vizit Domisily�&nbsp; /&nbsp; 2. P�s Rasanbleman&nbsp; /&nbsp; 3. Kl�b Manman&nbsp; /&nbsp; 4. Kl�b Jenn&nbsp; /&nbsp; 5. Vizit nan Lek�l&nbsp; /&nbsp; 6. Reyinyon Vwazinaj</td>
							   </tr>
					      	</table>
					      </td>
					    </tr>
					  </table>
			       <pagebreak />  
			       <br/>
			       
			       <table width="100%" border="0" cellpadding="0" cellspacing="0" >
				    <tr>
				      <td align="center" width="100%">Fomile Suivi pou Aktivite Kominikasyon pou Chanjman Konpotman</td>
				    </tr>
				  </table>
				  <br/>
				  <table width="100%" border="0">
					    <tr>
					      <td width="100%">
					      	<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
					      		<tr>
						          <td width="5%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Dat</td>
						          <td width="20%" rowspan="2" align="center" bgcolor="#E5E5E5">Tem<br>(*)</td>
						          <td width="5%" rowspan="2" align="center" bgcolor="#E5E5E5">Aktivite<br>(*)</td>
						          <td width="40%" colspan="2" align="center" bgcolor="#E5E5E5">Benefisye</td>
						          <td width="30%" rowspan="2" align="center" bgcolor="#E5E5E5"><br><br>Komante</td>
						        </tr>
						        <tr>
						          <td width="5%" align="center" bgcolor="#E5E5E5">Nimewo</td>
						          <td width="25%" align="center" bgcolor="#E5E5E5">Non</td>
						        </tr>
						        <?php $nbre = 0; ?>
						        
						        <?php foreach($activites->getModels() as $record): ?>
						            <?php 
						                  //$type = TypeActivites::findOne($record->id_type_activite);
						                  $sous = SousActivites::find()->where(['id_activite' => $record->id])->all();
						                  $nomcomplet = array();
						                  $nomcomplet1 = array();
						                  $i = 0;
						                  $z = 0;
						                  $a = Monitoringsousactivites::find()->where(['activite' => $record->id])->all();
						                  
						                  foreach ($a as $b){
						                  	$nomcomplet1[$z][0] = $b->type_sous_activites;
						                  	$nomcomplet1[$z][1] = $b;
						                  	$z += 1;
						                  }
						                  
						                  foreach ($sous as $c){
						                  	$nomcomplet[$i] = $c->id_type_sous_activite;
						                  	$i += 1;
						                  }
						            ?>
						            
						           <?php for ($j=0;$j<$i;$j++):?>
						           		<?php if($nomcomplet[$j] == 1 && sizeof($nomcomplet1) == 0):?>
						           			<tr>
									          <td height="19px">&nbsp;<?php echo $record->date_activite;?></td>
									          <td>&nbsp;</td>
									          <td>&nbsp;<?php echo $record->id;?></td>
									          <td>&nbsp;</td>
									          <td>&nbsp;</td>
									          
									          <td>&nbsp;</td>
									          
									        </tr>
						           		<?php endif;?>
						           <?php endfor;?>
						           
						           <?php for ($o=0;$o<$z;$o++):?>
						           		<?php if($nomcomplet1[$o][0] == 1):?>
						           		    <?php echo $o;?>
						           			<tr>
									          <td height="19px">&nbsp;<?php echo $nomcomplet1[$o][1]->date_creation;?></td>
									          <td>&nbsp;<?php 
									          		$type = TypeThemes::findOne($nomcomplet1[$o][1]->typetheme);
									          		echo $type->libelle;
									          ?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->activite;?></td>
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->id_membre; ?></td>
									          <td>&nbsp;<?php 
									              $mem = Membre::find()->where(['memberid' => $nomcomplet1[$o][1]->id_membre, 'famille' => $nomcomplet1[$o][1]->id_famille])->one();
									              echo $mem->prenom.' '.$mem->nom;
									          ?></td>
									          
									          <td>&nbsp;<?php echo $nomcomplet1[$o][1]->commentaire;?></td>
									        </tr>
						           		<?php endif;?>
						           <?php endfor;?>
						           
						        <?php endforeach;?>
						        
						        <?php if($nbre < 24):?>
						        	<?php $j = 24 - $nbre;?>
						        	
						        	<?php for ($i=1;$i<23;$i++):?>
						        	<tr>
							          <td height="19px">&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							        </tr>
			                    <?php endfor;?>
						        <?php endif;?>
						        <tr>
							      <td height="12px" colspan="6" align="left" bgcolor="#E5E5E5"><br><strong></td>
							   </tr>  
					      	</table>
					      </td>
					    </tr>
					  </table>
					  <pagebreak />  
			          <br/>
					  
					  <table width="100%" border="0" cellpadding="0" cellspacing="0" >
					    <tr>
					      <td align="center" width="100%">Fomile Suivi pou Distribisyon Pwodui Esansyel yo</td>
					    </tr>
					  </table>
					  <br/>
					  
					  
					  
			</div>
		</div>
	</div>
</div>