<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $model app\models\Plans */

$this->title = 'Create Plans';
$this->params['breadcrumbs'][] = ['label' => 'Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div>
<div class="col-md-6 col-sm-7">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_createPlan', [
       // 'model' => $model,
        'modelActivite'=>$modelActivite,
        'modelSousActivite'=>$modelSousActivite,
        'modelTheme'=>$modelTheme,
        'dataProvider' => $dataProvider,
       // 'themes' => $themes,
    ]) ?>
    
     <?php /*  GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           
            'code_plan',
            //'ts',
            'communeName',
            //'akf',
             'activite',
             'sous_activite',
             'theme',
            // 'lieu',
            'date',
            // 'famille',
            // 'status',
            // 'commentaires:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); */ 
      ?>
</div>
<div class="col-lg-9">
   
     

</div>
    </div>

