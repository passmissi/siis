<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\User;
use app\models\Operateurs;
use app\models\Plans;
use app\models\TypeActivites;
use app\models\SousActivites;
use app\models\TypeSousActivites;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrPlans */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Plans PDF";
?>
                    	          	
<div class="content">
	<div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
					<span> PLAN DE TRAVAIL 
						
					</span>
				</h4>
		   	</div>
		</div>
		<br/>
		<div class="row">
			<div class="container">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica;font-size: 10px;">
				  <tr>
				     <td align="left" width="70%">Rezume Plan Travay la</td>
				     <td align="right" width="30%">
				     <?php 
                    	          	   $id = Yii::$app->getRequest()->getQueryParam('id');
                    	          	   $plan = Plans::findOne($id);
                    	          	   if($plan->mode == 1){
                    	          	   		$op = Operateurs::findOne($plan->akf);
                                            echo 'AKF :'.$op->nom.''.$op->prenom.'________________________________';
                    	          	   }
                    	          	   else{
                    	          	   	    $op1 = Operateurs::findOne($plan->ts);
                                            echo 'TS :'.$op1->nom.''.$op1->prenom.'________________________________';
                    	          	   }
                    	          	   
                    	?></td>
				  </tr>
			   </table>
				
				<table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Helvetica;font-size: 10px;">
					 <tr>
					  	<td align="left" width="10%"><?php echo 'Code Plan : '.$plan->id.' /'?></td>
					  	<td align="left" width="15%"><?php echo 'Periode : '.$plan->mois.' '.$plan->annee.' /'?></td>
					  	<td align="right" width="75%"></td>
					  </tr>
				</table>
				
				<br/><br/>
			   <table width="100%" border="0">
			   
				   <?php if($plan->mode == 1):?>
				   		<tr>
			      <td width="100%"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
			        <tr>
			          <td width="8%" align="center" bgcolor="#E5E5E5"><br><br><br>Dat</td>
			          <td width="19%" align="center" bgcolor="#E5E5E5"><br><br><br>Aktivite</td>
			          
			          <td width="4%" align="center" bgcolor="#E5E5E5"><br><br>Id<br>Fanmi</td>
			          <td width="15%" align="center" bgcolor="#E5E5E5"><br><br><br>Ki kote</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5">Fomasyon<br>(*)</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Distribisyon</td>
			          <td width="6%" align="center" bgcolor="#E5E5E5"><br><br><br>Pwomosyon</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Referens</td>
			          <td width="6%" align="center" bgcolor="#E5E5E5"><br><br><br>Vaksinasyon</td>
			          <td width="3%" align="center" bgcolor="#E5E5E5"><br><br><br>Pwa</td>
			          <td width="5%" align="center" bgcolor="#E5E5E5"><br><br><br>Apwi TS la</td>
			          <td width="19%" align="center" bgcolor="#E5E5E5"><br><br><br>Komante</td>
			        </tr>
			        <?php $nbre = 0; ?>
			        <?php foreach($activites->getModels() as $record): ?>
			            <?php $type = TypeActivites::findOne($record->id_type_activite);
			                  $sous = SousActivites::find()->where(['id_activite' => $record->id])->all();
			                  $nomcomplet = array();
			                  
			                  $i = 0;
			                  $a = 0;
			                  
			                  foreach ($sous as $c){
			                  	$nomcomplet[$i] = $c->id_type_sous_activite;
			                  	$i += 1;
			                  }
			            ?>
			        	<?php $nbre += 1;?>
			        	<tr>
				          <td height="19px">&nbsp;<?= $record->date_activite?></td>
				          <td>&nbsp;<?= $record->id.' - '.$type->libelle?></td>
				          <td>&nbsp;<?= 
				                 $record->id_famille
				          		//if($type->libelle == 'Vizit DomisilyŤ'){echo '$record->id_famille';}
				          ?></td>
				          <td>&nbsp;<?= $record->lieu?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          		for ($j=0;$j<$i;$j++){
				          			if($nomcomplet[$j] == 4){ echo 'X';break;}
				          		}
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
						        for ($j=0;$j<$i;$j++){
						          	if($nomcomplet[$j] == 2){ echo 'X';break;}
						        }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
						        for ($j=0;$j<$i;$j++){
						          	if($nomcomplet[$j] == 1){ echo 'X';break;}
						        }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 3){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 6){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 7){ echo 'X';break;}
				          }
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
				          for ($j=0;$j<$i;$j++){
				          	if($nomcomplet[$j] == 8){ echo 'X';break;}
				          }
				          ?></td>
				          <td>&nbsp;</td>
			           </tr>
			        <?php endforeach;?>
			        
			        <?php if($nbre < 24):?>
			        	<?php $j = 24 - $nbre;?>
			        	
			        	<?php for ($i=1;$i<$j;$i++):?>
			        	<tr>
				          <td height="19px">&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
				          <td>&nbsp;</td>
			           </tr>
			        <?php endfor;?>
			        	
			        <?php endif;?>
			        
			        <tr>
				      <td height="12px" colspan="12" align="left" bgcolor="#E5E5E5"><br><strong> (*) Aktivite:</strong> &nbsp; Vizit Domisilye&nbsp; /&nbsp; Pos Rasanbleman&nbsp; /&nbsp; Kleb Manman&nbsp; /&nbsp; Kleb Jenn&nbsp; /&nbsp; Vizit nan Lekol&nbsp; /&nbsp; Reyinyon Vwazinaj&nbsp; /&nbsp; Travay Administratif&nbsp; /&nbsp; Fomasyon</td>
				   </tr>   
				   
				      </table></td>
				    </tr>
				   <?php elseif ($plan->mode == 2):?>
				   
				   		<tr>
			      <td width="100%"><table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
			        <tr>
			          <td width="11%" align="center" bgcolor="#E5E5E5"><br><br>Dat</td>
			          <td width="20%" align="center" bgcolor="#E5E5E5"><br><br>Aktivite</td>
			          
			          <td width="15%" align="center" bgcolor="#E5E5E5"><br><br>Ki kote</td>
			          
			          <td width="9%" align="center" bgcolor="#E5E5E5">Vizit</td>
			          <td width="9%" align="center" bgcolor="#E5E5E5"><br><br>Sipevizyon</td>
			          <td width="10%" align="center" bgcolor="#E5E5E5"><br><br>Reyinyon</td>
			          <td width="7%" align="center" bgcolor="#E5E5E5"><br><br>Otr</td>
			          <td width="19%" align="center" bgcolor="#E5E5E5"><br><br>Komante</td>
			        </tr>
			        <?php $nbre = 0; ?>
			        <?php foreach($activites->getModels() as $record): ?>
			            <?php 
			            	  $type = TypeActivites::findOne($record->id_type_activite);
			            ?>
			        	<?php $nbre += 1;?>
			        	<tr>
				          <td height="10px">&nbsp;<?= $record->date_activite?></td>
				          <td>&nbsp;<?= $record->id.' - '.$type->libelle?></td>
				          
				          <td>&nbsp;<?= $record->lieu?></td>
				          
				          <td style="text-align:center">&nbsp;<?php 
				          		if($type->id == 9){ echo 'X';}
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
								if($type->id == 10){ echo 'X';}
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
								if($type->id == 11){ echo 'X';}		
				          ?></td>
				          <td style="text-align:center">&nbsp;<?php 
								if($type->id == 12){ echo 'X';}
				          ?></td>
				          
				          <td>&nbsp;</td>
			           </tr>
			        <?php endforeach;?>
			        
			        <?php if($nbre < 24):?>
			        	<?php $j = 24 - $nbre;?>
			        	
			        	<?php for ($i=1;$i<$j;$i++):?>
				        	<tr>
					          <td height="10px">&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
					          <td>&nbsp;</td>
				           </tr>
				        <?php endfor;?>
			        	
			        <?php endif;?>
			        
				        <tr>
					      <td height="10px" colspan="8" align="left" bgcolor="#E5E5E5"><br><strong> (*) Aktivite:</strong> &nbsp; Vizit Domisilye&nbsp; /&nbsp; Pos Rasanbleman&nbsp; /&nbsp; Kleb Manman&nbsp; /&nbsp; Kleb Jenn&nbsp; /&nbsp; Vizit nan Lekol&nbsp; /&nbsp; Reyinyon Vwazinaj&nbsp; /&nbsp; Travay Administratif&nbsp; /&nbsp; Fomasyon</td>
					   </tr>   
				      </table></td>
				    </tr>
				   <?php endif;?>
			    
			   </table>
			</div>
		</div>
	</div>
</div>