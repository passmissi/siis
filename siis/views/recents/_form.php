<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Recents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recents-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'memberid')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E4_71')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E5_71')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E6_71')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E7_71')->textInput() ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E7B_71')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
