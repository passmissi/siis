<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Recents */

$this->title = $model->Id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recents-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->Id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->Id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Id',
            'famille',
            'memberid',
            'MANM_HH1_HH_MALAD_71_E4_71',
            'MANM_HH1_HH_MALAD_71_E5_71',
            'MANM_HH1_HH_MALAD_71_E6_71',
            'MANM_HH1_HH_MALAD_71_E7_71',
            'MANM_HH1_HH_MALAD_71_E7B_71',
        ],
    ]) ?>

</div>
