<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrRecents */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Recents');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recents-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Recents'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Id',
            'famille',
            'memberid',
            'MANM_HH1_HH_MALAD_71_E4_71',
            'MANM_HH1_HH_MALAD_71_E5_71',
            // 'MANM_HH1_HH_MALAD_71_E6_71',
            // 'MANM_HH1_HH_MALAD_71_E7_71',
            // 'MANM_HH1_HH_MALAD_71_E7B_71',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
