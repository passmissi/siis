<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrRecents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Id') ?>

    <?= $form->field($model, 'famille') ?>

    <?= $form->field($model, 'memberid') ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E4_71') ?>

    <?= $form->field($model, 'MANM_HH1_HH_MALAD_71_E5_71') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_71_E6_71') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_71_E7_71') ?>

    <?php // echo $form->field($model, 'MANM_HH1_HH_MALAD_71_E7B_71') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
