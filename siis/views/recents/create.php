<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Recents */

$this->title = Yii::t('app', 'Create Recents');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Recents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recents-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
