<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrMonitoringplan */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Monitoringplans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringplan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Monitoringplan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'Plan',
            'isExecuted',
            'isExecutedBy',
            'isExecutedOn',
            // 'isActif',
            // 'isClosed',
            // 'mode',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
