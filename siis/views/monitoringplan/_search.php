<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrMonitoringplan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoringplan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'Plan') ?>

    <?= $form->field($model, 'isExecuted') ?>

    <?= $form->field($model, 'isExecutedBy') ?>

    <?= $form->field($model, 'isExecutedOn') ?>

    <?php // echo $form->field($model, 'isActif') ?>

    <?php // echo $form->field($model, 'isClosed') ?>

    <?php // echo $form->field($model, 'mode') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
