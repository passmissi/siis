<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Monitoringplan */

$this->title = Yii::t('app', 'Create Monitoringplan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoringplans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringplan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
