<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Monitoringplan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoringplan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Plan')->textInput() ?>

    <?= $form->field($model, 'isExecuted')->textInput() ?>

    <?= $form->field($model, 'isExecutedBy')->textInput() ?>

    <?= $form->field($model, 'isExecutedOn')->textInput() ?>

    <?= $form->field($model, 'isActif')->textInput() ?>

    <?= $form->field($model, 'isClosed')->textInput() ?>

    <?= $form->field($model, 'mode')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
