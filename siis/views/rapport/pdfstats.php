<?php

use yii\helpers\Html;

use app\models\Options;
?>

<div class="content">
	<div class="container">
		<br/>
		<div class="row">
			<div class="Container">
		    	<h4 class="content-title" style="text-align:center;">
					<span> KORE FANMI - DONNEES ENQUETES </span>
				</h4>
				<h5 class="content-title" style="text-align:center;">
					<span> <?php 
								if(isset($section_communale) || isset($localite)){
									echo strtoupper($commune);
								}else{
									echo strtoupper($commune).' / '.strtoupper($section_communale).' / '.strtoupper($localite);
								}
						   ?> </span>
				</h5>
		    </div>
		</div>
		<br/>
		<div class="row">
			<div class="container">
				<?php foreach($data as $record): ?>
					<?php 
						$result = array();
						$xy = new Options();
						$result = $xy->getData($record, $commune, $section_communale, $localite);
						$ar = json_decode($record->options);
					?>
					
					<table width="100%" border="0">
						<tr>
						   <td align="left"><?php echo ' '.$record->question; ?></td>
						</tr>
						<tr>
						   <td align="center"></td>
						</tr>
					</table> 
					
					<table width="100%" border="0">  
					    <tr>
					      	<td width="100%">
					      		<table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
					      			<tr>
							          <td width="10%" align="center" bgcolor="#E5E5E5">Commune</td>
							          <td width="10%" align="center" bgcolor="#E5E5E5">Section Communale</td>
							          <td width="10%" align="center" bgcolor="#E5E5E5">Localite</td>
							          <td width="60%" align="center" bgcolor="#E5E5E5">Reponse</td>
							          <td width="10%" align="center" bgcolor="#E5E5E5">Quantite</td>
							        </tr>	
							        <?php $o = 0;?>
					      			<?php foreach($result as $record2): ?>
					      				<tr>
					      					<td>&nbsp;<?= iconv("iso-8859-1","UTF-8",$commune) ?></td>
						      				<td>&nbsp;<?= iconv("iso-8859-1","UTF-8",$section_communale) ?></td>
						      				<td>&nbsp;<?= iconv("iso-8859-1","UTF-8",$localite) ?></td>
						      				<td>&nbsp;<?php
						      					if($result[$o][1] == 0){
						      						echo "-- PA REPONN --";
						      					}else{
						      						echo $ar[$result[$o][1]-1];
						      					}
						      				?></td>
						      				<td>&nbsp;<?= iconv("iso-8859-1","UTF-8",$result[$o][0]) ?></td>
					      				</tr>
					      				<?php $o += 1;?>
					      			<?php endforeach;?>
					      			<tr>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							          <td>&nbsp;</td>
							        </tr>
					      		</table>
					      	</td>
					      </tr>
					 </table>
					 <br/><br/>
				<?php endforeach;?>
			</div>
		</div>
	</div>
</div>