<?php
use yii\helpers\Html;
use app\models\Famille;
use app\models\User;
use app\models\Operateurs;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
/* @var $this yii\web\View */
$this->title = 'Rapport';

?>
<section class="content-header">
	<h1>
       Statistiques Thiotte
       <small>Donnees d'Enquetes (Avril 2015)</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	
	<div class="row">
		<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nombre de familles</span>
                  <span class="info-box-number">
                  	<?php
                       $number_famille = Famille::find()->where(['commune' => 'thiotte'])->count(); 
                       echo $number_famille; 
                    ?>
                  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description">
                    Enquete
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
        <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Nombre d'habitants</span>
                  <span class="info-box-number">
                  	<?= 
                       $total[0]; 
                    ?>
                  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width: 100%"></div>
                  </div>
                  <span class="progress-description">
                    Enquete
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
           <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-male"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total hommes</span>
                  <span class="info-box-number"><?= $total_m[0] ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$total_m[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$total_m[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-female"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Total Femmes</span>
                  <span class="info-box-number"><?= $total_f[0] ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$total_f[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$total_f[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div>
	
	
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		
                           <div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-map-marker"></i> Demographie', ['']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-eye-slash"></i> Cond. Sanitaires', ['/rapport/sanitairethiotte']); '.</li>';
                                                	 
                                                	echo  '<li>'. Html::a('<i class="fa fa-tasks"></i> Dimensions', ['/rapport/dimensions']); '.</li>';
                                                	
                                                	?>
                               </ul>
                           </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<div class="col-md-8">
                    				<div class="box box-success">
		                                <div class="box-header">
		                                    <i class="fa fa-bar-chart-o"></i>
		                                    <h3 class="box-title">Rapport Demographique</h3>
		                                </div>
		                                <div class="box-body">
		                                		<?php 
				                                	echo Highcharts::widget([
				                                	    'options' => [
															'exporting' =>['enabled' => true],
				                                			'title' => ['text' => 'Donnees Demographiques'],
															'xAxis' => [
																'categories' => ['Thiotte']
															],
				                                			'yAxis' => [
				                                				'title' => ['text' => 'Nombre Habitants']
				                                			],
				                                			'series' => [
				                                				['name' => 'Total', 
																 'type' => 'column',
																 'data' => $total,
																 'color' => new JsExpression('Highcharts.getOptions().colors[0]'),],
				                                				['name' => 'Feminin', 
																 'type' => 'column',
																 'data' =>  $total_f,
																 'color' => new JsExpression('Highcharts.getOptions().colors[1]'),],
																['name' => 'Masculin',
																 'type' => 'column',
																 'data' => $total_m,
																 'color' => new JsExpression('Highcharts.getOptions().colors[2]'),],
				                                			]
				                                		]
				                                	]);
			                                	
			                                	?>
		                                </div><!-- /.box-body -->
		                            </div><!-- /.box -->
                    			</div>
                    			<div class="col-md-4">
                    				<div class="box box-warning">
						                <div class="box-header with-border">
						                  <h3 class="box-title">Section Communale</h3>
						                </div><!-- /.box-header -->
						                <div class="box-footer no-padding">
						                  <ul class="nav nav-pills nav-stacked">
						                    <?php $arrlength = sizeof($total_localite); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
						                    	
							    			 <li><a href="#"><?= $total_localite[$i][1] ?> <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $total_localite[$i][0] ?></span></a></li>
						                    
						                    <?php endfor; ?>
						                    
						                    </ul>
						                </div><!-- /.footer -->
						              </div>
						              
						              <div class="box box-warning">
						                <div class="box-header with-border">
						                  <h3 class="box-title">% Enfants / Population</h3>
						                </div><!-- /.box-header -->
						                <div class="box-footer no-padding">
						                  <ul class="nav nav-pills nav-stacked">
						                    <?php $arrlength = sizeof($total_enfantgarcon05); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
							    			 <li><a href="#">Garcons (0 - 5) ans 
							    			    <span class="pull-right text-green"><i class="fa fa-angle-up"></i> <?= $total_enfantgarcon05[0] ?> - <?php  echo round(100*$total_enfantgarcon05[0]/$total[0],2).'%' ?></span></a>
							    			 </li>
						                    <?php endfor; ?>
						                    
						                    <?php $arrlength = sizeof($total_enfantfille05); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
							    			 <li><a href="#">Filles (0 - 5) ans 
							    			    <span class="pull-right text-green"><i class="fa fa-angle-up"></i> <?= $total_enfantfille05[0] ?> - <?php  echo round(100*$total_enfantfille05[0]/$total[0],2).'%' ?></span></a>
							    			 </li>
						                    <?php endfor; ?>
						                    
						                    <?php $arrlength = sizeof($total_enfantgarcon518); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
							    			 <li><a href="#">Garcons (5 - 18) ans 
							    			    <span class="pull-right text-green"><i class="fa fa-angle-up"></i> <?= $total_enfantgarcon518[0] ?> - <?php  echo round(100*$total_enfantgarcon518[0]/$total[0],2).'%' ?></span></a>
							    			 </li>
						                    <?php endfor; ?>
						                    
						                    <?php $arrlength = sizeof($total_enfantfille518); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
							    			 <li><a href="#">Filles (5 - 18) ans 
							    			    <span class="pull-right text-green"><i class="fa fa-angle-up"></i> <?= $total_enfantfille518[0] ?> - <?php  echo round(100*$total_enfantfille518[0]/$total[0],2).'%' ?></span></a>
							    			 </li>
						                    <?php endfor; ?>
						                    </ul>
						                </div><!-- /.footer -->
						              </div>
                    			</div>
                    		</div>
                    	</div></div></div>
           </div>
        </div>
    </div>
	
</section>

