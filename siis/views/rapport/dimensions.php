<?php
use yii\helpers\Html;
use app\models\Famille;
use app\models\User;
use app\models\Operateurs;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use kartik\select2\Select2;

$this->title = 'Dimensions';

?>

<section class="content-header">
	<h1>
       Statistiques/Dimensions
       <small>Donnees d'Enquetes (Avril 2015)</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dimensions</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                           <div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                   <?php 
                                        echo  '<li class="header">Actions</li>';
                                        echo  '<li>'. Html::a('<i class="fa fa-map-marker"></i> Demographie', ['/rapport/index']); '.</li>';
                                        echo  '<li>'. Html::a('<i class="fa fa-eye-slash"></i> Cond. Sanitaires', ['/rapport/sanitairethomassique']); '.</li>';
                                        echo  '<li class="active">'. Html::a('<i class="fa fa-tasks"></i> Dimensions', ['/rapport/dimensions']); '.</li>';
                                   ?>
                               </ul>
                           </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<h4 class="page-header">Dimensions/Donnees</h4>
                    			<div class="col-md-9">
                    				<?php echo $this->render('/options/_form', ['model' => $model]); ?>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
         </div>
     </div>
</section>