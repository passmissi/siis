<?php
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

$this->title = 'Rapport';
?>

<section class="content-header">
	<h1>
       Statistiques Thomassique
       <small>Donnees d'Enquetes (Avril 2015)</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		
		<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-aqua">
                <span class="info-box-icon"><i class="fa fa-flash"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Courant EDH</span>
                  <span class="info-box-number"><?= $edh[0] ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$edh[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$edh[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
		
			<div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-coffee"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Cuisines (Bois, Charbons)</span>
                  <span class="info-box-number">
                  	<?= 
                       $cuisines[0]; 
                    ?>
                  </span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$cuisines[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$cuisines[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-minus"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Sol (Bois, Planches)</span>
                  <span class="info-box-number"><?= $maisons[0] ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$maisons[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$maisons[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box bg-red">
                <span class="info-box-icon"><i class="fa fa-flag"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Toits (Paille, Plastique, Bambou)</span>
                  <span class="info-box-number"><?= $toits[0] ?></span>
                  <div class="progress">
                    <div class="progress-bar" style="width: <?php if($total[0] !=0) echo round(100*$toits[0]/$total[0],2).'%' ?>"></div>
                  </div>
                  <span class="progress-description">
                    <?php if($total[0] !=0) echo round(100*$toits[0]/$total[0],2).'%' ?>
                  </span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
	</div>
	
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		
                           <div style="margin-top: 15px;">
                               <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-map-marker"></i> Demographie', ['/rapport/index']); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-eye-slash"></i> Cond. Sanitaires', ['/rapport/sanitairethomassique']); '.</li>';
                                                	echo  '<li>'. Html::a('<i class="fa fa-tasks"></i> Dimensions', ['/rapport/dimensions']); '.</li>';
                                                	
                                                	?>
                               </ul>
                           </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    			<div class="col-md-8">
                    				<div class="box box-success">
		                                <div class="box-header">
		                                    <i class="fa fa-bar-chart-o"></i>
		                                    <h3 class="box-title">Rapport Donnees/Latrines</h3>
		                                </div>
		                                <div class="box-body">
		                                	<?php 
			                                	echo Highcharts::widget([
			                                		'options' => [
				                                		'exporting' =>['enabled' => true],
				                                		'title' => ['text' => 'Type - Latrines'],
				                                		'xAxis' => [
				                                			'categories' => ['Type']
				                                		],
				                                		'yAxis' => [
				                                			'title' => ['text' => 'Nombre']
				                                		],
				                                		'series' => [
				                                			['name' => 'Lanati',
				                                			'type' => 'column',
				                                			'data' => $latrine1,
				                                			'color' => new JsExpression('Highcharts.getOptions().colors[0]'),],

															['name' => 'Twou',
															'type' => 'column',
															'data' =>  $latrine2,
															'color' => new JsExpression('Highcharts.getOptions().colors[1]'),],
															
															['name' => 'Latrin',
															'type' => 'column',
															'data' => $latrine3,
															'color' => new JsExpression('Highcharts.getOptions().colors[2]'),],
															
															
															['name' => 'Twalet ijyenik (WC)',
															'type' => 'column',
															'data' => $latrine7,
															'color' => new JsExpression('Highcharts.getOptions().colors[6]'),],

				                                		]
			                                		]
			                                	]);
		                                	?>
		                                </div>
		                             </div>
                    			</div>
                    			<div class="col-md-4">
                    				<div class="box box-warning">
						                <div class="box-header with-border">
						                  <h3 class="box-title">Qualite Maisons</h3>
						                </div><!-- /.box-header -->
						                <div class="box-footer no-padding">
						                  <ul class="nav nav-pills nav-stacked">
						                    <?php $arrlength = sizeof($kalite); ?>
						                    <?php for ($i=0;$i<$arrlength;$i++):?>
						                    	
						                    	<?php if ($kalite[$i][1] == 1): ?>
										        	<li><a href="#">Kay te/kay klise <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 2): ?>
										        	<li><a href="#">Kay palisad <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 3): ?>
										        	<li><a href="#">Ajoupa/Taudis <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 4): ?>
										        	<li><a href="#">Kay you sel etaj an blok <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 5): ?>
										        	<li><a href="#">Kay ki gen +1 etaj <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 6): ?>
										        	<li><a href="#">Tant/tonnelle/abris provisoire <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 7): ?>
										        	<li><a href="#">Appartement <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 8): ?>
										        	<li><a href="#">Kay estyl kolonial <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 9): ?>
										        	<li><a href="#">Complexe industriel, administratif, commercial <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 10): ?>
										        	<li><a href="#">Villa <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php elseif ($kalite[$i][1] == 11): ?>
										        	<li><a href="#">Lot kalite kay <span class="pull-right text-red"><i class="fa fa-angle-down"></i> <?= $kalite[$i][0] ?></span></a></li>
						                    
										        <?php endif; ?>
						                    <?php endfor; ?>
						                    
						                    </ul>
						                </div><!-- /.footer -->
						              </div>
                    			</div>
                    		</div>
                    	</div></div></div></div></div>
	</div>
</section>