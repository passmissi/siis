<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SrcTypeActivites */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Type Activites');
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content-header">
     <h1><?= Html::encode($this->title) ?></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <p>
        <?= Html::a(Yii::t('app', 'Create Type Activites'), ['create'], ['class' => 'btn btn-success']) ?>
       </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'libelle',
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
        
        </div>
    </div>
</section>


   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    


