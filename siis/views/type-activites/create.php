<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TypeActivites */

$this->title = Yii::t('app', 'Create Type Activites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Type Activites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="content-header">
     <h1><?= Html::encode($this->title) ?></h1>
</section>

<section class="content">
    
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

        </div>
    </div>
</section>

