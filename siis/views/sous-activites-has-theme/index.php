<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrSousActivitesHasTheme */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sous Activites Has Themes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sous-activites-has-theme-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Sous Activites Has Theme'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_sous_activite',
            'id_type_theme',
            'id_plan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
