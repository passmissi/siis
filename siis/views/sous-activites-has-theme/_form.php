<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SousActivitesHasTheme */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sous-activites-has-theme-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_sous_activite')->textInput() ?>

    <?= $form->field($model, 'id_type_theme')->textInput() ?>

    <?= $form->field($model, 'id_plan')->textInput(['maxlength' => 45]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
