<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SousActivitesHasTheme */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Sous Activites Has Theme',
]) . ' ' . $model->id_sous_activite;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sous Activites Has Themes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_sous_activite, 'url' => ['view', 'id_sous_activite' => $model->id_sous_activite, 'id_type_theme' => $model->id_type_theme]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="sous-activites-has-theme-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
