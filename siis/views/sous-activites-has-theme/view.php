<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SousActivitesHasTheme */

$this->title = $model->id_sous_activite;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sous Activites Has Themes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sous-activites-has-theme-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id_sous_activite' => $model->id_sous_activite, 'id_type_theme' => $model->id_type_theme], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id_sous_activite' => $model->id_sous_activite, 'id_type_theme' => $model->id_type_theme], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_sous_activite',
            'id_type_theme',
            'id_plan',
        ],
    ]) ?>

</div>
