<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\SousActivitesHasTheme */

$this->title = Yii::t('app', 'Create Sous Activites Has Theme');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sous Activites Has Themes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sous-activites-has-theme-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
