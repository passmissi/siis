<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FamilyAkf */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="family-akf-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'operateur_akf')->textInput() ?>

    <?= $form->field($model, 'operateur_ts')->textInput() ?>

    <?= $form->field($model, 'commune')->textInput() ?>

    <?= $form->field($model, 'localite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'createdOn')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
