<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrFamilyAkf */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Family Akfs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-akf-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Family Akf'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'famille',
            'operateur_akf',
            'operateur_ts',
            'commune',
            'localite',
            // 'createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
