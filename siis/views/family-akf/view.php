<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\FamilyAkf */

$this->title = $model->famille;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Family Akfs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-akf-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'famille' => $model->famille, 'operateur_akf' => $model->operateur_akf], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'famille' => $model->famille, 'operateur_akf' => $model->operateur_akf], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'famille',
            'operateur_akf',
            'operateur_ts',
            'commune',
            'localite',
            'createdOn',
        ],
    ]) ?>

</div>
