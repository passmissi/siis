<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Operateurs;
use app\models\Communes;
use app\models\Famille;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\ScrFamilyAkf */
/* @var $form yii\widgets\ActiveForm */

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);
$commune = Communes::findOne($operateur->commune_lie);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$listlocalite = ArrayHelper::map(Famille::find()->select('localite')->distinct()->where(['commune' => $commune->n_communal])
		->asArray()->orderBy('localite')->all(),
		'localite','localite');
?>

    <?php $form = ActiveForm::begin([
        'action' => ['/socialworker/assignation'],
        'method' => 'get',
    	'options' => ['class' => 'form-inline'],
    ]); ?>

	    <div class="input-group">
		    <?= $form->field($model, 'operateur_akf')->label(false)
		             ->dropDownList($listAKF, ['style'=>'width:100%', 'class' => 'form-control input-sm','prompt' => 'Choisir AKF']) ?>
			<span class="input-group-btn" style="width:0px;"></span>
		     <?= $form->field($model, 'localite')->widget(Select2::classname(), [
				'data' => $listlocalite,
				'language' => 'en',
				'options' => ['placeholder' => 'Select localite','style'=>'width:100%',],
				'size' => 'sm',
				'theme' => Select2::THEME_BOOTSTRAP,
				'pluginOptions' => [
					'allowClear' => true,
			    ],
		    ])->label(false);?>
		    <span class="input-group-btn" style="width:0px;"></span>
		    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
	    </div>
    <?php ActiveForm::end(); ?>
