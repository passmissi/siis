<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FamilyAkf */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Family Akf',
]) . ' ' . $model->famille;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Family Akfs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->famille, 'url' => ['view', 'famille' => $model->famille, 'operateur_akf' => $model->operateur_akf]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="family-akf-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
