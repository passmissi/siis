<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FamilyAkf */

$this->title = Yii::t('app', 'Create Family Akf');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Family Akfs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="family-akf-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
