<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Monitoringsousactivites */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoringsousactivites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringsousactivites-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'mplan',
            'mactivites',
            'plan',
            'activite',
            'sousactivites',
            'mode',
            'type_sous_activites',
            'isExecuted',
            'isExecutedBy',
            'isExecutedOn',
            'isActif',
            'isClosed',
            'typetheme',
            'id_famille',
            'id_membre',
            'date_creation',
            'vaccin',
            'poids',
            'taille',
            'rapportpt',
            'pb',
            'oedeme',
            'en',
            'type_reference',
            'commentaire',
            'quantite',
            'secteur',
            'institution',
            'evenement_type',
            'date_evenement',
            'create_date',
            'evenement',
        ],
    ]) ?>

</div>
