<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Plans;
use app\models\FamilyAkf;
use app\models\Membre;
use kartik\datecontrol\DateControl;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Monitoringsousactivites */
/* @var $form yii\widgets\ActiveForm */

$x =Yii::$app->getRequest()->getQueryParam('id');
$y =Yii::$app->getRequest()->getQueryParam('id2');

$plan = Plans::findOne($x);

$listfamille = ArrayHelper::map(FamilyAkf::find()->where(['operateur_akf' => $plan->akf])->asArray()->all(),
		'famille',
		function($element) {
			$membre = Membre::find()->where(['relation_chef_famille' => 1, 'famille' => $element['famille']])->one();
			return $membre['nom'] ." ". $membre['prenom']." - ".$membre['memberid'];
		});



?>

<?php $form = ActiveForm::begin(); ?>
<div class="modal-body">

	<div class="row">
		<div class="col-lg-6">
			 <?= $form->field($model, 'id_famille')->textInput(['ondblclick' => 'showUser(this.value)']) 
    		->label(null, ['id' => 'id_famille'])?>
		</div>
		
		<div class="col-lg-6">
			<?= $form->field($model, 'id_membre')->textInput(['maxlength' => 20])
			->dropDownList([], ['prompt' => 'Choisir un membre', 'disabled' => 'disabled'])
    		->label(null, ['id' => 'id_membre'])?>
		</div>
	</div>
	

	<div class="row" id="avaccin">
		<div class="col-lg-6">
			<?= $form->field($model, 'vaccin')
			->dropDownList(['1' => 'Polio 0', '2' => 'Polio 1', '3' => 'Polio 2', '4' => 'Polio 3',
							'5' => 'Polio 4', '6' => 'BCG', '7' => 'DTP 1', '8' => 'DTP 2', 
       						'9' => 'DTP 3', '10' => 'Woujol Ribeyol', '11' => 'DT', '12' => 'Pentavalan',
							'13' => 'Completement Vaccine', '14' => 'N/A'], 
                 ['prompt' => 'Choisir un vaccin']) 
    		->label(null, ['id' => 'vaccin'])?>
		</div>
		<div class="col-lg-6">
			 <?= $form->field($model, 'poids')->textInput() 
    		->label(null, ['id' => 'poids'])?>
		</div>
	</div>
	
	<div class="row" id="ataille">
		<div class="col-lg-6">
			<?= $form->field($model, 'taille')->textInput() 
    		->label(null, ['id' => 'taille'])?>
			
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'rapportpt')
			->dropDownList(['1' => '< -4', '2' => '< -3', '3' => '> -3 et < -2', '4' => '>= -2'],
					['prompt' => 'Choisir un rapport pt'])
    		->label(null, ['id' => 'rapportpt'])?>
		</div>
	</div>
    
	<div class="row" id="apb">
		<div class="col-lg-6">
			<?= $form->field($model, 'pb')->textInput() 
    		->label(null, ['id' => 'pb'])?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'oedeme')
			->dropDownList(['1' => 'Absan Edem', '2' => '+', '3' => '++', '4' => '+++'],
					['prompt' => 'Choisir un oedeme'])
    		->label(null, ['id' => 'oedeme'])?>
			
		</div>
	</div>
   
    <div class="row" id="aen">
		<div class="col-lg-12">
			 <?= $form->field($model, 'en')
			 ->dropDownList(['1' => 'Nomal/Sen', '2' => 'Malnitrisyon Egi Modere', 
							 '3' => 'Malnitrisyon Egi Seve san Konplikasyon', '4' => 'Malnitrisyon Egi Seve ak Konplikasyon'],
			 		['prompt' => 'Choisir EN'])
    		->label(null, ['id' => 'en']) ?>
		</div>
	</div>
    
    <div class="row" id="reference">
		<div class="col-lg-6">
			<?= $form->field($model, 'secteur')
			->dropDownList([], ['prompt' => 'Choisir un secteur'])
    		->label(null, ['id' => 'secteur'])?>
		</div>
		<div class="col-lg-6">
			<?= $form->field($model, 'institution')->textInput(['maxlength' => 100]) 
    		->label(null, ['id' => 'institution'])?>
		</div>
	</div>
	
    <div class="row" id="type">
		<div class="col-lg-12">
			<?= $form->field($model, 'typetheme')
			->dropDownList([], ['prompt' => 'Choisir un theme'])
    		->label(null, ['id' => 'typetheme']) ?>
	    </div>
	</div>
	
    <div class="row">
		<div class="col-lg-7">
			<?= $form->field($model, 'commentaire')->textArea(['rows' => '3'])
    		->label(null, ['id' => 'commentaire']) ?>
		</div>
		<div class="col-lg-5">
			<?= $form->field($model, 'date_creation')->input('date', ['required'])
    		->label(null, ['id' => 'date_creation']) ?>
    		
		</div>
	</div>
    
    <div class="row">
		<div class="col-lg-12">
			<?= $form->field($model, 'quantite')->textInput()
    		->label(null, ['id' => 'quantite']) ?>
		</div>
	</div>
    
    <?= $form->field($model, 'evenement_type')->textInput() 
    		->label(null, ['id' => 'evenement_type'])?>

    <?= $form->field($model, 'date_evenement')->textInput()
    		->label(null, ['id' => 'date_evenement']) ?>

    <?= $form->field($model, 'create_date')->textInput()
    		->label(null, ['id' => 'create_date']) ?>

    <?= $form->field($model, 'evenement')->textInput(['maxlength' => 100])
    		->label(null, ['id' => 'evenement']) ?>
    		
     <?= $form->field($model, 'sousactivites')->textInput(['maxlength' => 100])
    		->label(null, ['id' => 'sousactivites']) ?>
    
	<div class="modal-footer">
	     <?= Html::submitButton('close', ['class' => 'btn btn-default', 'name' => 'close']) ?>
	
	     <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'name' => 'creation']) ?>
	
	</div>
</div>
 <?php ActiveForm::end(); ?>


   

     
