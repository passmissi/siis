<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Monitoringsousactivites */

$this->title = Yii::t('app', 'Create Monitoringsousactivites');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Monitoringsousactivites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoringsousactivites-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
