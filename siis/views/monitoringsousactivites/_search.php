<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrMonitoringsousactivites */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoringsousactivites-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'mplan') ?>

    <?= $form->field($model, 'mactivites') ?>

    <?= $form->field($model, 'plan') ?>

    <?= $form->field($model, 'activite') ?>

    <?php // echo $form->field($model, 'sousactivites') ?>

    <?php // echo $form->field($model, 'mode') ?>

    <?php // echo $form->field($model, 'type_sous_activites') ?>

    <?php // echo $form->field($model, 'isExecuted') ?>

    <?php // echo $form->field($model, 'isExecutedBy') ?>

    <?php // echo $form->field($model, 'isExecutedOn') ?>

    <?php // echo $form->field($model, 'isActif') ?>

    <?php // echo $form->field($model, 'isClosed') ?>

    <?php // echo $form->field($model, 'typetheme') ?>

    <?php // echo $form->field($model, 'id_famille') ?>

    <?php // echo $form->field($model, 'id_membre') ?>

    <?php // echo $form->field($model, 'date_creation') ?>

    <?php // echo $form->field($model, 'vaccin') ?>

    <?php // echo $form->field($model, 'poids') ?>

    <?php // echo $form->field($model, 'taille') ?>

    <?php // echo $form->field($model, 'rapportpt') ?>

    <?php // echo $form->field($model, 'pb') ?>

    <?php // echo $form->field($model, 'oedeme') ?>

    <?php // echo $form->field($model, 'en') ?>

    <?php // echo $form->field($model, 'type_reference') ?>

    <?php // echo $form->field($model, 'commentaire') ?>

    <?php // echo $form->field($model, 'quantite') ?>

    <?php // echo $form->field($model, 'secteur') ?>

    <?php // echo $form->field($model, 'institution') ?>

    <?php // echo $form->field($model, 'evenement_type') ?>

    <?php // echo $form->field($model, 'date_evenement') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'evenement') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
