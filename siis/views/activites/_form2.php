<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TypeActivites;
use app\models\User;
use app\models\Operateurs;
use app\models\Familles;
use app\models\Membres;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\Activites;

$listTypeActivites = ArrayHelper::map(TypeActivites::find()->where(['mode' => 2])->asArray()->all(), 'id', 'libelle');

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});

$activites = ArrayHelper::map(Activites::find()->asArray()->all(), 'id', 'id_type_activite');

$script1 = <<< JS
$(document).ready(function(){

});
function getState(val) {
	var titre = document.getElementById('modalTitle').value;
		
	$.ajax({
		type: 'GET',
		url: 'index.php?r=activites/listactivites',
		data: { id: val, id1: document.getElementById('activites-date_activite').value, id2: document.getElementById('activites-id_type_activite').value},
		success: function(data){
			$("#activites-activitesmonitoringcode").html(data);
		},
		error: function() {
			aa = $("#modalTitle").val();
			alert(aa);
		},
	});
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<?php $form = ActiveForm::begin(); ?>
	    <div id="modalBody" class="modal-body">
	    
	    	<?= $form->field($model, 'id_type_activite')->dropDownList(
			    $listTypeActivites, ['prompt' => 'Choisir un type'])->label(null, ['id' => 'type_activite']) ?>

			 <?= $form->field($model, 'date_activite')
		          ->textInput()
		          ->label(null, ['id' => 'date'])?>
		        
		     <?= $form->field($model, 'lieu')
		        ->textInput(['maxlength' => 45])
		        ->label(null, ['id' => 'Lieu']) ?>
		    
		    <?= $form->field($model, 'AKFMonitoringCode')->dropDownList($listAKF, 
				['prompt'=>'Choisir un AKF','onchange' =>'getState(this.value);'])
		        ->label(null, ['id' => 'famille']); ?>
							
		    <?= $form->field($model, 'ActivitesMonitoringCode')
		        ->dropDownList([],['prompt' => 'Choisir une Activite'])
		        ->label(null, ['id' => 'ActivitesMonitoring']) ?>
		        
		    <?= $form->field($model, 'description')->textarea(array('rows'=>3)) ?>
		    
		    <?= $form->field($model, 'formatreunion')->dropDownList(
			    ['Rencontre Administrative' => 'Rencontre Administrative', 'Rencontre Communautaire' => 'Rencontre Communautaire', 'Reunion de Suivi' => 'Reunion de Suivi'], 
                ['prompt' => 'Choisir un format'])->label(null, ['id' => 'format_activite']) ?>
		    
		    
	    </div>
    
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    		 
		</div>
    <?php ActiveForm::end(); ?>