<?php

use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use kartik\detail\DetailView;
use app\models\Operateurs;
use app\models\Activites;
use app\models\Plans;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Activites */

/* $this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Activites'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title; */

$this->title = 'Details Activites';
$x =Yii::$app->getRequest()->getQueryParam('id');
$activite = Activites::findOne($x);
$plan = Plans::findOne($activite->id_plan);

?>

<section class="content-header">
	<h1>
       Plan
       <small>Details Activites <strong>
       </strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Details Activites</li>
    </ol>
</section>

<section class="content">
	<div class="row">
			<div class="col-xs-12">
	           <div class="box box-solid">
	               <div class="box-body">
	                    <div class="row">
	                    	<div class="col-md-3 col-sm-4">
                    		
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	if($plan->mode == 1){
                                                		echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';

                                                	}elseif ($plan->mode == 2){
                                                		echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li class="active">'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
														
	                                                }
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
	                    	
	                    	<div class="col-md-9 col-sm-8">
	                    		<div class="row">
                    				<div  class="col-md-12">
                    					 <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'id_plan',
            'id_type_activite',
            'date_activite',
            [
	            'attribute'=>'date_activite',
	            'format'=>'date',
	            'type'=>DetailView::INPUT_DATE,
	            'widgetOptions'=>[
	            	'pluginOptions'=>['format'=>'yyyy-mm-dd'],
	            	'data'=>ArrayHelper::map(Operateurs::find()->orderBy('nom')->asArray()->all(), 'id', 'nom'),
	             ],
	              'inputWidth'=>'40%'
	         ],
            'lieu',
            'id_famille',
            'description',
        ],
    ]) ?>
                    				</div>
                    			</div>
	                    	</div>
	                    </div>
	               </div>
	          </div>
	       </div>
	</div>
</section>

