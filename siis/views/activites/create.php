<?php

use yii\helpers\Html;
use app\models\Operateurs;
use app\models\TypeActivites;
use app\models\User;
use app\models\Plans;
use app\models\FamilyAkf;
use app\models\Activites;

/* @var $this yii\web\View */
/* @var $model app\models\Activites */

$this->title = 'Activites';
$x =Yii::$app->getRequest()->getQueryParam('id');
$plan = Plans::findOne($x);
$a = new Plans();
$date = "'$plan->annee-".$a->getMonthNumber($plan->mois)."-01'";
$titre = '';
$script = <<< JS
$(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {
            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }
		
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        m = 'December',
        y = date.getFullYear();
		
		
		$('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
		
          buttonText: {
            today: 'Aujourd\'hui',
            month: 'mois',
            week: 'semaine',
            day: 'jour'
          },
		
          //Random default events
         
		  
          events: '?r=activites/calendar1&id=$x',
		  
		  dayClick: function(date, jsEvent, view) {
				alert('Clicked on: ' + date.format());
		 	},
				
          viewRender: function(view, element) {
		    $('#calendar').fullCalendar( 'gotoDate', $date );
		  },
		    		
          editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
		
		  eventDrop: function(event, delta, revertFunc) {
			if (!confirm("Are you sure about this change?")){
				revertFunc();
			}
		  },
			
		  eventClick:  function(event, jsEvent, view) {
            
          },
		
		  eventDragStop: function( event, jsEvent, ui, view ) {
              alert(event.title + " was dropped on " + event.start.format());
		  },
		  
		  eventReceive: function( event ) {
			      
            
		    var title = event.title;
		
		    if (title == 'Visite Domiciliaire') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';  
		    	
		    	var divOne6 = document.getElementById('check');
            	divOne6.style.display='block';
		    	
		    	var divOne = document.getElementById('famille');
            	divOne.style.display='block';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='block';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "1";
		        document.getElementById('Lieu').style.display = 'none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
		        var divOne6 = document.getElementById('activites-lieu');
            	divOne6.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		    		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		
			} else if(title == 'Poste de rassemblement') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "2";
		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		    	
		    	var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
		        document.getElementById('Lieu').style.display = 'block';
		        document.getElementById('activites-lieu').style.display = 'block';
		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
		
		        var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			} else if(title == 'Club des Meres') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "3";
		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			} else if(title == 'Club des Jeunes') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "4";
		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			}else if(title == 'Visite Ecole') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "5";
		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
				var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
				var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			} else if(title == 'Rencontre') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "6";
		
		    	document.getElementById("activites-formation").disabled = true;
		    	document.getElementById("activites-promotion").disabled = false;
		    	document.getElementById("activites-distribution").disabled = false;
		        document.getElementById("activites-sousactivites").disabled = false;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			} else if(title == 'Travail Administratif') {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "7";
		
		    	document.getElementById("activites-formation").disabled = false;
		    	document.getElementById("activites-promotion").disabled = true;
		    	document.getElementById("activites-distribution").disabled = true;
		        document.getElementById("activites-sousactivites").disabled = true;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
		        var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
		        var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
		
			} else {
		    	var divOne = document.getElementById('activites-id_famille');
            	divOne.value = '';
		    		
				document.getElementById("activites-id_type_activite").selectedIndex = "8";
		
		    	document.getElementById("activites-formation").disabled = false;
		    	document.getElementById("activites-promotion").disabled = true;
		    	document.getElementById("activites-distribution").disabled = true;
		        document.getElementById("activites-sousactivites").disabled = true;
		    		
		        var divOne4 = document.getElementById('activites-id_type_activite');
            	divOne4.style.display='none';
		        var divOne5 = document.getElementById('activites-date_activite');
            	divOne5.style.display='none';
				var divOne6 = document.getElementById('check');
            	divOne6.style.display='none';
		    		
				document.getElementById('Lieu').style.display = 'block';
				document.getElementById('activites-lieu').style.display = 'block';
		
				var divOne = document.getElementById('famille');
            	divOne.style.display='none';  
		        var divOne1 = document.getElementById('activites-id_famille');
            	divOne1.style.display='none';
		        var divOne2= document.getElementById('type_activite');
            	divOne2.style.display='none';
				var divOne3 = document.getElementById('date');
            	divOne3.style.display='none';
				var element = document.getElementById("activites-date_activite");
                element.value = event.start.format();
			}
		
			$('#modalTitle').html(title);
            $('#fullCalModal').modal('show');
		  },
		  
          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }
		
        });
		
		function custom_sort(a, b) {
		    return new Date(a.start).getTime() - new Date(b.start).getTime();
		}
		
		events_array.sort(custom_sort);
    
    	$('#calendar').fullCalendar('gotoDate', "2016-11-10");
		
        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
          e.preventDefault();
          //Save color
          currColor = $(this).css("color");
          //Add color effect to button
          $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
          e.preventDefault();
          //Get value and make sure it is not null
          var val = $("#new-event").val();
          if (val.length == 0) {
            return;
          }

          //Create events
          var event = $("<div />");
          event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
          event.html(val);
          $('#external-events').prepend(event);

          //Add draggable funtionality
          ini_events(event);

          //Remove event from text input
          $("#new-event").val("");
        });
      });
JS;
$this->registerJs($script, \yii\web\View::POS_END);
		
?>

<section class="content-header">
	<h1>
       Activites
       <small>Activites du Plan #<strong><?php echo $plan->id ?></strong> pour <strong>
       <?php 
         
           $op = Operateurs::findOne($plan->akf);
           if($op!=null)
           echo ' '.$op->nom.' '.$op->prenom ?></strong></small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Ajouter Activites</li>
    </ol>
</section>

<section class="content">
    <div class="row">
    	
    </div>
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            
                                            <div class="row">
                                            	<div class="col-md-12">
                                            		<div class="btn-group container">
                                            			<?= Html::a('  <i class="glyphicon glyphicon glyphicon-share"></i>  Finir Plan', 
                                            					['/socialworker/index'], ['class' => 'btn btn-primary', 'type' => 'button']);?>
                                            			<?php 
                                            				$pan = Plans::findOne($x);
                                            				$count = Activites::find()->where(['id_plan' => $pan->id])->count();
                                            				if($pan->dimagi == 1 && $count >= 1){
                                            					echo Html::a('  <i class="glyphicon glyphicon-cloud"></i>  Dimagi',
                                            							['/socialworker/export2', 'id' => $x, 'id2' => 0], ['class' => 'btn btn-warning','type' => 'button']);
                                            				}elseif($pan->dimagi == 1 && $count == 0){
                                            					echo Html::a('  <i class="glyphicon glyphicon-cloud"></i>  Dimagi',
                                            							['/socialworker/export2', 'id' => $x, 'id2' => 0], ['class' => 'btn btn-warning','disabled' => true, 'type' => 'button']);
                                            				}
                                            				?>
                                            			<?= Html::a('  <i class="glyphicon glyphicon-duplicate"></i>  PDF', 
                                            					['/plans/pdf', 'id' => $x], ['class' => 'btn btn-primary', 'type' => 'button','target' => '_blank']);?>
                                            		</div>
                                            	</div>
                                            </div>
                                            
                                      
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                    <?php 
                                                	
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	echo  '<li class="header">Actions</li>';
                                                	if($plan->mode == 1){
                                                		echo  '<li class="active">'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';

                                                	}elseif ($plan->mode == 2){
                                                		echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
														echo  '<li>'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
														echo  '<li class="active">'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
														
	                                                }
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    		<div class="row">
                    	       <div class="col-md-4">
                    	       		<div class="box box-warning">
						                <div class="box-header  with-border">
						                  <h4 class="box-title">Activites</h4>
						                </div>
						                <div class="box-body">
						                  <!-- the events -->
						                  <div id='external-events'>
						                    <div class='external-event bg-green' data-event='{"title":"Visite Domiciliaire"}' data-duration='03:00'>Visite Domiciliaire</div>
						                    <div class='external-event bg-yellow' data-event='{"title":"Poste de rassemblement"}' data-duration='03:00'>Poste de rassemblement</div>
						                    <div class='external-event bg-aqua' data-event='{"title":"Club des Meres"}' data-duration='03:00'>Club des Meres</div>
						                    <div class='external-event bg-teal' data-event='{"title":"Club des Jeunes"}' data-duration='03:00'>Club des Jeunes</div>
						                    
						                    <div class='external-event bg-light-blue' data-event='{"title":"Visite Ecole"}' data-duration='03:00'>Visite Ecole</div>
						                    <div class='external-event bg-red' data-event='{"title":"Rencontre"}' data-duration='03:00'>Rencontre</div>
						                    <div class='external-event bg-olive' data-event='{"title":"Travail Administratif"}' data-duration='03:00'>Travail Administratif</div>
						                    <div class='external-event bg-maroon' data-event='{"title":"Formation"}' data-duration='03:00'>Formation</div>
						                    <div class="checkbox">
						                      <label for='drop-remove'>
						                        <input type='checkbox' id='drop-remove' />
						                        remove after drop
						                      </label>
						                    </div>
						                  </div>
						                </div><!-- /.box-body -->
						              </div><!-- /. box -->			
                    	       </div>
                    	       <div class="col-md-8">
                    	       		<div class="box box-primary">
						                <div class="box-body no-padding">
						                  <!-- THE CALENDAR -->
						                  <div id="calendar"></div>
						                </div><!-- /.box-body -->
						              </div><!-- /. box -->
                    	       </div>
                    	    </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
   </div>
</section>

<div id="fullCalModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span> <span class="sr-only">close</span></button>
				<h4 id="modalTitle" class="modal-title"></h4>
			</div>
			<?= $this->render('_form', [
		        'model' => $model,
		    ]) ?>
		</div>
	</div>
</div>
