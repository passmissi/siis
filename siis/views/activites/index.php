<?php

use yii\helpers\Html;
use kartik\grid\GridView;


use yii\widgets\ActiveForm;
use app\models\User;
use app\models\Operateurs;

use yii\helpers\ArrayHelper;
use app\models\Activites;
use app\models\Plans;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrActivites */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Activites';

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$listAKF = ArrayHelper::map(Operateurs::find()->where(['akf_lie' => $operateur->id])->asArray()->all(),
		'id',
		function($element) {
			return $element['nom'] ." ". $element['prenom'] ;
		});


$script2 = <<< JS
function showUser2(str) {
	if(str == ''){
		document.getElementById("plans-annee").disabled = true;
	}
	else{
		document.getElementById("plans-annee").disabled = false;
	}
}
JS;
$this->registerJs($script2, \yii\web\View::POS_END);

$script = <<< JS
function showUser1(str) {
	if(str == ''){
		document.getElementById("plans-mois").disabled = true;
	}
	else{
		document.getElementById("plans-mois").disabled = false;
	}
}
JS;
$this->registerJs($script, \yii\web\View::POS_END);

$script1 = <<< JS
function showUser(str) {
	if(str == ''){
		
	}
	else{
		var akf = document.getElementById("plans-akf").value;
		var ann = document.getElementById("plans-mois").value;
		$.ajax({
			type: 'GET',
			url: 'index.php?r=socialworker/verifplan',
			data: { akf: akf, mois: ann, annee : str, mode : 1},
			success: function(data){
				if(data >= 1){
					var divOne448= document.getElementById('msg');
	            	divOne448.style.display='block';
					document.getElementById("ok").disabled = true;
					$("#myModal").modal("show");
				}else{
					var divOne448= document.getElementById('msg');
	            	divOne448.style.display='none';
					document.getElementById("ok").disabled = false;
					$("#myModal").modal("show");
				}
			},
			error: function() {
			},
		});
	}
}
JS;
$this->registerJs($script1, \yii\web\View::POS_END);
?>

<section class="content-header">
	<h1>
       Dashboard
       <small>Centre de Gestion</small>
    </h1>
    <ol class="breadcrumb">
       <li><a href="#"><i class="fa fa-dashboard"></i> Accueil</a></li>
       <li class="active">Dashboard</li>
    </ol>
</section>

<section class="content">
	<div class="row">
		<div class="col-xs-12">
           <div class="box box-solid">
               <div class="box-body">
                    <div class="row">
                    	<div class="col-md-3 col-sm-4">
                    		<div class="box-header">
                                                <i class="fa fa-inbox"></i>
                                                <h3 class="box-title">PANEL DE GESTION</h3>
                                            </div>
                                            <!-- compose message btn -->
                                             <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-plus-square"></i> Creer un plan de travail (AKF)</a>
                                            <!-- Navigation - folders-->
                                            <div style="margin-top: 15px;">
                                                <ul class="nav nav-pills nav-stacked">
                                                	<?php 
                                                	$user = User::findOne(Yii::$app->user->identity->id);
                                                	$operateur = Operateurs::findOne($user->idOperateurs);
                                                	
                                                	echo  '<li class="header">Actions</li>';
                                                	
                                                	echo  '<li>'. Html::a('<i class="fa fa-retweet"></i>Plans AKF', ['/plans/index', 'id' => $operateur->id]); '.</li>';
                                                	echo  '<li class="active">'. Html::a('<i class="fa fa-angle-up"></i>Activites AKF', ['/activites/index']); '.</li>';
                                                	echo  '<li>'.Html::a('<i class="fa  fa-ellipsis-v"></i> Plans TS', ['/plans/index2']); '.</li>';
                                                	
                                                	?>
                                                </ul>
                                            </div>
                    	</div>
                    	<div class="col-md-9 col-sm-8">
                    	
                    	     <div class="row pad">
						    	<div class="col-sm-5">
						    		
                                </div>
                                <div class="col-sm-7">
                                	<?php echo $this->render('_search', ['model' => $searchModel, 'id' => $operateur->id]); ?>
                                </div>
						    </div>
						    
                    	    		 <div class="box">
                    	    		        <div class="box-header with-border">
			                    	          	<h3 class="box-title">Liste des Activites</h3>
			                    	          </div>
			                    	   		<div class="box-body table-responsive no-padding">
			                    	    		<?php 
					                    			echo GridView::widget([
												        'dataProvider' => $dataProvider,
														'export' => false,
														'showPageSummary' => false,
														'bordered' => false,
														'responsive' => true,
														'hover' => true,
														'rowOptions' => function($model1){
															if($model1->isDone != 0 && $model1->isDone != null){
																return ['class' => 'success',];
															}
															elseif($model1->isDone == 0 || $model1->isDone == null){
																return ['class' => 'warning'];
															}
														},
												        //'filterModel' => $searchModel,
												        'columns' => [
												            ['class' => 'yii\grid\SerialColumn'],
                                                             'id',
												            'id_plan',
												            'idTypeActivite.libelle',
												            'date_activite',
												            'lieu',
															[
															'header'=>'Etat',
															'format'=>'raw',
															'value'=> function ($model1, $key, $index, $widget) {
																if($model1->isDone != 0 && $model1->isDone != null){
																	return '<span class="label label-success">realise</span>';
																}
																elseif($model1->isDone == 0 || $model1->isDone == null){
																	return '<span class="label label-warning">non realise</span>';
																}
															},
															],
												            [
																'class' => 'yii\grid\ActionColumn',
																'template' => '{activites} {details} {effacer}',
																'buttons' => [
																	'activites' => function ($url,$model) {
																		$count = Plans::find()->where(['id' => $model->id_plan, 'isMonitoring' => 0,
																				'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0, 'mode' => 1])->count();
																		return !$count ? Html::a(
																				'<span class="glyphicon glyphicon-edit"></span>',
																				['activites/view', 'id' => $model->id],
																				['class' => 'btn btn-xs btn-default']): "";
																	},
																	'details' => function ($url,$model) {
																		return Html::a(
																				'<span class="glyphicon glyphicon-folder-open"></span>',
																				['sous-activites/index', 'id' => $model->id],
																				['class' => 'btn btn-xs btn-warning']);
																	},
																	'effacer' => function ($url,$model) {
																		$count = Plans::find()->where(['id' => $model->id_plan, 'isMonitoring' => 0,
																				'isExecuted' => 1, 'isDeleted' => 0, 'isClosed' => 0])->count();
																		return !$count ? Html::a(
																				'<span class="fa fa-trash-o"></span>',
																				['activites/delete1', 'id' => $model->id],['class' => 'btn btn-xs btn-danger', 'data-method' => 'post', 'data-confirm'=>'Voulez vous vraiment effacer cette activite?', 'onmouseover'=>'Tip("Effacer AKF")', 'onmouseout'=>'UnTip()']) : "";
																	},
																],
															],
												        ],
												    ]);
					                    		?>
		
			                    	    	</div>
			                    	 </div>
                    	</div>
                    </div>
               </div>
           </div>
        </div>
	</div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"> Plan - AKF </h4>
      </div>
      <?php $form = ActiveForm::begin(); ?>
	      <div class="modal-body">
	      	<div id="msg" class="row" style="display : none;">
				<div class="col-md-12">
					<div class="alert alert-danger alert-dismissable">
						 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						 Duplicate Planning not allowed. Choose a another month.
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?= $form->field($model, 'akf')->dropDownList(
					   	$listAKF, ['prompt' => 'Choisir un AKF','onchange' => 'showUser1(this.value)'])->label(null, ['id' => 'type_activite'])
			    	?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<?= $form->field($model, 'mois')->dropDownList(
				          ['Janvier'=>'Janvier','Fevrier'=>'Fevrier', 'Mars'=>'Mars','Avril'=>'Avril',
		                   'Mai'=>'Mai','Juin'=>'Juin', 'Juillet'=>'Juillet','Aout'=>'Aout',
		                   'Septembre'=>'Septembre', 'Octobre'=>'Octobre', 'Novembre'=>'Novembre',
		                   'Decembre'=>'Decembre'], ['prompt'=>'--Choisir un mois --', 'disabled' => true,'onchange' => 'showUser2(this.value)']
				    );?>
				</div>
				<div class="col-md-6">
					 <?= $form->field($model,'annee')
		          		->dropDownList(
				          ['2012'=>'2012','2013'=>'2013', '2014'=>'2014','2015'=>'2015',
		                   '2016'=>'2016','2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020'], ['prompt'=>'--Choisir une annee --','disabled' => true,'onchange' => 'showUser(this.value)']
				    )?>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					 <?= $form->field($model, 'dimagi')->checkbox(array('label'=>''))
							->label('Will upload to Dimagi'); ?> 
				</div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'id' => 'ok']) ?>
	      </div>
      <?php ActiveForm::end(); ?>
    </div>
  </div>
</div>