<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TypeActivites;
use app\models\User;
use yii\helpers\ArrayHelper;
use app\models\Plans;
use app\models\Operateurs;

use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\ScrActivites */
/* @var $form yii\widgets\ActiveForm */

$listTypeActivites = ArrayHelper::map(TypeActivites::find()->asArray()->all(), 'id', 'libelle');

$user = User::findOne(Yii::$app->user->identity->id);
$operateur = Operateurs::findOne($user->idOperateurs);

$listplan = ArrayHelper::map(Plans::find()->where(['ts' => $operateur->id, 'mode' => 1])->asArray()->all(),
		'id',
		function($element) {
			$akf = Operateurs::findOne($element['akf']);
			if($akf!=null)
			return $element['mois'] ." ". $element['annee']." ".$akf->nom." ".$akf->prenom ;
			else
				return null;
		});
?>


<?php $form = ActiveForm::begin([
	    'action' => ['index'],
	    'method' => 'get',
		'options' => ['class' => 'form-inline'],
	]); ?>
	    <div class="input-group">
	      <?= $form->field($model, 'id_plan')->widget(Select2::classname(), [
				'data' => $listplan,
				'language' => 'en',
				'options' => ['placeholder' => 'Choisir un plan','style'=>'width:100%',],
				'size' => 'sm',
				'theme' => Select2::THEME_BOOTSTRAP,
				'pluginOptions' => [
					'allowClear' => true,
			    ],
		    ])->label(false);?>
		  
		  <span class="input-group-btn" style="width:0px;"></span>
	      <?= $form->field($model, 'id_type_activite')->dropDownList(
			  $listTypeActivites, ['prompt' => 'Choisir un type', 'class'=>'form-control input-sm'])->label(false)
	      ?>
	      <span class="input-group-btn" style="width:0px;"></span>
		  <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-sm btn-primary']) ?>
		</div>
<?php ActiveForm::end(); ?>


