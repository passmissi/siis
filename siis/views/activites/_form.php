<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\TypeActivites;
use app\models\User;
use app\models\Operateurs;
use app\models\Familles;
use app\models\Membres;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\Plans;
use app\models\FamilyAkf;
use app\models\Membre;

/* @var $this yii\web\View */
/* @var $model app\models\Activites */
/* @var $form yii\widgets\ActiveForm */

$listTypeActivites = ArrayHelper::map(TypeActivites::find()->asArray()->all(), 'id', 'libelle');

$x =Yii::$app->getRequest()->getQueryParam('id');
$plan = Plans::findOne($x);
if($plan!=null)
$listfamille = ArrayHelper::map(FamilyAkf::find()->where(['operateur_akf' => $plan->akf])->asArray()->all(),
		'famille',
		function($element) {
			$membre = Membre::find()->where(['memberid' => 1, 'famille' => $element['famille']])->one();
			if($membre!=null)
			return $membre['nom'] ." ". $membre['prenom'] ;
			else return null;
		});
else 
	$listfamille = ArrayHelper::map(FamilyAkf::find()->asArray()->all(),
			'famille',
			function($element) {
				$membre = Membre::find()->where(['memberid' => 1, 'famille' => $element['famille']])->one();
				if($membre!=null)
					return $membre['nom'] ." ". $membre['prenom'] ;
				else return null;
			});

//$user = User::findOne(Yii::$app->user->identity->id);
//$operateur = Operateurs::findOne($user->idOperateurs);
//$mem = new Membres();
//$famille = $mem->getFamilleByCommune($operateur->commune_lie);

$script = <<< JS
function showUser(str) {
	if(str == ''){
		document.getElementById("activites-family_name").value = '';
	}
	else{
		$.ajax({
		type: 'GET',
		url: 'index.php?r=socialworker/listmembre3',
		data: { id: str },
		success: function(data){
			document.getElementById("activites-family_name").value = data;
		},
		error: function() {
		},
	});
	}
}
JS;
$this->registerJs($script, \yii\web\View::POS_BEGIN);
?>

    <?php $form = ActiveForm::begin(); ?>
	    <div id="modalBody" class="modal-body">
	    
	    	<?= $form->field($model, 'id_type_activite')->dropDownList(
			    	$listTypeActivites, ['prompt' => 'Choisir un type'])->label(null, ['id' => 'type_activite'])
	    	        ?>

			 <?= $form->field($model, 'date_activite')
		          ->textInput()
		          ->label(null, ['id' => 'date'])?>
		    
		    <div class="row" id="check">
		    	<div class="col-lg-6">
				   <?= $form->field($model, 'id_famille')->textInput(['ondblclick' => 'showUser(this.value)']) 
    		            ->label(null, ['id' => 'famille'])?>
		    	</div>
		    	<div class="col-lg-6">
		    		<?= $form->field($model, 'family_name')->textInput(['maxlength' => 45, 'readonly' => 'readonly']) 
    		            ->label(null, ['id' => 'name_famille'])?>
		    	</div>
		    </div>
		    
		    <?= $form->field($model, 'lieu')
		        ->textInput(['maxlength' => 45])
		        ->label(null, ['id' => 'Lieu']) ?>
		        
		    <?= $form->field($model, 'description')->textarea(array('rows'=>3)) ?>
		    
		     <div class="row">
		    	<div class="col-lg-4">
		    		<?php  
		    		      echo '<label class="control-label">Promotion</label>';
		    		      echo Select2::widget([
		    					'model' => $model,
		    					'attribute' => 'promotion',
		    					'name' => 'promotion',
                                'pluginLoading' => false,
		    					'data' => app\models\TypeThemes::getThemes(1),
		    					'options' => [
		    							'placeholder' => 'Select a promotion...',
		    							'multiple' => true
		    						], 
								]);
		    		?>      
		    	</div>
		    	<div class="col-lg-4">
		    		<?php 
		    		      echo '<label class="control-label">Distribution</label>';
		    		      echo Select2::widget([
		    					'model' => $model,
		    					'attribute' => 'distribution',
		    					'name' => 'distribution',
                                'pluginLoading' => false,
		    					'data' => app\models\TypeThemes::getThemes(2),
		    					'options' => [
		    							'placeholder' => 'Select a distribution...',
		    							'multiple' => true
		    						], 
								]);?>
		    	</div>
		    	
		    	<div class="col-lg-4">
		    		<?php 
				    		echo '<label class="control-label">Formation</label>';
				    		echo Select2::widget([
				    				'model' => $model,
				    				'attribute' => 'formation',
				    				'name' => 'formation',
				    				'pluginLoading' => false,
				    				'data' => app\models\TypeThemes::getThemes(3),
				    				'options' => [
				    				'placeholder' => 'Select a formation...',
				    				'multiple' => true
				    				],
				    										]);?>
		    		
		    		
		    	</div>
		    </div>
		    
		    <div class="row">
		    	<div class="col-lg-12">
		    		<?php $list = ['3'=>'reference','6'=>'vaccination','7'=>'controlepoids','8'=>'Appui TS']; ?>
		    		<?= $form->field($model, 'sousactivites')->checkboxList($list) ?>
		    	</div>
		    </div>
		    
	    </div>
    
        <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    		 
		</div>
    <?php ActiveForm::end(); ?>


