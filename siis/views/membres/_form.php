<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Membres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_family')->textInput(['maxlength' => 20]) ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'id_person')->textInput(['maxlength' => 12]) ?>

    <?= $form->field($model, 'firstname')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'nickname')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'sex')->textInput() ?>

    <?= $form->field($model, 'age')->textInput() ?>

    <?= $form->field($model, 'date_birth')->textInput() ?>

    <?= $form->field($model, 'document_type')->textInput() ?>

    <?= $form->field($model, 'document_number')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'b7')->textInput() ?>

    <?= $form->field($model, 'b8')->textInput() ?>

    <?= $form->field($model, 'b9')->textInput() ?>

    <?= $form->field($model, 'b10')->textInput() ?>

    <?= $form->field($model, 'b11')->textInput() ?>

    <?= $form->field($model, 'b12')->textInput() ?>

    <?= $form->field($model, 'b13')->textInput() ?>

    <?= $form->field($model, 'b14')->textInput() ?>

    <?= $form->field($model, 'b15')->textInput() ?>

    <?= $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'date_register')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
