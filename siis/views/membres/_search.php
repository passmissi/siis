<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrMembres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="membres-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_family') ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_person') ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'nickname') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'age') ?>

    <?php // echo $form->field($model, 'date_birth') ?>

    <?php // echo $form->field($model, 'document_type') ?>

    <?php // echo $form->field($model, 'document_number') ?>

    <?php // echo $form->field($model, 'b7') ?>

    <?php // echo $form->field($model, 'b8') ?>

    <?php // echo $form->field($model, 'b9') ?>

    <?php // echo $form->field($model, 'b10') ?>

    <?php // echo $form->field($model, 'b11') ?>

    <?php // echo $form->field($model, 'b12') ?>

    <?php // echo $form->field($model, 'b13') ?>

    <?php // echo $form->field($model, 'b14') ?>

    <?php // echo $form->field($model, 'b15') ?>

    <?php // echo $form->field($model, 'id_user') ?>

    <?php // echo $form->field($model, 'date_register') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
