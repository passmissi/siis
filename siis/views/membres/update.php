<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Membres */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Membres',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id_family' => $model->id_family, 'id' => $model->id, 'id_person' => $model->id_person]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="membres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
