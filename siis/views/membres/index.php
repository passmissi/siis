<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrMembres */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Membres');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membres-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Membres'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_family',
            'id',
            'id_person',
            'firstname',
            'name',
            // 'nickname',
            // 'sex',
            // 'age',
            // 'date_birth',
            // 'document_type',
            // 'document_number',
            // 'b7',
            // 'b8',
            // 'b9',
            // 'b10',
            // 'b11',
            // 'b12',
            // 'b13',
            // 'b14',
            // 'b15',
            // 'id_user',
            // 'date_register',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
