<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Membres */

$this->title = Yii::t('app', 'Create Membres');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membres-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
