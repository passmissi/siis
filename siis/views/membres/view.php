<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Membres */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Membres'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="membres-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id_family' => $model->id_family, 'id' => $model->id, 'id_person' => $model->id_person], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id_family' => $model->id_family, 'id' => $model->id, 'id_person' => $model->id_person], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_family',
            'id',
            'id_person',
            'firstname',
            'name',
            'nickname',
            'sex',
            'age',
            'date_birth',
            'document_type',
            'document_number',
            'b7',
            'b8',
            'b9',
            'b10',
            'b11',
            'b12',
            'b13',
            'b14',
            'b15',
            'id_user',
            'date_register',
        ],
    ]) ?>

</div>
