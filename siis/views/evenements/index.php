<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ScrEvenements */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Evenements');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evenements-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Evenements'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'famille',
            'id_membre',
            'plan',
            'mplan',
            // 'evenements',
            // 'date_evenements',
            // 'commentaires',
            // 'createdBy',
            // 'createdOn',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
