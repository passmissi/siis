<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Evenements */

$this->title = Yii::t('app', 'Create Evenements');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Evenements'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evenements-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
