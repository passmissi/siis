<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\RefEvenements;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Evenements */
/* @var $form yii\widgets\ActiveForm */
$objectif2 = ArrayHelper::map(RefEvenements::find()->asArray()->all(), 'id', 'libelle');
?>

<div class="evenements-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="modal-body">
		<div class="row">
			<div class="col-lg-6">
				<?= $form->field($model, 'famille')->textInput(['maxlength' => true]) ?>
				
			</div>
			<div class="col-lg-6">
				<?= $form->field($model, 'id_membre')->textInput() ?>
				
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-6">
				<?= $form->field($model, 'evenements')->dropDownList($objectif2, 
                 ['prompt' => 'Choisir un evenement'])  ?>
				
			</div>
			<div class="col-lg-6">
				<?= $form->field($model, 'date_evenements')->input('date', ['required']) ?>
				
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<?= $form->field($model, 'commentaires')->textArea(['rows' => '3']) ?>
				
			</div>
		</div>
		<div class="modal-footer">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','name' => 'evenements']) ?>
  
		</div>
	</div>
    <?php ActiveForm::end(); ?>

</div>
