<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ScrEvenements */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evenements-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'famille') ?>

    <?= $form->field($model, 'id_membre') ?>

    <?= $form->field($model, 'plan') ?>

    <?= $form->field($model, 'mplan') ?>

    <?php // echo $form->field($model, 'evenements') ?>

    <?php // echo $form->field($model, 'date_evenements') ?>

    <?php // echo $form->field($model, 'commentaires') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'createdOn') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
