<?php

namespace app\commands;
use yii\console\Controller;

use Yii;
use yii\helpers\Url;
use app\models\User;
use app\modules\auth\models\ResetPasswordForm;
use app\models\Operateurs;
use app\models\Plans;
use app\models\Activites;
use Yii\db;
use app\models\Communes;
use app\models\Evenements;
use kartik\mpdf\Pdf;
use app\models\ScrPlans;
use yii\data\ActiveDataProvider;
use app\models\ScrOperateurs;
use app\models\FamilyAkf;
use app\models\TypeThemes;
use app\models\Monitoringplan;
use app\models\TypeActivites;
use app\models\DataTabulation;
use app\models\Monitoringactivites;
use app\models\Monitoringsousactivites;
use app\models\SousActivites;
use app\models\Membre;
use app\models\Famille;
use app\models\ScrFamilyAkf;
use app\models\ScrMembre;
use app\models\Chroniques;
use app\models\ObjectifsVie;
use app\models\Mort;
use app\models\ScrMonitoringplan;
use app\models\ScrMonitoringsousactivites;
use app\models\TypeSousActivites;
use app\models\DataFamille;
use app\models\DataMembre;
use app\models\DataIntegration;
use app\models\DataSummaryIntegration;
use app\models\DetailsSummaryIntegration;

 
Class MakefileController extends Controller
{
	public function actionTakedimagiform(){
		
		$rootyii = realpath(dirname(__FILE__).'/../');
		$this->write2($rootyii.'/web/data_integration/result2.txt', 1, 1);
		$temoin = DataSummaryIntegration::find()->count();
		$today = date('Y-m-d');
		$tenday_earlier = date('Y-m-d', strtotime($today. ' - 10 days'));
		$lenght = $this->totalCountSuivi($temoin,$today,$tenday_earlier);
		$j = 0;
		$no = 0;
		
		for($i=0; $i < $lenght; $i++)
		{
			$this->write2($rootyii.'/web/data_integration/result3.txt', 2, 2);
			$connected = @fsockopen("www.commcarehq.com", 80);
			if($connected){
				$indice = $i * 100;
				$data = $this->downLoadDataSuivi($temoin, $today, $tenday_earlier, $indice);
				$obj = array();
				$obj = $data['objects'];
				
				if (is_array($obj) && $this->is_iterable($obj))
				{
					foreach ($obj as $item) {
						$tabulation = DataTabulation::find()->where(['form_id' => $item['id']])->count();
						if($tabulation == 0 && array_key_exists('id', $item) 
								&& $item['form']['@name'] == 'Vizit Kay Fanmi'
								&& array_key_exists('@name', $item['form'])){
							$name = $rootyii."/web/data_integration/vizit_kay/".$item['id'].'.json';
							$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
							$j++;
						}elseif($tabulation == 0 && array_key_exists('id', $item) 
								&& $item['form']['@name'] == 'Vaksinasyon'
								&& array_key_exists('@name', $item['form'])){
							$name = $rootyii."/web/data_integration/vaksinasyon/".$item['id'].'.json';
							$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
							$j++;
						}elseif($tabulation == 0 && array_key_exists('id', $item) 
								&& $item['form']['@name'] == 'Aktivite Gwoup - Manb Pa W'
								&& array_key_exists('@name', $item['form'])){
							$name = $rootyii."/web/data_integration/manbpaw/".$item['id'].'.json';
							$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
							$j++;
						}elseif($tabulation == 0 && array_key_exists('id', $item) 
								&& $item['form']['@name'] == 'Aktivite Gwoup - Manb L�t AKF'
								&& array_key_exists('@name', $item['form'])){
							$name = $rootyii."/web/data_integration/manblot/".$item['id'].'.json';
							$this->writeJson($name, $item, $item['id'], $item['form']['@name']);
							$j++;
						}
					}
				}
			}else{
				$no++;
				$this->write2($rootyii.'/web/data_integration/result3.txt', 2, 2);
			}
		}
		
		$this->write2($rootyii.'/web/data_integration/result.txt', $j, $no);
	}
	
	private function write2($name, $ok, $no){
		try {
			$myfile = fopen($name, "w") or die("Unable to open file!");
			$data = $ok." FORMULAIRES RECUS ---- ".$no." ECHEC CAUSES INTERNET ----";
			fwrite($myfile, $data);
			fclose($myfile);
		}catch (Exception $e) {
			unlink($name);
		}
	}
	
	private function writeJson($name, $item, $id, $form){
		try {
			$myfile = fopen($name, "w") or die("Unable to open file!");
			$data = json_encode($item);
			fwrite($myfile, $data);
			fclose($myfile);
		
			$o = new DataTabulation();
			$o->source = 'DIMAGI SYSTEM';
			$o->date_creation = date('Y-m-d');
			$o->form_id = $id;
			$o->form_name = $form;
			$o->path = $name;
			$o->save();
		}catch (Exception $e) {
			unlink($name);
		}
	}
	
	public function actionMake1(){
		// root of directory yii2
		// /var/www/html/<yii2>
		$rootyii = realpath(dirname(__FILE__).'/../');
	
		// create file <jam:menit:detik>.txt
		$filename = date('H:i:s') . '.txt';
		$folder = $rootyii.'/cronjob/'.$filename;
		$f = fopen($folder, 'w');
		$fw = fwrite($f, 'now : ' . $filename);
		fclose($f);
	}
	
    public function actionMake(){
        
    	//ini_set("memory_limit","-1");
    	set_time_limit(0);
    	
    	$temoin = DataSummaryIntegration::find()->count();
    	$connection = \Yii::$app->db;
    	$today = date('Y-m-d');
    	$tenday_earlier = date('Y-m-d', strtotime($today. ' - 10 days'));
    	$list_form = array();
    	$j = 0;
    	$data_summary_t = 0;
    	$communication = ["Al�tman Mat�n�l","Nitrisyon ak kwasans","Dyare","Tous, larim ak maladi ki pi grav yo",
    			"Malarya","Vaksinasyon","VIH/SIDA ak maladi moun pran nan f� bagay","Espasman nesans","Mat�nite san risk ak sante timoun ki fenk f�t yo",
    			"Ijy�n ak asenisman","Devlopman ak aprantisaj timoun","Pwoteksyon timoun","Prevansyon aksidan",
    			"Sitiyasyon ijans: preparasyon ak ent�vansyon","Idantifikasyon moun yo","Diagnostik ak suivi andikap yo","Edikasyon timoun yo",
    			"Prevansyon Maladi","Zika","L�t t�m"];
    	 
    	$connected = @fsockopen("www.commcarehq.com", 80);
    	if($connected)
    	{
    		if(!$temoin)
    		{
    			$lenght = $this->totalCountSuivi($temoin,$today,$tenday_earlier);
    			 
    			/* $rootyii = realpath(dirname(__FILE__).'/../');
    			
    			// create file <jam:menit:detik>.txt
    			$filename = date('H:i:s') . "--".$lenght.'.txt';
    			$folder = $rootyii.'/cronjob/'.$filename;
    			$f = fopen($folder, 'w');
    			$fw = fwrite($f, 'now : ' . $filename);
    			fclose($f); */
    			
    			for($i=0; $i < $lenght; $i++)
    			{
	    			$indice = $i * 100;
	    			$data = $this->downLoadDataSuivi($temoin, $today, $tenday_earlier, $indice);
	    			$obj = array();
	    			$obj = $data['objects'];
	    	
	    			if (is_array($obj) && $this->is_iterable($obj))
	    			{
		    			foreach ($obj as $item) {
			    			if(array_key_exists('id', $item) && $item['form']['@name'] = 'Vizit Kay Fanmi'){
				    			$list_form[$j] = $item['id'];
				    			$j++;
			    			}
		    			}
	    			}
    			}
    			 
    			if (!empty($list_form) && is_array($list_form) && $this->is_iterable($list_form))
    			{
	    			$data_summary_integration = new DataSummaryIntegration();
	    			$data_summary_integration->date_creation = date('Y-m-d');
	    			$data_summary_integration->type = 'Vizit Kay Fanmi';
	    			$data_summary_integration->source = 'DIMAGI SYSTEM';
	    			$data_summary_integration->save(false);
	    	
	    			$data_summary_t = $data_summary_integration->id;
    			}
    			 
    			for($e=0; $e < count($list_form); $e++)
    			{
	    			$data5 = array();
	    			$obj2 = array();
	    			$metadata = array();
	    			$data5 = $this->downLoadData44($list_form[$e]);
	    			$details = 0;
	    	
	    			if (!empty($data5) && is_array($data5) && $this->is_iterable($data5))
	    			{
		    			$id = $data5["id"];
		    			$obj2 = $data5["form"];
		    			$metadata = $data5["metadata"];
	    			}else{continue;}
	    	
	    			$details_s_i_1 = DetailsSummaryIntegration::find()->where(['id_form' => $id])->count();
	    	
	    			if($details_s_i_1 == 0 && $obj2['@name'] == 'Vizit Kay Fanmi' &&
	    					isset($obj2['kominikasyon_chanjman_konpotman']['manb_fanmi_resevwa_kominikasyon']) &&
	    					array_key_exists('manb_fanmi_resevwa_kominikasyon',$obj2['kominikasyon_chanjman_konpotman']) &&
	    					array_key_exists('tem_list',$obj2['kominikasyon_chanjman_konpotman']) &&
	    					isset($obj2['kominikasyon_chanjman_konpotman']['tem_list']))
	    			{
	    				
	    					$transaction = $connection->beginTransaction();
	    					try {
	    						$details_s_i = new DetailsSummaryIntegration();
	    						$details_s_i->id_integration = $data_summary_t;
	    	    				$details_s_i->type = 'Vizit Kay Fanmi';
	    	    				$details_s_i->id_form = $list_form[$e];
	    	    				$details_s_i->date_creation = date('Y-m-d');
	    	    				$details_s_i->source = 'DIMAGI SYSTEM';
	    	    				$details_s_i->save(false);
	    	    												
	    	    				$username = $metadata['username'];
	    	    				$client = array();
	    	    				$tem_list = array();
	    	    				$client = explode(" ", $obj2['kominikasyon_chanjman_konpotman']['manb_fanmi_resevwa_kominikasyon']);
	    	    				$tem_list = explode(" ", $obj2['kominikasyon_chanjman_konpotman']['tem_list']['tem']);
	    	
	    						for($z = 0; $z < count($client); $z++){
	    							for($y = 0; $y < count($tem_list); $y++){
	    								$data = new DataIntegration();
	    								//$data->plan = $plan->id;
	    								$data->source = 'DIMAGI SYSTEM';
	    								
	    								//$s = '8/29/2011 11:16:12 AM';
	    								$dt = new DateTime($metadata['timeEnd']);
	    								
	    								$date = $dt->format('m/d/Y');
	    								//$time = $dt->format('H:i:s');
	    								
	    								$data->date_creation = $date;
	    								//$data->date_creation = date('Y-m-d');
	    								$data->isValid = 1;
	    								$data->details_integration = $details_s_i->id;
	    								$data->type_activite = 1;
	    								$data->type_sous_activites = 1;
	    								$data->type_theme = $tem_list[$y];
	    									
	    								if(is_numeric($tem_list[$y]))$data->theme = $communication[$tem_list[$y]-1];
	    								else{$data->theme = 'Lot';}
	    									
	    								$data->location_altitude = $metadata['location'][3];
	    								$data->location_precision = $metadata['location'][2];
	    								$data->location_latitude = $metadata['location'][1];
	    								$data->location_longitude = $metadata['location'][0];
	    								$data->case_id_membre = $client[$z];
	    								$data->form_id = $id;
	    								$data->form_name = $obj2['@name'];
	    								$data->user_id = $metadata['username'];
	    								$data->commentaire = $obj2['kominikasyon_chanjman_konpotman']['komante_kominikasyon'];
	    								$data->save(false);
	    							}
	    						}
		    					$transaction->commit();
		    					
		    					/* $rootyii = realpath(dirname(__FILE__).'/../');
		    					 
		    					// create file <jam:menit:detik>.txt
		    					$filename = date('H:i:s') . 'commit.txt';
		    					$folder = $rootyii.'/cronjob/'.$filename;
		    					$f = fopen($folder, 'w');
		    					$fw = fwrite($f, 'now : ' . $filename);
		    					fclose($f); */
		    				}catch(Exception $e)
		    				{
			    				$transaction->rollback();
			    				
			    				/* $rootyii = realpath(dirname(__FILE__).'/../');
			    				 
			    				// create file <jam:menit:detik>.txt
			    				$filename = date('H:i:s') . 'rollback.txt';
			    				$folder = $rootyii.'/cronjob/'.$filename;
			    				$f = fopen($folder, 'w');
			    				$fw = fwrite($f, 'now : ' . $filename);
			    				fclose($f); */
		    				}
	    				}
	    				
	    				/* $rootyii = realpath(dirname(__FILE__).'/../');
	    				 
	    				// create file <jam:menit:detik>.txt
	    				$filename = date('H:i:s') . 'if.txt';
	    				$folder = $rootyii.'/cronjob/'.$filename;
	    				$f = fopen($folder, 'w');
	    				$fw = fwrite($f, 'now : ' . $filename);
	    				fclose($f); */
    				}
    		}else{
    			$rootyii = realpath(dirname(__FILE__).'/../');
    			
    			// create file <jam:menit:detik>.txt
    			$filename = date('H:i:s') . 'temoin.txt';
    			$folder = $rootyii.'/cronjob/'.$filename;
    			$f = fopen($folder, 'w');
    			$fw = fwrite($f, 'now : ' . $filename);
    			fclose($f);
    		}
    	}else{
    		$rootyii = realpath(dirname(__FILE__).'/../');
    		
    		// create file <jam:menit:detik>.txt
    		$filename = date('H:i:s') . '_no_internet.txt';
    		$folder = $rootyii.'/cronjob/'.$filename;
    		$f = fopen($folder, 'w');
    		$fw = fwrite($f, 'now : ' . $filename);
    		fclose($f);
    	}
    }
    
    private function downLoadData44($form_id){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/'.$form_id.'/';
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
    
    private function totalCountSuivi($temoin, $date0, $date7){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	 
    	if(!$temoin){
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100';
    	}else{
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset=0&received_on_start='.$date7.'&received_on_end='.$date0;
    	}
    	 
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	$value = $data['meta']['total_count'];
    	$result_t = $value/100;
    	$result = round($value/100);
    
    	if($result_t > $result)$result++;
    
    	return $result;
    }
    
    private function downLoadDataSuivi11($temoin, $date0, $date7,$offset){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    
    	if(!$temoin){
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset;
    	}else{
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset.'&received_on_start='.$date7.'&received_on_end='.$date0;
    	}
    
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	//$data = json_decode($string_exec,true);
    
    	return $string_exec;
    }
    
    private function downLoadDataSuivi($temoin, $date0, $date7,$offset){
    	$username='velder10@yahoo.fr';
    	$password='Jesuisfidele100';
    	 
    	if(!$temoin){
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset;
    	}else{
    		$URL = 'https://www.commcarehq.org/a/kore-fanmi/api/v0.5/form/?limit=100&offset='.$offset.'&received_on_start='.$date7.'&received_on_end='.$date0;
    	}
    	 
    	$string_exec = shell_exec("curl --user ".$username.":".$password." '".$URL."'");
    	$data = json_decode($string_exec,true);
    
    	return $data;
    }
}
